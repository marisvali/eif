#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter
{
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Linq;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;
    using global::ZeroFormatter.Comparers;

    public static partial class ZeroFormatterInitializer
    {
        static bool registered = false;

        [UnityEngine.RuntimeInitializeOnLoadMethod(UnityEngine.RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Register()
        {
            if(registered) return;
            registered = true;
            // Enums
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::AstarPath.AstarDistribution>.Register(new ZeroFormatter.DynamicObjectSegments.AstarPath_AstarDistributionFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::AstarPath.AstarDistribution>.Register(new ZeroFormatter.DynamicObjectSegments.AstarPath_AstarDistributionEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::AstarPath.AstarDistribution?>.Register(new ZeroFormatter.DynamicObjectSegments.NullableAstarPath_AstarDistributionFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::AstarPath.AstarDistribution?>.Register(new NullableEqualityComparer<global::AstarPath.AstarDistribution>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.StartEndModifier.Exactness>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.StartEndModifier_ExactnessFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.StartEndModifier.Exactness>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.StartEndModifier_ExactnessEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.StartEndModifier.Exactness?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableStartEndModifier_ExactnessFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.StartEndModifier.Exactness?>.Register(new NullableEqualityComparer<global::Pathfinding.StartEndModifier.Exactness>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.NumNeighbours>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NumNeighboursFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.NumNeighbours>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NumNeighboursEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.NumNeighbours?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableNumNeighboursFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.NumNeighbours?>.Register(new NullableEqualityComparer<global::Pathfinding.NumNeighbours>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.HeuristicOptimizationMode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.HeuristicOptimizationModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.HeuristicOptimizationMode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.HeuristicOptimizationModeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.HeuristicOptimizationMode?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableHeuristicOptimizationModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.HeuristicOptimizationMode?>.Register(new NullableEqualityComparer<global::Pathfinding.HeuristicOptimizationMode>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.RaycastModifier.Quality>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.RaycastModifier_QualityFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.RaycastModifier.Quality>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.RaycastModifier_QualityEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.RaycastModifier.Quality?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableRaycastModifier_QualityFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.RaycastModifier.Quality?>.Register(new NullableEqualityComparer<global::Pathfinding.RaycastModifier.Quality>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.SimpleSmoothModifier.SmoothType>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.SimpleSmoothModifier_SmoothTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.SimpleSmoothModifier.SmoothType>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.SimpleSmoothModifier_SmoothTypeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.SimpleSmoothModifier.SmoothType?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableSimpleSmoothModifier_SmoothTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.SimpleSmoothModifier.SmoothType?>.Register(new NullableEqualityComparer<global::Pathfinding.SimpleSmoothModifier.SmoothType>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Seeker.ModifierPass>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.Seeker_ModifierPassFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.Seeker.ModifierPass>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.Seeker_ModifierPassEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Seeker.ModifierPass?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableSeeker_ModifierPassFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.Seeker.ModifierPass?>.Register(new NullableEqualityComparer<global::Pathfinding.Seeker.ModifierPass>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.ColliderType>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.ColliderTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.ColliderType>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.ColliderTypeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.ColliderType?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableColliderTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.ColliderType?>.Register(new NullableEqualityComparer<global::Pathfinding.ColliderType>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.RayDirection>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.RayDirectionFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.RayDirection>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.RayDirectionEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.RayDirection?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableRayDirectionFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.RayDirection?>.Register(new NullableEqualityComparer<global::Pathfinding.RayDirection>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GraphModifier.EventType>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GraphModifier_EventTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.GraphModifier.EventType>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GraphModifier_EventTypeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GraphModifier.EventType?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableGraphModifier_EventTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.GraphModifier.EventType?>.Register(new NullableEqualityComparer<global::Pathfinding.GraphModifier.EventType>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GraphUpdateStage>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GraphUpdateStageFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.GraphUpdateStage>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GraphUpdateStageEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GraphUpdateStage?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableGraphUpdateStageFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.GraphUpdateStage?>.Register(new NullableEqualityComparer<global::Pathfinding.GraphUpdateStage>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GraphUpdateThreading>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GraphUpdateThreadingFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.GraphUpdateThreading>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GraphUpdateThreadingEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GraphUpdateThreading?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableGraphUpdateThreadingFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.GraphUpdateThreading?>.Register(new NullableEqualityComparer<global::Pathfinding.GraphUpdateThreading>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.PathLog>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.PathLogFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.PathLog>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.PathLogEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.PathLog?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullablePathLogFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.PathLog?>.Register(new NullableEqualityComparer<global::Pathfinding.PathLog>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Heuristic>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.HeuristicFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.Heuristic>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.HeuristicEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Heuristic?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableHeuristicFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.Heuristic?>.Register(new NullableEqualityComparer<global::Pathfinding.Heuristic>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GraphDebugMode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GraphDebugModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.GraphDebugMode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GraphDebugModeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GraphDebugMode?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableGraphDebugModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.GraphDebugMode?>.Register(new NullableEqualityComparer<global::Pathfinding.GraphDebugMode>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.ThreadCount>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.ThreadCountFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.ThreadCount>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.ThreadCountEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.ThreadCount?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableThreadCountFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.ThreadCount?>.Register(new NullableEqualityComparer<global::Pathfinding.ThreadCount>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.PathState>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.PathStateFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.PathState>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.PathStateEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.PathState?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullablePathStateFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.PathState?>.Register(new NullableEqualityComparer<global::Pathfinding.PathState>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.PathCompleteState>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.PathCompleteStateFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.PathCompleteState>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.PathCompleteStateEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.PathCompleteState?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullablePathCompleteStateFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.PathCompleteState?>.Register(new NullableEqualityComparer<global::Pathfinding.PathCompleteState>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.CloseToDestinationMode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.CloseToDestinationModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.CloseToDestinationMode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.CloseToDestinationModeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.CloseToDestinationMode?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableCloseToDestinationModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.CloseToDestinationMode?>.Register(new NullableEqualityComparer<global::Pathfinding.CloseToDestinationMode>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Side>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.SideFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.Side>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.SideEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Side?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableSideFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.Side?>.Register(new NullableEqualityComparer<global::Pathfinding.Side>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.InspectorGridHexagonNodeSize>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.InspectorGridHexagonNodeSizeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.InspectorGridHexagonNodeSize>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.InspectorGridHexagonNodeSizeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.InspectorGridHexagonNodeSize?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableInspectorGridHexagonNodeSizeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.InspectorGridHexagonNodeSize?>.Register(new NullableEqualityComparer<global::Pathfinding.InspectorGridHexagonNodeSize>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.InspectorGridMode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.InspectorGridModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.InspectorGridMode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.InspectorGridModeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.InspectorGridMode?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableInspectorGridModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.InspectorGridMode?>.Register(new NullableEqualityComparer<global::Pathfinding.InspectorGridMode>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.OrientationMode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.OrientationModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.OrientationMode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.OrientationModeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.OrientationMode?>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NullableOrientationModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::Pathfinding.OrientationMode?>.Register(new NullableEqualityComparer<global::Pathfinding.OrientationMode>());
            
            // Objects
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GridNode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GridNodeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GridNodeBase>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GridNodeBaseFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GraphCollision>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GraphCollisionFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GridGraph>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GridGraphFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GraphNode>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.GraphNodeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.NavGraph>.Register(new ZeroFormatter.DynamicObjectSegments.Pathfinding.NavGraphFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            // Structs
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.Pathfinding.Int3Formatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Int3>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Int3?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Int3>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.Pathfinding.Int2Formatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Int2>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Int2?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Int2>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.UnityEngine.Vector3Formatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector3>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector3?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector3>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.UnityEngine.Vector2Formatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector2>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector2?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector2>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.UnityEngine.LayerMaskFormatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.LayerMask>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.LayerMask?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.LayerMask>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.Pathfinding.Util.GuidFormatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Util.Guid>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Util.Guid?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.Util.Guid>(structFormatter));
            }
            // Unions
            // Generics
            ZeroFormatter.Formatters.Formatter.RegisterArray<ZeroFormatter.Formatters.DefaultResolver, global::Pathfinding.GridNode>();
        }
    }
}
#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments.Pathfinding
{
    using global::System;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;

    public class GridNodeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GridNode>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GridNode value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (2 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.Int3>(ref bytes, startOffset, offset, 0, value.position);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, uint>(ref bytes, startOffset, offset, 1, value.Flags);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, ushort>(ref bytes, startOffset, offset, 2, value.gridFlags);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 2);
            }
        }

        public override global::Pathfinding.GridNode Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new GridNodeObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class GridNodeObjectSegment<TTypeResolver> : global::Pathfinding.GridNode, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 12, 4, 2 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;


        // 0
        public override global::Pathfinding.Int3 position
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, global::Pathfinding.Int3>(__originalBytes, 0, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, global::Pathfinding.Int3>(__originalBytes, 0, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 1
        public override uint Flags
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, uint>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, uint>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 2
        public override ushort gridFlags
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, ushort>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, ushort>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }


        public GridNodeObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 2, __elementSizes);

        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (2 + 1));

                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, global::Pathfinding.Int3>(ref targetBytes, startOffset, offset, 0, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, uint>(ref targetBytes, startOffset, offset, 1, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, ushort>(ref targetBytes, startOffset, offset, 2, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 2);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }

    public class GridNodeBaseFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GridNodeBase>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GridNodeBase value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (2 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.Int3>(ref bytes, startOffset, offset, 0, value.position);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, uint>(ref bytes, startOffset, offset, 1, value.Flags);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, ushort>(ref bytes, startOffset, offset, 2, value.gridFlags);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 2);
            }
        }

        public override global::Pathfinding.GridNodeBase Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new GridNodeBaseObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class GridNodeBaseObjectSegment<TTypeResolver> : global::Pathfinding.GridNodeBase, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 12, 4, 2 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;


        // 0
        public override global::Pathfinding.Int3 position
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, global::Pathfinding.Int3>(__originalBytes, 0, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, global::Pathfinding.Int3>(__originalBytes, 0, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 1
        public override uint Flags
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, uint>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, uint>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 2
        public override ushort gridFlags
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, ushort>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, ushort>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }


        public GridNodeBaseObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 2, __elementSizes);

        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (2 + 1));

                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, global::Pathfinding.Int3>(ref targetBytes, startOffset, offset, 0, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, uint>(ref targetBytes, startOffset, offset, 1, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, ushort>(ref targetBytes, startOffset, offset, 2, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 2);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }

    public class GraphCollisionFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GraphCollision>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GraphCollision value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (14 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.ColliderType>(ref bytes, startOffset, offset, 0, value.type);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 1, value.diameter);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 2, value.height);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 3, value.collisionOffset);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.RayDirection>(ref bytes, startOffset, offset, 4, value.rayDirection);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::UnityEngine.LayerMask>(ref bytes, startOffset, offset, 5, value.mask);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::UnityEngine.LayerMask>(ref bytes, startOffset, offset, 6, value.heightMask);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 7, value.fromHeight);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 8, value.thickRaycast);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 9, value.thickRaycastDiameter);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 10, value.unwalkableWhenNoGround);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 11, value.use2D);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 12, value.collisionCheck);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 13, value.heightCheck);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::UnityEngine.Vector3>(ref bytes, startOffset, offset, 14, value.up);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 14);
            }
        }

        public override global::Pathfinding.GraphCollision Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new GraphCollisionObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class GraphCollisionObjectSegment<TTypeResolver> : global::Pathfinding.GraphCollision, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 4, 4, 4, 4, 4, 4, 4, 4, 1, 4, 1, 1, 1, 1, 0 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;

        CacheSegment<TTypeResolver, global::UnityEngine.Vector3> _up;

        // 0
        public override global::Pathfinding.ColliderType type
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, global::Pathfinding.ColliderType>(__originalBytes, 0, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, global::Pathfinding.ColliderType>(__originalBytes, 0, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 1
        public override float diameter
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 2
        public override float height
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 3
        public override float collisionOffset
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 3, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 3, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 4
        public override global::Pathfinding.RayDirection rayDirection
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, global::Pathfinding.RayDirection>(__originalBytes, 4, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, global::Pathfinding.RayDirection>(__originalBytes, 4, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 5
        public override global::UnityEngine.LayerMask mask
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, global::UnityEngine.LayerMask>(__originalBytes, 5, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, global::UnityEngine.LayerMask>(__originalBytes, 5, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 6
        public override global::UnityEngine.LayerMask heightMask
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, global::UnityEngine.LayerMask>(__originalBytes, 6, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, global::UnityEngine.LayerMask>(__originalBytes, 6, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 7
        public override float fromHeight
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 7, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 7, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 8
        public override bool thickRaycast
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 8, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 8, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 9
        public override float thickRaycastDiameter
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 9, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 9, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 10
        public override bool unwalkableWhenNoGround
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 10, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 10, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 11
        public override bool use2D
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 11, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 11, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 12
        public override bool collisionCheck
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 12, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 12, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 13
        public override bool heightCheck
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 13, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 13, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 14
        public override global::UnityEngine.Vector3 up
        {
            get
            {
                return _up.Value;
            }
            set
            {
                _up.Value = value;
            }
        }


        public GraphCollisionObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 14, __elementSizes);

            _up = new CacheSegment<TTypeResolver, global::UnityEngine.Vector3>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 14, __binaryLastIndex, __tracker));
        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (14 + 1));

                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, global::Pathfinding.ColliderType>(ref targetBytes, startOffset, offset, 0, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 1, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 2, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 3, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, global::Pathfinding.RayDirection>(ref targetBytes, startOffset, offset, 4, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, global::UnityEngine.LayerMask>(ref targetBytes, startOffset, offset, 5, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, global::UnityEngine.LayerMask>(ref targetBytes, startOffset, offset, 6, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 7, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 8, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 9, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 10, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 11, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 12, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 13, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::UnityEngine.Vector3>(ref targetBytes, startOffset, offset, 14, ref _up);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 14);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }

    public class GridGraphFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GridGraph>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GridGraph value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (28 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.Util.Guid>(ref bytes, startOffset, offset, 0, value.guid);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, uint>(ref bytes, startOffset, offset, 1, value.initialPenalty);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 2, value.open);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, string>(ref bytes, startOffset, offset, 3, value.name);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 4, value.drawGizmos);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 5, value.infoScreenOpen);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, string>(ref bytes, startOffset, offset, 6, value.serializedEditorSettings);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.InspectorGridMode>(ref bytes, startOffset, offset, 7, value.inspectorGridMode);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.InspectorGridHexagonNodeSize>(ref bytes, startOffset, offset, 8, value.inspectorHexagonSizeMode);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 9, value.aspectRatio);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 10, value.isometricAngle);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 11, value.uniformEdgeCosts);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::UnityEngine.Vector3>(ref bytes, startOffset, offset, 12, value.rotation);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::UnityEngine.Vector3>(ref bytes, startOffset, offset, 13, value.center);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::UnityEngine.Vector2>(ref bytes, startOffset, offset, 14, value.unclampedSize);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 15, value.nodeSize);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.GraphCollision>(ref bytes, startOffset, offset, 16, value.collision);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 17, value.maxClimb);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 18, value.maxSlope);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, int>(ref bytes, startOffset, offset, 19, value.erodeIterations);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 20, value.erosionUseTags);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, int>(ref bytes, startOffset, offset, 21, value.erosionFirstTag);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.NumNeighbours>(ref bytes, startOffset, offset, 22, value.neighbours);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 23, value.cutCorners);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 24, value.useJumpPointSearch);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 25, value.showMeshOutline);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 26, value.showNodeConnections);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 27, value.showMeshSurface);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.GridNode[]>(ref bytes, startOffset, offset, 28, value.nodes);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 28);
            }
        }

        public override global::Pathfinding.GridGraph Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new GridGraphObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class GridGraphObjectSegment<TTypeResolver> : global::Pathfinding.GridGraph, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 0, 4, 1, 0, 1, 1, 0, 4, 4, 4, 4, 1, 0, 0, 0, 4, 0, 4, 4, 4, 1, 4, 4, 1, 1, 1, 1, 1, 0 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;

        CacheSegment<TTypeResolver, global::Pathfinding.Util.Guid> _guid;
        CacheSegment<TTypeResolver, string> _name;
        CacheSegment<TTypeResolver, string> _serializedEditorSettings;
        CacheSegment<TTypeResolver, global::UnityEngine.Vector3> _rotation;
        CacheSegment<TTypeResolver, global::UnityEngine.Vector3> _center;
        CacheSegment<TTypeResolver, global::UnityEngine.Vector2> _unclampedSize;
        global::Pathfinding.GraphCollision _collision;
        CacheSegment<TTypeResolver, global::Pathfinding.GridNode[]> _nodes;

        // 0
        public override global::Pathfinding.Util.Guid guid
        {
            get
            {
                return _guid.Value;
            }
            set
            {
                _guid.Value = value;
            }
        }

        // 1
        public override uint initialPenalty
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, uint>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, uint>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 2
        public override bool open
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 3
        public override string name
        {
            get
            {
                return _name.Value;
            }
            set
            {
                _name.Value = value;
            }
        }

        // 4
        public override bool drawGizmos
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 4, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 4, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 5
        public override bool infoScreenOpen
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 5, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 5, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 6
        public override string serializedEditorSettings
        {
            get
            {
                return _serializedEditorSettings.Value;
            }
            set
            {
                _serializedEditorSettings.Value = value;
            }
        }

        // 7
        public override global::Pathfinding.InspectorGridMode inspectorGridMode
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, global::Pathfinding.InspectorGridMode>(__originalBytes, 7, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, global::Pathfinding.InspectorGridMode>(__originalBytes, 7, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 8
        public override global::Pathfinding.InspectorGridHexagonNodeSize inspectorHexagonSizeMode
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, global::Pathfinding.InspectorGridHexagonNodeSize>(__originalBytes, 8, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, global::Pathfinding.InspectorGridHexagonNodeSize>(__originalBytes, 8, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 9
        public override float aspectRatio
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 9, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 9, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 10
        public override float isometricAngle
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 10, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 10, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 11
        public override bool uniformEdgeCosts
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 11, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 11, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 12
        public override global::UnityEngine.Vector3 rotation
        {
            get
            {
                return _rotation.Value;
            }
            set
            {
                _rotation.Value = value;
            }
        }

        // 13
        public override global::UnityEngine.Vector3 center
        {
            get
            {
                return _center.Value;
            }
            set
            {
                _center.Value = value;
            }
        }

        // 14
        public override global::UnityEngine.Vector2 unclampedSize
        {
            get
            {
                return _unclampedSize.Value;
            }
            set
            {
                _unclampedSize.Value = value;
            }
        }

        // 15
        public override float nodeSize
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 15, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 15, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 16
        public override global::Pathfinding.GraphCollision collision
        {
            get
            {
                return _collision;
            }
            set
            {
                __tracker.Dirty();
                _collision = value;
            }
        }

        // 17
        public override float maxClimb
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 17, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 17, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 18
        public override float maxSlope
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 18, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 18, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 19
        public override int erodeIterations
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, int>(__originalBytes, 19, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, int>(__originalBytes, 19, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 20
        public override bool erosionUseTags
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 20, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 20, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 21
        public override int erosionFirstTag
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, int>(__originalBytes, 21, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, int>(__originalBytes, 21, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 22
        public override global::Pathfinding.NumNeighbours neighbours
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, global::Pathfinding.NumNeighbours>(__originalBytes, 22, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, global::Pathfinding.NumNeighbours>(__originalBytes, 22, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 23
        public override bool cutCorners
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 23, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 23, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 24
        public override bool useJumpPointSearch
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 24, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 24, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 25
        public override bool showMeshOutline
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 25, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 25, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 26
        public override bool showNodeConnections
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 26, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 26, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 27
        public override bool showMeshSurface
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 27, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 27, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 28
        public override global::Pathfinding.GridNode[] nodes
        {
            get
            {
                return _nodes.Value;
            }
            set
            {
                _nodes.Value = value;
            }
        }


        public GridGraphObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 28, __elementSizes);

            _guid = new CacheSegment<TTypeResolver, global::Pathfinding.Util.Guid>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 0, __binaryLastIndex, __tracker));
            _name = new CacheSegment<TTypeResolver, string>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 3, __binaryLastIndex, __tracker));
            _serializedEditorSettings = new CacheSegment<TTypeResolver, string>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 6, __binaryLastIndex, __tracker));
            _rotation = new CacheSegment<TTypeResolver, global::UnityEngine.Vector3>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 12, __binaryLastIndex, __tracker));
            _center = new CacheSegment<TTypeResolver, global::UnityEngine.Vector3>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 13, __binaryLastIndex, __tracker));
            _unclampedSize = new CacheSegment<TTypeResolver, global::UnityEngine.Vector2>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 14, __binaryLastIndex, __tracker));
            _collision = ObjectSegmentHelper.DeserializeSegment<TTypeResolver, global::Pathfinding.GraphCollision>(originalBytes, 16, __binaryLastIndex, __tracker);
            _nodes = new CacheSegment<TTypeResolver, global::Pathfinding.GridNode[]>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 28, __binaryLastIndex, __tracker));
        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (28 + 1));

                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::Pathfinding.Util.Guid>(ref targetBytes, startOffset, offset, 0, ref _guid);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, uint>(ref targetBytes, startOffset, offset, 1, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 2, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, string>(ref targetBytes, startOffset, offset, 3, ref _name);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 4, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 5, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, string>(ref targetBytes, startOffset, offset, 6, ref _serializedEditorSettings);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, global::Pathfinding.InspectorGridMode>(ref targetBytes, startOffset, offset, 7, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, global::Pathfinding.InspectorGridHexagonNodeSize>(ref targetBytes, startOffset, offset, 8, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 9, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 10, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 11, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::UnityEngine.Vector3>(ref targetBytes, startOffset, offset, 12, ref _rotation);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::UnityEngine.Vector3>(ref targetBytes, startOffset, offset, 13, ref _center);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::UnityEngine.Vector2>(ref targetBytes, startOffset, offset, 14, ref _unclampedSize);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 15, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeSegment<TTypeResolver, global::Pathfinding.GraphCollision>(ref targetBytes, startOffset, offset, 16, _collision);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 17, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 18, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, int>(ref targetBytes, startOffset, offset, 19, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 20, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, int>(ref targetBytes, startOffset, offset, 21, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, global::Pathfinding.NumNeighbours>(ref targetBytes, startOffset, offset, 22, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 23, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 24, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 25, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 26, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 27, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::Pathfinding.GridNode[]>(ref targetBytes, startOffset, offset, 28, ref _nodes);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 28);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }

    public class GraphNodeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GraphNode>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GraphNode value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (1 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.Int3>(ref bytes, startOffset, offset, 0, value.position);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, uint>(ref bytes, startOffset, offset, 1, value.Flags);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 1);
            }
        }

        public override global::Pathfinding.GraphNode Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new GraphNodeObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class GraphNodeObjectSegment<TTypeResolver> : global::Pathfinding.GraphNode, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 12, 4 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;


        // 0
        public override global::Pathfinding.Int3 position
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, global::Pathfinding.Int3>(__originalBytes, 0, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, global::Pathfinding.Int3>(__originalBytes, 0, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 1
        public override uint Flags
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, uint>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, uint>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }


        public GraphNodeObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 1, __elementSizes);

        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (1 + 1));

                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, global::Pathfinding.Int3>(ref targetBytes, startOffset, offset, 0, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, uint>(ref targetBytes, startOffset, offset, 1, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 1);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }

    public class NavGraphFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.NavGraph>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.NavGraph value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (6 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Pathfinding.Util.Guid>(ref bytes, startOffset, offset, 0, value.guid);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, uint>(ref bytes, startOffset, offset, 1, value.initialPenalty);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 2, value.open);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, string>(ref bytes, startOffset, offset, 3, value.name);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 4, value.drawGizmos);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, bool>(ref bytes, startOffset, offset, 5, value.infoScreenOpen);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, string>(ref bytes, startOffset, offset, 6, value.serializedEditorSettings);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 6);
            }
        }

        public override global::Pathfinding.NavGraph Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new NavGraphObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class NavGraphObjectSegment<TTypeResolver> : global::Pathfinding.NavGraph, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 0, 4, 1, 0, 1, 1, 0 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;

        CacheSegment<TTypeResolver, global::Pathfinding.Util.Guid> _guid;
        CacheSegment<TTypeResolver, string> _name;
        CacheSegment<TTypeResolver, string> _serializedEditorSettings;

        // 0
        public override global::Pathfinding.Util.Guid guid
        {
            get
            {
                return _guid.Value;
            }
            set
            {
                _guid.Value = value;
            }
        }

        // 1
        public override uint initialPenalty
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, uint>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, uint>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 2
        public override bool open
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 3
        public override string name
        {
            get
            {
                return _name.Value;
            }
            set
            {
                _name.Value = value;
            }
        }

        // 4
        public override bool drawGizmos
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 4, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 4, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 5
        public override bool infoScreenOpen
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, bool>(__originalBytes, 5, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, bool>(__originalBytes, 5, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 6
        public override string serializedEditorSettings
        {
            get
            {
                return _serializedEditorSettings.Value;
            }
            set
            {
                _serializedEditorSettings.Value = value;
            }
        }


        public NavGraphObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 6, __elementSizes);

            _guid = new CacheSegment<TTypeResolver, global::Pathfinding.Util.Guid>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 0, __binaryLastIndex, __tracker));
            _name = new CacheSegment<TTypeResolver, string>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 3, __binaryLastIndex, __tracker));
            _serializedEditorSettings = new CacheSegment<TTypeResolver, string>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 6, __binaryLastIndex, __tracker));
        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (6 + 1));

                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::Pathfinding.Util.Guid>(ref targetBytes, startOffset, offset, 0, ref _guid);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, uint>(ref targetBytes, startOffset, offset, 1, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 2, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, string>(ref targetBytes, startOffset, offset, 3, ref _name);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 4, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, bool>(ref targetBytes, startOffset, offset, 5, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, string>(ref targetBytes, startOffset, offset, 6, ref _serializedEditorSettings);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 6);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }


}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments.Pathfinding
{
    using global::System;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;

    public class Int3Formatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.Int3>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, int> formatter0;
        readonly Formatter<TTypeResolver, int> formatter1;
        readonly Formatter<TTypeResolver, int> formatter2;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                    && formatter2.NoUseDirtyTracker
                ;
            }
        }

        public Int3Formatter()
        {
            formatter0 = Formatter<TTypeResolver, int>.Default;
            formatter1 = Formatter<TTypeResolver, int>.Default;
            formatter2 = Formatter<TTypeResolver, int>.Default;
            
        }

        public override int? GetLength()
        {
            return 12;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.Int3 value)
        {
            BinaryUtil.EnsureCapacity(ref bytes, offset, 12);
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.x);
            offset += formatter1.Serialize(ref bytes, offset, value.y);
            offset += formatter2.Serialize(ref bytes, offset, value.z);
            return offset - startOffset;
        }

        public override global::Pathfinding.Int3 Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item2 = formatter2.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::Pathfinding.Int3(item0, item1, item2);
        }
    }

    public class Int2Formatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.Int2>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, int> formatter0;
        readonly Formatter<TTypeResolver, int> formatter1;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                ;
            }
        }

        public Int2Formatter()
        {
            formatter0 = Formatter<TTypeResolver, int>.Default;
            formatter1 = Formatter<TTypeResolver, int>.Default;
            
        }

        public override int? GetLength()
        {
            return 8;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.Int2 value)
        {
            BinaryUtil.EnsureCapacity(ref bytes, offset, 8);
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.x);
            offset += formatter1.Serialize(ref bytes, offset, value.y);
            return offset - startOffset;
        }

        public override global::Pathfinding.Int2 Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::Pathfinding.Int2(item0, item1);
        }
    }


}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments.UnityEngine
{
    using global::System;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;

    public class Vector3Formatter<TTypeResolver> : Formatter<TTypeResolver, global::UnityEngine.Vector3>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, float> formatter0;
        readonly Formatter<TTypeResolver, float> formatter1;
        readonly Formatter<TTypeResolver, float> formatter2;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                    && formatter2.NoUseDirtyTracker
                ;
            }
        }

        public Vector3Formatter()
        {
            formatter0 = Formatter<TTypeResolver, float>.Default;
            formatter1 = Formatter<TTypeResolver, float>.Default;
            formatter2 = Formatter<TTypeResolver, float>.Default;
            
        }

        public override int? GetLength()
        {
            return 12;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::UnityEngine.Vector3 value)
        {
            BinaryUtil.EnsureCapacity(ref bytes, offset, 12);
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.x);
            offset += formatter1.Serialize(ref bytes, offset, value.y);
            offset += formatter2.Serialize(ref bytes, offset, value.z);
            return offset - startOffset;
        }

        public override global::UnityEngine.Vector3 Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item2 = formatter2.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::UnityEngine.Vector3(item0, item1, item2);
        }
    }

    public class Vector2Formatter<TTypeResolver> : Formatter<TTypeResolver, global::UnityEngine.Vector2>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, float> formatter0;
        readonly Formatter<TTypeResolver, float> formatter1;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                ;
            }
        }

        public Vector2Formatter()
        {
            formatter0 = Formatter<TTypeResolver, float>.Default;
            formatter1 = Formatter<TTypeResolver, float>.Default;
            
        }

        public override int? GetLength()
        {
            return 8;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::UnityEngine.Vector2 value)
        {
            BinaryUtil.EnsureCapacity(ref bytes, offset, 8);
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.x);
            offset += formatter1.Serialize(ref bytes, offset, value.y);
            return offset - startOffset;
        }

        public override global::UnityEngine.Vector2 Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::UnityEngine.Vector2(item0, item1);
        }
    }

    public class LayerMaskFormatter<TTypeResolver> : Formatter<TTypeResolver, global::UnityEngine.LayerMask>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, int> formatter0;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                ;
            }
        }

        public LayerMaskFormatter()
        {
            formatter0 = Formatter<TTypeResolver, int>.Default;
            
        }

        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::UnityEngine.LayerMask value)
        {
            BinaryUtil.EnsureCapacity(ref bytes, offset, 4);
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.value);
            return offset - startOffset;
        }

        public override global::UnityEngine.LayerMask Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            var obj = new global::UnityEngine.LayerMask();
            obj.value = item0;
            return obj;
        }
    }


}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments.Pathfinding.Util
{
    using global::System;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;

    public class GuidFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.Util.Guid>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, ulong> formatter0;
        readonly Formatter<TTypeResolver, ulong> formatter1;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                ;
            }
        }

        public GuidFormatter()
        {
            formatter0 = Formatter<TTypeResolver, ulong>.Default;
            formatter1 = Formatter<TTypeResolver, ulong>.Default;
            
        }

        public override int? GetLength()
        {
            return 16;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.Util.Guid value)
        {
            BinaryUtil.EnsureCapacity(ref bytes, offset, 16);
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value._a);
            offset += formatter1.Serialize(ref bytes, offset, value._b);
            return offset - startOffset;
        }

        public override global::Pathfinding.Util.Guid Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::Pathfinding.Util.Guid(item0, item1);
        }
    }


}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments
{
    using global::System;
    using global::System.Collections.Generic;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;


    public class AstarPath_AstarDistributionFormatter<TTypeResolver> : Formatter<TTypeResolver, global::AstarPath.AstarDistribution>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::AstarPath.AstarDistribution value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::AstarPath.AstarDistribution Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::AstarPath.AstarDistribution)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableAstarPath_AstarDistributionFormatter<TTypeResolver> : Formatter<TTypeResolver, global::AstarPath.AstarDistribution?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::AstarPath.AstarDistribution? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::AstarPath.AstarDistribution? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::AstarPath.AstarDistribution)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class AstarPath_AstarDistributionEqualityComparer : IEqualityComparer<global::AstarPath.AstarDistribution>
    {
        public bool Equals(global::AstarPath.AstarDistribution x, global::AstarPath.AstarDistribution y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::AstarPath.AstarDistribution x)
        {
            return (int)x;
        }
    }



}
#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments.Pathfinding
{
    using global::System;
    using global::System.Collections.Generic;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;


    public class StartEndModifier_ExactnessFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.StartEndModifier.Exactness>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.StartEndModifier.Exactness value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.StartEndModifier.Exactness Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.StartEndModifier.Exactness)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableStartEndModifier_ExactnessFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.StartEndModifier.Exactness?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.StartEndModifier.Exactness? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.StartEndModifier.Exactness? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.StartEndModifier.Exactness)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class StartEndModifier_ExactnessEqualityComparer : IEqualityComparer<global::Pathfinding.StartEndModifier.Exactness>
    {
        public bool Equals(global::Pathfinding.StartEndModifier.Exactness x, global::Pathfinding.StartEndModifier.Exactness y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.StartEndModifier.Exactness x)
        {
            return (int)x;
        }
    }



    public class NumNeighboursFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.NumNeighbours>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.NumNeighbours value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.NumNeighbours Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.NumNeighbours)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableNumNeighboursFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.NumNeighbours?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.NumNeighbours? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.NumNeighbours? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.NumNeighbours)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class NumNeighboursEqualityComparer : IEqualityComparer<global::Pathfinding.NumNeighbours>
    {
        public bool Equals(global::Pathfinding.NumNeighbours x, global::Pathfinding.NumNeighbours y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.NumNeighbours x)
        {
            return (int)x;
        }
    }



    public class HeuristicOptimizationModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.HeuristicOptimizationMode>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.HeuristicOptimizationMode value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.HeuristicOptimizationMode Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.HeuristicOptimizationMode)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableHeuristicOptimizationModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.HeuristicOptimizationMode?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.HeuristicOptimizationMode? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.HeuristicOptimizationMode? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.HeuristicOptimizationMode)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class HeuristicOptimizationModeEqualityComparer : IEqualityComparer<global::Pathfinding.HeuristicOptimizationMode>
    {
        public bool Equals(global::Pathfinding.HeuristicOptimizationMode x, global::Pathfinding.HeuristicOptimizationMode y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.HeuristicOptimizationMode x)
        {
            return (int)x;
        }
    }



    public class RaycastModifier_QualityFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.RaycastModifier.Quality>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.RaycastModifier.Quality value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.RaycastModifier.Quality Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.RaycastModifier.Quality)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableRaycastModifier_QualityFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.RaycastModifier.Quality?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.RaycastModifier.Quality? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.RaycastModifier.Quality? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.RaycastModifier.Quality)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class RaycastModifier_QualityEqualityComparer : IEqualityComparer<global::Pathfinding.RaycastModifier.Quality>
    {
        public bool Equals(global::Pathfinding.RaycastModifier.Quality x, global::Pathfinding.RaycastModifier.Quality y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.RaycastModifier.Quality x)
        {
            return (int)x;
        }
    }



    public class SimpleSmoothModifier_SmoothTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.SimpleSmoothModifier.SmoothType>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.SimpleSmoothModifier.SmoothType value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.SimpleSmoothModifier.SmoothType Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.SimpleSmoothModifier.SmoothType)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableSimpleSmoothModifier_SmoothTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.SimpleSmoothModifier.SmoothType?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.SimpleSmoothModifier.SmoothType? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.SimpleSmoothModifier.SmoothType? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.SimpleSmoothModifier.SmoothType)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class SimpleSmoothModifier_SmoothTypeEqualityComparer : IEqualityComparer<global::Pathfinding.SimpleSmoothModifier.SmoothType>
    {
        public bool Equals(global::Pathfinding.SimpleSmoothModifier.SmoothType x, global::Pathfinding.SimpleSmoothModifier.SmoothType y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.SimpleSmoothModifier.SmoothType x)
        {
            return (int)x;
        }
    }



    public class Seeker_ModifierPassFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.Seeker.ModifierPass>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.Seeker.ModifierPass value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.Seeker.ModifierPass Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.Seeker.ModifierPass)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableSeeker_ModifierPassFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.Seeker.ModifierPass?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.Seeker.ModifierPass? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.Seeker.ModifierPass? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.Seeker.ModifierPass)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class Seeker_ModifierPassEqualityComparer : IEqualityComparer<global::Pathfinding.Seeker.ModifierPass>
    {
        public bool Equals(global::Pathfinding.Seeker.ModifierPass x, global::Pathfinding.Seeker.ModifierPass y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.Seeker.ModifierPass x)
        {
            return (int)x;
        }
    }



    public class ColliderTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.ColliderType>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.ColliderType value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.ColliderType Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.ColliderType)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableColliderTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.ColliderType?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.ColliderType? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.ColliderType? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.ColliderType)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class ColliderTypeEqualityComparer : IEqualityComparer<global::Pathfinding.ColliderType>
    {
        public bool Equals(global::Pathfinding.ColliderType x, global::Pathfinding.ColliderType y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.ColliderType x)
        {
            return (int)x;
        }
    }



    public class RayDirectionFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.RayDirection>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.RayDirection value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.RayDirection Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.RayDirection)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableRayDirectionFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.RayDirection?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.RayDirection? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.RayDirection? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.RayDirection)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class RayDirectionEqualityComparer : IEqualityComparer<global::Pathfinding.RayDirection>
    {
        public bool Equals(global::Pathfinding.RayDirection x, global::Pathfinding.RayDirection y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.RayDirection x)
        {
            return (int)x;
        }
    }



    public class GraphModifier_EventTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GraphModifier.EventType>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GraphModifier.EventType value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.GraphModifier.EventType Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.GraphModifier.EventType)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableGraphModifier_EventTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GraphModifier.EventType?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GraphModifier.EventType? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.GraphModifier.EventType? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.GraphModifier.EventType)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class GraphModifier_EventTypeEqualityComparer : IEqualityComparer<global::Pathfinding.GraphModifier.EventType>
    {
        public bool Equals(global::Pathfinding.GraphModifier.EventType x, global::Pathfinding.GraphModifier.EventType y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.GraphModifier.EventType x)
        {
            return (int)x;
        }
    }



    public class GraphUpdateStageFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GraphUpdateStage>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GraphUpdateStage value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.GraphUpdateStage Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.GraphUpdateStage)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableGraphUpdateStageFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GraphUpdateStage?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GraphUpdateStage? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.GraphUpdateStage? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.GraphUpdateStage)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class GraphUpdateStageEqualityComparer : IEqualityComparer<global::Pathfinding.GraphUpdateStage>
    {
        public bool Equals(global::Pathfinding.GraphUpdateStage x, global::Pathfinding.GraphUpdateStage y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.GraphUpdateStage x)
        {
            return (int)x;
        }
    }



    public class GraphUpdateThreadingFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GraphUpdateThreading>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GraphUpdateThreading value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.GraphUpdateThreading Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.GraphUpdateThreading)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableGraphUpdateThreadingFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GraphUpdateThreading?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GraphUpdateThreading? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.GraphUpdateThreading? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.GraphUpdateThreading)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class GraphUpdateThreadingEqualityComparer : IEqualityComparer<global::Pathfinding.GraphUpdateThreading>
    {
        public bool Equals(global::Pathfinding.GraphUpdateThreading x, global::Pathfinding.GraphUpdateThreading y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.GraphUpdateThreading x)
        {
            return (int)x;
        }
    }



    public class PathLogFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.PathLog>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.PathLog value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.PathLog Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.PathLog)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullablePathLogFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.PathLog?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.PathLog? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.PathLog? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.PathLog)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class PathLogEqualityComparer : IEqualityComparer<global::Pathfinding.PathLog>
    {
        public bool Equals(global::Pathfinding.PathLog x, global::Pathfinding.PathLog y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.PathLog x)
        {
            return (int)x;
        }
    }



    public class HeuristicFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.Heuristic>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.Heuristic value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.Heuristic Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.Heuristic)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableHeuristicFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.Heuristic?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.Heuristic? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.Heuristic? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.Heuristic)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class HeuristicEqualityComparer : IEqualityComparer<global::Pathfinding.Heuristic>
    {
        public bool Equals(global::Pathfinding.Heuristic x, global::Pathfinding.Heuristic y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.Heuristic x)
        {
            return (int)x;
        }
    }



    public class GraphDebugModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GraphDebugMode>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GraphDebugMode value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.GraphDebugMode Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.GraphDebugMode)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableGraphDebugModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.GraphDebugMode?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.GraphDebugMode? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.GraphDebugMode? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.GraphDebugMode)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class GraphDebugModeEqualityComparer : IEqualityComparer<global::Pathfinding.GraphDebugMode>
    {
        public bool Equals(global::Pathfinding.GraphDebugMode x, global::Pathfinding.GraphDebugMode y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.GraphDebugMode x)
        {
            return (int)x;
        }
    }



    public class ThreadCountFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.ThreadCount>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.ThreadCount value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.ThreadCount Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.ThreadCount)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableThreadCountFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.ThreadCount?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.ThreadCount? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.ThreadCount? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.ThreadCount)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class ThreadCountEqualityComparer : IEqualityComparer<global::Pathfinding.ThreadCount>
    {
        public bool Equals(global::Pathfinding.ThreadCount x, global::Pathfinding.ThreadCount y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.ThreadCount x)
        {
            return (int)x;
        }
    }



    public class PathStateFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.PathState>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.PathState value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.PathState Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.PathState)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullablePathStateFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.PathState?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.PathState? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.PathState? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.PathState)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class PathStateEqualityComparer : IEqualityComparer<global::Pathfinding.PathState>
    {
        public bool Equals(global::Pathfinding.PathState x, global::Pathfinding.PathState y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.PathState x)
        {
            return (int)x;
        }
    }



    public class PathCompleteStateFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.PathCompleteState>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.PathCompleteState value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.PathCompleteState Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.PathCompleteState)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullablePathCompleteStateFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.PathCompleteState?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.PathCompleteState? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.PathCompleteState? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.PathCompleteState)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class PathCompleteStateEqualityComparer : IEqualityComparer<global::Pathfinding.PathCompleteState>
    {
        public bool Equals(global::Pathfinding.PathCompleteState x, global::Pathfinding.PathCompleteState y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.PathCompleteState x)
        {
            return (int)x;
        }
    }



    public class CloseToDestinationModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.CloseToDestinationMode>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.CloseToDestinationMode value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.CloseToDestinationMode Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.CloseToDestinationMode)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableCloseToDestinationModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.CloseToDestinationMode?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.CloseToDestinationMode? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.CloseToDestinationMode? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.CloseToDestinationMode)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class CloseToDestinationModeEqualityComparer : IEqualityComparer<global::Pathfinding.CloseToDestinationMode>
    {
        public bool Equals(global::Pathfinding.CloseToDestinationMode x, global::Pathfinding.CloseToDestinationMode y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.CloseToDestinationMode x)
        {
            return (int)x;
        }
    }



    public class SideFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.Side>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 1;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.Side value)
        {
            return BinaryUtil.WriteByte(ref bytes, offset, (Byte)value);
        }

        public override global::Pathfinding.Side Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 1;
            return (global::Pathfinding.Side)BinaryUtil.ReadByte(ref bytes, offset);
        }
    }


    public class NullableSideFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.Side?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 2;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.Side? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteByte(ref bytes, offset + 1, (Byte)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 2);
            }

            return 2;
        }

        public override global::Pathfinding.Side? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 2;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.Side)BinaryUtil.ReadByte(ref bytes, offset + 1);
        }
    }



    public class SideEqualityComparer : IEqualityComparer<global::Pathfinding.Side>
    {
        public bool Equals(global::Pathfinding.Side x, global::Pathfinding.Side y)
        {
            return (Byte)x == (Byte)y;
        }

        public int GetHashCode(global::Pathfinding.Side x)
        {
            return (int)(Byte)x;
        }
    }



    public class InspectorGridHexagonNodeSizeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.InspectorGridHexagonNodeSize>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.InspectorGridHexagonNodeSize value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.InspectorGridHexagonNodeSize Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.InspectorGridHexagonNodeSize)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableInspectorGridHexagonNodeSizeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.InspectorGridHexagonNodeSize?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.InspectorGridHexagonNodeSize? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.InspectorGridHexagonNodeSize? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.InspectorGridHexagonNodeSize)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class InspectorGridHexagonNodeSizeEqualityComparer : IEqualityComparer<global::Pathfinding.InspectorGridHexagonNodeSize>
    {
        public bool Equals(global::Pathfinding.InspectorGridHexagonNodeSize x, global::Pathfinding.InspectorGridHexagonNodeSize y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.InspectorGridHexagonNodeSize x)
        {
            return (int)x;
        }
    }



    public class InspectorGridModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.InspectorGridMode>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.InspectorGridMode value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.InspectorGridMode Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.InspectorGridMode)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableInspectorGridModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.InspectorGridMode?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.InspectorGridMode? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.InspectorGridMode? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.InspectorGridMode)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class InspectorGridModeEqualityComparer : IEqualityComparer<global::Pathfinding.InspectorGridMode>
    {
        public bool Equals(global::Pathfinding.InspectorGridMode x, global::Pathfinding.InspectorGridMode y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.InspectorGridMode x)
        {
            return (int)x;
        }
    }



    public class OrientationModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.OrientationMode>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.OrientationMode value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::Pathfinding.OrientationMode Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::Pathfinding.OrientationMode)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableOrientationModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Pathfinding.OrientationMode?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Pathfinding.OrientationMode? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::Pathfinding.OrientationMode? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::Pathfinding.OrientationMode)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class OrientationModeEqualityComparer : IEqualityComparer<global::Pathfinding.OrientationMode>
    {
        public bool Equals(global::Pathfinding.OrientationMode x, global::Pathfinding.OrientationMode y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::Pathfinding.OrientationMode x)
        {
            return (int)x;
        }
    }



}
#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
