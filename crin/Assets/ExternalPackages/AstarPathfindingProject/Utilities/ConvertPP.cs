using System.Text;
using UnityEngine;

public class ConvertPP
{
    private char[] chars16 = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private int[] ints127 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 10, 11, 12, 13, 14, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    
    public string BytesToString(byte[] bytes)
    {
        var builder = new StringBuilder();
        for (int idx = 0; idx < bytes.Length; ++idx)
        {
            byte b = bytes[idx];
            var low = b & 0x0F; // b % 16
            var high = b >> 4; // b / 16
            builder.Append(chars16[high]);
            builder.Append(chars16[low]);
        }
        return builder.ToString();
    }
    
    public byte[] StringToBytes(string str)
    {
        byte[] bytes = new byte[str.Length / 2];
        int low = 0;
        int high = 0;
        var ints = ints127; // Keep this in a local variable instead of calling a member.
        for (int idx = 0; idx < str.Length; idx += 2)
        {
            high = ints[(int)str[idx]];
            low = ints[(int)str[idx + 1]];
            bytes[idx / 2] = (byte)((high << 4) | low);
        }
        return bytes;
    }
    
    public static void Test()
    {
        ConvertPP pp = new ConvertPP();
        
        byte[] bytes = new byte[1000];
        for (int idx = 0; idx < bytes.Length; ++idx)
            bytes[idx] = (byte)(idx % 256);
            
        var sb = new System.Text.StringBuilder();
        foreach (var b in bytes)
            sb.Append(b).Append(' ');
        Debug.Log($"Before: {sb.ToString()}");
        
        
        var str = pp.BytesToString(bytes);
        Debug.Log($"ConvertPP: {str}");
        byte[] bytes2 = pp.StringToBytes(str);
        
        sb.Clear();
        foreach (var b in bytes2)
            sb.Append(b).Append(' ');
        Debug.Log($"After: {sb.ToString()}");
        
        
        Debug.Log($"len1 {bytes.Length} len2 {bytes2.Length}");
        bool allGood = true;
        for (int idx = 0; idx < bytes.Length; ++idx)
            if (bytes[idx] != bytes2[idx])
                allGood = false;
        Debug.Log($"all good: {allGood}");
    }
}
