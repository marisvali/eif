using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class prof
{
    static public void Start()
    {
        ++i.mLevel;
        i.mSW[i.mLevel].Restart();
    }
    static public void Capture(string designation)
    {
        var measurement = new Measurement();
        measurement.duration = i.mSW[i.mLevel].Elapsed.TotalSeconds;
        measurement.designation = designation;
        i.mMeasurements.Add(measurement);
        --i.mLevel;
    }
    
    static public void OutputAll()
    {
        UnityEngine.Debug.Log($"profiler is high resolution: {Stopwatch.IsHighResolution}");
        foreach (var m in i.mMeasurements)
            UnityEngine.Debug.Log($"{m.designation}: {(m.duration * 1000.0f).ToString("F3")}");
    }
    
    private static prof i = new prof();
    private Stopwatch[] mSW = new Stopwatch[20];
    private int mLevel = -1;
    prof()
    {
        for (int iSW = 0; iSW < mSW.Length; ++iSW)
            mSW[iSW] = new Stopwatch();
    }
    struct Measurement
    {
        public double duration;
        public string designation;
    }
    private List<Measurement> mMeasurements = new List<Measurement>();
}