using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using System.Text;
using System;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
unsafe public class SerializePP
{
    private byte* mBytes;
    private int mIndex = 0;
    
    public SerializePP(byte* bytes)
    {
        mBytes = bytes;
    }
    
    public int NumBytesUsed()
    {
        return mIndex;
    }
    
    public void Add(ulong val)
    {
        *((ulong*)(mBytes + mIndex)) = val;
        mIndex += sizeof(ulong);
    }
    
    public void Add(int val)
    {
        *((int*)(mBytes + mIndex)) = val;
        mIndex += sizeof(int);
    }
    
    public void Add(bool val)
    {
        *((bool*)(mBytes + mIndex)) = val;
        mIndex += sizeof(bool);
    }
    
    public void Add(float val)
    {
        *((float*)(mBytes + mIndex)) = val;
        mIndex += sizeof(float);
    }
    
    public void Add(Vector2 val)
    {
        Add(val.x);
        Add(val.y);
    }
    
    public void Add(Vector3 val)
    {
        Add(val.x);
        Add(val.y);
        Add(val.z);
    }
    
    public void Add(char val)
    {
        *((char*)(mBytes + mIndex)) = val;
        mIndex += sizeof(char);
    }
    
    public void Add(string val)
    {
        if (val is null)
        {
            Add(0);
        }
        else
        {
            Add(val.Length);
            foreach (var ch in val)
                Add(ch);
        }
    }
}


[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
unsafe public class DeserializePP
{
    private byte* mBytes;
    private int mIndex = 0;
    
    public DeserializePP(byte* bytes)
    {
        mBytes = bytes;
    }
    
    public int NumBytesUsed()
    {
        return mIndex;
    }
    
    public void Get(ref ulong val)
    {
        val = *((ulong*)(mBytes + mIndex));
        mIndex += sizeof(ulong);
    }
    
    public void Get(ref bool val)
    {
        val = *((bool*)(mBytes + mIndex));
        mIndex += sizeof(bool);
    }
    
    public void Get(ref int val)
    {
        val = *((int*)(mBytes + mIndex));
        mIndex += sizeof(int);
    }
    
    public void Get(ref float val)
    {
        val = *((float*)(mBytes + mIndex));
        mIndex += sizeof(float);
    }
    
    public void Get(ref Vector2 val)
    {
        Get(ref val.x);
        Get(ref val.y);
    }
    
    public void Get(ref Vector3 val)
    {
        Get(ref val.x);
        Get(ref val.y);
        Get(ref val.z);
    }
    
    public void Get(ref char val)
    {
        val = *((char*)(mBytes + mIndex));
        mIndex += sizeof(char);
    }
    
    public void Get(ref string val)
    {
        int len = 0;
        Get(ref len);
        if (len == 0)
        {
            val = null;
            return;
        }
        
        var builder = new StringBuilder();
        for (int iCh = 0; iCh < len; ++iCh)
        {
            char ch = ' ';
            Get(ref ch);
            builder.Append(ch);
        }
        val = builder.ToString();
    }
}