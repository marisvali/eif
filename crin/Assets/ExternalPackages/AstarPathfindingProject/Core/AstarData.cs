using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding.WindowsStore;
#if UNITY_WINRT && !UNITY_EDITOR
//using MarkerMetro.Unity.WinLegacy.IO;
//using MarkerMetro.Unity.WinLegacy.Reflection;
#endif

namespace Pathfinding {
	[System.Serializable]
	/// <summary>
	/// Stores the navigation graphs for the A* Pathfinding System.
	/// \ingroup relevant
	///
	/// An instance of this class is assigned to AstarPath.data, from it you can access all graphs loaded through the <see cref="graphs"/> variable.\n
	/// This class also handles a lot of the high level serialization.
	/// </summary>
	public class AstarData {
		/// <summary>Shortcut to AstarPath.active</summary>
		public static AstarPath active {
			get {
				return AstarPath.active;
			}
		}

		#region Fields
		
		/// <summary>
		/// All graphs this instance holds.
		/// This will be filled only after deserialization has completed.
		/// May contain null entries if graph have been removed.
		/// </summary>
		[System.NonSerialized]
		public GridGraph graph;

		//Serialization Settings

		/// <summary>
		/// Serialized data for all graphs and settings.
		/// Stored as a base64 encoded string because otherwise Unity's Undo system would sometimes corrupt the byte data (because it only stores deltas).
		///
		/// This can be accessed as a byte array from the <see cref="data"/> property.
		///
		/// \since 3.6.1
		/// </summary>
		[SerializeField]
		string dataString;

		/// <summary>
		/// Data from versions from before 3.6.1.
		/// Used for handling upgrades
		/// \since 3.6.1
		/// </summary>
		[SerializeField]
		[UnityEngine.Serialization.FormerlySerializedAs("data")]
		private byte[] upgradeData;

		/// <summary>Serialized data for all graphs and settings</summary>
		private byte[] data {
			get {
				var etc = dataString != null? convertPP.StringToBytes(dataString) : null;
				return etc;
			}
			set {
				dataString = value != null? convertPP.BytesToString(value) : null;
			}
		}

		/// <summary>
		/// Serialized data for cached startup.
		/// If set, on start the graphs will be deserialized from this file.
		/// </summary>
		public TextAsset file_cachedStartup;

		/// <summary>
		/// Serialized data for cached startup.
		///
		/// Deprecated: Deprecated since 3.6, AstarData.file_cachedStartup is now used instead
		/// </summary>
		public byte[] data_cachedStartup;

		/// <summary>
		/// Should graph-data be cached.
		/// Caching the startup means saving the whole graphs - not only the settings - to a file (<see cref="file_cachedStartup)"/> which can
		/// be loaded when the game starts. This is usually much faster than scanning the graphs when the game starts. This is configured from the editor under the "Save & Load" tab.
		///
		/// See: save-load-graphs (view in online documentation for working links)
		/// </summary>
		[SerializeField]
		public bool cacheStartup;

		//End Serialization Settings

		List<bool> graphStructureLocked = new List<bool>();
		
		#endregion

		public byte[] GetData () {
			return data;
		}

		public void SetData (byte[] data) {
			this.data = data;
		}

		/// <summary>Loads the graphs from memory, will load cached graphs if any exists</summary>
		public void Awake () {
			if (cacheStartup && file_cachedStartup != null) {
				LoadFromCache();
			} else {
				DeserializeGraph();
			}
		}
		
		private ConvertPP convertPP = new ConvertPP();

		/// <summary>
		/// Prevent the graph structure from changing during the time this lock is held.
		/// This prevents graphs from being added or removed and also prevents graphs from being serialized or deserialized.
		/// This is used when e.g an async scan is happening to ensure that for example a graph that is being scanned is not destroyed.
		///
		/// Each call to this method *must* be paired with exactly one call to <see cref="UnlockGraphStructure"/>.
		/// The calls may be nested.
		/// </summary>
		internal void LockGraphStructure (bool allowAddingGraphs = false) {
			graphStructureLocked.Add(allowAddingGraphs);
		}

		/// <summary>
		/// Allows the graph structure to change again.
		/// See: <see cref="LockGraphStructure"/>
		/// </summary>
		internal void UnlockGraphStructure () {
			if (graphStructureLocked.Count == 0) throw new System.InvalidOperationException();
			graphStructureLocked.RemoveAt(graphStructureLocked.Count - 1);
		}

		PathProcessor.GraphUpdateLock AssertSafe (bool onlyAddingGraph = false) {
			if (graphStructureLocked.Count > 0) {
				bool allowAdding = true;
				for (int i = 0; i < graphStructureLocked.Count; i++) allowAdding &= graphStructureLocked[i];
				if (!(onlyAddingGraph && allowAdding)) throw new System.InvalidOperationException("Graphs cannot be added, removed or serialized while the graph structure is locked. This is the case when a graph is currently being scanned and when executing graph updates and work items.\nHowever as a special case, graphs can be added inside work items.");
			}

			// Pause the pathfinding threads
			var graphLock = active.PausePathfinding();
			if (!active.IsInsideWorkItem) {
				// Make sure all graph updates and other callbacks are done
				// Only do this if this code is not being called from a work item itself as that would cause a recursive wait that could never complete.
				// There are some valid cases when this can happen. For example it may be necessary to add a new graph inside a work item.
				active.FlushWorkItems();

				// Paths that are already calculated and waiting to be returned to the Seeker component need to be
				// processed immediately as their results usually depend on graphs that currently exist. If this was
				// not done then after destroying a graph one could get a path result with destroyed nodes in it.
				active.pathReturnQueue.ReturnPaths(false);
			}
			return graphLock;
		}

		/// <summary>
		/// Calls the callback with every node in all graphs.
		/// This is the easiest way to iterate through every existing node.
		///
		/// <code>
		/// AstarPath.active.data.GetNodes(node => {
		///     Debug.Log("I found a node at position " + (Vector3)node.position);
		/// });
		/// </code>
		///
		/// See: <see cref="Pathfinding.NavGraph.GetNodes"/> for getting the nodes of a single graph instead of all.
		/// See: graph-updates (view in online documentation for working links)
		/// </summary>
		public void GetNodes (System.Action<GraphNode> callback) {
			if (graph != null) graph.GetNodes(callback);
		}

		/// <summary>Load from data from <see cref="file_cachedStartup"/></summary>
		public void LoadFromCache () {
			var graphLock = AssertSafe();

			if (file_cachedStartup != null) {
				var bytes = file_cachedStartup.bytes;
				DeserializeGraph(bytes);

				GraphModifier.TriggerEvent(GraphModifier.EventType.PostCacheLoad);
			} else {
				Debug.LogError("Can't load from cache since the cache is empty");
			}
			graphLock.Release();
		}

		#region Serialization

		/// <summary>
		/// Main serializer function.
		/// Serializes all graphs to a byte array
		/// A similar function exists in the AstarPathEditor.cs script to save additional info
		/// </summary>
		public byte[] SerializeGraph () {
			if (graph is null)
				return new byte[0];

			var graphLock = AssertSafe();
			var bytes = graph.SerializePP();
			graphLock.Release();
			
			return bytes;
		}

		/// <summary>Deserializes graphs from <see cref="data"/></summary>
		public void DeserializeGraph () {
			if (data != null) {
				DeserializeGraph(data);
			}
		}

		public void OnDestroy () {
		}

		/// <summary>
		/// Deserializes graphs from the specified byte array.
		/// An error will be logged if deserialization fails.
		/// </summary>
		public void DeserializeGraph (byte[] bytes) {
			var graphLock = AssertSafe();
			try {
				if (bytes != null) {
					graph = GridGraph.DeserializePP(bytes);
					graph.active = AstarPath.active;
					
					graph.PostDeserialization();
				} else {
					throw new System.ArgumentNullException("bytes");
				}
				active.VerifyIntegrity();
			} catch (System.Exception e) {
				Debug.LogError("Caught exception while deserializing data.\n"+e);
				graph = null;
			}
			
			graphLock.Release();
		}

		/// <summary>
		/// Deserializes graphs from the specified byte array additively.
		/// An error will be logged if deserialization fails.
		/// This function will add loaded graphs to the current ones.
		/// </summary>
		public void DeserializeGraphsAdditive (byte[] bytes) {
			
		}

		#endregion

		#region GraphCreation
		/// <summary>
		/// Creates a new graph instance of type type
		/// See: <see cref="CreateGraph(string)"/>
		/// </summary>
		public void CreateGraph() {
			graph = new GridGraph();
			graph.Init();
			graph.active = active;
			data = SerializeGraph();
		}
		
		public void RemoveGraph() {
			graph = null;
			data = null;
		}

		#endregion
	}
}