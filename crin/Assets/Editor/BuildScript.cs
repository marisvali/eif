using System;
using System.Globalization;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;

public class BuildScript 
{
    [MenuItem("Tools/Build")]
    static void Build()
    {
        string apkPath = "eif.apk";
        var args = System.Environment.GetCommandLineArgs();
        for( int idx = 0; idx < args.Length; ++idx )
        {
            if (args[idx] == "-apk" && idx < args.Length - 1)
                apkPath = args[idx + 1];
        }
        Debug.Log("APK PATH: " + apkPath);
        
        // Set the all important passwords to protect myself from all the thieves out there.
        PlayerSettings.keystorePass = "parola";
        PlayerSettings.keyaliasPass = "parola";
        
        // Make sure all assets are imported correctly.
        AssetDatabase.Refresh();
        
        // Find the folder in Assets that contains the primary scene and get all the Unity scenes
        // from it. Sort them alphabetically then put the primary scene first.
        string primaryScene = "PrimaryScene.unity";
        var primaryScenes = Directory.GetFiles("Assets", primaryScene, SearchOption.AllDirectories);
        if (primaryScenes.Length != 1)
        {
            string msg = "Expected only one " + primaryScene + " file in the Assets folder. Instead found: " + primaryScenes.Length.ToString() + ": ";
            foreach (var scene in primaryScenes)
                msg += scene + " ";
            throw new FileNotFoundException(msg);
        }
        
        var dir = Path.GetDirectoryName(primaryScenes[0]);
        var scenes = new List<string>(Directory.GetFiles(dir, "*.unity"));
        scenes.Sort();
        var primaryScenePath = scenes.First(x => x.Contains(primaryScene));
        scenes.Remove(primaryScenePath);
        scenes.Insert(0, primaryScenePath);
        Debug.Log("DETECTED SCENES:");
        foreach (var scene in scenes)
            Debug.Log(scene.ToString());
        
        BuildPipeline.BuildPlayer(scenes.ToArray(), apkPath,
            BuildTarget.Android, BuildOptions.CleanBuildCache);
    }
}