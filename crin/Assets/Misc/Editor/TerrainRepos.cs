﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class TerrainRepos
{
    [MenuItem("Window/TerrainRepos")]
    public static void Execute()
    {
        UnityEngine.SceneManagement.Scene scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
        List<GameObject> rootObjects = new List<GameObject>(scene.rootCount + 1);
        scene.GetRootGameObjects(rootObjects);

        for (int idx = 0; idx < rootObjects.Count; ++idx)
        {
            GameObject originalGameObject = rootObjects[idx];

            if (originalGameObject.name == "Terrain")
            {
                Vector3 absolutePositionTerrain = new Vector3(1620, 1620, 0);
                originalGameObject.transform.position = absolutePositionTerrain;

                Vector3 differencePosition = new Vector3(0, 0, 0);

                for (int jdx = 0; jdx < originalGameObject.transform.childCount; jdx++)
                {
                    Vector3 absolutePositionBackground = new Vector3(0, 0, 0) + absolutePositionTerrain;

                    Transform child = originalGameObject.transform.GetChild(jdx);

                    if (child.name.Contains("Background"))
                    {
                        differencePosition = child.transform.position - absolutePositionBackground;
                        child.transform.position = absolutePositionBackground;
                    }
                }

                for (int jdx = 0; jdx < originalGameObject.transform.childCount; jdx++)
                {
                    Transform child = originalGameObject.transform.GetChild(jdx);

                    if (!child.name.Contains("Background"))
                    {
                        child.transform.position = child.transform.position - differencePosition;
                        child.transform.position = new Vector3(child.transform.position.x, child.transform.position.y, 0);
                    }
                }
            }
        }

        EditorUtility.DisplayDialog(
        "TerrainRepos",
        "Terrain repositioning done!",
        "OK");
    }
}
