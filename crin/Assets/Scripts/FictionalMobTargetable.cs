﻿using UnityEngine;

public class FictionalMobTargetable : MonoBehaviour
{
    float duration = 0.5f;
    float startTime;
    
    void Start()
    {
        startTime = Time.time;
    }

    public bool IsTargetable()
    {
        return (Time.time - startTime) > duration;
	}
}
