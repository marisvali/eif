using UnityEngine;

public class musicLevel : MonoBehaviour
{
    private static musicLevel i;
    
    AudioSource mMusicSource;
    
    musicLevel()
    {
        i = this;
    }
    
    void Awake()
    {
        mMusicSource = GetComponent<AudioSource>();
    }

    public static void Pause()
    {
        i.mMusicSource.Pause();
    }   
    
    public static void Unpause()
    {
        i.mMusicSource.Play();
    }
    
    public static void SetVolume(float v)
    {
        i.mMusicSource.volume = v;
    }
}
