﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayActiveBoost : MonoBehaviour
{
    string previousActiveBoost = "random";

    void Update()
    {
        string activeBoost = PlayerPrefs.GetString("Boost1");
        if (PlayerPrefs.GetInt("BoostIdx") == 0)
            activeBoost = "";
        if (previousActiveBoost != activeBoost )
        {
            if (activeBoost == "")
            {
                GetComponent<Image>().sprite = null;
                GetComponent<Image>().color = new Color(255, 255, 255, 0);
            }
            else
            {
                GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/Boosts/Boosts " + activeBoost);
                GetComponent<Image>().color = new Color(255, 255, 255, 255);
            }
            previousActiveBoost = activeBoost;
        }
    }
}
