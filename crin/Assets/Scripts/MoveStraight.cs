﻿using UnityEngine;

public class MoveStraight : MonoBehaviour {

    public Vector2 move;
    public float speed = 0;
	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        Rigidbody2D body = GetComponent<Rigidbody2D>();
        body.velocity = Vector3.zero;
        body.angularVelocity = 0;
        body.MovePosition(body.position + move * speed * Time.deltaTime);
    }
}
