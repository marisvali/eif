﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BasicOptions : MonoBehaviour
{
    public GameObject leftRightPanel;
    public GameObject femaleMalePanel;
    void Start()
    {
        PlayerPrefs.SetInt("BestScore", 0);
        femaleMalePanel.SetActive(false);
    }
    public void SelectedLeft()
    {
        PlayerPrefs.SetString("Handedness", "Left");
        leftRightPanel.SetActive(false);
        femaleMalePanel.SetActive(true);
    }

    public void SelectedRight()
    {
        PlayerPrefs.SetString("Handedness", "Right");
        leftRightPanel.SetActive(false);
        femaleMalePanel.SetActive(true);
    }
    public void SelectedFemale()
    {
        PlayerPrefs.SetString("Gender", "Female");
        PlayerPrefs.SetInt("BasicOptionsSelected", 1);
        PlayerPrefs.SetInt("Start", 1);
        SceneManager.LoadScene("SceneSelection");
    }
    public void SelectedMale()
    {
        PlayerPrefs.SetString("Gender", "Male");
        PlayerPrefs.SetInt("BasicOptionsSelected", 1);
        PlayerPrefs.SetInt("Start", 1);
        SceneManager.LoadScene("SceneSelection");
    }
}
