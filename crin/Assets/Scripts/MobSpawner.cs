﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobSpawner : WorldObject
{
    public int NrDepressions = 0;
    public int NrSocialAnxieties = 0;
    public int NrAngers = 0;
    public int NrParanoias = 0;
    public int NrTerrors = 0;
    public float timeUntilAppearance = 0;
    public float timeBeforeFirstSpawn = 0;
    public float timeBetweenSpawns = 0;
    public float timeAfterLastSpawn = 1;
    public bool stopped = false;
    public bool spawnQuietSocialAnxiety = false;
    public bool spawnRoamingInSameArea = false;
    private Boosts boostsObj;
    public enum OwnerEnum
    {
        None,
        RejectionAuxAnger,
        RejectionAuxDepression,
        RejectionAuxPanic,
        RejectionAuxParanoia,
        RejectionAuxSocialAnxiety,
    }
    public OwnerEnum owner = OwnerEnum.None;


    int freezes = 0;
    bool frozen = false;
    float timeSinceFreeze = 0;
    Animator Anim;
    List<string> mobsLeftToSpawn = new List<string>();

    void Start()
    {
        boostsObj = g.i.boosts;
        mobsLeftToSpawn = GetRandomizedMobList();
        StartCoroutine(ExecuteSpawningProcedure());
        Anim = GetComponent<Animator>();
    }
	
	protected override void UpdateImpl()
    {
        if (frozen &&
            (Time.time - timeSinceFreeze) > 5)
        {
            frozen = false;
            Anim.speed = 1;
        }
    }

    List<string> GetRandomizedMobList()
    {
        List<string> mobs = new List<string>();
        List<string> mobTypes = new List<string>();
        List<int> mobCounts = new List<int>();
        mobTypes.Add("Depression"); mobCounts.Add(NrDepressions);
        mobTypes.Add("SocialAnxiety"); mobCounts.Add(NrSocialAnxieties);
        mobTypes.Add("Anger"); mobCounts.Add(NrAngers);
        mobTypes.Add("Paranoia"); mobCounts.Add(NrParanoias);
        mobTypes.Add("Terror"); mobCounts.Add(NrTerrors);
        while (mobTypes.Count > 0)
        {
            int selectedMobType = UnityEngine.Random.Range(0, mobTypes.Count);
            if (mobCounts[selectedMobType] == 0)
            {
                mobTypes.RemoveAt(selectedMobType);
                mobCounts.RemoveAt(selectedMobType);
                continue;
            }
            mobs.Add(mobTypes[selectedMobType]);
            --mobCounts[selectedMobType];
        }
        return mobs;
    }

    IEnumerator ExecuteSpawningProcedure()
    {
        while (stopped)
        {
            yield return null;
        }

        //wait
        yield return new WaitForSeconds(timeUntilAppearance);

        //add the freeze times to the wait time
        for( int idx = 0; idx < freezes; ++idx )
            yield return new WaitForSeconds(5);

        //reveal
        Anim.SetInteger("AnimationState", 1);
        yield return new WaitForSeconds(0.33f);

        //wait before the first spawn
        yield return new WaitForSeconds(timeBeforeFirstSpawn);

        //spawn mobs
        yield return SpawnMobs(mobsLeftToSpawn);
    }

    IEnumerator SpawnMobs(List<string> mobs)
    {
        //check if there are mobs left
        if (mobs.Count == 0)
        {
            Anim.SetInteger("AnimationState", 2);
            yield return new WaitForSeconds(0.33f);
            Destroy(gameObject);
            yield break;
        }

        //pick mob
        string mob = mobs[mobs.Count - 1];
        mobs.RemoveAt(mobs.Count - 1);

        //wait, if frozen
        while (frozen)
        {
            //wait until next frame
            yield return null;
        }

        //spawn it
        //debug13 - probably not gonna use mob spawners in the future so why bother fixing this - GameObject spawnedMob = g.CreatePrefab($"Mobs/{mob}", transform.position);
        GameObject spawnedMob = null;

        if (spawnQuietSocialAnxiety && mob == "SocialAnxiety")
        {
            spawnedMob.GetComponent<SocialAnxietyAI>().pauseBeforeFirstHit = 1000000;
        }

        if (mob == "SocialAnxiety")
            spawnedMob.GetComponent<SocialAnxietyAI>().MakeReal();
        if (mob == "Depression")
            spawnedMob.GetComponent<DepressionAI>().MakeReal();
        if (mob == "Anger")
            spawnedMob.GetComponent<AngerAI>().MakeReal();

        if ( spawnRoamingInSameArea )
        {
            if (mob == "SocialAnxiety")
                spawnedMob.GetComponent<SocialAnxietyAI>().roamInOriginalRegion = true;
            if (mob == "Depression")
                spawnedMob.GetComponent<DepressionAI>().roamInOriginalRegion = true;
            if (mob == "Anger")
                spawnedMob.GetComponent<AngerAI>().roamInOriginalRegion = true;
        }

        //set health to maximum
        MobHealth mobHealth = spawnedMob.GetComponent<MobHealth>();
        mobHealth.startHealth = mobHealth.maxHealth;

        //cripple it if necessary
        if (PlayerPrefs.GetInt("Sick spawns") != 0 &&
            mob != "Anger")
        {
            if ((UnityEngine.Random.value * 100f) < boostsObj.SickSpawnsPercent)
            {
                mobHealth.startHealth = mobHealth.maxHealth - (int)boostsObj.SickSpawnsAmount;
            }
        }

        //consume any flames that we spawned on top of
        var collider = spawnedMob.GetComponent<Mob>().clickArea;
        foreach (var flame in g.i.ammos)
            if (collider.OverlapPoint(flame.transform.position))
                Destroy(flame.gameObject);

        //wait
        if (mobs.Count > 0)
        {
            float realTimeBetweenSpawns = timeBetweenSpawns;
            yield return new WaitForSeconds(realTimeBetweenSpawns);
        }
        else
            yield return new WaitForSeconds(timeAfterLastSpawn);

        //spawn the next mob
        yield return SpawnMobs(mobs);
    }

    public void Freeze()
    {
        ++freezes;
        frozen = true;
        Anim.speed = 0;
        timeSinceFreeze = Time.time;
    }

    public float GetTimeLeftBeforeSpawning()
    {
        float timeLeft = timeUntilAppearance;
        //add the freeze times to the wait time
        for (int idx = 0; idx < freezes; ++idx)
            timeLeft += 5;
        timeLeft -= Time.timeSinceLevelLoad;
        if (timeLeft < 0)
            timeLeft = 0;
        return timeLeft;
    }
    public List<string> GetMobsLeftToSpawn()
    {
        return mobsLeftToSpawn;
    }
}
