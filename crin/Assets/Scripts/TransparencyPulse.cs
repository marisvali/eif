﻿using System.Collections;
using UnityEngine;

public class TransparencyPulse : MonoBehaviour
{
    public float transitionTime = 0;
    public float peakDelay = 0;
    public float alphaDifference = 0;

    private float initialAlpha;

    void Awake()
    {
        initialAlpha = GetComponent<SpriteRenderer>().color.a;
    }

    void OnEnable()
    {
        StartCoroutine(Transition());
    }

    void OnDisable()
    {
        StopAllCoroutines();
        var col = GetComponent<SpriteRenderer>().color;
        GetComponent<SpriteRenderer>().color = new Color(col.r, col.g, col.b, initialAlpha);
    }

    IEnumerator Transition(bool goingUp = true)
    {
        float startTime = Time.time;
        float endTime = startTime + transitionTime;

        while (endTime >= Time.time)
        {
            float change = (Time.time - startTime) / transitionTime;
            float newAlpha = initialAlpha + alphaDifference * (goingUp ? change : (1 - change));
            var col = GetComponent<SpriteRenderer>().color;
            GetComponent<SpriteRenderer>().color = new Color(col.r, col.g, col.b, newAlpha);
            yield return null;
        }

        float peakAlpha = initialAlpha + alphaDifference * (goingUp ? 1 : 0);
        var peakCol = GetComponent<SpriteRenderer>().color;
        GetComponent<SpriteRenderer>().color = new Color(peakCol.r, peakCol.g, peakCol.b, peakAlpha);

        yield return new WaitForSeconds(peakDelay);
        yield return Transition(!goingUp);
    }
}
