﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingLevel : MonoBehaviour
{
    public float lampMoveTime = 0;
    public float lampWaitTime = 0;
    public GameObject healedParent;
    public GameObject pointsParent;
    public GameObject lamp;

    AsyncOperation async;
    List<Image> healed = new List<Image>();
    List<RectTransform> points = new List<RectTransform>();
    

    void Start()
    {
        int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
        async = SceneManager.LoadSceneAsync(PlayerPrefs.GetString("Loading screen scene"));
        async.allowSceneActivation = false;
        healed.AddRange(healedParent.GetComponentsInChildren<Image>());
        for (int idx = 1; idx <= 30; ++idx)
            if (idx < currentLevel)
                healed[idx - 1].enabled = true;
            else
                healed[idx - 1].enabled = false;
        points.AddRange(pointsParent.GetComponentsInChildren<RectTransform>());
        points.RemoveAt(0); //the RectTransform of the "Points" object was also added
        StartCoroutine(MoveLamp(currentLevel));
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            StopAllCoroutines();
            async.allowSceneActivation = true;
        }
    }

    IEnumerator MoveLamp(int toLevel)
    {
        int fromLevel = toLevel - 1;
        Vector3 startPos = points[fromLevel].position;
        Vector3 dir = points[toLevel].position - startPos;
        float totalDist = dir.magnitude;
        dir.Normalize();

        float dist = 0;
        float speed = totalDist / lampMoveTime;
        while (dist < totalDist)
        {
            dist += speed * Time.deltaTime;
            Vector3 currentPos = startPos + dist * dir;
            lamp.GetComponent<RectTransform>().position = currentPos;
            yield return null;
        }

        yield return new WaitForSeconds(lampWaitTime);
        async.allowSceneActivation = true;
    }
}
