using UnityEngine;

public class Mob : MonoBehaviour
{
    public Collider2D clickArea;
    public Collider2D hitArea;
    public SpriteRenderer icon;
    
    void Awake()
    {
        g.i.mobs.Add(this);
    }
    
    void OnDestroy()
    {
        g.i.mobs.Remove(this);
    }
}
