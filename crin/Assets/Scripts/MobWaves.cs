﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobWaves : WorldObject
{
    public int minTotalHealthPhase1 = 8;
    public int maxTotalHealthPhase1 = 21;
    public int minTotalHealthPhase2 = 18;
    public int maxTotalHealthPhase2 = 23;
    public int minTotalHealthPhase3 = 11;
    public int maxTotalHealthPhase3 = 38;
    public float minDelayBetweenWaves = 15;
    public float maxDelayBetweenWaves = 30;
    public float phase1Duration = 90;
    public float phase2Duration = 180;
    public enum Mode
    {
        Phase1,
        Phase2,
        Phase3
    }
    public Mode mode = Mode.Phase1;
    public bool noBigMobs = false;
    public GameObject spawnerRef;

    List<Vector3> possibleSpawnPositions = new List<Vector3>();

	public void Initialize()
    {
        if ( !noBigMobs )
        {
            minTotalHealthPhase1 = PlayerPrefs.GetInt("minTotalHealthPhase1");
            maxTotalHealthPhase1 = PlayerPrefs.GetInt("maxTotalHealthPhase1");
            minTotalHealthPhase2 = PlayerPrefs.GetInt("minTotalHealthPhase2");
            maxTotalHealthPhase2 = PlayerPrefs.GetInt("maxTotalHealthPhase2");
            minTotalHealthPhase3 = PlayerPrefs.GetInt("minTotalHealthPhase3");
            maxTotalHealthPhase3 = PlayerPrefs.GetInt("maxTotalHealthPhase3");
            if (PlayerPrefs.GetString("Difficulty") == "Casual")
            {
                minDelayBetweenWaves = PlayerPrefs.GetInt("minDelayBetweenWaves") * 1.5f;
                maxDelayBetweenWaves = PlayerPrefs.GetInt("maxDelayBetweenWaves") * 1.5f;
                phase1Duration = PlayerPrefs.GetInt("phase1Duration") * 1.5f;
                phase2Duration = PlayerPrefs.GetInt("phase2Duration") * 1.5f;
            }
            else if (PlayerPrefs.GetString("Difficulty") == "Medium")
            {
                minDelayBetweenWaves = PlayerPrefs.GetInt("minDelayBetweenWaves") * 1.25f;
                maxDelayBetweenWaves = PlayerPrefs.GetInt("maxDelayBetweenWaves") * 1.25f;
                phase1Duration = PlayerPrefs.GetInt("phase1Duration") * 1.25f;
                phase2Duration = PlayerPrefs.GetInt("phase2Duration") * 1.25f;
            }
            else
            {
                minDelayBetweenWaves = PlayerPrefs.GetInt("minDelayBetweenWaves");
                maxDelayBetweenWaves = PlayerPrefs.GetInt("maxDelayBetweenWaves");
                phase1Duration = PlayerPrefs.GetInt("phase1Duration");
                phase2Duration = PlayerPrefs.GetInt("phase2Duration");
            }
        }

        foreach (Transform tr in transform)
            possibleSpawnPositions.Add(tr.position);
        InitializeDifficulty();
        StartCoroutine(ProgressDifficulty());
        StartCoroutine(SpawnWaves());
    }

    protected override void UpdateImpl()
    {
        SetTimeSinceLevelStart(GetTimeSinceLevelStart() + Time.deltaTime);
    }

    int GetMinTotalHealth()
    {
        if( mode == Mode.Phase1 )
            return minTotalHealthPhase1;
        if (mode == Mode.Phase2)
            return minTotalHealthPhase2;
        if (mode == Mode.Phase3)
            return minTotalHealthPhase3;
        return 0;
    }

    int GetMaxTotalHealth()
    {
        if (mode == Mode.Phase1)
            return maxTotalHealthPhase1;
        if (mode == Mode.Phase2)
            return maxTotalHealthPhase2;
        if (mode == Mode.Phase3)
            return maxTotalHealthPhase3;
        return 0;
    }

    float TimeUntilNextWave()
    {
        if (GetLastSpawnMoment() < 0)
            return 0; //we are before the first wave
        else
        {
            //we are in between two waves
            float timeSinceLastWave = GetTimeSinceLevelStart() - GetLastSpawnMoment();
            float timeBetweenLastWaveAndNextWave = g.RndValBetween(minDelayBetweenWaves, maxDelayBetweenWaves);
            float timeFromNowUntilNextWave = timeBetweenLastWaveAndNextWave - timeSinceLastWave;
            return timeFromNowUntilNextWave;
        }
    }

    IEnumerator SpawnWaves()
    {
        //wait until the first wave
        yield return new WaitForSeconds(TimeUntilNextWave());
        
        while ( true )
        {
            //spawn wave
            SetLastSpawnMoment(GetTimeSinceLevelStart());
            int totalHealth = (int)g.RndValBetween(GetMinTotalHealth(), GetMaxTotalHealth());
            SpawnWave(totalHealth);

            //wait until the next wave
            float delayUntilNextWave = g.RndValBetween(minDelayBetweenWaves, maxDelayBetweenWaves);
            yield return new WaitForSeconds(delayUntilNextWave);
        }
    }

    enum MobType
    {
        SocialAnxiety,
        Depression,
        Anger,
        Paranoia,
        Terror
    }

    MobType PickMob()
    {
        if(noBigMobs)
        {
            float socialAnxietyChance = 0.33f;
            float depressionChance = socialAnxietyChance + 0.33f;
            float angerChance = depressionChance + 0.34f;

            float dice = UnityEngine.Random.value;
            if (dice < socialAnxietyChance)
                return MobType.SocialAnxiety;
            if (dice < depressionChance)
                return MobType.Depression;
            if (dice < angerChance)
                return MobType.Anger;
        }
        else
        {
            float socialAnxietyChance = 0.35f;
            float depressionChance = socialAnxietyChance + 0.35f;
            float angerChance = depressionChance + 0.2f;
            float paranoiaChance = angerChance + 0.05f;
            float terrorChance = paranoiaChance + 0.05f;

            float dice = UnityEngine.Random.value;
            if (dice < socialAnxietyChance)
                return MobType.SocialAnxiety;
            if (dice < depressionChance)
                return MobType.Depression;
            if (dice < angerChance)
                return MobType.Anger;
            if (dice < paranoiaChance)
                return MobType.Paranoia;
            if (dice < terrorChance)
                return MobType.Terror;
        }
        throw new System.Exception("Not supposed to happen");
    }

    int MobHealth(MobType mob)
    {
        if (mob == MobType.SocialAnxiety)
            return 3;
        if (mob == MobType.Depression)
            return 2;
        if (mob == MobType.Anger)
            return 1;
        if (mob == MobType.Paranoia)
            return 9;
        if (mob == MobType.Terror)
            return 9;
        throw new System.Exception("Not supposed to happen");
    }

    List<MobType> DecideMobs(int totalHealth)
    {
        List<MobType> mobs = new List<MobType>();

        int healthLeft = totalHealth;

        while( healthLeft > 0 )
        {
            int healthRequired = 0;
            MobType mob = MobType.Anger;
            do
            {
                mob = PickMob();
                healthRequired = MobHealth(mob);
            } while (healthRequired > healthLeft);

            healthLeft -= healthRequired;
            mobs.Add(mob);
        }

        return mobs;
    }

    List<Vector3> GetSpawnPositions(int nrSpawners)
    {
        int nrSpawnersToPlace = nrSpawners;
        List<bool> positions = new List<bool>();
        foreach (var dummy in possibleSpawnPositions)
            positions.Add(false);

        while (nrSpawnersToPlace > 0)
        {
            int posistion = UnityEngine.Random.Range(0, positions.Count);
            if (!positions[posistion])
            {
                positions[posistion] = true;
                --nrSpawnersToPlace;
            }
        }

        List<Vector3> spawnPositions = new List<Vector3>();
        for (int idx = 0; idx < positions.Count; ++idx)
            if (positions[idx])
                spawnPositions.Add(possibleSpawnPositions[idx]);

        return spawnPositions;
    }

    List<List<MobType>> PlaceMobs(List<MobType> mobs, int nrSpawners)
    {
        List<List<MobType>> mobGroups = new List<List<MobType>>();
        for (int idx = 0; idx < nrSpawners; ++idx)
            mobGroups.Add(new List<MobType>());

        //place the big mobs in different spawners
        int spawnerIdx = 0;
        int mobIndex = 0;
        while(mobIndex < mobs.Count )
        {
            var mob = mobs[mobIndex];
            if (mob == MobType.Paranoia || mob == MobType.Terror)
            {
                mobGroups[spawnerIdx].Add(mob);
                ++spawnerIdx;
                mobs.RemoveAt(mobIndex);
            }
            else
                ++mobIndex;
        }

        //place the rest of the mobs wherever
        while ( mobs.Count > 0 )
        {
            var mob = mobs[mobs.Count - 1];
            int mobGroupIdx = UnityEngine.Random.Range(0, mobGroups.Count);
            mobGroups[mobGroupIdx].Add(mob);
            mobs.RemoveAt(mobs.Count - 1);
        }

        return mobGroups;
    }

    void SpawnWave(int totalHealth)
    {
        //decide which mobs will spawn
        var mobs = DecideMobs(totalHealth);

        //decide the number of spawners
        int minNrSpawners = 0;
        foreach (var mob in mobs)
            if (mob == MobType.Paranoia || mob == MobType.Terror)
                ++minNrSpawners;
        if (minNrSpawners == 0)
            minNrSpawners = 1;
        int maxNrSpawners = Mathf.Min(possibleSpawnPositions.Count, mobs.Count);
        int nrMobSpawners = UnityEngine.Random.Range(minNrSpawners, maxNrSpawners + 1);

        //cut the list of mobs into groups of mobs, one for each spawner
        List<List<MobType>> mobGroups = PlaceMobs(mobs, nrMobSpawners);

        //place spawners on the map
        var spawnPositions = GetSpawnPositions(nrMobSpawners);

        //generate spawners
        for( int idx = 0; idx < spawnPositions.Count; ++idx )
        {
            if (mobGroups[idx].Count <= 0)
                continue;

            MobSpawner spawner = g.Clone(spawnerRef, spawnPositions[idx]).GetComponent<MobSpawner>();
            
            spawner.NrSocialAnxieties = 0;
            spawner.NrDepressions = 0;
            spawner.NrAngers = 0;
            spawner.NrParanoias = 0;
            spawner.NrTerrors = 0;
            List<MobType> mobGroup = mobGroups[idx];
            foreach( var mob in mobGroup )
            {
                if (mob == MobType.SocialAnxiety)
                    ++spawner.NrSocialAnxieties;
                if (mob == MobType.Depression)
                    ++spawner.NrDepressions;
                if (mob == MobType.Anger)
                    ++spawner.NrAngers;
                if (mob == MobType.Paranoia)
                    ++spawner.NrParanoias;
                if (mob == MobType.Terror)
                    ++spawner.NrTerrors;
            }
            spawner.stopped = false;

            //GetAllMobs.RegisterMobSpawner(spawner.gameObject);
        }
    }

    void InitializeDifficulty()
    {
        float timeSinceLevelStart = GetTimeSinceLevelStart();
        if (timeSinceLevelStart < phase1Duration)
            mode = Mode.Phase1;
        else if (timeSinceLevelStart < phase1Duration + phase2Duration)
            mode = Mode.Phase2;
        else
            mode = Mode.Phase3;
    }

    IEnumerator ProgressDifficulty()
    {
        float timeSinceLevelStart = GetTimeSinceLevelStart();
        if ( timeSinceLevelStart < phase1Duration )
            yield return new WaitForSeconds(phase1Duration - timeSinceLevelStart);
        mode = Mode.Phase2;
        if( timeSinceLevelStart < phase1Duration + phase2Duration)
            yield return new WaitForSeconds(phase2Duration - (timeSinceLevelStart - phase1Duration));
        mode = Mode.Phase3;
    }

    //we maintain a time since the level was started
    //this time is different from Time.timeSinceLevelLoad because the player may have interrupted the level multiple times
    //the time we maintain is a sum of all the seconds spent playing this level
    float GetTimeSinceLevelStart()
    {
        if (noBigMobs)
            return PlayerPrefs.GetFloat("CampaignPreviousPlaytime");
        else
            return PlayerPrefs.GetFloat("EndlessPreviousPlaytime");
    }
    void SetTimeSinceLevelStart(float time)
    {
        if (noBigMobs)
            PlayerPrefs.SetFloat("CampaignPreviousPlaytime", time);
        else
            PlayerPrefs.SetFloat("EndlessPreviousPlaytime", time);
    }
    float GetLastSpawnMoment()
    {
        if (noBigMobs)
            return PlayerPrefs.GetFloat("CampaignMobWavesLastSpawnMoment");
        else
            return PlayerPrefs.GetFloat("EndlessMobWavesLastSpawnMoment");
    }
    void SetLastSpawnMoment(float time)
    {
        if (noBigMobs)
            PlayerPrefs.SetFloat("CampaignMobWavesLastSpawnMoment", time);
        else
            PlayerPrefs.SetFloat("EndlessMobWavesLastSpawnMoment", time);
    }
}