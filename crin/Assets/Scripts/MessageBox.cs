﻿using UnityEngine;

public class MessageBox : MonoBehaviour
{
    public bool unpause = false;

    public enum Results
    {
        Yes,
        No,
        Ok
    }
    // Results Result;

    public void Yes()
    {
        // Result = Results.Yes;
        Close();
    }

    public void No()
    {
        // Result = Results.No;
        Close();
    }

    public void Ok()
    {
        // Result = Results.Ok;
        Close();
    }

    private void Close()
    {
        if (unpause)
            g.i.Unpause();
        gameObject.SetActive(false);
    }
}
