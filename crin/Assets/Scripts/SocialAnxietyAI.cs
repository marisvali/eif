﻿using System;
using System.Collections;
using UnityEngine;

public class SocialAnxietyAI : WorldObject
{
    public float stunnedTime = 0;
    public float roamingRadius = 0;
    public float minTimeBetweenRoams = 0;
    public float maxTimeBetweenRoams = 0;
    public float minPauseBetweenHits = 0;
    public float maxPauseBetweenHits = 0;
    public float pauseBeforeFirstHit = 0;
    public float attackWindupDurationBeforeSound = 0; //the time between the start of the attack animation and the start of the attack sound
    public float attackWindupDurationAfterSound = 0; //the time between the start of the attack sound and the instantiation of the projectile
    public float totalAttackDuration = 0; //the total time for the attack animation
    public float hitTime = 0;
    public bool roamInOriginalRegion = false;
    public float projectileOffsetX = 0;
    public float projectileOffsetY = 0;
    public float appearanceTime = 0;
    public AudioClip attack;
    public AudioClip hit;
    public AudioClip death;
    public GameObject projectile;
    public GameObject hitEffect;

    float lastStateChangeMoment = 0;
    bool stateJustChanged = false;
    GameObject mTarget;
    bool targetSet = false;
    bool resetTargetTimerStarted = false;
    bool fictional = false;
    bool wasJustHit = false;
    bool freeze = false;
    float timeSinceFreeze = 0;
    Animator Anim;

    enum State
    {
        Appearing,
        Roaming,
        Attacking,
        Dead,
        Hit,
        Aiming,
        Frozen
    }
    State currentState;
    void Start()
    {
        Anim = GetComponent<Animator>();
        if (PlayerPrefs.GetString("Difficulty") == "Casual")
        {
            minPauseBetweenHits = 1.3f;
            maxPauseBetweenHits = 1.3f;
        }
        resetTargetTimerStarted = false;
        wasJustHit = false;
        freeze = false;
        mTarget = g.CreatePathTarget();
        GetComponent<MobHealth>().SetHealth(GetComponent<MobHealth>().startHealth);
        GetComponent<MobExpiration>().ResetExpiration();
    }
    
    void OnDestroy()
    {
        Destroy(mTarget);
    }
    
    bool InSameRegion(Mob mob, Player player)
    {
        Rigidbody2D playerBody = player.GetComponent<Rigidbody2D>();
        Vector2 playerRegion = g.i.Region(playerBody.position);
        return g.i.MobPartiallyInRegion(mob, playerRegion);
    }

    void Appear()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            Anim.SetInteger("AnimationState", 4);
        }

        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }

        float time = Time.time - lastStateChangeMoment;
        if (time > appearanceTime)
        {
            currentState = State.Roaming;
            return;
        }
    }
    void Roam()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            targetSet = false;
            Anim.SetInteger("AnimationState", 0);
        }

        if (!g.i.player.GetComponent<PlayerHealth>().alive)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            targetSet = false;
            Anim.SetInteger("AnimationState", 0);
            return; //just idle with the roaming state animation
        }

        if (!GetComponent<MobHealth>().alive)
        {
            StopAllCoroutines();
            currentState = State.Dead;
            return;
        }

        if (wasJustHit)
        {
            StopAllCoroutines();
            currentState = State.Hit;
            return;
        }

        if (InSameRegion(GetComponent<Mob>(), g.i.player))
        {
            StopAllCoroutines();
            currentState = State.Aiming;
            return;
        }

        if( freeze )
        {
            StopAllCoroutines();
            currentState = State.Frozen;
            return;
        }

        if (!targetSet)
        {
            if( roamInOriginalRegion )
            {
                Vector2 region = g.i.Region(transform.position);
                mTarget.transform.position = g.i.GetFreeRandomPositionInRegion(region, gameObject.transform.position);
            }
            else
            {
                mTarget.transform.position = g.i.GetFreeRandomPosition(gameObject.transform.position, roamingRadius);
            }
            
            GetComponent<Pathing>().SetTarget(mTarget);
            targetSet = true;
            resetTargetTimerStarted = false;
            return;
        }

        if (!resetTargetTimerStarted)
        {
            if (GetComponent<Pathing>().AtTarget())
            {
                StartCoroutine(ResetTarget());
                resetTargetTimerStarted = true;
            }
        }
    }
    IEnumerator ResetTarget()
    {
        float timeToWait = minTimeBetweenRoams + (maxTimeBetweenRoams - minTimeBetweenRoams) * UnityEngine.Random.value;
        yield return new WaitForSeconds(timeToWait);
        targetSet = false;
        yield return null;
    }

    void Attack()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            StartCoroutine(AttackPlayer());
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            StopAllCoroutines();
            return;
        }
        if (wasJustHit)
        {
            currentState = State.Hit;
            StopAllCoroutines();
            return;
        }
        if (!InSameRegion(GetComponent<Mob>(), g.i.player))
        {
            currentState = State.Roaming;
            StopAllCoroutines();
            return;
        }
        if (freeze)
        {
            currentState = State.Frozen;
            StopAllCoroutines();
            return;
        }
        if(!g.i.player.GetComponent<PlayerHealth>().alive)
        {
            currentState = State.Roaming;
            StopAllCoroutines();
            return;
        }
    }
    IEnumerator AttackPlayer()
    {
        yield return new WaitForSeconds(pauseBeforeFirstHit);
        yield return AttackPlayerRepeatedly();
    }
    IEnumerator AttackPlayerRepeatedly()
    {
        //start attack animation
        Anim.SetInteger("AnimationState", 3);

        yield return new WaitForSeconds(attackWindupDurationBeforeSound / 3 * 2);
        
        //get the player's position in the middle of winding up
        Vector3 playerPos = g.i.player.transform.position;

        //make SA face the position it attacks, during the whole attack
        if (g.i.player.transform.position.x < transform.position.x)
            transform.localEulerAngles = new Vector3(0, 180, 0);
        else
            transform.localEulerAngles = new Vector3(0, 0, 0);

        yield return new WaitForSeconds(attackWindupDurationBeforeSound / 3);
        g.i.audioManager.PlayClip(attack, transform.position);
        yield return new WaitForSeconds(attackWindupDurationAfterSound);


        //at this point in the attack animation we can instantiate the projectile
        Vector3 myPos = gameObject.transform.position;
        Vector3 dir = Vector3.right;
        dir = Quaternion.Euler(transform.rotation.eulerAngles) * dir;
        myPos += dir * projectileOffsetX;
        myPos.y += projectileOffsetY;
        GameObject newProjectile = g.Clone(projectile, myPos);
        if (IsFictional())
            newProjectile.GetComponent<SocialAnxietyProjectile>().MakeFictional();
        newProjectile.GetComponent<SocialAnxietyProjectile>().attackTags.Add("Player");
        Vector2 moveDir = (playerPos - myPos).normalized;
        newProjectile.GetComponent<MoveStraight>().move = moveDir;
        if (PlayerPrefs.GetString("Difficulty") == "Casual")
            newProjectile.GetComponent<MoveStraight>().speed = newProjectile.GetComponent<MoveStraight>().speed * 0.5f;
        if ( PlayerPrefs.GetInt("Slower projectiles") != 0 )
            newProjectile.GetComponent<MoveStraight>().speed = newProjectile.GetComponent<MoveStraight>().speed * (100 - g.i.boosts.SlowerProjectilesPercent) / 100;
             
        if (fictional)
        {
            newProjectile.GetComponent<SocialAnxietyProjectile>().damage = 0;
            SpriteRenderer[] renderers = newProjectile.GetComponentsInChildren<SpriteRenderer>();
            renderers[1].enabled = false;
        }

        //wait for the attack animation to finish
        yield return new WaitForSeconds(totalAttackDuration - attackWindupDurationBeforeSound - attackWindupDurationAfterSound);

        //end attack animation
        Anim.SetInteger("AnimationState", 0);

        //wait the appropriate time between hits
        float timeToWait = minPauseBetweenHits + (maxPauseBetweenHits - minPauseBetweenHits) * UnityEngine.Random.value;
        
        yield return new WaitForSeconds(timeToWait);
        yield return AttackPlayer();
    }
    void Dead()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = true;
            if (fictional)
            {
                Anim.SetInteger("AnimationState", 5);
                Record.Event("mob dead", "social anxiety", "fictional");
            }
            else
            {
                Anim.SetInteger("AnimationState", 2);
                g.i.audioManager.PlayClip(death, transform.position);
                if (g.i.spawnDrops)
                    g.i.spawnDrops.RegisterDeath(GetComponent<Mob>(), g.i.spawnDrops.SocialAnxietyHealthDropRate, g.i.spawnDrops.SocialAnxietyResourceDropRate, g.i.spawnDrops.SocialAnxietyResourceDropAmount, GetComponent<MobHealth>().maxHealth, g.i.spawnDrops.SocialAnxietyBoostDropRate, g.i.spawnDrops.SocialAnxietyConsumableDropRate);
                Record.Event("mob dead", "social anxiety");
            }
            GetComponent<MobExpiration>().StartExpiration();
        }
        float timeSpentStunned = Time.time - lastStateChangeMoment;
        if (timeSpentStunned > stunnedTime && fictional)
            Destroy(gameObject);
    }
    protected override void UpdateImpl()
    {
        State oldState = currentState;
        switch (currentState)
        {
            case State.Appearing:
                Appear();
                return;
            case State.Roaming:
                Roam();
                break;
            case State.Attacking:
                Attack();
                break;
            case State.Dead:
                Dead();
                break;
            case State.Hit:
                Hit();
                break;
            case State.Aiming:
                Aim();
                break;
            case State.Frozen:
                Frozen();
                break;
        }
        State newState = currentState;
        stateJustChanged = oldState != newState;
        if (stateJustChanged)
            lastStateChangeMoment = Time.time;
    }
    public bool IsFictional()
    {
        return fictional;
    }
    public void MakeFictional()
    {
        fictional = true;
        GetComponent<Mob>().icon.enabled = false;
        
        currentState = State.Appearing;
        stateJustChanged = true;
    }
    public void MakeReal()
    {
        fictional = false;
        GetComponent<Mob>().icon.enabled = true;
           
        currentState = State.Roaming;
        stateJustChanged = true;
    }
    public void OnHit()
    {
        g.Splash(transform.position);
        if( GetComponent<MobHealth>().alive )
        {
            GameObject outline = g.Clone(hitEffect, transform.position);
            outline.transform.localEulerAngles = transform.localEulerAngles;
        }
        wasJustHit = true;
    }
    void Hit()
    {
        if (stateJustChanged)
        {
            wasJustHit = false;
            GetComponent<Pathing>().zombie = true;
            Anim.SetInteger("AnimationState", 1);
            g.i.audioManager.PlayClip(hit, transform.position);
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }
        if (wasJustHit)
        {
            lastStateChangeMoment = Time.time;
            Anim.Play("SocialAnxietyHit", -1, 0f);
            wasJustHit = false;
            g.i.audioManager.PlayClip(hit, transform.position);
            return;
        }
        float timeSpentHit = Time.time - lastStateChangeMoment;
        if (timeSpentHit > hitTime)
        {
            if (freeze)
                currentState = State.Frozen;
            else
                currentState = State.Roaming;
            return;
        }
    }
    void Aim()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(g.i.player.gameObject);
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }
        if (wasJustHit)
        {
            currentState = State.Hit;
            return;
        }

        if (!InSameRegion(GetComponent<Mob>(), g.i.player))
        {
            currentState = State.Roaming;
            return;
        }

        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }

        Vector2 region = g.i.Region(g.i.player.transform.position);
        if( g.i.MobCompletelyInRegion(GetComponent<Mob>(), region) )
        {
            currentState = State.Attacking;
            return;
        }
    }
    public void OnTeleport()
    {
        //no repathing needed for SocialAnxiety, the state machine will
        //take into consideration the player's new position
        //GetComponent<Pathing>().Repath();
    }

    public void Freeze()
    {
        freeze = true;
        timeSinceFreeze = Time.time;
    }

    void Frozen()
    {
        if( stateJustChanged )
        {
            Anim.speed = 0;
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
        }

        if (!GetComponent<MobHealth>().alive)
        {
            freeze = false;
            Anim.speed = 1;
            currentState = State.Dead;
            return;
        }

        if (wasJustHit)
        {
            Anim.speed = 1;
            currentState = State.Hit;
            return;
        }

        float time = Time.time - timeSinceFreeze;
        if (time > 5)
        {
            freeze = false;
            Anim.speed = 1;
            currentState = State.Roaming;
            return;
        }
    }
}