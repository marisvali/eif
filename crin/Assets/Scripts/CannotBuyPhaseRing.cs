﻿using UnityEngine;
using UnityEngine.UI;

public class CannotBuyPhaseRing : MonoBehaviour
{
    public Text text;
    public void SetLevel(int level)
    {
        text.text = "You must reach level " + level.ToString() + " before you can buy this ring.";
    }
}
