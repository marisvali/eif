﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial1 : MonoBehaviour {
    private GameObject goal1;
    private GameObject goal2;
    private GameObject goal3;
    private GameObject goal4;
    private GameObject goal5;
    private GameObject goal6;
    private GameObject goal8;
    int activeGoal = 1;
    int monstersDefeated = -1;
    private Camera minimap;
    private MinimapBehavior minimapBehavior;
    private GameObject mask1;
    private GameObject mask2;
    private GameObject spawner1;
    private GameObject spawner2;
    private GameObject spawner3;
    private bool spawning;
    private bool timerStarted = false;
    private float time;
    private SpawnItems spawnItems;
    private bool mobWasHit = false;
    private int teleports = 0;
    private int lastTeleports = 0;
    private bool minimapTap = false;
    private List<GameObject> startingAmmos = new List<GameObject>();
    private bool startingAmmosRevealed = false;

    void Start()
    {
        goal1 = transform.GetChild(0).gameObject;
        goal2 = transform.GetChild(1).gameObject;
        goal3 = transform.GetChild(2).gameObject;
        goal4 = transform.GetChild(3).gameObject;
        goal5 = transform.GetChild(4).gameObject;
        goal6 = transform.GetChild(5).gameObject;
        goal8 = transform.GetChild(6).gameObject;
        goal1.SetActive(true);
        goal2.SetActive(false);
        goal3.SetActive(false);
        goal4.SetActive(false);
        goal5.SetActive(false);
        goal6.SetActive(false);
        goal8.SetActive(false);
        minimap = GameObject.Find("Minimap camera").GetComponent<Camera>();
        minimapBehavior = GameObject.FindObjectOfType<MinimapBehavior>();

        //enable masks which hide the minimap
        mask1 = GameObject.Find("Mask1");
        mask2 = GameObject.Find("Mask2");
        foreach (Transform child in mask1.transform)
            child.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        foreach (Transform child in mask2.transform)
            child.gameObject.GetComponent<SpriteRenderer>().enabled = true;

        spawner1 = GameObject.Find("Spawner1");
        spawner2 = GameObject.Find("Spawner2");
        spawner3 = GameObject.Find("Spawner3");
        spawning = false;
        spawnItems = GameObject.FindObjectOfType<SpawnItems>();
        spawnItems.active = false;

        var ammos = GameObject.Find("Ammo");
        for (int idx = 0; idx < ammos.transform.childCount; ++idx)
            startingAmmos.Add(ammos.transform.GetChild(idx).gameObject);
        foreach (var ammo in startingAmmos)
            ammo.SetActive(false);
    }
    void Update()
    {
        var mobs = GameObject.FindGameObjectsWithTag("SocialAnxiety");
        var mobsLeft = new List<GameObject>();
        foreach (var mob in mobs)
            if (!mob.GetComponent<MobExpiration>().Expired())
                mobsLeft.Add(mob);

        if ( activeGoal == 1 )
        {
            //check if the player has teleported since the last update
            if( teleports != lastTeleports )
            {
                if( teleports == 3)
                {
                    //the player has teleported 3 times, move to the next goal
                    activeGoal = 2;
                    goal1.SetActive(false);
                    goal2.SetActive(true);
                }
                else
                {
                    if( teleports == 1) //grammar check
                    {
                        goal1.transform.Find("TutorialText/Text").GetComponent<Text>().text = "Teleport anywhere by tapping. Try it! Teleport " + (3 - teleports).ToString() + " times.";
                    }
                    else
                    {
                        goal1.transform.Find("TutorialText/Text").GetComponent<Text>().text = "Teleport anywhere by tapping. Try it! Teleport once.";
                    }
                }
            }
        }
        lastTeleports = teleports;

        if (activeGoal == 2)
        {
            if (!timerStarted)
            {
                timerStarted = true;
                time = Time.time;
            }
            else if (Time.time - time > 1.5)
            {
                activeGoal = 3;
                timerStarted = false;
                goal2.SetActive(false);
                StartCoroutine(RevealStartingAmmos());
            }
         }

        if ( activeGoal == 3 && startingAmmosRevealed)
        {
            //check if there are ammos left in the default region
            int ammoLeft11 = 0;
            var ammoLeft = GameObject.FindGameObjectsWithTag("Ammo");
            foreach (var ammo in ammoLeft)
                if (g.i.Region(ammo.transform.position) == new Vector2(0, 1) )
                    ++ammoLeft11;
            
            if (ammoLeft11 == 0)
            {
                activeGoal = 4;
                spawnItems.active = true;
                goal3.SetActive(false);
                goal4.SetActive(true);
                spawnItems.allowedRegions.Add(new Vector2(0, 1));
            }
        }

        if (activeGoal == 4)
        {
            if( !timerStarted)
            {
                timerStarted = true;
                time = Time.time;
            }
            else if( Time.time - time > 1.5 )
            {
                activeGoal = 5;
                timerStarted = false;
                goal4.SetActive(false);
                goal5.SetActive(true);
            }
        }

        if (activeGoal == 5)
        {
            if (mobWasHit)
                goal5.SetActive(false);

            //check if there are mobs left in the default region
            int mobsLeft01 = 0;
            foreach (var mob in mobsLeft)
                if (g.i.Region(mob.transform.position) == new Vector2(0, 1))
                    ++mobsLeft01;
            if (mobsLeft01 == 0 && monstersDefeated == 3)
            {
                activeGoal = 6;
                goal5.SetActive(false);
                goal6.SetActive(true);
                mask2.SetActive(false);
                spawnItems.allowedRegions.Add(new Vector2(1, 1));
                mobWasHit = false;
            }
            if (mobsLeft01 == 0 && !spawning)
            {
                ++monstersDefeated;

                if (monstersDefeated == 0)
                    spawner1.GetComponent<MobSpawner>().stopped = false;

                if (monstersDefeated == 1)
                    spawner2.GetComponent<MobSpawner>().stopped = false;

                if (monstersDefeated == 2)
                    spawner3.GetComponent<MobSpawner>().stopped = false;

                spawning = true;
            }
            if (mobsLeft01 != 0)
                spawning = false;
        }

        if (activeGoal == 6)
        {
            if (minimapTap)
                goal6.SetActive(false);

            //check if there are mobs left in the next region
            int mobsLeft11 = 0;
            foreach (var mob in mobsLeft)
                if (g.i.Region(mob.transform.position) == new Vector2(1, 1))
                    ++mobsLeft11;

            if (mobsLeft11 == 0)
            {
                activeGoal = 7;
                mask1.SetActive(false);
                spawnItems.allowedRegions.Clear();
                spawnItems.maxItems = 5;
            }
        }

        if (activeGoal == 7)
        {
            if (mobsLeft.Count == 0)
            {
                goal8.SetActive(true);
                activeGoal = 8;
                PlayerPrefs.SetInt("Tutorial done", 1);
            }
        }
    }
    public bool AllowTap(UserInput input)
    {
        if (!Input.GetMouseButtonDown(0))
            return false;
        if (activeGoal < 6)
        {
            //don't allow minimap taps
            Vector3 vpmp = minimap.ScreenToViewportPoint(new Vector3(input.x, input.y, 0));
            Rect one = new Rect(0, 0, 1, 1);
            if (one.Contains(vpmp))
                return false;
        }
        if (activeGoal == 6)
        {
            //don't allow minimap taps
            Vector3 vpmp = minimap.ScreenToViewportPoint(new Vector3(input.x, input.y, 0));
            Rect one = new Rect(0, 0, 1, 1);
            if (one.Contains(vpmp))
            {
                Rect visibleSquare1 = new Rect(0, 0.33f, 0.33f, 0.33f);
                Rect visibleSquare2 = new Rect(0.33f, 0.33f, 0.33f, 0.33f);
                if (visibleSquare1.Contains(vpmp) || visibleSquare2.Contains(vpmp))
                    return true;
                else
                    return false;
            }
        }
        return true;
    }
    public bool Over()
    {
        return activeGoal == 8;
    }
    public void RegisterHit()
    {
        mobWasHit = true;
    }
    public void RegisterTeleport()
    {
        ++teleports;
    }
    public void RegisterMinimapChange()
    {
        minimapTap = true;
    }
    public IEnumerator RevealStartingAmmos()
    {   
        foreach( var ammo in startingAmmos )
        {
            ammo.SetActive(true);
            yield return new WaitForSeconds(0.1f);
        }
        startingAmmosRevealed = true;
        goal3.SetActive(true);
    }
}
