﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingTutorial : MonoBehaviour
{
    public float minimumLoadTime = 1;
    AsyncOperation async;

    void Start()
    {
        StartCoroutine(LoadNextScene());
        async = SceneManager.LoadSceneAsync(PlayerPrefs.GetString("Loading screen scene"));
        async.allowSceneActivation = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            StopAllCoroutines();
            async.allowSceneActivation = true;
        }
    }

    IEnumerator LoadNextScene()
    {
        yield return new WaitForSecondsRealtime(minimumLoadTime);
        async.allowSceneActivation = true;
        while (!async.isDone)
        {
            yield return null;
        }
    }
}
