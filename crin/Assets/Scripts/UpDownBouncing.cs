﻿using System.Collections;
using UnityEngine;

public class UpDownBouncing : MonoBehaviour
{
    public float transitionTime = 0;
    public float peakDelay = 0;
    public float movementRange = 0;

    private Vector3 initialPos;

    void Awake()
    {
        initialPos = transform.localPosition;
    }

    void OnEnable()
    {
        StartCoroutine(Transition(true));
    }

    void OnDisable()
    {
        StopAllCoroutines();
        transform.localPosition = initialPos;
    }

    IEnumerator Transition(bool goingUp)
    {
        float startTime = Time.time;
        float endTime = startTime + transitionTime;

        while (endTime >= Time.time)
        {
            float start, end;
            if (goingUp)
            {
                start = 0f;
                end = movementRange;
            }
            else
            {
                start = movementRange;
                end = 0;
            }
            
            float deltaY = Mathf.Lerp(start, end, (Time.time - startTime) / transitionTime);
            transform.localPosition = initialPos + new Vector3(0, deltaY, 0);
            yield return null;
        }

        yield return new WaitForSeconds(peakDelay);
        transform.localPosition = initialPos;
        yield return Transition(!goingUp);
    }
}
