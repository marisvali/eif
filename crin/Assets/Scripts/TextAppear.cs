﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextAppear : MonoBehaviour
{
    public float TimeUntilFadeIn = 0;
    public float FadeInDuration = 0;
    public float TimeUntilFadeOut = 0;
    public float FadeOutDuration = 0;
    void Start ()
    {
        StartCoroutine(Execute());
	}
	
	void Update ()
    {
		
	}

    IEnumerator Execute()
    {
        //disappear
        Color col = GetComponent<Text>().color;
        GetComponent<Text>().color = new Color(col.r, col.g, col.b, 0);

        //wait
        yield return new WaitForSeconds(TimeUntilFadeIn);

        //fade in
        {
            float startTime = Time.time;
            float endTime = startTime + FadeInDuration;
            while (endTime >= Time.time)
            {
                float alpha = Mathf.Lerp(0, 1, (Time.time - startTime) / FadeInDuration);
                GetComponent<Text>().color = new Color(col.r, col.g, col.b, alpha);
                yield return null;
            }
        }
        
        //optionally fade out
        if ( TimeUntilFadeOut > 0)
        {
            //wait
            yield return new WaitForSeconds(TimeUntilFadeOut);

            //fade out
            float startTime = Time.time;
            float endTime = startTime + FadeInDuration;
            while (endTime >= Time.time)
            {
                float alpha = Mathf.Lerp(1, 0, (Time.time - startTime) / FadeInDuration);
                GetComponent<Text>().color = new Color(col.r, col.g, col.b, alpha);
                yield return null;
            }
            GetComponent<Text>().color = new Color(col.r, col.g, col.b, 0);
        }
        
        yield return null;
    }
}
