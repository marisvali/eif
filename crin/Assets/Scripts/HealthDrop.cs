using UnityEngine;

public class HealthDrop : MonoBehaviour
{
    void Awake()
    {
        g.i.healthDrops.Add(this);
    }
    void OnDestroy()
    {
        g.i.healthDrops.Remove(this);
    }
}