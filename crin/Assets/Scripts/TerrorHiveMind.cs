﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TerrorHiveMind : MonoBehaviour
{
    public float timeBetweenTerrorAttacks = 0;
    public float activeTimeForTerrorAttack = 0;
    public GameObject fogOfWar = null;
    int activeTerrors = 0;
    float stateChangeMoment = 0;
    bool currentlyTerrorizing = false;
    private Boosts boostsObj;

    void Start()
    {
        boostsObj = g.i.boosts;
        stateChangeMoment = Time.time;
    }

    public void AddActiveTerror()
    {
        ++activeTerrors;
    }
    public void RemoveActiveTerror()
    {
        --activeTerrors;
    }
    private void Update()
    {
        if( currentlyTerrorizing )
        {
            float timeSpentTerrorizing = Time.time - stateChangeMoment;
            float realTerrorizingDuration = activeTimeForTerrorAttack;
            if (PlayerPrefs.GetInt("Shorter terror") != 0)
                realTerrorizingDuration -= boostsObj.ShorterTerrorDuration;
            if(activeTerrors <= 0 || timeSpentTerrorizing > realTerrorizingDuration)
            {
                fogOfWar.GetComponent<MeshRenderer>().enabled = false;
                currentlyTerrorizing = false;
                stateChangeMoment = Time.time;
            }
        }
        else
        {
            float timeSpentWaiting = Time.time - stateChangeMoment;
            if( activeTerrors > 0 && timeSpentWaiting > timeBetweenTerrorAttacks )
            {
                fogOfWar.GetComponent<MeshRenderer>().enabled = true;
                currentlyTerrorizing = true;
                stateChangeMoment = Time.time;
            }
        }
    }
    public bool CurrentlyTerrorizing()
    {
        return currentlyTerrorizing;
    }
    public void Freeze()
    {
        stateChangeMoment += 5;
    }
}