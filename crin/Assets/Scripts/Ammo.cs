﻿using UnityEngine;

public class Ammo : WorldObject
{
    public int Quantity = 0;
    Animator Anim;
    float timeSinceFreeze = 0;

    void Awake()
    {
        g.i.ammos.Add(this);
    }
    void OnDestroy()
    {
        g.i.ammos.Remove(this);
    }
    void Start()
    {
        Anim = GetComponent<Animator>();
    }
    public void Freeze()
    {
        Anim.speed = 0;
        timeSinceFreeze = Time.time;
    }
    protected override void UpdateImpl()
    {
        if (Anim.speed == 0)
        {
            float time = Time.time - timeSinceFreeze;
            if (time > 5)
            {
                Anim.speed = 1;
            }
        }
    }
}