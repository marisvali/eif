﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PulseColorButton : MonoBehaviour
{
    public Color startColor;
    public Color endColor;
    public float transitionTime = 0;
    public bool autoStart = false;
    public float totalPulseTime = 0;

    private Color originalColor;

    void Awake()
    {
        originalColor = GetComponent<Image>().color;
    }
    private void OnEnable()
    {
        if (autoStart)
        {
            StartCoroutine(Transition(true));
        }
    }

    void OnDisable()
    {
        if (autoStart)
        {
            StopPulsing();
        }
    }

    public void StartPulsing()
    {
        StartCoroutine(Transition(true));
        if (totalPulseTime > 0)
            StartCoroutine(StopPulsingAfter(totalPulseTime));
    }

    public void StopPulsing()
    {
        StopAllCoroutines();
        GetComponent<Image>().color = originalColor;
    }

    IEnumerator StopPulsingAfter(float duration)
    {
        yield return new WaitForSeconds(duration);
        StopPulsing();
    }

    IEnumerator Transition(bool goingUp)
    {
        float startTime = Time.time;
        float endTime = startTime + transitionTime;

        while (endTime >= Time.time)
        {
            float change = (Time.time - startTime) / transitionTime;
            if (!goingUp)
                change = 1.0f - change;
            Color diffCol = endColor - startColor;
            Color newCol = startColor + diffCol * change;
            GetComponent<Image>().color = newCol;
            yield return null;
        }
        yield return Transition(!goingUp);
    }
}