﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayLevel : MonoBehaviour
{
    int level = -10;

    void Update()
    {
        int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
        if (level != currentLevel)
        {
            level = currentLevel;
            if (level <= 0)
            {
                Text text = GetComponent<Text>();
                text.text = "-/30";
            }
            else
            {
                Text text = GetComponent<Text>();
                text.text = level.ToString() + "/30";
            }
        }
    }
}