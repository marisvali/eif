﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseColor : MonoBehaviour
{
    public Color startColor;
    public Color endColor;
    public float fadeDuration;

    private Color originalColor;

    private void OnEnable()
    {
        originalColor = GetComponent<SpriteRenderer>().color;
        StartCoroutine(Transition(true));
    }
    
    void OnDisable()
    {
        StopAllCoroutines();
        GetComponent<SpriteRenderer>().color = originalColor;
    }

    IEnumerator Transition(bool goingUp)
    {
        float startTime = Time.time;
        float endTime = startTime + fadeDuration;

        while (endTime >= Time.time)
        {
            float change = (Time.time - startTime) / fadeDuration;
            if (!goingUp)
                change = 1.0f - change;
            Color diffCol = endColor - startColor;
            Color newCol = startColor + diffCol * change;
            GetComponent<SpriteRenderer>().color = newCol;
            yield return null;
        }
        yield return Transition(!goingUp);
    }
}