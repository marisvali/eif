﻿using UnityEngine;

public class Warning : MonoBehaviour
{
    public float startAlpha = 0;
    public float endAlpha = 1;
    public float maxRange = 500;
    public float minRange = 0f;
    public float hitDuration = 0.5f;
    float hitStart = 0;
    bool showHit = false;

	void Start()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }
	
	void Update()
    {
        if (!g.i.player)
            return;

        if( showHit )
        {
            if( Time.time - hitStart < hitDuration )
                return;
            else
            {
                showHit = false;
                GetComponent<SpriteRenderer>().enabled = false;
            }
        }

        //react to mobs being close
        //List<GameObject> seekers = new List<GameObject>();;
        //seekers.AddRange(("Depression"));
        //seekers.AddRange(("SocialAnxietyProjectile"));
        //seekers.AddRange(("Anger"));
        //float min = 100000;
        //foreach (GameObject obj in seekers)
        //    if (obj.GetComponent<Health>() == null || obj.GetComponent<Health>().alive)
        //    {
        //        Vector2 dist = obj.transform.position - player.transform.position;
        //        if (dist.magnitude < min)
        //            min = dist.magnitude;
        //    }

        //float alpha = 0;
        //if (min < maxRange && min > minRange)
        //    alpha = startAlpha + (endAlpha - startAlpha) * (maxRange - min) / (maxRange - minRange);
        //if (min < minRange)
        //    alpha = 1.0f;

        //SetAlpha(alpha);
    }

    public void RegisterHit()
    {
        hitStart = Time.time;
        showHit = true;
        GetComponent<SpriteRenderer>().enabled = true;
    }
}