﻿using System.Collections.Generic;
using UnityEngine;

public class BoostDrop : MonoBehaviour
{
    public SpriteRenderer icon;
    
    string[] possibleBoostDrops =
    {
        "More ammo",
        "Free shot",
        "More health",
        "Deflect",
        "Slower depression",
        "Weaker depression",
        "Slower projectiles",
        "Boom",
        "Slower anger",
        "Shorter terror",
        "Shorter paranoia"
    };
    Boosts.Boost chosenBoost;

    public void Initialize()
    {
        //choose a random boost
        List<string> possibleBoostDropsList = new List<string>(possibleBoostDrops);
        if (PlayerPrefs.GetString("Difficulty") == "VeryHard")
            possibleBoostDropsList.Remove("More health");

        string boostName = possibleBoostDropsList[UnityEngine.Random.Range(0, possibleBoostDropsList.Count)];
        Initialize(boostName);
    }

    public void Initialize(string boostName)
    {
        var boosts = g.i.boosts.GetBoosts();
        foreach (var boost in boosts)
            if (boost.InternalName == boostName)
            {
                chosenBoost = boost;
                break;
            }

        //load the boost icon as this object's sprite
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Images/Boosts/Boosts " + chosenBoost.InternalName);
        icon.sprite = Resources.Load<Sprite>("Images/Boosts/Boosts " + chosenBoost.InternalName);
    }

    public Boosts.Boost GetChosenBoost()
    {
        return chosenBoost;
    }
}