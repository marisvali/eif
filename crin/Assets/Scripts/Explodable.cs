﻿using System.Collections;
using UnityEngine;

public class Explodable : WorldObject
{
    public GameObject explosionReference;
    GameObject explosion;
    bool canGenerateChainReaction = true;
	protected override void UpdateImpl()
    {
        if (explosion == null)
            return;
        if (!explosion.activeSelf)
        {
            explosion = null;
            return;
        }

        explosion.transform.position = transform.position;
    }
    IEnumerator ExplodeHit()
    {
        yield return new WaitForSeconds(0.2f);
        if(GetComponent<MobHealth>())
            GetComponent<MobHealth>().Hit(1);
        if (GetComponent<DepressionAI>() != null)
            GetComponent<DepressionAI>().OnHit();
        if (GetComponent<AngerAI>() != null)
            GetComponent<AngerAI>().OnHit();
        if (GetComponent<SocialAnxietyAI>() != null)
            GetComponent<SocialAnxietyAI>().OnHit();
        if (GetComponent<ParanoiaAI>() != null)
            GetComponent<ParanoiaAI>().OnHit();
        if (GetComponent<TerrorAI>() != null)
            GetComponent<TerrorAI>().OnHit();
    }
    public void Explode()
    {
        if (explosion != null)
            return;

        explosion = g.Clone(explosionReference, transform.position);
        StartCoroutine(ExplodeHit());
    }
    public bool CanGenerateChainReaction()
    {
        return canGenerateChainReaction;
    }
    public void DisableChainReaction()
    {
        canGenerateChainReaction = false;
        StartCoroutine(EnableChainReaction());
    }
    IEnumerator EnableChainReaction()
    {
        yield return new WaitForSeconds(0.1f);
        canGenerateChainReaction = true;
    }
}
