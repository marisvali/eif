﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RejectionAuxAI : MonoBehaviour
{
    public AnimationClip hitAnimation;
    public float hitTime = 0;
    public AudioClip death;
    public AudioClip hit;
    public MobSpawner.OwnerEnum isOwnerOfType;
    public RejectionAI rejection;

    float lastStateChangeMoment = 0;
    bool stateJustChanged = false;
    bool wasJustHit = false;
    bool freeze = false;
    float timeSinceFreeze = 0;
    Animator Anim;
    bool mustBeRetreated = false;

    enum State
    {
        Dead,
        Idle,
        Frozen,
        Hit,
        Retreated
    }
    State currentState;
    void Start ()
    {
        currentState = State.Idle;
        stateJustChanged = true;
        Anim = transform.GetComponent<Animator>();
    }
    public bool IsDead()
    {
        return currentState == State.Dead;
    }
    void Dead()
    {
        if (stateJustChanged)
        {
            Anim.SetInteger("AnimationState", 2);
            GetComponent<MobExpiration>().StartExpiration();
            GetComponent<Mob>().icon.enabled = false;
            g.i.audioManager.PlayClip(death, transform.position);
            // var spawners = g.i.mobs.GetOnlySpawners();
            // foreach( var spawner in spawners )
            //     if( spawner.GetComponent<MobSpawner>().owner == isOwnerOfType )
            //         spawner.GetComponent<MobSpawner>().stopped = false;
        }
    }
    void Idle()
    {
        if (stateJustChanged)
        {
            Anim.SetInteger("AnimationState", 0);
            Anim.speed = 1;
            GetComponent<MobExpiration>().SetExpired(false);
            GetComponent<MobHealth>().ShowHealth();
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }
        if (wasJustHit)
        {
            currentState = State.Hit;
            return;
        }
        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }
        if( mustBeRetreated)
        {
            currentState = State.Retreated;
            return;
        }
    }
    void Frozen()
    {
        if (stateJustChanged)
        {
            Anim.speed = 0;
        }

        if (!GetComponent<MobHealth>().alive)
        {
            freeze = false;
            Anim.speed = 1;
            currentState = State.Dead;
            return;
        }

        if (wasJustHit)
        {
            Anim.speed = 1;
            currentState = State.Hit;
            return;
        }

        float time = Time.time - timeSinceFreeze;
        if (time > 5)
        {
            freeze = false;
            Anim.speed = 1;
            if (mustBeRetreated)
                currentState = State.Retreated;
            else
                currentState = State.Idle;
            return;
        }
    }
    void Hit()
    {
        if (stateJustChanged)
        {
            wasJustHit = false;
            Anim.SetInteger("AnimationState", 1);
            g.i.audioManager.PlayClip(hit, transform.position);
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }
        if (wasJustHit || stateJustChanged && freeze)
        {
            lastStateChangeMoment = Time.time;
            Anim.Play(hitAnimation.name, -1, 0f);
            wasJustHit = false;
            return;
        }
        if (mustBeRetreated)
        {
            currentState = State.Retreated;
            return;
        }
        float timeSpentHit = Time.time - lastStateChangeMoment;
        if (timeSpentHit > hitTime)
        {
            if (freeze)
                currentState = State.Frozen;
            else
                currentState = State.Idle;
            return;
        }
    }
    void Retreated()
    {
        if (stateJustChanged)
        {
            wasJustHit = false;
            Anim.SetInteger("AnimationState", 3);
            GetComponent<MobExpiration>().SetExpired(true);
            GetComponent<MobHealth>().HideHealth();
        }

        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }

        if ( !mustBeRetreated )
        {
            currentState = State.Idle;
            GetComponent<MobExpiration>().SetExpired(false);
            GetComponent<MobHealth>().ShowHealth();
            return;
        }
    }
    void Update ()
    {
        State oldState = currentState;
        switch (currentState)
        {
            case State.Dead:
                Dead();
                break;
            case State.Idle:
                Idle();
                break;
            case State.Frozen:
                Frozen();
                break;
            case State.Hit:
                Hit();
                break;
            case State.Retreated:
                Retreated();
                break;
        }
        State newState = currentState;
        stateJustChanged = oldState != newState;
        if (stateJustChanged)
            lastStateChangeMoment = Time.time;
    }
    public void Freeze()
    {
        freeze = true;
        timeSinceFreeze = Time.time;
    }
    public void OnHit()
    {
        Vector3 hitPos = transform.position;
        if (GetComponent<Mob>().clickArea != null)
            hitPos = GetComponent<Mob>().clickArea.bounds.center;
        g.Splash(hitPos);
        wasJustHit = true;
        rejection.OnHit();
    }
    public void OnRetreat()
    {
        mustBeRetreated = true;
    }
    public void OnEmerge()
    {
        mustBeRetreated = false;
    }
}
