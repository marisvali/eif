﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextAppearDelayer : MonoBehaviour
{
    public float GlobalApparitionDelay = 0;
	void Awake ()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.GetComponent<TextAppear>().TimeUntilFadeIn += GlobalApparitionDelay;
        }
	}
}
