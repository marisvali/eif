﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SoundEffectsVolume : MonoBehaviour, IEndDragHandler, IBeginDragHandler
{
    bool isBeingDragged = false;
    StartMenu menu;

    void Start()
    {
        menu = GameObject.FindObjectOfType<StartMenu>();
    }

    public bool IsBeingDragged()
    {
        return isBeingDragged;
    }

    public void OnEndDrag(PointerEventData data)
    {
        Debug.Log("drag end");
        isBeingDragged = false;
        menu.SoundEffectsVolumeChanged();
    }

    public void OnBeginDrag(PointerEventData data)
    {
        Debug.Log("drag start");

        isBeingDragged = true;
    }
}
