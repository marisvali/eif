﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : WorldObject
{
    static public Player i; // To be used only by g.
    public float timeBetweenFrenzyShots = 0;
    public float invulnerabilityDurationAfterHit = 0;
    public float invulnerabilityDurationAfterRevival = 0;
    public int maxAmmo = 0;
    public bool beamHitsMultiple = false;
    public bool justHitExcessAmmo = false;
    public bool justHitExcessHealth = false;
    public float disappearTime1 = 1.5f;
    public float disappearTime2 = 1f;
    public AudioClip attack;
    public AudioClip teleport;
    public AudioClip hit;
    public AudioClip death;
    public AudioClip ammoPickUp;
    public AudioClip reviveClip;
    public AudioClip freeShotClip;
    public AudioClip resourcePickUpClip;
    public AudioClip healingClip;
    public SpriteRenderer mainSprite;
    public SpriteRenderer icon;
    public Transform ammoStack;
    public GameObject visibility1;
    public GameObject visibility2;
    public Collider2D placementBounds;
    public GameObject energyBeam;
    public GameObject energyBeamBlue;
    public GameObject playerLeaving;
    public GameObject playerLeavingFem;
    public GameObject resourceChange;
    public GameObject scoreChange;
    public GameObject evade;
    public GameObject healEffectRef;
    public GameObject freeShotRef;
    public GameObject revivalScrollAnimationRef;
    public GameObject ammoObj;

    int ammo = 0;
    Animator Anim;
    int BeamOffsetX = 55;
    int BeamOffsetY = 65;
    bool invulnerable = false;
    float invulnerableStartTime = 0;
    float invulnerabilityDuration = 0;
    bool disappeared = false;
    List<GameObject> freeShots = new List<GameObject>();
    bool depressionAttached = false;
    Collider2D[] colliders = null;
    bool displayFullAmmoFullHealthMessages = true;
    Coroutine runningEvade = null;
    bool disappearing = false;
    int hitsTaken = 0;

    void Awake()
    {
        i = this;
    }
    void Start()
    {
        Anim = GetComponent<Animator>();
        UpdateAmmoStackDisplay();
        colliders = new Collider2D[100];
    }

    protected override void UpdateImpl()
    {
        if (invulnerable &&
            Time.time - invulnerableStartTime > invulnerabilityDuration)
        {
            invulnerable = false;
        }

        //check triggers here instead of OnTrigger2D because we might have objects that we were not able to collect when we moved over them (because of full ammo or full health) and that have become collectable in the meantime
        ContactFilter2D filter = new ContactFilter2D();
        filter.useTriggers = true;
        int nrColliders = GetComponent<Collider2D>().OverlapCollider(filter, colliders);
        for (int idx = 0; idx < nrColliders; ++idx)
        {
            Collider2D collider = colliders[idx];

            if (!collider.gameObject.activeSelf)
                return;

            if (!GetComponent<PlayerHealth>().alive)
                return;

            bool collected = false;
            if (collider.tag == "Resource")
            {
                int amount = collider.gameObject.GetComponent<Resource>().amount;
                int oldTotal = PlayerPrefs.GetInt("Resources");
                int newTotal = oldTotal + amount;
                PlayerPrefs.SetInt("Resources", newTotal);
                collected = true;
                Record.Event("player collected", "coins", oldTotal.ToString(), newTotal.ToString());
                g.i.mainScript.ShowFloatingText(resourceChange, amount, transform.position, GetComponent<Collider2D>().bounds);
                g.i.audioManager.PlayPositionIndependentClip(resourcePickUpClip);
            }

            if (collider.tag == "Ammo")
            {
                if (ammo < maxAmmo)
                {
                    ammo += collider.GetComponent<Ammo>().Quantity;
                    if (ammo > maxAmmo)
                        ammo = maxAmmo;
                    PlayerPrefs.SetInt("PlayerAmmo", ammo);
                    UpdateAmmoStackDisplay();
                    Record.Event("player collected", "ammo", (ammo - collider.GetComponent<Ammo>().Quantity).ToString(), ammo.ToString());
                    collected = true;
                    if (!disappearing)
                    {
                        Anim.SetInteger("AnimationState", 5);
                        Anim.SetInteger("AnimationStateDoReset", 1);
                    }
                    g.i.audioManager.PlayPositionIndependentClip(ammoPickUp);
                }
                else
                {
                    if (displayFullAmmoFullHealthMessages)
                    {
                        justHitExcessAmmo = true;
                        displayFullAmmoFullHealthMessages = false;
                    }
                }
            }

            if (collider.tag == "Health")
            {
                var oldHealth = GetComponent<PlayerHealth>().GetHealth();
                if (oldHealth < GetComponent<PlayerHealth>().maxHealth && PlayerPrefs.GetInt("Bought Shield 1") == 0 ||
                    PlayerPrefs.GetInt("Shield 1") == 0 && PlayerPrefs.GetInt("Bought Shield 1") == 1)
                {
                    GetComponent<PlayerHealth>().AddHealth();
                    ShowHealedAnimation();
                    Record.Event("player collected", "health", oldHealth.ToString(), GetComponent<PlayerHealth>().GetHealth().ToString());
                    collected = true;
                }
                else
                {
                    if (displayFullAmmoFullHealthMessages)
                    {
                        justHitExcessHealth = true;
                        displayFullAmmoFullHealthMessages = false;
                    }
                }
            }

            if (collider.tag == "Boost")
            {
                Boosts.Boost boost = collider.GetComponent<BoostDrop>().GetChosenBoost();
                collected = true;
            }

            if (collider.tag == "FirestormConsumable")
            {
                g.i.ExecuteFirestorm();
                collected = true;
            }

            if (collider.tag == "StopwatchConsumable")
            {
                if (!g.i.IsStopwatchActive())
                {
                    g.i.ExecuteStopwatch();
                    collected = true;
                }
            }

            if (collected)
                GameObject.Destroy(collider.gameObject);
        }

        //update visibility
        if (PlayerPrefs.GetInt("Terror resistance") == 0)
        {
            if (!visibility1.activeSelf)
                visibility1.SetActive(true);
            if (visibility2.activeSelf)
                visibility2.SetActive(false);
        }
        else
        {
            if (visibility1.activeSelf)
                visibility1.SetActive(false);
            if (!visibility2.activeSelf)
                visibility2.SetActive(true);
        }
    }

    public void Attack(Mob target, bool stopAtFirstMob)
    {
        Vector3 dest3 = target.clickArea.bounds.center;
        var dest = new Vector2(dest3.x, dest3.y) + target.clickArea.offset;

        if (depressionAttached)
            return;

        if (!GetComponent<PlayerHealth>().alive)
            return;

        Vector2 source = new Vector2(transform.position.x, transform.position.y);

        if (dest.x < source.x)
        {
            source.x -= BeamOffsetX;
            source.y += BeamOffsetY;
            transform.localEulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            source.x += BeamOffsetX;
            source.y += BeamOffsetY;
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }

        if (invulnerable)
        {
            Anim.SetInteger("AnimationState", 0);
            invulnerable = false;
        }

        Anim.SetInteger("AnimationState", 1);

        if (ammo <= 0)
            return;

        bool freeShot = false;
        if (PlayerPrefs.GetInt("Free shot") != 0)
        {
            if ((UnityEngine.Random.value * 100.0) <= g.i.boosts.FreeShotPercent)
            {
                ShowFreeShotAnimation();
                freeShot = true;
            }
        }

        GameObject newProjectile;
        if (freeShot)
        {
            g.i.audioManager.PlayPositionIndependentClip(freeShotClip);
            newProjectile = g.Clone(energyBeamBlue);
        }
        else
        {
            newProjectile = g.Clone(energyBeam);
        }

        newProjectile.GetComponent<EnergyBeam>().Initialize(source, target, beamHitsMultiple, stopAtFirstMob, true);

        if (!freeShot && PlayerPrefs.GetInt("PlayerInfiniteAmmo") == 0)
        {
            ammo -= newProjectile.GetComponent<EnergyBeam>().ammoCost;
            if (ammo == 0)
                SpawnAmmo();
            PlayerPrefs.SetInt("PlayerAmmo", ammo);
            UpdateAmmoStackDisplay();
        }

        g.i.audioManager.PlayPositionIndependentClip(attack);
    }

    public void Attack(Vector3 target, bool stopAtFirstMob)
    {
        var dest = (Vector2)target;

        if (depressionAttached)
            return;

        if (!GetComponent<PlayerHealth>().alive)
            return;

        Vector2 source = new Vector2(transform.position.x, transform.position.y);

        if (dest.x < source.x)
        {
            source.x -= BeamOffsetX;
            source.y += BeamOffsetY;
            transform.localEulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            source.x += BeamOffsetX;
            source.y += BeamOffsetY;
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }

        Anim.SetInteger("AnimationState", 1);

        if (ammo <= 0)
            return;

        GameObject newProjectile;
        newProjectile = g.Clone(energyBeam);

        newProjectile.GetComponent<EnergyBeam>().Initialize(source, target, beamHitsMultiple, stopAtFirstMob, true);

        ammo -= newProjectile.GetComponent<EnergyBeam>().ammoCost;
        if (ammo == 0)
            SpawnAmmo();
        PlayerPrefs.SetInt("PlayerAmmo", ammo);
        UpdateAmmoStackDisplay();

        g.i.audioManager.PlayPositionIndependentClip(attack);
    }

    public void TriggerEndSequence1()
    {
        transform.localEulerAngles = new Vector3(0, 0, 0);
        Anim.SetInteger("AnimationState", 8);
    }

    public void TriggerEndSequence2()
    {
        Anim.SetInteger("AnimationState", 9);
    }

    public void TriggerEndSequence3()
    {
        Anim.SetInteger("AnimationState", 10);
    }

    public void CreatePlayerLeaving(Vector2 pos)
    {
        if (PlayerPrefs.GetString("Gender") == "Male")
            g.Clone(playerLeaving, pos);
        else
            g.Clone(playerLeavingFem, pos);
    }

    public void Teleport(Vector2 newPos)
    {
        if (!GetComponent<PlayerHealth>().alive)
            return;

        invulnerable = false;
        depressionAttached = false;
        Anim.SetInteger("AnimationState", 3);

        CreatePlayerLeaving(transform.position);

        Rigidbody2D body = GetComponent<Rigidbody2D>();
        body.transform.position = newPos; //change the position by assinging to
        //body.transform.position, because assigning to body.position doesn't work well
        //sometimes the position gets changed, as in the enemies take into account the
        //new position, but the character still gets drawn at its old position
        body.velocity = Vector2.zero;
        body.angularVelocity = 0;

        foreach (var mob in g.i.mobs)
            if (mob.GetComponent<DepressionAI>())
                mob.GetComponent<DepressionAI>().OnTeleport();
            else if (mob.GetComponent<AngerAI>())
                mob.GetComponent<AngerAI>().OnTeleport();
            else if (mob.GetComponent<SocialAnxietyAI>())
                mob.GetComponent<SocialAnxietyAI>().OnTeleport();

        g.i.audioManager.PlayPositionIndependentClip(teleport, 0.2f);

        displayFullAmmoFullHealthMessages = true;
    }

    public int GetAmmo()
    {
        return ammo;
    }
    public void SetAmmo(int qty)
    {
        ammo = qty;
        PlayerPrefs.SetInt("PlayerAmmo", ammo);
        UpdateAmmoStackDisplay();
    }
    public void AngerAttack(int damage, int nrResources)
    {
        ++hitsTaken;
        if (PlayerPrefs.GetInt("PlayerInvulnerable") != 0)
            damage = 0;
        if (invulnerable)
            return;

        if (!GetComponent<PlayerHealth>().alive)
            return;

        if (PlayerPrefs.GetInt("Deflect") != 0)
        {
            if ((UnityEngine.Random.value * 100f) < g.i.boosts.DeflectPercent)
            {
                if (runningEvade != null)
                    StopCoroutine(runningEvade);
                runningEvade = StartCoroutine(EvadeSequence());
                MakeInvulnerable(invulnerabilityDurationAfterHit);
                g.i.mainScript.ShowFloatingText(evade, "Evaded!", transform.position, GetComponent<Collider2D>().bounds);
                return;
            }
        }

        int oldHealth = GetComponent<PlayerHealth>().GetHealth();
        GetComponent<PlayerHealth>().Hit(damage);
        int newHealth = GetComponent<PlayerHealth>().GetHealth();
        Record.Event("player got hit", oldHealth.ToString(), newHealth.ToString());

        Anim.SetInteger("AnimationState", 11);
        g.i.warningFog.RegisterHit();
        g.i.cameraMain.GetComponent<CameraShaker>().StartShake();
        if (damage != 0)
        {
            if (PlayerPrefs.GetInt("Calm") == 0)
            {
                DropResources(nrResources);
            }
            StartFrenzy();
            MakeInvulnerable(invulnerabilityDurationAfterHit);
        }
    }
    IEnumerator EvadeSequence()
    {
        Color originalColor = new Color(1, 1, 1, 1);
        Color targetColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        float duration = 0;
        float totalDuration = 0.1f;
        while (duration < totalDuration)
        {
            float factor = duration / totalDuration;
            mainSprite.color = (1 - factor) * originalColor + factor * targetColor;
            yield return null;
            duration += Time.deltaTime;
        }

        yield return new WaitForSeconds(0.3f);

        originalColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        targetColor = new Color(1, 1, 1, 1);
        duration = 0;
        totalDuration = 0.1f;
        while (duration < totalDuration)
        {
            float factor = duration / totalDuration;
            mainSprite.color = (1 - factor) * originalColor + factor * targetColor;
            yield return null;
            duration += Time.deltaTime;
        }
    }
    public void SocialAnxietyAttack(int damage)
    {
        if (!GetComponent<PlayerHealth>().alive)
            return;

        RegisterHit(damage, 6);
    }
    public void DepressionAttack(int damage)
    {
        if (!GetComponent<PlayerHealth>().alive)
            return;

        depressionAttached = true;
        RegisterHit(damage, 7);
    }
    public void RejectionWaveAttack(int damage)
    {
        if (!GetComponent<PlayerHealth>().alive)
            return;

        RegisterHit(damage, 6);
    }
    void ShowFreeShotAnimation()
    {
        GameObject freeShot = g.Clone(freeShotRef, g.i.cameraMainCanvas.transform);
        freeShot.GetComponent<FreeShot>().Initialize();
        Vector3 worldPos = transform.position;
        worldPos.y = GetComponent<Collider2D>().bounds.max.y + GetComponent<Collider2D>().bounds.size.y * 0.3f;
        Vector3 vpPos = g.i.cameraMain.WorldToViewportPoint(worldPos);
        var canvasRect = g.i.cameraMainCanvas.GetComponent<RectTransform>().rect;
        Vector2 canvasPos = new Vector2(vpPos.x * canvasRect.width, vpPos.y * canvasRect.height);
        freeShot.GetComponent<RectTransform>().anchoredPosition = canvasPos;
        freeShots.Add(freeShot);
    }
    public void DestroyFreeShots()
    {
        foreach (var freeShot in freeShots)
            Destroy(freeShot);
        freeShots.Clear();
    }
    protected void DropResources(int nr)
    {
        int oldTotal = PlayerPrefs.GetInt("Resources");
        int dif = -nr;
        if (oldTotal + dif < 0)
            dif = -oldTotal;
        if (dif != 0)
        {
            PlayerPrefs.SetInt("Resources", oldTotal + dif);
            g.i.mainScript.ShowFloatingText(resourceChange, dif, transform.position, GetComponent<Collider2D>().bounds);
        }
    }
    protected void DropScore(int nr)
    {
        int oldTotal = PlayerPrefs.GetInt("CurrentScore");
        int dif = -nr;
        if (oldTotal + dif < 0)
            dif = -oldTotal;
        if (dif != 0)
        {
            PlayerPrefs.SetInt("CurrentScore", oldTotal + dif);
            g.i.mainScript.ShowFloatingText(scoreChange, -nr, transform.position, GetComponent<Collider2D>().bounds);
        }
    }
    protected void StartFrenzy()
    {
        int nrFrenzyShots = ammo;
        StartCoroutine(ExecuteFrenzy(nrFrenzyShots));
    }
    IEnumerator ExecuteFrenzy(int shotsLeft)
    {
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(ExecuteFrenzyImpl(shotsLeft));
    }
    IEnumerator ExecuteFrenzyImpl(int shotsLeft)
    {
        if (shotsLeft == 0)
            yield break;

        float randomAngle = (float)(2 * Math.PI * UnityEngine.Random.value);
        Vector3 randomDir = new Vector3((float)Math.Cos(randomAngle), (float)Math.Sin(randomAngle), 0) * UnityEngine.Random.value * 1000;
        Vector3 pos = transform.position + randomDir;
        Rect rect = g.i.RegionRect(transform.position);
        while (!rect.Contains(pos))
        {
            randomDir = randomDir * 0.9f;
            pos = transform.position + randomDir;
        }

        Attack(pos, true);
        yield return new WaitForSeconds(timeBetweenFrenzyShots);
        --shotsLeft;
        yield return ExecuteFrenzyImpl(shotsLeft);
    }
    void RegisterHit(int damage, int newAnimationState)
    {
        ++hitsTaken;
        if (PlayerPrefs.GetInt("PlayerInvulnerable") != 0)
            damage = 0;
        if (invulnerable)
            return;

        Anim.SetInteger("AnimationState", newAnimationState);

        if (PlayerPrefs.GetInt("Deflect") != 0)
        {
            if ((UnityEngine.Random.value * 100f) < g.i.boosts.DeflectPercent)
            {
                if (runningEvade != null)
                    StopCoroutine(runningEvade);
                runningEvade = StartCoroutine(EvadeSequence());
                MakeInvulnerable(invulnerabilityDurationAfterHit);
                g.i.mainScript.ShowFloatingText(evade, "Evaded!", transform.position, GetComponent<Collider2D>().bounds);
                return;
            }
        }

        int oldHealth = GetComponent<PlayerHealth>().GetHealth();
        GetComponent<PlayerHealth>().Hit(damage);
        int newHealth = GetComponent<PlayerHealth>().GetHealth();
        Record.Event("player got hit", oldHealth.ToString(), newHealth.ToString());
        g.i.warningFog.RegisterHit();
        MakeInvulnerable(invulnerabilityDurationAfterHit);
        g.i.cameraMain.GetComponent<CameraShaker>().StartShake();

        g.i.audioManager.PlayPositionIndependentClip(hit);
    }
    public void MakeInvulnerable(float duration)
    {
        invulnerable = true;
        invulnerabilityDuration = duration;
        invulnerableStartTime = Time.time;
    }

    IEnumerator Disappear()
    {
        disappearing = true;

        yield return new WaitForSeconds(disappearTime1);

        Teleport(new Vector2(-2000, -2000));

        yield return new WaitForSeconds(disappearTime2);

        disappeared = true;
    }
    public void StartDisappear()
    {
        StartCoroutine(Disappear());
    }

    public bool Disappeared()
    {
        return disappeared;
    }

    public void UpdateAmmoStackDisplay()
    {
        for (int idx = 0; idx < ammoStack.childCount; ++idx)
            ammoStack.GetChild(idx).gameObject.SetActive(idx < ammo);
    }

    public void Die()
    {
        PlayerPrefs.SetInt("PlayerAmmo", 0);
        g.i.audioManager.PlayPositionIndependentClip(death);
        Anim.SetInteger("AnimationState", 9);
    }

    public void ShowHealedAnimation()
    {
        GameObject healEffect = g.Clone(healEffectRef, transform);
        healEffect.transform.localPosition = new Vector3(0, 0, 0);
        g.i.audioManager.PlayPositionIndependentClip(healingClip);
    }

    public void Revive()
    {
        Anim.SetInteger("AnimationState", 10);

        GameObject revivalScrollAnimation = g.Clone(revivalScrollAnimationRef, transform);
        revivalScrollAnimation.transform.localPosition = new Vector3(0, 100, 0);

        GetComponent<PlayerHealth>().Set(GetComponent<PlayerHealth>().maxHealth);
        if (PlayerPrefs.GetInt("Bought Shield 1") == 1)
            PlayerPrefs.SetInt("Shield 1", 1);
        if (PlayerPrefs.GetInt("Bought Shield 2") == 1)
            PlayerPrefs.SetInt("Shield 2", 1);
        if (PlayerPrefs.GetInt("Bought Shield 3") == 1)
            PlayerPrefs.SetInt("Shield 3", 1);
        GetComponent<Player>().MakeInvulnerable(invulnerabilityDurationAfterRevival);

        g.i.audioManager.PlayPositionIndependentClip(reviveClip);
    }

    public bool DepressionAttached()
    {
        return depressionAttached;
    }

    public int NrHitsTaken()
    {
        return hitsTaken;
    }

    void SpawnAmmo()
    {
        Vector2 newPos = new Vector2(0, 0);

        List<Collider2D> collidersToAvoid = new List<Collider2D>();

        var tags = new List<string>
        {
            "Paranoia",
            "Terror",
            "SocialAnxiety",
            "Rejection",
            "RejectionAux"
        };

        foreach (var mob in g.i.mobs)
            if (tags.Contains(mob.tag))
                collidersToAvoid.Add(mob.clickArea);

        collidersToAvoid.Add(g.i.player.GetComponent<Collider2D>());

        List<GameObject> allDrops = g.i.spawnDrops.GetAllDrops();

        for (int idx = 0; idx < 1000; ++idx)
        {
            newPos = g.GetRandomPositionInRect(g.i.RegionRect(g.RandomRegion()).Shrinked(100f));
            if (ValidSpawnPosition(newPos, collidersToAvoid, allDrops))
                break;
        }
        Instantiate(ammoObj, new Vector2(newPos.x, newPos.y), Quaternion.identity);
    }

    bool ValidSpawnPosition(Vector2 pos, List<Collider2D> collidersToAvoid, List<GameObject> allDrops)
    {
        if (!g.i.PlayerCanTeleportGenerally(pos))
            return false;

        //check if it overlaps a forbidden collider
        foreach (var collider in collidersToAvoid)
            if (collider.OverlapPoint(pos))
                return false;

        //check if it's too close to another drop
        foreach (var drop in allDrops)
        {
            Vector2 dropPos = drop.transform.position;
            if ((dropPos - pos).magnitude < 200)
                return false;
        }

        return true;
    }
}