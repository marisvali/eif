﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReskinRejection : MonoBehaviour
{
    public string path1 = "";
    public string path2 = "";

    Sprite[] sprites1 = null;
    Sprite[] sprites2 = null;

    private void Start()
    {
        if (path1.Contains("\\"))
        {
            string realPath1 = path1.Substring(0, path1.LastIndexOf('\\'));
            sprites1 = Resources.LoadAll<Sprite>(realPath1);
        }
        if (path2.Contains("\\"))
        {
            string realPath2 = path2.Substring(0, path2.LastIndexOf('\\'));
            sprites2 = Resources.LoadAll<Sprite>(realPath2);
        }
    }

    private void LateUpdate()
    {
        if( PlayerPrefs.GetString("Gender") == "Female" )
        {
            if(GetComponent<SpriteRenderer>().sprite != null)
            {
                string spriteName = GetComponent<SpriteRenderer>().sprite.name;
                if( !spriteName.StartsWith("Fem_") )
                {
                    if (sprites1 != null)
                    {
                        foreach (var altSprite in sprites1)
                            if (altSprite.name.Contains(spriteName))
                            {
                                GetComponent<SpriteRenderer>().sprite = altSprite;
                                break;
                            }
                    }

                    if (sprites2 != null)
                    {
                        foreach (var altSprite in sprites2)
                            if (altSprite.name.Contains(spriteName))
                            {
                                GetComponent<SpriteRenderer>().sprite = altSprite;
                                break;
                            }
                    }
                }
            }
        }
    }
}