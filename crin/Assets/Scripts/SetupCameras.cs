﻿using UnityEngine;

public class SetupCameras : MonoBehaviour
{
    private void Awake()
    {
        DoSetupCameras();
    }
    public void DoSetupCameras()
    {
        float mainCameraWidth = 1080f;
        float mainCameraHeight = 1080f;
        float verticalSpacing = 0;
        float horizontalSpacing = 0;
        float minimapCameraWidth = 840 - verticalSpacing;
        float minimapCameraHeight = 840 - horizontalSpacing;
        float menuWidth = 840 - verticalSpacing;
        float menuHeight = 240;
        float totalHeight = 1080f;
        float totalWidth = 1920f;

        float screenWidth;
        float screenHeight;
        if (g.Portrait())
        {
            screenWidth = Screen.height;
            screenHeight = Screen.width;
        }
        else
        {
            screenWidth = Screen.width;
            screenHeight = Screen.height;
        }

        float screenAspectRatio = screenWidth / screenHeight;
        float totalAspectRatio = totalWidth / totalHeight;
        float totalWidthScreen;
        float totalHeightScreen;
        if (screenAspectRatio > totalAspectRatio)
        {
            //the screen is wider than the total image we want to display
            //the total image will have the same screen height as the screen
            //totalHeightScreen = Screen.height;
            totalHeightScreen = Screen.width;
            totalWidthScreen = totalHeightScreen * totalAspectRatio;
        }
        else
        {
            //the screen is narrower than the total image we want to display
            //the total image will have the same screen width as the screen
            totalWidthScreen = screenWidth;
            totalHeightScreen = totalWidthScreen / totalAspectRatio;
        }

        float xMarginViewport = (screenWidth - totalWidthScreen) / 2 / screenWidth;
        float yMarginViewport = (screenHeight - totalHeightScreen) / 2 / screenHeight;
        float totalWidthViewport = totalWidthScreen / screenWidth;
        float totalHeightViewport = totalHeightScreen / screenHeight;
        float viewportFactorWidth = totalWidthViewport / totalWidth;
        float viewportFactorHeight = totalHeightViewport / totalHeight;

        float verticalSpacingViewport = verticalSpacing * viewportFactorWidth;
        float horizontalSpacingViewport = horizontalSpacing * viewportFactorHeight;
        float mainCameraViewportWidth = mainCameraWidth * viewportFactorWidth;
        float mainCameraViewportHeight = mainCameraHeight * viewportFactorHeight;
        float minimapCameraViewportWidth = minimapCameraWidth * viewportFactorWidth;
        float minimapCameraViewportHeight = minimapCameraHeight * viewportFactorHeight;
        float menuViewportWidth = menuWidth * viewportFactorWidth;
        float menuViewportHeight = menuHeight * viewportFactorHeight;

        bool leftHanded = PlayerPrefs.GetString("Handedness") == "Left";
        if (leftHanded)
        {
            var r = new Rect(xMarginViewport + mainCameraViewportWidth + verticalSpacingViewport, yMarginViewport + menuViewportHeight + horizontalSpacingViewport, minimapCameraViewportWidth, minimapCameraViewportHeight);
            g.i.cameraMinimap.rect = r;
            r = new Rect(xMarginViewport, yMarginViewport, mainCameraViewportWidth, mainCameraViewportHeight);
            g.i.cameraMain.rect = r;
            r = new Rect(xMarginViewport + mainCameraViewportWidth + verticalSpacingViewport, yMarginViewport, menuViewportWidth, menuViewportHeight);
            g.i.cameraMenu.rect = r;
        }
        else
        {
            var r = new Rect(xMarginViewport, yMarginViewport + menuViewportHeight + horizontalSpacingViewport, minimapCameraViewportWidth, minimapCameraViewportHeight);
            g.i.cameraMinimap.rect = r;

            r = new Rect(xMarginViewport + minimapCameraViewportWidth + verticalSpacingViewport, yMarginViewport, mainCameraViewportWidth, mainCameraViewportHeight);
            g.i.cameraMain.rect = r;

            r = new Rect(xMarginViewport, yMarginViewport, menuViewportWidth, menuViewportHeight);
            g.i.cameraMenu.rect = r;

            if (g.Portrait())
            {
                g.RotateCamera(g.i.cameraMinimap);
                g.RotateCamera(g.i.cameraMain);

                // Don't rotate, there's no reason to rotate as it doesn't show the world.
                r = g.i.cameraMenu.rect;
                g.i.cameraMenu.rect = new Rect(r.y, 1 - r.x - r.width, r.height, r.width);
            }
        }
    }
}
