﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopItems : MonoBehaviour
{
    public class ShopItem
    {
        public ShopItem(string internalName, string name, string desc, int price, bool consumable)
        {
            InternalName = internalName;
            Name = name;
            Description = desc;
            Price = price;
            Consumable = consumable;
        }
        public string InternalName;
        public string Name;
        public string Description;
        public int Price;
        public bool Consumable;
    }

    public int PiercingLightPrice = 0;
    public int ReinforcedGlass1Price = 0;
    public int ReinforcedGlass2Price = 0;
    public int ReinforcedGlass3Price = 0;
    public int ReinforcedGlass4Price = 0;
    public int ReinforcedGlass5Price = 0;
    public int Shield1Price = 0;
    public int Shield2Price = 0;
    public int Shield3Price = 0;
    public int PhaseRing1Price = 0;
    public int PhaseRing2Price = 0;
    public int PortalCharmPrice = 0;
    public int SmallTonicPrice = 0;
    public int LargeTonicPrice = 0;
    public int StopwatchPrice = 0;
    public int ExplosivesPrice = 0;
    public int RevivalScrollPrice = 0;
    
    List<ShopItem> Items = new List<ShopItem>();

    void Start ()
    {

	}
	
	void Update ()
    {
	}

    public List<ShopItem> GetItems()
    {
        if( Items.Count == 0 )
        {
            Items.Add(new ShopItem("Piercing light", "Piercing light", "Your light will pass through monsters.", PiercingLightPrice, false));
            Items.Add(new ShopItem("Reinforced glass 1", "Reinforced glass", "Your lamp will hold six flames.", ReinforcedGlass1Price, false));
            Items.Add(new ShopItem("Reinforced glass 2", "Reinforced glass", "Your lamp will hold seven flames.", ReinforcedGlass2Price, false));
            Items.Add(new ShopItem("Reinforced glass 3", "Reinforced glass", "Your lamp will hold eight flames.", ReinforcedGlass3Price, false));
            Items.Add(new ShopItem("Reinforced glass 4", "Reinforced glass", "Your lamp will hold nine flames.", ReinforcedGlass4Price, false));
            Items.Add(new ShopItem("Reinforced glass 5", "Reinforced glass", "Your lamp will hold ten flames.", ReinforcedGlass5Price, false));
            Items.Add(new ShopItem("Shield 1", "Shielded heart", "Each shield will protect you from one hit.", Shield1Price, false));
            Items.Add(new ShopItem("Shield 2", "Shielded heart", "Each shield will protect you from one hit.", Shield2Price, false));
            Items.Add(new ShopItem("Shield 3", "Shielded heart", "Each shield will protect you from one hit.", Shield3Price, false));
            if (PlayerPrefs.GetString("Difficulty") == "Hard" || PlayerPrefs.GetString("Difficulty") == "VeryHard")
            {
                Items.Add(new ShopItem("Phase ring 1", "Phase ring", "With this ring you always restart at level 6 when you die.", PhaseRing1Price, false));
                Items.Add(new ShopItem("Phase ring 2", "Phase ring", "With this ring you always restart at level 16 when you die.", PhaseRing2Price, false));
                Items.Add(new ShopItem("Portal charm", "Portal charm", "With this charm you always restart in the level where you died.", PortalCharmPrice, false));
            }
            Items.Add(new ShopItem("Small tonic", "Small tonic", "This tonic gives you back one heart.", SmallTonicPrice, true));
            Items.Add(new ShopItem("Large tonic", "Large tonic", "This tonic gives you back all your health.", LargeTonicPrice, true));
            Items.Add(new ShopItem("Stopwatch", "Stopwatch", "This will freeze all monsters currently on the map for 5 seconds.", StopwatchPrice, true));
            Items.Add(new ShopItem("Firestorm", "Firestorm", "This will do 1 damage to all the monsters currently on the map.", ExplosivesPrice, true));
            Items.Add(new ShopItem("Revival scroll", "Revival scroll", "This will revive you once.", RevivalScrollPrice, true));
        }
        return Items;
    }
}
