﻿using UnityEngine;
using UnityEngine.UI;

public class AlreadyHaveThisConsumable : MonoBehaviour
{
    public Text text;
    public void SetText(string equippedConsumable)
    {
        text.text = "You already have <b>" + equippedConsumable + "</b>.";
    }
}
