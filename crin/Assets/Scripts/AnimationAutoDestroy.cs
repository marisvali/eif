﻿using UnityEngine;

public class AnimationAutoDestroy : MonoBehaviour
{
    public float delay = 0f;

    void Start()
    {
        Destroy(gameObject, GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + delay);
    }
}