﻿using System.Collections;
using UnityEngine;

public class DepressionAI : WorldObject
{
    public float stunnedTime = 0;
    public float roamingSpeed = 0;
    public float hitTime = 0;
    public float chaseSpeed = 0;
    public float attachSpeed = 0;
    public float attachDist = 0;
    public float pauseBetweenAttachAndHit = 0;
    public float pauseBetweenHits = 0;
    public float roamingRadius = 0;
    public float minTimeBetweenRoams = 0;
    public float maxTimeBetweenRoams = 0;
    public bool zombie = false;
    public bool roamInOriginalRegion = false;
    public float appearanceTime = 0;
    public AudioClip chasing;
    public AudioClip attached;
    public AudioClip death;
    public AudioClip hit;
    public GameObject hitEffect;

    float lastStateChangeMoment = 0;
    bool stateJustChanged = false;
    bool teleported = false;
    GameObject mTarget;
    bool targetSet = false;
    bool resetTargetTimerStarted = false;
    bool fictional = false;
    bool wasJustHit = false;
    bool freeze = false;
    float timeSinceFreeze = 0;
    Animator Anim;

    enum State
    {
        Appearing,
        Roaming,
        Chasing,
        Dead,
        Attached,
        Waiting,
        Hit,
        Frozen
    }
    State currentState;
	void Start()
    {
        Anim = GetComponent<Animator>();
        if (PlayerPrefs.GetString("Difficulty") == "Casual")
        {
            roamingSpeed = roamingSpeed * 0.5f;
            chaseSpeed = chaseSpeed * 0.5f;
        }
        resetTargetTimerStarted = false;
        wasJustHit = false;
        freeze = false;
        zombie = false;
        mTarget = g.CreatePathTarget();
        GetComponent<MobHealth>().SetHealth(GetComponent<MobHealth>().startHealth);
        GetComponent<MobExpiration>().ResetExpiration();
    }
    
    void OnDestroy()
    {
        Destroy(mTarget);
    }
    
    void Appear()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            Anim.SetInteger("AnimationState", 6);
        }

        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }

        float time = Time.time - lastStateChangeMoment;
        if (time > appearanceTime)
        {
            currentState = State.Roaming;
            return;
        }
    }
    bool InSameRegion(Mob mob, Player player)
    {
        Rigidbody2D playerBody = player.GetComponent<Rigidbody2D>();
        Vector2 playerRegion = g.i.Region(playerBody.position);
        return g.i.MobPartiallyInRegion(mob, playerRegion);
    }
    void Roam()
    {
        if( stateJustChanged )
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            float actualSpeed = roamingSpeed;
            if (PlayerPrefs.GetInt("Slower depression") != 0)
                actualSpeed -= actualSpeed * g.i.boosts.SlowerDepressionPercent / 100f;
            GetComponent<Pathing>().speed = actualSpeed;
            targetSet = false;
            Anim.SetInteger("AnimationState", 0);
        }

        if (!g.i.player.GetComponent<PlayerHealth>().alive)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            targetSet = false;
            Anim.SetInteger("AnimationState", 0);
            return; //just idle with the roaming state animation
        }

        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            StopAllCoroutines();
            return;
        }

        if( wasJustHit )
        {
            currentState = State.Hit;
            StopAllCoroutines();
            return;
        }
        
        if (InSameRegion(GetComponent<Mob>(), g.i.player))
        {
            currentState = State.Chasing;
            StopAllCoroutines();
            return;
        }

        if( freeze )
        {
            currentState = State.Frozen;
            StopAllCoroutines();
            if( targetSet)
            return;
        }

        if (!targetSet)
        {
            if (roamInOriginalRegion)
            {
                Vector2 region = g.i.Region(transform.position);
                mTarget.transform.position = g.i.GetFreeRandomPositionInRegion(region, gameObject.transform.position);
            }
            else
            {
                mTarget.transform.position = g.i.GetFreeRandomPosition(gameObject.transform.position, roamingRadius);
            }
            Anim.SetInteger("AnimationState", 1);
            GetComponent<Pathing>().SetTarget(mTarget);
            targetSet = true;
            resetTargetTimerStarted = false;
            return;
        }

        if (!resetTargetTimerStarted)
        {
            if (GetComponent<Pathing>().AtTarget())
            {
                Anim.SetInteger("AnimationState", 0);
                StartCoroutine(ResetTarget());
                resetTargetTimerStarted = true;
            }
        }
    }
    
    IEnumerator ResetTarget()
    {
        float timeToWait = minTimeBetweenRoams + (maxTimeBetweenRoams - minTimeBetweenRoams) * UnityEngine.Random.value;
        yield return new WaitForSeconds(timeToWait);
        targetSet = false;
        yield return null;
    }

    bool IsTouchingPlayer()
    {
        return (transform.position - g.i.player.transform.position).magnitude < attachDist;
    }

    void Chase()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(g.i.player.gameObject);
            float actualSpeed = chaseSpeed;
            if (PlayerPrefs.GetInt("Slower depression") != 0)
                actualSpeed -= actualSpeed * g.i.boosts.SlowerDepressionPercent / 100f;
            GetComponent<Pathing>().speed = actualSpeed;
            Anim.SetInteger("AnimationState", 2);
            g.i.audioManager.PlayClip(chasing, transform.position);
        }

        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }

        if (wasJustHit)
        {
            currentState = State.Hit;
            return;
        }

        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }

        if (IsTouchingPlayer() && !zombie && g.i.hiveMindDepression.CanAttach(this))
        {
            g.i.hiveMindDepression.SetAttached(this);
            currentState = State.Attached;
            return;
        }

        if (InSameRegion(GetComponent<Mob>(), g.i.player))
        {
            if (!g.i.hiveMindDepression.CanAttach(this))
            {
                currentState = State.Waiting;
                return;
            }
        }
        else
        {
            currentState = State.Roaming;
            return;
        }
    }
    void Dead()
    {
        if( stateJustChanged )
        {
            StopAllCoroutines();
            GetComponent<Pathing>().zombie = true;
            if (fictional)
            {
                Anim.SetInteger("AnimationState", 7);
                Record.Event("mob dead", "depression", "fictional");
            }
            else
            {
                Anim.SetInteger("AnimationState", 3);
                if (g.i.spawnDrops)
                    g.i.spawnDrops.RegisterDeath(GetComponent<Mob>(), g.i.spawnDrops.DepressionHealthDropRate, g.i.spawnDrops.DepressionResourceDropRate, g.i.spawnDrops.DepressionResourceDropAmount, GetComponent<MobHealth>().maxHealth, g.i.spawnDrops.DepressionBoostDropRate, g.i.spawnDrops.DepressionConsumableDropRate);
                g.i.audioManager.PlayClip(death, transform.position);
                Record.Event("mob dead", "depression");
            }
            GetComponent<MobExpiration>().StartExpiration();
        }
        float timeSpentStunned = Time.time - lastStateChangeMoment;
        if (timeSpentStunned > stunnedTime && fictional)
            Destroy(gameObject);
    }

    void Attached()
    {
        if( stateJustChanged )
        {
            if (PlayerPrefs.GetInt("Weaker depression") != 0)
            {
                if ((UnityEngine.Random.value * 100f) <= g.i.boosts.WeakerDepressionPercent)
                {
                    g.i.hiveMindDepression.RemoveAttached(this);
                    GetComponent<MobHealth>().SetHealth(0);
                    OnHit();
                    currentState = State.Dead;
                    return;
                }
            }
            
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            GetComponent<Pathing>().speed = 0;
            StartCoroutine(AttachHitPlayer());
            teleported = false;
            Record.Event("mob attack", "depression", "attached");
            Anim.SetInteger("AnimationState", 5);
            MoveToPlayer();
            GetComponent<SortingOrderUpdater>().alwaysInFront = g.i.player.mainSprite;
            GetComponent<Mob>().icon.GetComponent<SortingOrderUpdater>().alwaysInFront = g.i.player.icon;
            g.i.audioManager.PlayClip(attached, transform.position);
        }
        if (teleported)
        {
            currentState = State.Roaming;
            StopAllCoroutines();
            GetComponent<SortingOrderUpdater>().alwaysInFront = null;
            GetComponent<Mob>().icon.GetComponent<SortingOrderUpdater>().alwaysInFront = null;
            return;
        }
        if (freeze)
        {
            StopAllCoroutines();
            currentState = State.Frozen;
            GetComponent<SortingOrderUpdater>().alwaysInFront = null;
            GetComponent<Mob>().icon.GetComponent<SortingOrderUpdater>().alwaysInFront = null;
            return;
        }
    }
    void Wait()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            GetComponent<Pathing>().speed = 0;
            Anim.SetInteger("AnimationState", 0);
        }

        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            StopAllCoroutines();
            return;
        }

        if (wasJustHit)
        {
            StopAllCoroutines();
            currentState = State.Hit;
            return;
        }

        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }

        if (InSameRegion(GetComponent<Mob>(), g.i.player))
        {
            if (g.i.hiveMindDepression.CanAttach(this))
            {
                currentState = State.Chasing;
                return;
            }
        }
        else
        {
            currentState = State.Roaming;
            return;
        }
        
        if( g.i.player.transform.position.x < transform.position.x )
            transform.localEulerAngles = new Vector3(0, 180, 0);
        else
            transform.localEulerAngles = new Vector3(0, 0, 0);
    }
    
    protected override void UpdateImpl()
    {
        State oldState = currentState;
        
        switch ( currentState )
        {
            case State.Appearing:
                Appear();
                return;
            case State.Roaming:
                Roam();
                break;
            case State.Chasing:
                Chase();
                break;
            case State.Dead:
                Dead();
                break;
            case State.Attached:
                Attached();
                break;
            case State.Waiting:
                Wait();
                break;
            case State.Hit:
                Hit();
                break;
            case State.Frozen:
                Frozen();
                break;
        }
        State newState = currentState;
        stateJustChanged = oldState != newState;
        if( stateJustChanged )
            lastStateChangeMoment = Time.time;
    }
    
    public void OnTeleport()
    {
        teleported = true;
        g.i.hiveMindDepression.RemoveAttached(this);
        if (currentState == State.Chasing)
            GetComponent<Pathing>().Repath();
    }
    public void OnHit()
    {
        g.Splash(transform.position);
        if (GetComponent<MobHealth>().alive)
        {
            GameObject outline = g.Clone(hitEffect, transform.position);
            outline.transform.localEulerAngles = transform.localEulerAngles;
        }
        wasJustHit = true;
    }
    private void MoveToPlayer()
    {
        Vector3 pos = gameObject.transform.position;
        pos.x = g.i.player.transform.position.x;
        pos.y = g.i.player.transform.position.y;
        if (transform.localEulerAngles.y == 0 )
            pos.x += 13;
        else
            pos.x -= 13;
        gameObject.transform.position = pos;
    }
    
    IEnumerator HitPlayer()
    {
        if( fictional )
            g.i.player.GetComponent<Player>().DepressionAttack(0);
        else
        {
            Record.Event("mob attack", "depression");
            g.i.player.GetComponent<Player>().DepressionAttack(1);
        }
        yield return new WaitForSeconds(pauseBetweenHits);
        yield return HitPlayer();
    }
    IEnumerator AttachHitPlayer()
    {
        yield return new WaitForSeconds(pauseBetweenAttachAndHit);
        yield return HitPlayer();
    }

    public bool IsFictional()
    {
        return fictional;
    }

    public void MakeFictional()
    {
        fictional = true;
        GetComponent<Mob>().icon.enabled = false;
        
        currentState = State.Appearing;
        stateJustChanged = true;
    }
    public void MakeReal()
    {
        fictional = false;
        GetComponent<Mob>().icon.enabled = true;
           
        currentState = State.Roaming;
        stateJustChanged = true;
    }

    void Hit()
    {
        if (stateJustChanged)
        {
            wasJustHit = false;
            GetComponent<Pathing>().zombie = true;
            Anim.SetInteger("AnimationState", 4);
            g.i.audioManager.PlayClip(hit, transform.position);
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }
        if (wasJustHit)
        {
            lastStateChangeMoment = Time.time;
            wasJustHit = false;
            return;
        }
        float timeSpentHit = Time.time - lastStateChangeMoment;
        if (timeSpentHit > hitTime)
        {
            if (freeze)
                currentState = State.Frozen;
            else
                currentState = State.Roaming;
            return;
        }
    }

    public void Freeze()
    {
        freeze = true;
        timeSinceFreeze = Time.time;
    }

    void Frozen()
    {
        if (stateJustChanged)
        {
            Anim.speed = 0;
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
        }

        if (!GetComponent<MobHealth>().alive)
        {
            freeze = false;
            Anim.speed = 1;
            currentState = State.Dead;
            return;
        }

        if (wasJustHit)
        {
            Anim.speed = 1;
            currentState = State.Hit;
            return;
        }

        float time = Time.time - timeSinceFreeze;
        if (time > 5)
        {
            freeze = false;
            Anim.speed = 1;
            currentState = State.Roaming;
            return;
        }
    }
}
