﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems; //required when using Event data

public class InstantToggle : MonoBehaviour, IPointerDownHandler //required interface when using the OnPointerDown method
{
    public void OnPointerDown(PointerEventData eventData)
    {
        GetComponent<Toggle>().isOn = true;
        GetComponentInParent<ToggleGroup>().allowSwitchOff = false;
    }
}
