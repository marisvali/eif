﻿using UnityEngine;

public class FreeShot : MonoBehaviour
{
    public AudioClip freeShotAudio;

    public void Initialize()
    {
        GetComponent<AudioSource>().clip = freeShotAudio;
        GetComponent<AudioSource>().Play();
    }
}
