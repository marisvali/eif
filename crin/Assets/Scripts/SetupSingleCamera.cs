﻿using UnityEngine;

public class SetupSingleCamera : MonoBehaviour
{
    public float totalHeight = 1080f;
    public float totalWidth = 1920f;
    private void OnEnable()
    {
        float screenWidth;
        float screenHeight;
        if (g.Portrait())
        {
            screenWidth = Screen.height;
            screenHeight = Screen.width;
        }
        else
        {
            screenWidth = Screen.width;
            screenHeight = Screen.height;
        }

        float screenAspectRatio = screenWidth / screenHeight;
        float totalAspectRatio = totalWidth / totalHeight;
        float totalWidthScreen = 0f;
        float totalHeightScreen = 0f;
        if (screenAspectRatio > totalAspectRatio)
        {
            //the screen is wider than the total image we want to display
            //the total image will have the same screen height as the screen
            totalHeightScreen = Screen.height;
            totalWidthScreen = totalHeightScreen * totalAspectRatio;
        }
        else
        {
            //the screen is narrower than the total image we want to display
            //the total image will have the same screen width as the screen
            totalWidthScreen = screenWidth;
            totalHeightScreen = totalWidthScreen / totalAspectRatio;
        }

        float xMarginViewport = (screenWidth - totalWidthScreen) / 2 / screenWidth;
        float yMarginViewport = (screenHeight - totalHeightScreen) / 2 / screenHeight;
        float totalWidthViewport = totalWidthScreen / screenWidth;
        float totalHeightViewport = totalHeightScreen / screenHeight;

        var r = new Rect(xMarginViewport, yMarginViewport, totalWidthViewport, totalHeightViewport);
        GetComponent<Camera>().rect = r;
    }
}
