using UnityEngine;

public class Rotator : MonoBehaviour {

	public void OnEnable()
    {
        if (g.Portrait())
        {
            GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0, -90);
        }
    }
}