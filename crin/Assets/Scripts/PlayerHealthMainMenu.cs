﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthMainMenu : MonoBehaviour
{
    public Transform fullHeartsParent;
    public Transform halfHeartsParent;
    public Transform emptyHeartsParent;
    public Transform shieldedHeartsParent;
    
    List<GameObject> fullHearts = new List<GameObject>();
    List<GameObject> halfHearts = new List<GameObject>();
    List<GameObject> emptyHearts = new List<GameObject>();
    List<GameObject> shieldedHealth = new List<GameObject>();
    int previousHealth = -1;
    List<int> previousShields = new List<int>();
    void Start()
    {
        foreach ( Transform child in fullHeartsParent)
            fullHearts.Add(child.gameObject);

        foreach (Transform child in halfHeartsParent)
            halfHearts.Add(child.gameObject);

        foreach (Transform child in emptyHeartsParent)
            emptyHearts.Add(child.gameObject);

        foreach (Transform child in shieldedHeartsParent)
            shieldedHealth.Add(child.gameObject);

        previousShields.Add(PlayerPrefs.GetInt("Shield 1", 0));
        previousShields.Add(PlayerPrefs.GetInt("Shield 2", 0));
        previousShields.Add(PlayerPrefs.GetInt("Shield 3", 0));
    }
    private void Update()
    {
        int health = PlayerPrefs.GetInt("PlayerHealth");
        
        List<int> newShields = new List<int>();
        newShields.Add(PlayerPrefs.GetInt("Shield 1", 0));
        newShields.Add(PlayerPrefs.GetInt("Shield 2", 0));
        newShields.Add(PlayerPrefs.GetInt("Shield 3", 0));

        bool shieldsDifferent = false;
        for (int idx = 0; idx <= 2; ++idx)
            if (previousShields[idx] != newShields[idx])
                shieldsDifferent = true;

        if (previousHealth == health && !shieldsDifferent)
            return;
        previousHealth = health;
        previousShields = newShields;

        int totalHearts = 3;
        int fullHearts = health / 2;
        int halfHearts = health % 2;
        int emptyHearts = totalHearts - fullHearts - halfHearts;

        for (int idx = 0; idx < totalHearts; ++idx)
            if (idx <= fullHearts)
                this.fullHearts[idx].SetActive(true);
            else
                this.fullHearts[idx].SetActive(false);

        for (int idx = 0; idx < totalHearts; ++idx)
            if (idx >= fullHearts && (idx < fullHearts + halfHearts))
                this.halfHearts[idx].SetActive(true);
            else
                this.halfHearts[idx].SetActive(false);

        for (int idx = 0; idx < totalHearts; ++idx)
            if ((idx >= fullHearts + halfHearts) && (idx < fullHearts + halfHearts + emptyHearts))
                this.emptyHearts[idx].SetActive(true);
            else
                this.emptyHearts[idx].SetActive(false);


        if (PlayerPrefs.GetInt("Shield 3", 0) == 1)
            shieldedHealth[0].SetActive(true);
        else
            shieldedHealth[0].SetActive(false);

        if (PlayerPrefs.GetInt("Shield 2", 0) == 1)
            shieldedHealth[1].SetActive(true);
        else
            shieldedHealth[1].SetActive(false);

        if (PlayerPrefs.GetInt("Shield 1", 0) == 1)
            shieldedHealth[2].SetActive(true);
        else
            shieldedHealth[2].SetActive(false);
    }
}