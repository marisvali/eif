﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainScript : MonoBehaviour
{
    public bool winByCollectingShards = false;
    public AudioClip showMessageClip;
    public GameObject confirmRevivalScroll;
    public GameObject continueButton;
    public Canvas mFadeOutCanvas;
    public GameObject tapMessage;

    bool mStopped = false;
    GameObject mMessage;
    IEnumerator mShowMessageCoroutine;
    bool mEndSequenceEngaged = false;
    List<GameObject> mFloatingTexts = new List<GameObject>();

    enum eWinPhase
    {
        NoWin,
        WaitForPlayerDisappearance
    }
    private eWinPhase WinPhase = eWinPhase.NoWin;

    enum eLossPhase
    {
        NoLoss,
        StartedFalling,
        DoneFalling,
        WaitingPlayerInput,
        EndLevel,
        Reviving
    }
    eLossPhase LossPhase = eLossPhase.NoLoss;

    public void Start()
    {
        g.i.player.maxAmmo = 5;
        if (PlayerPrefs.GetInt("Bought Reinforced glass 1") == 1)
            g.i.player.maxAmmo = 6;
        if (PlayerPrefs.GetInt("Bought Reinforced glass 2") == 1)
            g.i.player.maxAmmo = 7;
        if (PlayerPrefs.GetInt("Bought Reinforced glass 3") == 1)
            g.i.player.maxAmmo = 8;
        if (PlayerPrefs.GetInt("Bought Reinforced glass 4") == 1)
            g.i.player.maxAmmo = 9;
        if (PlayerPrefs.GetInt("Bought Reinforced glass 5") == 1)
            g.i.player.maxAmmo = 10;

        g.i.player.SetAmmo(PlayerPrefs.GetInt("PlayerAmmo"));
        g.i.playerHealth.Set(PlayerPrefs.GetInt("PlayerHealth"));

        if (PlayerPrefs.GetInt("Bought Piercing light") == 1)
            g.i.player.beamHitsMultiple = true;
        else
            g.i.player.beamHitsMultiple = false;

        PlayerPrefs.SetInt("Hits in the last level", 0);

        //check if the level was interrupted previously
        if (PlayerPrefs.GetInt("Load last state of the level") == 1)
        {
            PlayerPrefs.SetInt("Load last state of the level", 0);
            GetComponent<CampaignState>().LoadCampaignState();
        }

        if (GetComponent<CampaignState>() != null)
            PlayerPrefs.SetInt("Campaign level was active", 1);

        g.i.Pause();
    }

    bool thing = true;
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    if (Paused)
        //    {
        //        if (!GameLost())
        //        {
        //            Unpause();
        //            //menu.SetActive(false);
        //        }
        //    }
        //    else
        //    {
        //        Pause();
        //        PauseMusic();
        //        //menu.SetActive(true);
        //    }
        //}

        if (Input.GetKeyDown(KeyCode.F))
        {
            foreach (var mob in g.i.mobs)
                if (mob.GetComponent<MobHealth>() != null &&
                    mob.GetComponent<RejectionAuxAI>() == null &&
                    mob.GetComponent<RejectionAI>() == null)
                    mob.GetComponent<MobHealth>().SetHealth(0);
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            g.i.cameraMain.GetComponent<CameraShaker>().StartShake();
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            g.i.player.GetComponent<PlayerHealth>().Set(0);
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            g.i.player.ShowHealedAnimation();
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            g.i.player.SetAmmo(0);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (g.i.Paused())
                g.i.Unpause();
            else
                g.i.Pause();
        }

        if (g.i.Paused())
            return; //don't do anything while paused

        if (LossPhase == eLossPhase.DoneFalling)
        {
            LossPhase = eLossPhase.EndLevel;
            if (PlayerPrefs.GetString("Equipped consumable") == "Revival scroll")
            {
                LossPhase = eLossPhase.WaitingPlayerInput;
                g.i.Pause();
                musicLevel.Pause();
                confirmRevivalScroll.SetActive(true);
            }
            else
            {
                ReloadCurrentLevel();
            }
        }

        if (LossPhase != eLossPhase.NoLoss)
            return;

        if (mStopped)
            return; //don't do anything while stopped

        if (mEndSequenceEngaged)
            return; //don't allow any actions while the end sequence is running

        if (WinPhase == eWinPhase.WaitForPlayerDisappearance)
        {
            if (g.i.player.Disappeared())
            {
                LoadNextLevel();
            }
            return;
        }

        if (g.i.player.justHitExcessAmmo)
        {
            g.i.player.justHitExcessAmmo = false;
            ShowMessage(g.i.player.transform.position, "Lamp full!");
        }

        if (g.i.player.justHitExcessHealth)
        {
            g.i.player.justHitExcessHealth = false;
            ShowMessage(g.i.player.transform.position, "Health full!");
        }

        if (thing)
        {
            Debug.Log("game active");
            thing = false;
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject() &&
                EventSystem.current.currentSelectedGameObject == null)
                HandleMainCameraTap(Input.mousePosition);
            HandleMinimapTap(Input.mousePosition);
        }

        if (GameWon() &&
            WinPhase == eWinPhase.NoWin)
        {
            //kill any leftover monsters if necessary
            if (winByCollectingShards)
                foreach (var mob in g.i.mobs)
                    mob.GetComponent<MobHealth>().SetHealth(0);

            Record.Event("end level", "win");
            WinPhase = eWinPhase.WaitForPlayerDisappearance;
            g.i.player.StartDisappear();
        }

        if (GameLost() && LossPhase == eLossPhase.NoLoss)
        {
            Record.Event("end level", "player dead");
            LossPhase = eLossPhase.StartedFalling;
            StartCoroutine(FallOnDeathSequence());
        }
    }

    private void ReloadCurrentLevel()
    {
        StartCoroutine(FadeOutAndLoadPrimaryScene());
    }
    private void LoadNextLevel()
    {
        int level = PlayerPrefs.GetInt("Level") + 1;
        if (level < 1 || level > 29)
            level = 1;
        PlayerPrefs.SetInt("Level", level);

        StartCoroutine(FadeOutAndLoadPrimaryScene());
    }

    public void Revive()
    {
        StartCoroutine(ReviveSequence());
    }

    private IEnumerator ReviveSequence()
    {
        LossPhase = eLossPhase.NoLoss;
        mStopped = false;
        g.i.Unpause();
        g.i.player.Revive();
        //LossPhase = eLossPhase.Reviving;
        //yield return new WaitForSeconds(1.5f);
        yield return null;
    }

    public void Restart()
    {
        if (GetComponent<CampaignState>() != null)
            PlayerPrefs.SetInt("Campaign level was active", 0);
        g.i.Unpause();
        PlayerPrefs.SetString("Level", "Restart");
        SceneManager.LoadScene("SceneSelection");
    }

    Mob GetClosestMob(List<Mob> mobs, Vector3 pt)
    {
        bool first = true;
        float minDist = 0;
        pt.z = 0;
        Mob result = null;
        foreach (var mob in mobs)
        {
            Vector3 pt2 = mob.clickArea.bounds.center;
            pt2.z = 0;
            float dist = (pt2 - pt).sqrMagnitude;
            if (first || dist < minDist)
            {
                first = false;
                minDist = dist;
                result = mob;
            }
        }
        return result;
    }

    Mob MobIsHit(Vector3 scenemp)
    {
        Vector2 playerRegion = g.i.Region(g.i.player.transform.position);

        var fictionalTargets = new List<Mob>();
        var realTargets = new List<Mob>();
        foreach (var mob in g.i.mobs)
        {
            if (!mob.clickArea.OverlapPoint(scenemp))
                continue;
            if (mob.GetComponent<MobExpiration>().Expired())
                continue;
            bool sameRegion = g.i.MobPartiallyInRegion(mob, playerRegion);
            if (!sameRegion)
                continue;

            bool fictional = false;
            if (mob.tag == "Depression" && mob.GetComponent<DepressionAI>().IsFictional())
                fictional = true;
            if (mob.tag == "SocialAnxiety" && mob.GetComponent<SocialAnxietyAI>().IsFictional())
                fictional = true;
            if (mob.tag == "Anger" && mob.GetComponent<AngerAI>().IsFictional())
                fictional = true;
            if (fictional)
            {
                if (mob.GetComponent<FictionalMobTargetable>().IsTargetable())
                    fictionalTargets.Add(mob);
            }
            else
                realTargets.Add(mob);
        }

        if (realTargets.Count > 0)
            return GetClosestMob(realTargets, scenemp);
        else
            return GetClosestMob(fictionalTargets, scenemp);
    }
    List<Vector2> GetAdjacentPositions(Vector2 center)
    {
        List<Vector2> dirs = new List<Vector2>();
        dirs.Add(new Vector2(-1, 0));
        dirs.Add(new Vector2(0, -1));
        dirs.Add(new Vector2(1, 0));
        dirs.Add(new Vector2(0, 1));
        dirs.Add(new Vector2(-1, -1));
        dirs.Add(new Vector2(1, -1));
        dirs.Add(new Vector2(1, 1));
        dirs.Add(new Vector2(-1, 1));
        foreach (var dir in dirs)
            dir.Normalize();
        float dist = g.i.gridGraph.nodeSize / 2;
        int nrRings = 8;
        List<Vector2> positions = new List<Vector2>();
        for (int idxRing = 1; idxRing <= nrRings; ++idxRing)
            foreach (var dir in dirs)
                positions.Add(center + dir * dist * idxRing);
        return positions;
    }
    void HandlePlayerTap(Player player, Camera camera, Vector3 scenemp)
    {
        Vector2 destination = new Vector2(scenemp.x, scenemp.y);

        //check for target
        var target = MobIsHit(scenemp);

        bool playerInView = false;
        if (target)
        {
            //found target, check if the player is visible
            Vector2 pos = camera.WorldToViewportPoint(player.GetComponent<Rigidbody2D>().position);
            playerInView = new Rect(0, 0, 1, 1).Contains(pos);
        }
        if (target && playerInView)
        {
            //check if the target is still alive
            if (target.GetComponent<MobHealth>().alive)
            {
                //we can attack
                if (player.GetAmmo() > 0)
                {
                    //check if we are attacking Rejection
                    if (target.GetComponent<RejectionAI>() != null)
                    {
                        //check if rejection is still protected
                        if (target.GetComponent<RejectionAI>().IsProtected())
                            ShowMessage(new Vector3(2880, 455, 0), "Protected");
                        else
                        {
                            //trigger the end sequence
                            target.GetComponent<RejectionAI>().TriggerEndSequence();
                            mEndSequenceEngaged = true;

                            //destroy any mobs still remaining on the map
                            foreach (var mob in g.i.mobs)
                                if (mob.GetComponent<RejectionAI>() == null)
                                    Destroy(mob);

                            //make the player invulnerable (there could still be incoming attacks such as Rejection wave or Social anxiety projectiles)
                            g.i.player.MakeInvulnerable(100000);
                        }
                    }
                    else
                    {
                        player.Attack(target, false);
                        g.i.cameraMain.GetComponent<CameraShaker>().StartShake();
                    }
                }
                else
                    ShowMessage(scenemp, "Lamp empty!");

                Record.Event("player attack");
            }
            else
                Record.Event("player attack", "dead mob");
        }
        else
        {
            //no target, so just teleport
            //check if we can teleport to this location
            bool canTeleport = g.i.PlayerCanTeleportInMainView(destination);
            if (!canTeleport)
            {
                //if we can't teleport, try an adjacent position
                var positions = GetAdjacentPositions(destination);
                foreach (var pos in positions)
                    if (g.i.PlayerCanTeleportInMainView(pos))
                    {
                        destination = pos;
                        canTeleport = true;
                        break;
                    }
            }

            if (canTeleport)
            {
                Energy energy = g.i.player.GetComponent<Energy>();
                player.Teleport(destination);
                Record.Event("teleport", "success");
            }
            else
            {
                ShowMessage(scenemp, "Obstacle");
                Record.Event("teleport", "failed");
            }
        }
    }

    void ShowMessage(Vector3 worldPos, string messageText)
    {
        GetComponent<AudioManager>().PlayPositionIndependentClip(showMessageClip);
        if (mShowMessageCoroutine != null)
            StopCoroutine(mShowMessageCoroutine);
        mShowMessageCoroutine = ShowMessageCoroutine(worldPos, messageText);
        StartCoroutine(mShowMessageCoroutine);
    }

    IEnumerator ShowMessageCoroutine(Vector3 worldPos, string messageText)
    {
        if (mMessage != null)
            Destroy(mMessage);

        mMessage = g.Clone(tapMessage, g.i.cameraMainCanvas.transform);
        mMessage.GetComponentInChildren<Text>().text = messageText;
        // The x and y coordinates of the vp do not take rotation into account. If the Y is higher
        // it means the point is physically nearer the top of the screen.
        Vector3 vp = g.i.cameraMain.WorldToViewportPoint(worldPos);
        var canvasRect = g.i.cameraMainCanvas.GetComponent<RectTransform>().rect;
        Vector2 pos;
        Vector2 msgSize = mMessage.GetComponent<RectTransform>().sizeDelta;
        if (g.Portrait())
        {
            pos = new Vector2(vp.x * canvasRect.width + 150, vp.y * canvasRect.height);
            // Compensate for the rotation.
            msgSize = new Vector2(msgSize.y, msgSize.x);
        }
        else
        {
            pos = new Vector2(vp.x * canvasRect.width, vp.y * canvasRect.height + 150);
        }

        Vector2 min = new Vector2(msgSize.x / 2, msgSize.y / 2);
        Vector2 max = new Vector2(canvasRect.width - msgSize.x / 2, canvasRect.height - msgSize.y / 2);
        if (pos.x < min.x)
            pos.x = min.x;
        if (pos.y < min.y)
            pos.y = min.y;
        if (pos.x > max.x)
            pos.x = max.x;
        if (pos.y > max.y)
            pos.y = max.y;

        mMessage.GetComponent<RectTransform>().anchoredPosition = pos;
        yield return new WaitForSeconds(0.8f);
        Destroy(mMessage);
        yield return null;
    }
    void HandleMainCameraTap(Vector2 screenPoint)
    {
        Vector3 worldPoint = g.i.cameraMain.ScreenToWorldPoint(screenPoint);
        if (!g.i.WorldRectOfMainCamera().Contains(worldPoint))
            return;

        Record.Event("tap", "main camera");

        HandlePlayerTap(g.i.player, g.i.cameraMain, worldPoint);
    }
    void HandleMinimapTap(Vector2 screenPoint)
    {
        Vector3 worldPoint = g.i.cameraMinimap.ScreenToWorldPoint(screenPoint);
        if (!g.worldRect.Contains(worldPoint))
            return;

        Record.Event("tap", "minimap");

        bool changed = g.i.cameraMinimap.GetComponent<MinimapBehavior>().AdjustCamera(worldPoint);
        if (changed)
        {
            DestroyFloatingTexts();
            g.i.player.DestroyFreeShots();
        }
    }
    public void ShowFloatingText(GameObject prefab, int amount, Vector3 worldPos, Bounds bounds)
    {
        GameObject floatingText = g.Clone(prefab, g.i.cameraMainCanvas.transform);
        floatingText.GetComponent<FloatingText>().Initialize(amount);
        worldPos.y = bounds.max.y + bounds.size.y * 0.3f;
        Vector3 vpPos = g.i.cameraMain.WorldToViewportPoint(worldPos);
        var canvasRect = g.i.cameraMainCanvas.GetComponent<RectTransform>().rect;
        Vector2 canvasPos = new Vector2(vpPos.x * canvasRect.width, vpPos.y * canvasRect.height);
        floatingText.GetComponent<RectTransform>().anchoredPosition = canvasPos;
        mFloatingTexts.Add(floatingText);
    }
    public void ShowFloatingText(GameObject prefab, string message, Vector3 worldPos, Bounds bounds)
    {
        GameObject floatingText = g.Clone(prefab, g.i.cameraMainCanvas.transform);
        floatingText.GetComponent<FloatingText>().Initialize(message);
        worldPos.y = bounds.max.y + bounds.size.y * 0.3f;
        Vector3 vpPos = g.i.cameraMain.WorldToViewportPoint(worldPos);
        var canvasRect = g.i.cameraMainCanvas.GetComponent<RectTransform>().rect;
        Vector2 canvasPos = new Vector2(vpPos.x * canvasRect.width, vpPos.y * canvasRect.height);
        floatingText.GetComponent<RectTransform>().anchoredPosition = canvasPos;
        mFloatingTexts.Add(floatingText);
    }
    public void DestroyFloatingTexts()
    {
        foreach (var floatingText in mFloatingTexts)
            Destroy(floatingText);
        mFloatingTexts.Clear();
    }
    bool GameLost()
    {
        return !g.i.player.GetComponent<PlayerHealth>().alive || Input.GetKeyDown(KeyCode.A);
    }
    bool GameWon()
    {
        if (winByCollectingShards)
            return GameWonShardsCollected();
        else
            return GameWonMonstersDead();
    }

    bool GameWonShardsCollected()
    {
        return false;
    }

    bool GameWonMonstersDead()
    {
        bool allDead = true;
        foreach (var mob in g.i.mobs)
            if (mob.GetComponent<MobHealth>().alive)
                allDead = false;
        return allDead;
    }

    public void SkipTutorial()
    {
        if (GetComponent<CampaignState>() != null)
            PlayerPrefs.SetInt("Campaign level was active", 0);
        g.i.Unpause();
        PlayerPrefs.SetString("Level", "Skip tutorial");
        SceneManager.LoadScene("SceneSelection");
    }

    public void ResumeFromIntro()
    {
        g.i.Unpause();
        mStopped = false;
    }

    public void AdvanceIntro()
    {
    }

    public void ShowContinueButton()
    {
        continueButton.SetActive(true);
    }

    IEnumerator FallOnDeathSequence()
    {
        mStopped = true;
        if (g.i.minimap.AdjustCamera(g.i.player.transform.position))
            yield return new WaitForSeconds(2.0f); //change the view so the player can see his wretchedness

        g.i.player.Die();
        yield return new WaitForSeconds(2.0f);

        LossPhase = eLossPhase.DoneFalling;
    }

    IEnumerator FadeOutAndLoadPrimaryScene()
    {
        g.i.Pause();
        mFadeOutCanvas.enabled = true;
        Color col = mFadeOutCanvas.GetComponentInChildren<Image>().color;
        float totalAlphaDif = 1.0f;
        float alphaDifPerSecond = totalAlphaDif / 0.3f;
        while (col.a < 1.0f)
        {
            col.a += alphaDifPerSecond * Time.unscaledDeltaTime;
            mFadeOutCanvas.GetComponentInChildren<Image>().color = col;
            yield return null;
        }
        SceneManager.LoadScene("PrimaryScene");

    }

    public bool IsDeathSequenceEngaged()
    {
        return LossPhase > eLossPhase.NoLoss;
    }
}