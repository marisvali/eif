﻿using System.Collections.Generic;
using UnityEngine;

public class ParanoiaAI : WorldObject
{
    public float timeBeforeFirstWhisper = 0;
    public float whisperDuration = 0;
    public float timeBetweenWhispers = 0;
    public float hitTime = 0;
    public int nrDepressions = 0;
    public int nrSocialAnxieties = 0;
    public int nrAngers = 0;
    public AudioClip hit;
    public AudioClip attack;
    public AudioClip death;
    public GameObject angerRef;
    public GameObject depressionRef;
    public GameObject socialAnxietyRef;
    public GameObject paranoiaHitEffect;

    float lastStateChangeMoment = 0;
    bool stateJustChanged = false;
    bool hadFirstWhisper = false;
    List<GameObject> spawnedObjects = new List<GameObject>();
    bool freeze = false;
    float timeSinceFreeze = 0;
    bool wasWhisperingWhenFrozen = false;
    bool returningFromFreeze = false;
    Animator Anim;
    bool wasJustHit = false;
    bool wasWhisperingWhenHit = false;
    bool returningFromHit = false;
    float whisperStartMoment = 0;
    float waitStartMoment = 0;

    enum State
    {
        Waiting,
        Whispering,
        Dead,
        Frozen,
        Hit
    }
    State currentState;
    void Start()
    {
        Anim = GetComponent<Animator>();
        wasJustHit = false;
        freeze = false;
        stateJustChanged = true;
        GetComponent<MobHealth>().SetHealth(GetComponent<MobHealth>().startHealth);
        GetComponent<MobExpiration>().ResetExpiration();
        currentState = State.Waiting;
    }
    void Wait()
    {
        if (stateJustChanged)
        {
            Anim.SetInteger("AnimationState", 0);
        }
        if (!g.i.player.GetComponent<PlayerHealth>().alive)
        {
            return; //just idle in the wait state
        }
        if (stateJustChanged && !returningFromFreeze && !returningFromHit )
        {
            waitStartMoment = Time.time;
        }
        if (returningFromFreeze)
        {
            waitStartMoment += 5; //compensate for the duration of the freeze
        }
        returningFromFreeze = false;
        returningFromHit = false;
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }
        if (wasJustHit)
        {
            currentState = State.Hit;
            wasWhisperingWhenHit = false;
            return;
        }

        float timeSpentWaiting = Time.time - waitStartMoment;
        float waitTime = timeBeforeFirstWhisper;
        if (hadFirstWhisper)
            waitTime = timeBetweenWhispers;
        if ( timeSpentWaiting > waitTime )
        {
            currentState = State.Whispering;
            return;
        }
        if( freeze )
        {
            currentState = State.Frozen;
            wasWhisperingWhenFrozen = false;
            return;
        }
    }

    List<int> NrMobsInEachRegion = new List<int>();
    void InitializeDiversityComputation()
    {
        var mobsLeft = new List<Mob>();
        var tags = new List<string>();
        tags.Add("SocialAnxiety");
        tags.Add("Anger");
        tags.Add("Depression");
        foreach (var mob in g.i.mobs)
            if (tags.Contains(mob.tag))
                mobsLeft.Add(mob);
        
        NrMobsInEachRegion.Clear();
        for (int idx = 0; idx < 9; ++idx)
            NrMobsInEachRegion.Add(0);
        foreach (var mob in mobsLeft)
        {
            Vector2 region = g.i.Region(mob.transform.position);
            int regionIdx = (int)(region.y * 3 + region.x);
            ++NrMobsInEachRegion[regionIdx];
        }
    }

    int ComputePositionDiversity(Vector3 pos)
    {
        List<int> nrMobsInEachRegion = new List<int>();
        foreach (int val in NrMobsInEachRegion)
            nrMobsInEachRegion.Add(val);
        Vector2 region = g.i.Region(pos);
        int regionIdx = (int)(region.y * 3 + region.x);
        ++nrMobsInEachRegion[regionIdx];

        int diversity = 0;
        foreach (int nrMobs in nrMobsInEachRegion)
            diversity -= nrMobs * nrMobs;
        return diversity;
    }

    Vector3 GetRandomPositionWithMobDiversity()
    {
        //generate 30 random positions, compute the diversity of the distribution for each one, keep the one with the best diversity
        Vector3 bestPos = new Vector3();
        int maxDiversity = 0;
        for( int idx = 0; idx < 30; ++idx )
        {
            Vector3 pos = g.i.GetFreeRandomPosition();
            int diversity = ComputePositionDiversity(pos);
            if ( idx == 0 || diversity > maxDiversity)
            {
                bestPos = pos;
                maxDiversity = diversity;
            }
        }
        
        return bestPos;
    }

    void UpdateDiversityComputation(Vector3 pos)
    {
        Vector2 region = g.i.Region(pos);
        int regionIdx = (int)(region.y * 3 + region.x);
        ++NrMobsInEachRegion[regionIdx];
    }
    
    public void Shuffle<T>(IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = UnityEngine.Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    void Whisper()
    {
        if( stateJustChanged)
        {
            Anim.SetInteger("AnimationState", 1);
        }
        if (!g.i.player.GetComponent<PlayerHealth>().alive)
        {
            DestroySpawned();
            currentState = State.Waiting;
            return; //just idle in the wait state
        }
        if (stateJustChanged && !returningFromFreeze && !returningFromHit)
        {
            int totalNrSpawns = nrDepressions + nrSocialAnxieties + nrAngers;
            //get random positions that keep the distribution of mobs as diverse as possible
            List<Vector3> positions = new List<Vector3>();
            InitializeDiversityComputation();
            for (int idx = 0; idx < totalNrSpawns; ++idx)
            {
                Vector3 pos = GetRandomPositionWithMobDiversity();
                positions.Add(pos);
                UpdateDiversityComputation(pos);
            }

            //shuffle the list
            Shuffle(positions);

            int posIdx = 0;
            for (int idx = 0; idx < nrDepressions; ++idx)
            {
                GameObject obj = g.Clone(depressionRef, positions[posIdx]);
                ++posIdx;
                obj.GetComponent<DepressionAI>().MakeFictional();
                spawnedObjects.Add(obj);
                obj.GetComponent<MobHealth>().startHealth = obj.GetComponent<MobHealth>().maxHealth;
            }
            for (int idx = 0; idx < nrSocialAnxieties; ++idx)
            {
                GameObject obj = g.Clone(socialAnxietyRef, positions[posIdx]);
                ++posIdx;
                obj.GetComponent<SocialAnxietyAI>().MakeFictional();
                spawnedObjects.Add(obj);
                obj.GetComponent<MobHealth>().startHealth = obj.GetComponent<MobHealth>().maxHealth;
            }
            for (int idx = 0; idx < nrAngers; ++idx)
            {
                GameObject obj = g.Clone(angerRef, positions[posIdx]);
                ++posIdx;
                obj.GetComponent<AngerAI>().MakeFictional();
                spawnedObjects.Add(obj);
                obj.GetComponent<MobHealth>().startHealth = obj.GetComponent<MobHealth>().maxHealth;
            }
            Record.Event("mob attack", "paranoia", "start whisper");
            hadFirstWhisper = true;
            whisperStartMoment = Time.time;

            g.i.audioManager.PlayPositionIndependentClip(attack);
        }
        if( returningFromFreeze )
        {
            whisperStartMoment += 5; //compensate for the duration of the freeze
        }
        returningFromFreeze = false;
        returningFromHit = false;

        if (!GetComponent<MobHealth>().alive)
        {
            DestroySpawned();
            currentState = State.Dead;
            return;
        }

        if (wasJustHit)
        {
            currentState = State.Hit;
            wasWhisperingWhenHit = true;
            return;
        }

        if (freeze)
        {
            currentState = State.Frozen;
            wasWhisperingWhenFrozen = true;
            return;
        }

        float timeSpentWhispering = Time.time - whisperStartMoment;
        float realWhisperDuration = whisperDuration;
        if (PlayerPrefs.GetInt("Shorter paranoia") != 0)
            realWhisperDuration -= g.i.boosts.ShorterParanoiaDuration;
        if (timeSpentWhispering > realWhisperDuration)
        {
            DestroySpawned();
            currentState = State.Waiting;
            return;
        }
    }
    void DestroySpawned()
    {
        foreach (var obj in spawnedObjects)
            if( obj != null )
                obj.GetComponent<MobHealth>().SetHealth(0);
        spawnedObjects.Clear();
        Record.Event("mob attack", "paranoia", "stop whisper");
    }
    void Dead()
    {
        if (stateJustChanged)
        {
            DestroySpawned();
            Record.Event("mob dead", "paranoia");
            if (g.i.spawnDrops)
                g.i.spawnDrops.RegisterDeath(GetComponent<Mob>(), g.i.spawnDrops.ParanoiaHealthDropRate, g.i.spawnDrops.ParanoiaResourceDropRate, g.i.spawnDrops.ParanoiaResourceDropAmount, GetComponent<MobHealth>().maxHealth, g.i.spawnDrops.ParanoiaBoostDropRate, g.i.spawnDrops.ParanoiaConsumableDropRate);
            Anim.SetInteger("AnimationState", 2);
            GetComponent<MobExpiration>().StartExpiration();
            g.i.audioManager.PlayClip(death, transform.position);
        }
    }
    protected override void UpdateImpl()
    {
        State oldState = currentState;
        switch (currentState)
        {
            case State.Waiting:
                Wait();
                break;
            case State.Whispering:
                Whisper();
                break;
            case State.Dead:
                Dead();
                break;
            case State.Frozen:
                Frozen();
                break;
            case State.Hit:
                Hit();
                break;
        }
        State newState = currentState;
        stateJustChanged = oldState != newState;
        if (stateJustChanged)
            lastStateChangeMoment = Time.time;
    }

    public void Freeze()
    {
        freeze = true;
        timeSinceFreeze = Time.time;
    }

    void Frozen()
    {
        if (stateJustChanged)
        {
            Anim.speed = 0;
        }
        returningFromHit = false;

        if (!GetComponent<MobHealth>().alive)
        {
            DestroySpawned();
            freeze = false;
            Anim.speed = 1;
            currentState = State.Dead;
            return;
        }

        if (wasJustHit)
        {
            Anim.speed = 1;
            currentState = State.Hit;
            return;
        }

        float time = Time.time - timeSinceFreeze;
        if (time > 5)
        {
            freeze = false;
            Anim.speed = 1;
            returningFromFreeze = true;
            if (wasWhisperingWhenFrozen)
                currentState = State.Whispering;
            else
                currentState = State.Waiting;
            return;
        }
    }

    public void OnHit()
    {
        g.Splash(transform.position);
        if (GetComponent<MobHealth>().alive)
        {
            GameObject outline = g.Clone(paranoiaHitEffect, transform.position);
            outline.transform.localEulerAngles = transform.localEulerAngles;
        }
        wasJustHit = true;
    }

    void Hit()
    {
        if (stateJustChanged)
        {
            wasJustHit = false;
            Anim.SetInteger("AnimationState", 3);
            g.i.audioManager.PlayClip(hit, transform.position);
        }
        if (!GetComponent<MobHealth>().alive)
        {
            DestroySpawned();
            currentState = State.Dead;
            return;
        }
        if (wasJustHit || stateJustChanged && freeze)
        {
            lastStateChangeMoment = Time.time;
            Anim.Play("ParanoiaHit", -1, 0f);
            wasJustHit = false;
            g.i.audioManager.PlayClip(hit, transform.position);
            return;
        }
        float timeSpentHit = Time.time - lastStateChangeMoment;
        if (timeSpentHit > hitTime)
        {
            returningFromHit = true;
            if (freeze)
                currentState = State.Frozen;
            else if (wasWhisperingWhenHit)
                currentState = State.Whispering;
            else
                currentState = State.Waiting;
            return;
        }
    }
}