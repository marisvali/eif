﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapBehavior : MonoBehaviour
{
    public UnityEngine.UI.Image border;
    public GameObject gridCell;
    public Canvas minimapCanvas;
    public Vector2 startRegion = new Vector2(2, 2);
    private Tutorial1 tutorial1;
    bool initialized = false;

    void Start()
    {
        DrawGrid();
        AdjustCamera(g.i.RegionRect(startRegion).center);
    }

    void MoveBorder()
    {
        // We want a rectangle to highlight on the minimap which region is viewed in the main
        // camera. For this, we use the fact that both the main camera and the minimap view a
        // region in the world. So we transform coorinates between different spaces.
        // The logic is this:
        // - Get the rectangle that the main camera views, in world units.
        // - Expand the rectangle because the border image needs it.
        // - Translate coordinates from world points to screen points.
        // - Compute the proper offset for the border image, relative to its parent canvas, 
        // which is the minimap.
        // - Compute the proper size for the border image.
        Camera minimap = GetComponent<Camera>();
        Camera main = g.i.cameraMain;
        Vector3 borderOffset = new Vector3(50, 50, 0);
        
        var worldRect = g.i.WorldRectOfMainCamera();
        Vector3 bottomLeftWorld = (Vector3)worldRect.min - borderOffset;
        Vector3 topRightWorld = (Vector3)worldRect.max + borderOffset;
        
        // Screen space has unexpected coordinates. For example for the minimap I get these screen
        // coordinates for minimap.pixelRect, when there is a 90 degree rotation on the camera:
        // (x: 791 y: 607 width: 472.5 height: 472.5)
        // (xMin: 791 yMin: 607 xMax: 1263.5 yMax: 1079.5)
        // WorldToScreenPoint doesn't care about rotation. Screen units are reported independent
        // of any rotation and they should be taken as units relative to the physical bottom-left.
        Vector3 p1Screen = minimap.WorldToScreenPoint(bottomLeftWorld) / minimapCanvas.scaleFactor;
        Vector3 p2Screen = minimap.WorldToScreenPoint(topRightWorld) / minimapCanvas.scaleFactor;
        // Example output when the camera has a 90 degree rotation on Z axis:
        // p1Screen -> (784, 1087)
        // p2Screen -> (956, 915)
        
        // The UI elements don't care about the rotation of the camera. No matter the rotation,
        // adding 100 to the X coordinate moves it from left to right and adding 100 to the Y
        // coordinate moves it from bottom to the top.
        // So I need to be told how much to the right from the bottom left of the minimap,
        // and how much to move up from the bottom left of the minimap.
        var x = Mathf.Min(p1Screen.x, p2Screen.x) - minimap.pixelRect.x / minimapCanvas.scaleFactor;
        var y = Mathf.Min(p1Screen.y, p2Screen.y) - minimap.pixelRect.y / minimapCanvas.scaleFactor;
        var width = Mathf.Abs(p1Screen.x - p2Screen.x);
        var height = Mathf.Abs(p1Screen.y - p2Screen.y);
        
        RectTransform rectTransform = border.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector2(x, y);
        // Here we rely on the fact that the border image is symmetrical and doesn't have or
        // need any rotation on it.
        rectTransform.sizeDelta = new Vector2(width, height);
    }
    
    void DrawGrid()
    {
        Camera minimap = GetComponent<Camera>();

        float distX = 1.0f / g.nRegions;
        float distY = 1.0f / g.nRegions;

        for (int idxY = 0; idxY < g.nRegions; ++idxY)
            for (int idxX = 0; idxX < g.nRegions; ++idxX)
            {
                GameObject newGridCell = Instantiate(gridCell);
                RectTransform rtr = newGridCell.GetComponent<RectTransform>();
                rtr.SetParent(minimapCanvas.GetComponent<Transform>(), false);
                Vector3 pos = minimap.ViewportToScreenPoint(new Vector3(idxX * distX, idxY * distY, 0f));
                pos = pos / minimapCanvas.scaleFactor;
                Vector3 size = minimap.ViewportToScreenPoint(new Vector3((idxX + 1) * distX, (idxY + 1) * distY, 0));
                size = size / minimapCanvas.scaleFactor - pos;

                Vector3 minibl = minimap.ViewportToScreenPoint(Vector3.zero) / minimapCanvas.scaleFactor;
                pos -= minibl;
                rtr.anchoredPosition = new Vector2(pos.x, pos.y);
                rtr.sizeDelta = new Vector2(size.x, size.y);
            }
    }

    public bool AdjustCamera(Vector3 worldPoint)
    {
        Transform cameraMainTransform = g.i.cameraMain.GetComponent<Transform>();
        if (g.i.Region(cameraMainTransform.position) == g.i.Region(worldPoint) && initialized)
            return false; // No change necessary.

        if (tutorial1 != null)
            tutorial1.RegisterMinimapChange();

        g.i.cameraMain.GetComponent<CameraShaker>().StopShake(); // Without this the camera might be
        // shaking while we try to change the position and the shaking method will overwrite our
        // change.
        Vector3 newPosition = g.i.RegionRect(worldPoint).center;
        newPosition.z = cameraMainTransform.position.z;
        cameraMainTransform.position = newPosition;
        
        MoveBorder();
        initialized = true;
        Record.Event("minimap");
        return true;
    }
    public void RedrawGrid()
    {
        foreach (Transform child in minimapCanvas.transform)
            Destroy(child.gameObject);
        DrawGrid();
        MoveBorder();
    }
}