﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSequence : MonoBehaviour
{
    public float camerasResizeTime = 0;
    public GameObject healedLand;
    public AudioClip transformationSequence;
    public GameObject spriteMask;
    public GameObject finalBeam;
    public GameObject bigHit;
    public GameObject sustainedSplash;
    public GameObject finalExplosion;
    public GameObject revealCircle;
    public AudioSource blissMusic;
    public Transform mainSprite;

    void Start()
    {
        spriteMask.transform.localScale = new Vector3(0, 0, 1);
        
        StartCoroutine(EndSequenceImpl());
        g.i.mainScript.GetComponent<SpawnItems>().enabled = false;
        foreach (var ammo in g.i.ammos)
            Destroy(ammo.gameObject);
        foreach (var res in g.i.resources)
            Destroy(res.gameObject);
        foreach (var healthDrop in g.i.healthDrops)
            Destroy(healthDrop.gameObject);
        GetComponent<Animator>().enabled = false;
        
        revealCircle.transform.localScale = new Vector3(0, 0, 1);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(EndSequenceImpl());
        }
    }

    IEnumerator FadeOutBattleMusic()
    {
        float fadeOutDuration = 2f;
        float startTime = Time.time;
        while ((Time.time - startTime) < fadeOutDuration)
        {
            float timePassedNormalized = (Time.time - startTime) / fadeOutDuration;
            musicLevel.SetVolume(1.0f - timePassedNormalized);
            yield return null;
        }
    }

    IEnumerator FadeInBlissMusic()
    {
        blissMusic.Play();

        float fadeInDuration = 2f;
        float startTime = Time.time;
        while ((Time.time - startTime) < fadeInDuration)
        {
            float timePassedNormalized = (Time.time - startTime) / fadeInDuration;
            blissMusic.volume = timePassedNormalized;
            yield return null;
        }
    }

    IEnumerator EndSequenceImpl()
    {
        yield return null; //wait 1 frame

        //remove the UI elements from the player and turn him around
        g.i.player.TriggerEndSequence1();

        yield return new WaitForSeconds(0.5f);
        g.i.player.Teleport(new Vector2(2295, 250));
        yield return new WaitForSeconds(0.5f);

        StartCoroutine(FadeOutBattleMusic());

        //resize the camera
        float resizePerSecond = g.i.cameraMinimap.rect.width / camerasResizeTime;
        float resizePerFrame = resizePerSecond / 60;
        float finalMainCameraSize = 1920 / 2;
        float mainCameraSizeDif = finalMainCameraSize - g.i.cameraMain.orthographicSize;
        float mainCameraSizeDifPerFrame = mainCameraSizeDif / camerasResizeTime / 60.0f;
        g.i.cameraMinimap.enabled = false;
        g.i.cameraMenu.gameObject.SetActive(false);
        g.i.cameraMainCylinder.SetActive(false);
        Vector3 mainCameraTargetPos = mainSprite.position;
        mainCameraTargetPos.z = g.i.cameraMain.transform.position.z;
        Vector3 mainCameraPosDif = (mainCameraTargetPos - g.i.cameraMain.transform.position) / camerasResizeTime / 60.0f;

        if (PlayerPrefs.GetString("Handedness") == "Left")
        {
            float totalX = g.i.cameraMinimap.rect.width + g.i.cameraMain.rect.xMax;
            while (g.i.cameraMain.rect.xMax < totalX)
            {
                Rect mainCameraRect = new Rect(g.i.cameraMain.rect);
                //mainCameraRect.x += resizePerFrame;
                mainCameraRect.xMax += resizePerFrame;
                g.i.cameraMain.rect = mainCameraRect;
                g.i.cameraMain.orthographicSize += mainCameraSizeDifPerFrame;
                g.i.cameraMain.transform.position += mainCameraPosDif;
                yield return null;
            }
        }
        else
        {
            float xMargin = 1.0f - g.i.cameraMain.rect.xMax;
            while (g.i.cameraMain.rect.x > xMargin)
            {
                Rect mainCameraRect = new Rect(g.i.cameraMain.rect);
                mainCameraRect.x -= resizePerFrame;
                mainCameraRect.xMax += resizePerFrame;
                g.i.cameraMain.rect = mainCameraRect;
                g.i.cameraMain.orthographicSize += mainCameraSizeDifPerFrame;
                g.i.cameraMain.transform.position += mainCameraPosDif;
                yield return null;
            }
        }

        //start the player end animation (attack stance)
        g.i.player.TriggerEndSequence2();

        g.i.audioManager.PlayPositionIndependentClip(transformationSequence);

        //start the final beam animation
        finalBeam.SetActive(true);
        sustainedSplash.SetActive(true);

        //start the boss end sequence
        GetComponent<Animator>().enabled = true;
    }

    public void StartFinalExplosion()
    {
        finalExplosion.SetActive(true);
    }

    public void RevealHealedLand()
    {
        StartCoroutine(RevealHealedLandImpl());
    }

    IEnumerator RevealHealedLandImpl()
    {
        //yield return new WaitForSeconds(0.2f);

        revealCircle.SetActive(true);

        float step = 0.2f;
        float stepRevealCircle = 0.02f;
        for ( int idx = 0; idx < 150; ++idx )
        {
            Vector3 oldScale = spriteMask.transform.localScale;
            oldScale.x += step;
            oldScale.y += step;
            spriteMask.transform.localScale = oldScale;

            Vector3 oldScaleRevealCircle = revealCircle.transform.localScale;
            oldScaleRevealCircle.x += stepRevealCircle;
            oldScaleRevealCircle.y += stepRevealCircle;
            revealCircle.transform.localScale = oldScaleRevealCircle;
            yield return null;
        }

        revealCircle.SetActive(false);
        StartCoroutine(FadeInBlissMusic());

        yield return new WaitForSeconds(2);

        g.i.player.Teleport(new Vector2(-2000, -2000));

        yield return new WaitForSeconds(2);

        g.i.mainScript.ShowContinueButton();
    }

    public void ReadyForBigHit()
    {
        //end the final beam and sustained splash
        finalBeam.SetActive(false);
        sustainedSplash.SetActive(false);
        g.i.player.TriggerEndSequence3();

        //trigger the big hit animation
        bigHit.SetActive(true);
    }
}
