﻿using UnityEngine;

public class Clickable : MonoBehaviour {
    public Camera activeCamera;

	public bool Clicked()
    {
        if (!Input.GetMouseButtonDown(0))
            return false;

        Vector3 viewportPt = activeCamera.ScreenToViewportPoint(Input.mousePosition);
        Rect one = new Rect(0, 0, 1, 1);
        if (!one.Contains(viewportPt))
            return false;

        Vector3 worldPt3 = activeCamera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 worldPt2 = new Vector2(worldPt3.x, worldPt3.y);
        return GetComponent<Collider2D>().OverlapPoint(worldPt2);
    }
}