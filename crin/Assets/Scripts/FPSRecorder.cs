﻿using UnityEngine;
using System.Collections.Generic;

public class FPSRecorder : MonoBehaviour
{
    public struct FrameFPS
    {
        public int FrameIdx;
        public int FPS;
    }
    List<FrameFPS> FPS;
    int CurrentFrameIdx;

    private void Start()
    {
        FPS = new List<FrameFPS>();
        CurrentFrameIdx = 0;
    }
    void Update()
    {
        ++CurrentFrameIdx;
        int fps = (int)(1.0 / Time.deltaTime);
        if( fps < 60 )
        {
            FrameFPS frameFPS = new FrameFPS();
            frameFPS.FPS = fps;
            frameFPS.FrameIdx = CurrentFrameIdx;
        }
    }
    public List<FrameFPS> GetRecordedFPS()
    {
        List<FrameFPS> copyFPS = new List<FrameFPS>();
        foreach (var fps in FPS)
            copyFPS.Add(fps);
        FPS.Clear();
        return copyFPS;
    }
}