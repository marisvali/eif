﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayResources : MonoBehaviour
{
    int resources = -1;

    void Update()
    {
        if (resources != PlayerPrefs.GetInt("Resources"))
        {
            Text text = GetComponent<Text>();
            text.text = PlayerPrefs.GetInt("Resources").ToString();
            resources = PlayerPrefs.GetInt("Resources");
        }
    }
}
