﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobExpiration : MonoBehaviour
{
    public List<SpriteRenderer> sprites = new List<SpriteRenderer>();
    public float duration = 0;
    bool expired = false;
    public void StartExpiration()
    {
        StartCoroutine(Expire());
    }
    public void ResetExpiration()
    {
        expired = false;
        foreach (var sprite in sprites)
            sprite.sortingLayerName = "Mobs";
    }
    IEnumerator Expire()
    {
        yield return new WaitForSeconds(duration);
        expired = true;
        foreach (var sprite in sprites)
            sprite.sortingLayerName = "MobCorpses";
    }
    public bool Expired()
    {
        return expired;
    }
    public void SetExpired(bool isExpired)
    {
        expired = isExpired;
    }
}
