﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReskinCharacter : MonoBehaviour
{
    string path = "Images\\Player\\";

    Sprite[] sprites = null;

    private void Start()
    {
        sprites = Resources.LoadAll<Sprite>(path);
    }

    private void LateUpdate()
    {
        if (PlayerPrefs.GetString("Gender") == "Female")
        {
            if (GetComponent<Image>().sprite != null)
            {
                string spriteName = GetComponent<Image>().sprite.name;
                if (!spriteName.StartsWith("Fem_"))
                {
                    if (sprites != null)
                    {
                        foreach (var altSprite in sprites)
                            if (altSprite.name.Contains("Fem_" + spriteName))
                            {
                                GetComponent<Image>().sprite = altSprite;
                                break;
                            }
                    }
                }
            }
        }
    }
}
