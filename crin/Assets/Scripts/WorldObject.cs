using System.Collections;
using UnityEngine;

public class WorldObject : MonoBehaviour
{
    public void Update()
    {
        if (g.i && !g.i.Paused())
            UpdateImpl();
    }

    protected virtual void UpdateImpl()
    {

    }
}