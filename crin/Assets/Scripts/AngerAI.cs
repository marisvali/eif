﻿using System.Collections;
using UnityEngine;

public class AngerAI : WorldObject
{
    public float stunnedTime = 0;
    public float roamingSpeed = 0;
    public float roamingRadius = 0;
    public float minTimeBetweenRoams = 0;
    public float maxTimeBetweenRoams = 0;
    public float chargeSpeed = 0;
    public float chargeDistance = 0;
    public float timeBeforeFirstCharge = 0;
    public float timeBetweenCharges = 0;
    public float hitTime = 0;
    public bool roamInOriginalRegion = false;
    public float appearanceTime = 0;
    public int nrResourcesDrop = 0;
    public AudioClip attack;
    public AudioClip death;

    float lastStateChangeMoment = 0;
    bool stateJustChanged = false;
    GameObject mTarget;
    bool roamTargetSet = false;
    bool resetTargetTimerStarted = false;
    TouchingPlayer touchingPlayer;
    bool chargeTargetSet = false;
    bool fictional = false;
    bool wasJustHit = false;
    bool freeze = false;
    float timeSinceFreeze = 0;
    Animator Anim;

    enum State
    {
        Appearing,
        Roaming,
        Aiming,
        Charging,
        Dead,
        InsidePlayer,
        Hit,
        Frozen
    }
    State currentState;
    void Start()
    {
        touchingPlayer = GetComponent<Mob>().hitArea.GetComponent<TouchingPlayer>();
        Anim = GetComponent<Animator>();
        if (PlayerPrefs.GetString("Difficulty") == "Casual")
        {
            roamingSpeed = roamingSpeed * 0.6f;
            chargeSpeed = chargeSpeed * 0.6f;
        }
        resetTargetTimerStarted = false;
        wasJustHit = false;
        freeze = false;
        mTarget = g.CreatePathTarget();
        GetComponent<MobHealth>().SetHealth(GetComponent<MobHealth>().startHealth);
        GetComponent<MobExpiration>().ResetExpiration();
    }


    void OnDestroy()
    {
        Destroy(mTarget);
    }

    bool InSameRegion(Mob mob, Player player)
    {
        Rigidbody2D playerBody = player.GetComponent<Rigidbody2D>();
        Vector2 playerRegion = g.i.Region(playerBody.position);
        return g.i.MobPartiallyInRegion(mob, playerRegion);
    }
    bool CompletelyInSameRegion(Mob mob, Player player)
    {
        Rigidbody2D playerBody = player.GetComponent<Rigidbody2D>();
        Vector2 playerRegion = g.i.Region(playerBody.position);
        return g.i.MobCompletelyInRegion(mob, playerRegion);
    }
    void Appear()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            Anim.SetInteger("AnimationState", 4);
        }

        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }

        float time = Time.time - lastStateChangeMoment;
        if (time > appearanceTime)
        {
            currentState = State.Roaming;
            return;
        }
    }
    void Roam()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            float realRoamingSpeed = roamingSpeed;
            if (PlayerPrefs.GetInt("Slower anger") != 0)
                realRoamingSpeed -= g.i.boosts.SlowerAngerPercent * realRoamingSpeed / 100f;
            GetComponent<Pathing>().speed = realRoamingSpeed;
            roamTargetSet = false;
            Anim.SetInteger("AnimationState", 0);
        }
        if (!g.i.player.GetComponent<PlayerHealth>().alive)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            roamTargetSet = false;
            Anim.SetInteger("AnimationState", 0);
            return; //just idle with the roaming state animation
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            StopAllCoroutines();
            return;
        }

        if (wasJustHit)
        {
            currentState = State.Hit;
            StopAllCoroutines();
            return;
        }

        if (InSameRegion(GetComponent<Mob>(), g.i.player))
        {
            currentState = State.Aiming;
            StopAllCoroutines();
            return;
        }

        if (freeze)
        {
            currentState = State.Frozen;
            StopAllCoroutines();
            return;
        }

        if (!roamTargetSet)
        {
            if (roamInOriginalRegion)
            {
                Vector2 region = g.i.Region(transform.position);
                mTarget.transform.position = g.i.GetFreeRandomPositionInRegion(region, gameObject.transform.position);
            }
            else
            {
                mTarget.transform.position = g.i.GetFreeRandomPosition(gameObject.transform.position, roamingRadius);
            }

            GetComponent<Pathing>().SetTarget(mTarget);
            roamTargetSet = true;
            Anim.SetInteger("AnimationState", 1);
            resetTargetTimerStarted = false;
            return;
        }

        if (!resetTargetTimerStarted)
        {
            if (GetComponent<Pathing>().AtTarget())
            {
                Anim.SetInteger("AnimationState", 0);
                StartCoroutine(ResetTarget());
                resetTargetTimerStarted = true;
            }
        }
    }

    IEnumerator ResetTarget()
    {
        float timeToWait = minTimeBetweenRoams + (maxTimeBetweenRoams - minTimeBetweenRoams) * UnityEngine.Random.value;
        yield return new WaitForSeconds(timeToWait);
        roamTargetSet = false;
        yield return null;
    }

    void Aim()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(g.i.player.gameObject);
            Anim.SetInteger("AnimationState", 1);
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }
        if (wasJustHit)
        {
            currentState = State.Hit;
            return;
        }

        if (!InSameRegion(GetComponent<Mob>(), g.i.player))
        {
            currentState = State.Roaming;
            return;
        }

        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }

        if (CompletelyInSameRegion(GetComponent<Mob>(), g.i.player))
        {
            Vector2 dist = transform.position - g.i.player.transform.position;
            if (dist.magnitude < chargeDistance)
            {
                bool pathIsClear = !g.i.gridGraph.Linecast(transform.position, g.i.player.transform.position);
                if (pathIsClear)
                {
                    currentState = State.Charging;
                    return;
                }
            }
        }
    }

    void Charge()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            chargeTargetSet = false;
            GetComponent<SpriteRenderer>().sortingLayerName = "Mobs2";
            GetComponent<Mob>().icon.sortingLayerName = "Mobs2";
        }

        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            StopAllCoroutines();
            GetComponent<MoveStraightTo>().Disable();
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
            GetComponent<SpriteRenderer>().sortingLayerName = "Mobs";
            GetComponent<Mob>().icon.sortingLayerName = "Mobs";
            return;
        }

        if (wasJustHit)
        {
            currentState = State.Hit;
            StopAllCoroutines();
            GetComponent<MoveStraightTo>().Disable();
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
            GetComponent<SpriteRenderer>().sortingLayerName = "Mobs";
            GetComponent<Mob>().icon.sortingLayerName = "Mobs";
            return;
        }

        if (touchingPlayer.IsTouching())
        {
            currentState = State.InsidePlayer;
            StopAllCoroutines();
            GetComponent<MoveStraightTo>().Disable();
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
            GetComponent<SpriteRenderer>().sortingLayerName = "Mobs";
            GetComponent<Mob>().icon.sortingLayerName = "Mobs";
            return;
        }

        if (freeze)
        {
            currentState = State.Frozen;
            StopAllCoroutines();
            GetComponent<MoveStraightTo>().Disable();
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
            GetComponent<SpriteRenderer>().sortingLayerName = "Mobs";
            GetComponent<Mob>().icon.sortingLayerName = "Mobs";
            return;
        }

        //take into consideration changes in the player's position
        //only between charges
        if (GetComponent<MoveStraightTo>().AtTarget())
        {
            if (!InSameRegion(GetComponent<Mob>(), g.i.player))
            {
                currentState = State.Roaming;
                StopAllCoroutines();
                GetComponent<MoveStraightTo>().Disable();
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
                GetComponent<SpriteRenderer>().sortingLayerName = "Mobs";
                GetComponent<Mob>().icon.sortingLayerName = "Mobs";
                return;
            }

            bool pathIsClear = !g.i.gridGraph.Linecast(transform.position, g.i.player.transform.position);
            if (!pathIsClear)
            {
                currentState = State.Aiming;
                StopAllCoroutines();
                GetComponent<MoveStraightTo>().Disable();
                GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
                GetComponent<SpriteRenderer>().sortingLayerName = "Mobs";
                GetComponent<Mob>().icon.sortingLayerName = "Mobs";
                return;
            }
        }

        if (!chargeTargetSet)
        {
            StartCoroutine(StartCharging());
            chargeTargetSet = true;
        }
    }
    IEnumerator StartCharging()
    {
        Vector2 pos = g.i.player.GetComponent<Rigidbody2D>().position;
        yield return WaitAndShift(timeBeforeFirstCharge);
        yield return ChargeRepeatedly(pos);
    }
    IEnumerator WaitAndShift(float time)
    {
        int nrRedShifts = 10;
        float totalRedShift = 0.5f;
        for (int redShiftIdx = 0; redShiftIdx < nrRedShifts; ++redShiftIdx)
        {
            Color col = GetComponent<SpriteRenderer>().color;
            float subtract = (float)redShiftIdx / (float)nrRedShifts * totalRedShift;
            col.b = 1f - subtract;
            col.g = 1f - subtract;
            GetComponent<SpriteRenderer>().color = col;
            yield return new WaitForSeconds(time / nrRedShifts);
        }
    }
    IEnumerator ChargeRepeatedly(Vector2 pos)
    {
        transform.localEulerAngles = new Vector3(0, 0, 0);
        Vector2 dir = pos - new Vector2(transform.position.x, transform.position.y);
        if (pos.x < transform.position.x)
        {
            transform.localEulerAngles = new Vector3(0, 180, 0);
            float dirAngle = Quaternion.FromToRotation(Vector3.left, dir).eulerAngles.z;
            transform.Rotate(Vector3.back, dirAngle);
        }
        else
        {
            float dirAngle = Quaternion.FromToRotation(Vector3.right, dir).eulerAngles.z;
            transform.Rotate(Vector3.forward, dirAngle);
        }

        float realChargeSpeed = chargeSpeed;
        if (PlayerPrefs.GetInt("Slower anger") != 0)
            realChargeSpeed -= g.i.boosts.SlowerAngerPercent * realChargeSpeed / 100f;
        Anim.SetInteger("AnimationState", 2);
        g.i.audioManager.PlayClip(attack, transform.position);
        GetComponent<MoveStraightTo>().SetTarget(new Vector2(pos.x, pos.y), realChargeSpeed);
        while (!GetComponent<MoveStraightTo>().AtTarget())
            yield return new WaitForSeconds(0.01f);
        Anim.SetInteger("AnimationState", 0);
        if (pos.x < transform.position.x)
            transform.localEulerAngles = new Vector3(0, 180, 0);
        else
            transform.localEulerAngles = new Vector3(0, 0, 0);
        Vector2 newPos = g.i.player.GetComponent<Rigidbody2D>().position;
        yield return WaitAndShift(timeBetweenCharges);
        yield return ChargeRepeatedly(newPos);
    }
    void Dead()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = true;
            if (fictional)
            {
                Anim.SetInteger("AnimationState", 5);
                Record.Event("mob dead", "anger", "fictional");
            }
            else
            {
                Anim.SetInteger("AnimationState", 3);
                g.i.audioManager.PlayClip(death, transform.position);
                if (g.i.spawnDrops)
                    g.i.spawnDrops.RegisterDeath(GetComponent<Mob>(), g.i.spawnDrops.AngerHealthDropRate, g.i.spawnDrops.AngerResourceDropRate, g.i.spawnDrops.AngerResourceDropAmount, GetComponent<MobHealth>().maxHealth, g.i.spawnDrops.AngerBoostDropRate, g.i.spawnDrops.AngerConsumableDropRate);
                Record.Event("mob dead", "anger");
            }
            if (g.i.player.transform.position.x < transform.position.x)
                transform.localEulerAngles = new Vector3(0, 180, 0);
            else
                transform.localEulerAngles = new Vector3(0, 0, 0);
            GetComponent<MobExpiration>().StartExpiration();
        }
        float timeSpentStunned = Time.time - lastStateChangeMoment;
        if (timeSpentStunned > stunnedTime && fictional)
            Destroy(gameObject);
    }
    void InsidePlayer()
    {
        if (stateJustChanged)
        {
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
            if (fictional)
                g.i.player.GetComponent<Player>().AngerAttack(0, 0); //trigger warning fog
            else
            {
                g.i.player.GetComponent<Player>().AngerAttack(1, nrResourcesDrop);
            }
        }

        Destroy(gameObject);
        //MoveToPlayer();
    }
    protected override void UpdateImpl()
    {
        State oldState = currentState;
        switch (currentState)
        {
            case State.Appearing:
                Appear();
                return;
            case State.Roaming:
                Roam();
                break;
            case State.Charging:
                Charge();
                break;
            case State.Dead:
                Dead();
                break;
            case State.InsidePlayer:
                InsidePlayer();
                break;
            case State.Hit:
                Hit();
                break;
            case State.Aiming:
                Aim();
                break;
            case State.Frozen:
                Frozen();
                break;
        }
        State newState = currentState;
        stateJustChanged = oldState != newState;
        if (stateJustChanged)
            lastStateChangeMoment = Time.time;
    }
    private void MoveToPlayer()
    {
        Vector3 pos = gameObject.transform.position;
        pos.x = g.i.player.transform.position.x;
        pos.y = g.i.player.transform.position.y;
        gameObject.transform.position = pos;
    }
    public bool IsFictional()
    {
        return fictional;
    }
    public void MakeFictional()
    {
        fictional = true;
        SpriteRenderer renderer = GetComponent<Mob>().icon;
        renderer.enabled = false;

        currentState = State.Appearing;
        stateJustChanged = true;
    }
    public void MakeReal()
    {
        fictional = false;
        SpriteRenderer renderer = GetComponent<Mob>().icon;
        renderer.enabled = true;

        currentState = State.Roaming;
        stateJustChanged = true;
    }
    public void OnHit()
    {
        g.Splash(transform.position);
        wasJustHit = true;
    }
    void Hit()
    {
        if (stateJustChanged)
        {
            wasJustHit = false;
            GetComponent<Pathing>().zombie = true;
            GetComponent<Pathing>().SetTarget(null);
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }
        if (wasJustHit)
        {
            lastStateChangeMoment = Time.time;
            wasJustHit = false;
            return;
        }
        float timeSpentHit = Time.time - lastStateChangeMoment;
        if (timeSpentHit > hitTime)
        {
            if (freeze)
                currentState = State.Frozen;
            else
                currentState = State.Roaming;
            return;
        }
    }
    public void OnTeleport()
    {
        if (currentState == State.Aiming)
            GetComponent<Pathing>().Repath();
    }
    public void Freeze()
    {
        freeze = true;
        timeSinceFreeze = Time.time;
    }

    void Frozen()
    {
        if (stateJustChanged)
        {
            Anim.speed = 0;
            GetComponent<Pathing>().zombie = false;
            GetComponent<Pathing>().SetTarget(null);
        }

        if (!GetComponent<MobHealth>().alive)
        {
            freeze = false;
            Anim.speed = 1;
            currentState = State.Dead;
            return;
        }

        if (wasJustHit)
        {
            Anim.speed = 1;
            currentState = State.Hit;
            return;
        }

        float time = Time.time - timeSinceFreeze;
        if (time > 5)
        {
            freeze = false;
            Anim.speed = 1;
            currentState = State.Roaming;
            return;
        }
    }
}
