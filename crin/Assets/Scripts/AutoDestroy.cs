﻿using System.Collections;
using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    public float duration = 0;

    private float extraDuration = 0;

	void Start ()
    {
        StartCoroutine(DelayedDestroy());
	}

    IEnumerator DelayedDestroy()
    {
        yield return new WaitForSeconds(duration);
        yield return new WaitForSeconds(extraDuration);

        Destroy(gameObject);
    }

    public void DelayBy(float seconds)
    {
        extraDuration = seconds;
    }
}