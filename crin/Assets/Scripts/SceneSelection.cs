﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSelection : MonoBehaviour
{
    private Boosts boostsObj;

    List<int> ViableAppearances(List<int> appearances)
    {
        List<int> viable = new List<int>();
        for (int idx = 7; idx <= 30; ++idx)
            if (appearances[idx] != 1 && 
                appearances[idx - 1] != 1 && 
                appearances[idx + 1] != 1)
                viable.Add(idx);
        return viable;
    }
    void SetupShopAppearances()
    {
        List<int> appearances = new List<int>();
        //for (int idx = 0; idx <= 30; ++idx)
        //    appearances.Add(0);
        //appearances[5] = 1;
        //appearances[30] = 1;

        //int totalNrAppearances = UnityEngine.Random.Range(2, 6);
        //int nrAppearances = 0;
        //while ( true )
        //{
        //    var viableAppearances = ViableAppearances(appearances);
        //    if (viableAppearances.Count == 0)
        //        break;

        //    int viableIdx = UnityEngine.Random.Range(viableAppearances.Count);
        //    appearances[viableAppearances[viableIdx]] = 1;

        //    ++nrAppearances;
        //    if (nrAppearances == totalNrAppearances)
        //        break;
        //}

        //make shop appear before each level except the first one
        appearances.Add(0);
        appearances.Add(0);
        for (int idx = 2; idx <= 30; ++idx)
            appearances.Add(1);

        for (int idx = 1; idx <= 30; ++idx)
            PlayerPrefs.SetInt("Shop before level " + idx.ToString(), appearances[idx]);
    }
    bool ShopAppears()
    {
        int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
        return PlayerPrefs.GetInt("Shop before level " + currentLevel.ToString()) == 1;
    }
    
    bool BoostsAppears()
    {
        if (PlayerPrefs.GetInt("CatchUpBoosts") > 0)
            return true;
        int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
        int currentNrBoosts = PlayerPrefs.GetInt("BoostIdx");
        if (currentLevel == 6 && currentNrBoosts < 1)
            return true;
        if (currentLevel == 11 && currentNrBoosts < 2)
            return true;
        if (currentLevel == 16 && currentNrBoosts < 3)
            return true;
        if (currentLevel == 20 && currentNrBoosts < 4)
            return true;
        if (currentLevel == 25 && currentNrBoosts < 5)
            return true;
        if (currentLevel == 29 && currentNrBoosts < 6)
            return true;
        return false;
    }

    void SetCurrentLevel(int level)
    {
        //check for phase ring upgrades
        int currentLevel = level;
        if (PlayerPrefs.GetInt("Bought Phase ring 1") == 1 &&
            currentLevel < 6)
        {
            currentLevel = 6;
            PlayerPrefs.SetInt("CatchUpBoosts", 1);
        }
            
        if (PlayerPrefs.GetInt("Bought Phase ring 2") == 1 &&
            currentLevel < 16)
        {
            currentLevel = 16;
            PlayerPrefs.SetInt("CatchUpBoosts", 3);
        }

        //check for portal charm upgrade
        if (PlayerPrefs.GetInt("Bought Portal charm") == 1)
            if (PlayerPrefs.GetInt("CurrentLevel") > currentLevel)
            {
                currentLevel = PlayerPrefs.GetInt("CurrentLevel");

                int nrCatchUpBoosts = 0;
                if (currentLevel >= 6)
                    ++nrCatchUpBoosts;
                if (currentLevel >= 11)
                    ++nrCatchUpBoosts;
                if (currentLevel >= 16)
                    ++nrCatchUpBoosts;
                if (currentLevel >= 20)
                    ++nrCatchUpBoosts;
                if (currentLevel >= 25)
                    ++nrCatchUpBoosts;
                if (currentLevel >= 29)
                    ++nrCatchUpBoosts;
                PlayerPrefs.SetInt("CatchUpBoosts", nrCatchUpBoosts);
            }

        PlayerPrefs.SetInt("CurrentLevel", currentLevel);
    }

    void ClearBoosts()
    {
        var boosts = boostsObj.GetBoosts();
        foreach (var boost in boosts)
            PlayerPrefs.SetInt(boost.InternalName, 0);
    }

    void ClearShop()
    {
        List<ShopItems.ShopItem> allItems = GetComponent<ShopItems>().GetItems();
        foreach( var item in allItems )
            PlayerPrefs.SetInt("Bought " + item.InternalName.ToString(), 0);
    }

    string StartNewGame()
    {
        SetupShopAppearances();
        ResetCampaignState();
        LoadCampaignStateIntoCurrent();

        PlayerPrefs.SetInt("Playing tutorial", 0);
        SetCurrentLevel(1);

        if (PlayerPrefs.GetInt("WasDifficultySelected") == 0)
        {
            return "Difficulty";
        }
        else if( BoostsAppears() )
        {
            if (PlayerPrefs.GetInt("CatchUpBoosts") > 0)
                PlayerPrefs.SetInt("No Downpayment", 1);
            return "Boosts";
        }
        else
        {
            PlayerPrefs.SetString("Loading screen scene", PlayerPrefs.GetInt("CurrentLevel").ToString());
            return "LoadingScreenLevel";
        }
    }

    string StartTestLevel(int level)
    {
        SetupShopAppearances();
        ClearBoosts();

        SetCurrentLevel(level);
        PlayerPrefs.SetInt("PlayerHealth", 10000);
        PlayerPrefs.SetInt("PlayerAmmo", 0);
        if (PlayerPrefs.GetInt("Bought Shield 1") == 1)
            PlayerPrefs.SetInt("Shield 1", 1);
        if (PlayerPrefs.GetInt("Bought Shield 2") == 1)
            PlayerPrefs.SetInt("Shield 2", 1);
        if (PlayerPrefs.GetInt("Bought Shield 3") == 1)
            PlayerPrefs.SetInt("Shield 3", 1);
        
        PlayerPrefs.SetString("Loading screen scene", PlayerPrefs.GetInt("CurrentLevel").ToString());
        return "LoadingScreenLevel";
    }

    string GetTutorialScene(int level)
    {
        level += 7;
        return "Tutorial" + level.ToString();
    }

    string StartTutorial()
    {
        PlayerPrefs.SetInt("CurrentLevel", -6);
        PlayerPrefs.SetInt("PlayerHealth", 10000);
        PlayerPrefs.SetInt("PlayerAmmo", 0);
        PlayerPrefs.SetInt("Shield 1", 0);
        PlayerPrefs.SetInt("Shield 2", 0);
        PlayerPrefs.SetInt("Shield 3", 0);
        PlayerPrefs.SetInt("Playing tutorial", 1);
        PlayerPrefs.SetString("LastGameTypePlayed", "Campaign");

        PlayerPrefs.SetString("Loading screen scene", GetTutorialScene(PlayerPrefs.GetInt("CurrentLevel")));
        return "LoadingScreenTutorial";
    }

    void QuitGame()
    {
        ResetFlags();
#if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying needs to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }

    string AdvanceToNextLevel()
    {
        //don't use SetCurrentLevel because it screws boost apparitions
        PlayerPrefs.SetInt("CurrentLevel", PlayerPrefs.GetInt("CurrentLevel") + 1);
        PlayerPrefs.SetInt("PlayerHealth", 6);
        PlayerPrefs.SetInt("PlayerAmmo", g.MaxAmmo());
        
        if (ShopAppears())
            return "Shop";
        else if (BoostsAppears())
        {
            if (PlayerPrefs.GetInt("CatchUpBoosts") > 0)
                PlayerPrefs.SetInt("No Downpayment", 1);
            return "Boosts";
        }
        else
        {
            PlayerPrefs.SetString("Loading screen scene", PlayerPrefs.GetInt("CurrentLevel").ToString());
            return "LoadingScreenLevel";
        }
    }

    string AdvanceToNextTutorial()
    {
        int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
        int hits = PlayerPrefs.GetInt("Hits in the last level");

        if ( currentLevel == -6 )
        {
            if( hits <= 2 )
                PlayerPrefs.SetInt("CurrentLevel", -3);
            else
                PlayerPrefs.SetInt("CurrentLevel", -5);
        }
        
        if (currentLevel == -5)
        {
            if( hits <= 2)
                PlayerPrefs.SetInt("CurrentLevel", -3);
            else
                PlayerPrefs.SetInt("CurrentLevel", -4);
        }
        
        if( currentLevel == -4 )
            PlayerPrefs.SetInt("CurrentLevel", -3);

        if (currentLevel == -3)
        {
            if (hits <= 2)
            {
                PlayerPrefs.SetInt("Tutorial done", 1);
                if (PlayerPrefs.GetInt("Replaying tutorial") == 1)
                    return "Continue after replaying tutorial";
                else
                    return StartNewGame();
            }   
            else
                PlayerPrefs.SetInt("CurrentLevel", -2);
        }

        if (currentLevel == -2)
        {
            if (hits <= 2)
            {
                PlayerPrefs.SetInt("Tutorial done", 1);
                if (PlayerPrefs.GetInt("Replaying tutorial") == 1)
                    return "Continue after replaying tutorial";
                else
                    return StartNewGame();
            }
            else
                PlayerPrefs.SetInt("CurrentLevel", -1);
        }

        if (currentLevel == -1)
        {
            PlayerPrefs.SetInt("Tutorial done", 1);
            if (PlayerPrefs.GetInt("Replaying tutorial") == 1)
                return "Continue after replaying tutorial";
            else
                return StartNewGame();
        }

        PlayerPrefs.SetInt("PlayerHealth", 10000);
        PlayerPrefs.SetInt("Shield 1", 0);
        PlayerPrefs.SetInt("Shield 2", 0);
        PlayerPrefs.SetInt("Shield 3", 0);

        PlayerPrefs.SetString("Loading screen scene", GetTutorialScene(PlayerPrefs.GetInt("CurrentLevel")));
        return "LoadingScreenTutorial";
    }

    string RestartTutorialLevel()
    {
        PlayerPrefs.SetInt("PlayerHealth", 10000);
        PlayerPrefs.SetInt("Shield 1", 0);
        PlayerPrefs.SetInt("Shield 2", 0);
        PlayerPrefs.SetInt("Shield 3", 0);
        PlayerPrefs.SetString("LastGameTypePlayed", "Campaign");

        PlayerPrefs.SetString("Loading screen scene", GetTutorialScene(PlayerPrefs.GetInt("CurrentLevel")));
        return "LoadingScreenTutorial";
    }

    string ResetAll()
    {
        PlayerPrefs.DeleteAll();
        //debug13
        PlayerPrefs.SetInt("minTotalHealthPhase1", 8);
        PlayerPrefs.SetInt("maxTotalHealthPhase1", 21);
        PlayerPrefs.SetInt("minTotalHealthPhase2", 18);
        PlayerPrefs.SetInt("maxTotalHealthPhase2", 23);
        PlayerPrefs.SetInt("minTotalHealthPhase3", 11);
        PlayerPrefs.SetInt("maxTotalHealthPhase3", 38);
        PlayerPrefs.SetInt("minDelayBetweenWaves", 15);
        PlayerPrefs.SetInt("maxDelayBetweenWaves", 30);
        PlayerPrefs.SetInt("phase1Duration", 90);
        PlayerPrefs.SetInt("phase2Duration", 180);
        return "StartMenu";
    }

    string ContinueLevel()
    {
        if (PlayerPrefs.GetInt("WasDifficultySelected") == 0)
        {
            return "Difficulty";
        }
        else
        {
            int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
            //don't use SetCurrentLevel because it screws boost apparitions
            //SetCurrentLevel(currentLevel);
            if (currentLevel < 0)
                return "LoadingScreenTutorial";
            else
                return PlayerPrefs.GetInt("CurrentLevel").ToString();
        }
    }

    void EraseBoosts()
    {
        for (int idx = 1; idx <= 6; ++idx)
            PlayerPrefs.SetString("Boost" + idx.ToString(), "");
        PlayerPrefs.SetInt("BoostIdx", 0);
    }

    void ResetEndlessState()
    {
        PlayerPrefs.SetInt("CurrentScore", 0);
        PlayerPrefs.SetInt("PlayerHealthEndless", 6);
        PlayerPrefs.SetInt("PlayerAmmoEndless", 0);

        //boosts
        var boosts = boostsObj.GetBoosts();
        foreach (var boost in boosts)
            PlayerPrefs.SetInt(boost.InternalName + " Endless", 0);
        PlayerPrefs.SetInt("BoostIdxEndless", 0);

        //shields
        if (PlayerPrefs.GetInt("Bought Shield 1") == 1)
            PlayerPrefs.SetInt("Shield 1 Endless", 1);
        if (PlayerPrefs.GetInt("Bought Shield 2") == 1)
            PlayerPrefs.SetInt("Shield 2 Endless", 1);
        if (PlayerPrefs.GetInt("Bought Shield 3") == 1)
            PlayerPrefs.SetInt("Shield 3 Endless", 1);

        //elements on the map
        PlayerPrefs.SetInt("EndlessNrMobs", 0);
        PlayerPrefs.SetInt("EndlessNrBoostDrops", 0);
        PlayerPrefs.SetFloat("EndlessBoostExpiration", 0);
        PlayerPrefs.SetInt("EndlessNrFirestorms", 0);
        PlayerPrefs.SetInt("EndlessNrStopwatches", 0);
        PlayerPrefs.SetInt("EndlessHeartsNr", 0);

        //playtime
        PlayerPrefs.SetFloat("EndlessPreviousPlaytime", 0);
        PlayerPrefs.SetFloat("EndlessMobWavesLastSpawnMoment", -1);
    }
    void LoadEndlessStateIntoCurrent()
    {
        PlayerPrefs.SetInt("PlayerHealth", PlayerPrefs.GetInt("PlayerHealthEndless"));
        PlayerPrefs.SetInt("PlayerAmmo", PlayerPrefs.GetInt("PlayerAmmoEndless"));

        //boosts
        var boosts = boostsObj.GetBoosts();
        foreach (var boost in boosts)
            PlayerPrefs.SetInt(boost.InternalName, PlayerPrefs.GetInt(boost.InternalName + " Endless"));

        int boostIdx = PlayerPrefs.GetInt("BoostIdxEndless");
        PlayerPrefs.SetInt("BoostIdx", boostIdx);
        for (int idx = 1; idx <= boostIdx; ++idx)
            PlayerPrefs.SetString("Boost" + idx, PlayerPrefs.GetString("BoostEndless" + idx));

        //shields
        PlayerPrefs.SetInt("Shield 1", PlayerPrefs.GetInt("Shield 1 Endless"));
        PlayerPrefs.SetInt("Shield 2", PlayerPrefs.GetInt("Shield 2 Endless"));
        PlayerPrefs.SetInt("Shield 3", PlayerPrefs.GetInt("Shield 3 Endless"));

        //make sure this is set every time we load the campaign state
        PlayerPrefs.SetString("LastGameTypePlayed", "Endless");
    }
    void SaveCurrentStateIntoEndless()
    {
        PlayerPrefs.SetInt("PlayerHealthEndless", PlayerPrefs.GetInt("PlayerHealth"));
        PlayerPrefs.SetInt("PlayerAmmoEndless", PlayerPrefs.GetInt("PlayerAmmo"));

        //boosts
        var boosts = boostsObj.GetBoosts();
        foreach (var boost in boosts)
            PlayerPrefs.SetInt(boost.InternalName + " Endless", PlayerPrefs.GetInt(boost.InternalName));

        int boostIdx = PlayerPrefs.GetInt("BoostIdx");
        PlayerPrefs.SetInt("BoostIdxEndless", boostIdx);
        for (int idx = 1; idx <= boostIdx; ++idx)
            PlayerPrefs.SetString("BoostEndless" + idx, PlayerPrefs.GetString("Boost" + idx));

        //shields
        PlayerPrefs.SetInt("Shield 1 Endless", PlayerPrefs.GetInt("Shield 1"));
        PlayerPrefs.SetInt("Shield 2 Endless", PlayerPrefs.GetInt("Shield 2"));
        PlayerPrefs.SetInt("Shield 3 Endless", PlayerPrefs.GetInt("Shield 3"));
    }

    void ResetCampaignState()
    {
        PlayerPrefs.SetInt("PlayerHealthCampaign", 6);
        PlayerPrefs.SetInt("PlayerAmmoCampaign", g.MaxAmmo());
        
        //boosts
        var boosts = boostsObj.GetBoosts();
        foreach (var boost in boosts)
            PlayerPrefs.SetInt(boost.InternalName + " Campaign", 0);
        PlayerPrefs.SetInt("BoostIdxCampaign", 0);

        //shields
        if (PlayerPrefs.GetInt("Bought Shield 1") == 1)
            PlayerPrefs.SetInt("Shield 1 Campaign", 1);
        if (PlayerPrefs.GetInt("Bought Shield 2") == 1)
            PlayerPrefs.SetInt("Shield 2 Campaign", 1);
        if (PlayerPrefs.GetInt("Bought Shield 3") == 1)
            PlayerPrefs.SetInt("Shield 3 Campaign", 1);

        //consumable
        //don't do anything as the consumable is only visible in the tutorial and the campaign
        //so we can just leave this as is to indicate the last acquired consumable

        PlayerPrefs.SetFloat("CampaignPreviousPlaytime", 0);
        PlayerPrefs.SetFloat("CampaignMobWavesLastSpawnMoment", -1);
    }
    void LoadCampaignStateIntoCurrent()
    {
        PlayerPrefs.SetInt("PlayerHealth", PlayerPrefs.GetInt("PlayerHealthCampaign"));
        PlayerPrefs.SetInt("PlayerAmmo", PlayerPrefs.GetInt("PlayerAmmoCampaign"));

        //boosts
        var boosts = boostsObj.GetBoosts();
        foreach (var boost in boosts)
            PlayerPrefs.SetInt(boost.InternalName, PlayerPrefs.GetInt(boost.InternalName + " Campaign"));

        int boostIdx = PlayerPrefs.GetInt("BoostIdxCampaign");
        PlayerPrefs.SetInt("BoostIdx", boostIdx);
        for (int idx = 1; idx <= boostIdx; ++idx)
            PlayerPrefs.SetString("Boost" + idx, PlayerPrefs.GetString("BoostCampaign" + idx));

        //shields
        PlayerPrefs.SetInt("Shield 1", PlayerPrefs.GetInt("Shield 1 Campaign"));
        PlayerPrefs.SetInt("Shield 2", PlayerPrefs.GetInt("Shield 2 Campaign"));
        PlayerPrefs.SetInt("Shield 3", PlayerPrefs.GetInt("Shield 3 Campaign"));

        //consumable
        PlayerPrefs.SetString("Equipped consumable", PlayerPrefs.GetString("Equipped consumable Campaign"));

        //make sure this is set every time we load the campaign state
        PlayerPrefs.SetString("LastGameTypePlayed", "Campaign");
    }
    void SaveCurrentStateIntoCampaign()
    {
        PlayerPrefs.SetInt("PlayerHealthCampaign", PlayerPrefs.GetInt("PlayerHealth"));
        PlayerPrefs.SetInt("PlayerAmmoCampaign", PlayerPrefs.GetInt("PlayerAmmo"));

        //boosts
        var boosts = boostsObj.GetBoosts();
        foreach (var boost in boosts)
            PlayerPrefs.SetInt(boost.InternalName + " Campaign", PlayerPrefs.GetInt(boost.InternalName));

        int boostIdx = PlayerPrefs.GetInt("BoostIdx");
        PlayerPrefs.SetInt("BoostIdxCampaign", boostIdx);
        for (int idx = 1; idx <= boostIdx; ++idx)
            PlayerPrefs.SetString("BoostCampaign" + idx, PlayerPrefs.GetString("Boost" + idx));

        //shields
        PlayerPrefs.SetInt("Shield 1 Campaign", PlayerPrefs.GetInt("Shield 1"));
        PlayerPrefs.SetInt("Shield 2 Campaign", PlayerPrefs.GetInt("Shield 2"));
        PlayerPrefs.SetInt("Shield 3 Campaign", PlayerPrefs.GetInt("Shield 3"));

        //consumable
        PlayerPrefs.SetString("Equipped consumable Campaign", PlayerPrefs.GetString("Equipped consumable"));
    }
    void LoadTutorialState()
    {
        PlayerPrefs.SetInt("PlayerHealth", 6);
        PlayerPrefs.SetInt("PlayerAmmo", 0);

        //boosts
        var boosts = boostsObj.GetBoosts();
        foreach (var boost in boosts)
            PlayerPrefs.SetInt(boost.InternalName, 0);

        PlayerPrefs.SetInt("BoostIdx", 0);
        for (int idx = 1; idx <= 6; ++idx)
            PlayerPrefs.SetString("Boost" + idx, "");

        //shields
        PlayerPrefs.SetInt("Shield 1", 0);
        PlayerPrefs.SetInt("Shield 2", 0);
        PlayerPrefs.SetInt("Shield 3", 0);

        //consumable
        PlayerPrefs.SetString("Equipped consumable", "");
    }

    void Start()
    {
        string selectedScene = "";
        boostsObj = g.i.boosts;

        if (PlayerPrefs.GetString("Level") == "End")
        {
            if (PlayerPrefs.GetInt("Replaying finale") == 0)
            {
                PlayerPrefs.SetInt("MaxLevelWon", 30);

                //we set the player health to 0 so that the "New campaign" button will appear instead of "Continue campaign"
                PlayerPrefs.SetInt("PlayerHealth", 0);
                PlayerPrefs.SetInt("PlayerAmmo", 0);

                //we reset the portal charm and phase rings money in order to allow restarting the campaign from level 1
                if (PlayerPrefs.GetInt("Bought Phase ring 1") == 1)
                {
                    PlayerPrefs.SetInt("Bought Phase ring 1", 0);
                    PlayerPrefs.SetInt("Resources", PlayerPrefs.GetInt("Resources") + 50); //this price should be taken from the Shop scene somehow
                }

                if (PlayerPrefs.GetInt("Bought Phase ring 2") == 1)
                {
                    PlayerPrefs.SetInt("Bought Phase ring 2", 0);
                    PlayerPrefs.SetInt("Resources", PlayerPrefs.GetInt("Resources") + 225); //this price should be taken from the Shop scene somehow
                }

                if (PlayerPrefs.GetInt("Bought Portal charm") == 1)
                {
                    PlayerPrefs.SetInt("Bought Portal charm", 0);
                    PlayerPrefs.SetInt("Resources", PlayerPrefs.GetInt("Resources") + 1000); //this price should be taken from the Shop scene somehow
                }
            }

            selectedScene = "StartMenu";
        }

        //if we load a level for any reason and the finale was playing before, we return to the game mode before the finale
        //the finale isn't meant to be something you can interrupt and get back to
        //you press the Finale and you experience the whole thing right then and there
        if (PlayerPrefs.GetInt("Replaying finale") == 1)
        {
            PlayerPrefs.SetInt("Replaying finale", 0);

            //restore the state that was used before replaying the tutorial
            if (PlayerPrefs.GetString("LastGameTypePlayed") == "Endless")
                LoadEndlessStateIntoCurrent();
            else
                LoadCampaignStateIntoCurrent();
        }

        //the last game might have ended abruptly, so here we save the player stats as they were the last time any game type was played
        if (PlayerPrefs.GetInt("Replaying tutorial") == 0 && PlayerPrefs.GetInt("Replaying finale") == 0)
        {
            if (PlayerPrefs.GetString("LastGameTypePlayed") == "Endless")
                SaveCurrentStateIntoEndless();
            else if (PlayerPrefs.GetString("LastGameTypePlayed") == "Campaign")
                SaveCurrentStateIntoCampaign();
        }

        if (PlayerPrefs.GetString("Handedness") == "" )
        {
            PlayerPrefs.SetString("Handedness", "Right");
        }

        if ( PlayerPrefs.GetInt("Start") == 1 )
        {
            if( PlayerPrefs.GetInt("BasicOptionsSelected") == 0 )
            {
                selectedScene = "BasicOptions";
            }
            else
            {
                //we reset the flags here as a safety measure, just in case flags were not reset during a previous transition
                ResetFlags();
                
                EraseBoosts();
                if (PlayerPrefs.GetInt("Tutorial done") == 0)
                    selectedScene = StartTutorial();
                else
                    selectedScene = ContinueLevel();
            }
        }

        if (PlayerPrefs.GetInt("DifficultySelection") == 1) //difficulty just chosen
        {
            selectedScene = StartNewGame();
        }

        if (PlayerPrefs.GetString("Start menu") == "Quit")
        {
            QuitGame();
            return;
        }

        if (PlayerPrefs.GetString("Start menu") == "New")
        {
            EraseBoosts();
            if ( PlayerPrefs.GetInt("Tutorial done") == 0 )
                selectedScene = StartTutorial();
            else
            {
                if (PlayerPrefs.GetInt("Replaying tutorial") == 1)
                {
                    //just go to Level Win, which handles this case
                    PlayerPrefs.SetString("Level", "Win");
                }
                else
                {
                    selectedScene = StartNewGame();
                }
            }
        }
        
        if (PlayerPrefs.GetString("Start menu") == "Continue")
        {
            int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
            if (currentLevel > 0)
            {
                LoadCampaignStateIntoCurrent();
                if (PlayerPrefs.GetInt("Campaign level was active") == 1)
                {
                    PlayerPrefs.SetInt("Load last state of the level", 1);
                    selectedScene = ContinueLevel();
                }
                else
                {
                    if (PlayerPrefs.GetInt("CurrentLevel") == 1) //don't show shop before level 1
                    {
                        selectedScene = ContinueLevel();
                    }
                    else
                    {
                        //load the shop and the boosts menu if necessary
                        selectedScene = "Shop";
                    }
                }
            }
            else if( currentLevel == 0 )
            {
                PlayerPrefs.SetInt("CurrentLevel", 1);
                LoadCampaignStateIntoCurrent();
                selectedScene = ContinueLevel();
            }
            else
                selectedScene = RestartTutorialLevel();
        }

        if (PlayerPrefs.GetInt("Replay tutorial") == 1)
        {
            int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
            if(currentLevel >= 0)
                PlayerPrefs.SetInt("Level after tutorial replay", currentLevel);
            else
                PlayerPrefs.SetInt("Level after tutorial replay", 0);
            //remember boosts and reset them
            LoadTutorialState();
            PlayerPrefs.SetInt("Replaying tutorial", 1);
            PlayerPrefs.SetInt("Replay tutorial", 0);

            selectedScene = StartTutorial();
        }

        if( PlayerPrefs.GetInt("Replay finale") == 1 )
        {
            PlayerPrefs.SetInt("Replaying finale", 1);
            ClearBoosts();
            PlayerPrefs.SetInt("PlayerHealth", 6);
            selectedScene = "30b";
        }

        if (PlayerPrefs.GetString("Start menu") == "Editor 1")
        {
            ClearBoosts();
            selectedScene = "Editor 1";
        }

        if (PlayerPrefs.GetString("Start menu") == "Reset")
            selectedScene = ResetAll();

        int testLevel = 0;
        if( int.TryParse(PlayerPrefs.GetString("Start menu"), out testLevel) )
        {
            selectedScene = StartTestLevel(testLevel);
        }

        if (PlayerPrefs.GetString("Level") == "Quit")
        {
            PlayerPrefs.SetInt("PlayerHealth", 6);
            PlayerPrefs.SetInt("PlayerAmmo", g.MaxAmmo());
            selectedScene = ContinueLevel();
        }

        if (PlayerPrefs.GetString("Level") == "Restart")
            selectedScene = RestartTutorialLevel();

        if (PlayerPrefs.GetString("Level") == "Skip tutorial")
        {
            PlayerPrefs.SetInt("Tutorial done", 1);
            if (PlayerPrefs.GetInt("Replaying tutorial") == 1)
            {
                //act as if we just won the last tutorial level
                PlayerPrefs.SetInt("CurrentLevel", -1);
                //just go to Level Win, which handles this case
                PlayerPrefs.SetString("Level", "Win");
            }
            else
                selectedScene = StartNewGame();
        }

        if (PlayerPrefs.GetString("Level") == "Win")
        {
            int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
            
            if (currentLevel > PlayerPrefs.GetInt("MaxLevelWon"))
                PlayerPrefs.SetInt("MaxLevelWon", currentLevel);

            if (currentLevel < 0)
            {
                //tutorial
                selectedScene = AdvanceToNextTutorial();
                if(selectedScene == "Continue after replaying tutorial")
                {
                    PlayerPrefs.SetInt("Replaying tutorial", 0);
                    PlayerPrefs.SetInt("Playing tutorial", 0);
                    PlayerPrefs.SetInt("Just replayed tutorial", 1);
                    currentLevel = PlayerPrefs.GetInt("Level after tutorial replay");
                    PlayerPrefs.SetInt("CurrentLevel", currentLevel);

                    //restore the state that was used before replaying the tutorial
                    if (PlayerPrefs.GetString("LastGameTypePlayed") == "Endless")
                        LoadEndlessStateIntoCurrent();
                    else
                        LoadCampaignStateIntoCurrent();

                    selectedScene = "StartMenu";
                }
            }
            else if(currentLevel >= 0 )
            {
                //regular levels
                selectedScene = AdvanceToNextLevel();
            }   
        }

        if ( PlayerPrefs.GetInt("Shop") == 1) //shop just appeared
        {
            if (BoostsAppears())
            {
                if (PlayerPrefs.GetInt("CatchUpBoosts") > 0)
                    PlayerPrefs.SetInt("No Downpayment", 1);
                selectedScene = "Boosts";
            }
            else
            {
                //don't use SetCurrentLevel because it screws boost apparitions
                //SetCurrentLevel(PlayerPrefs.GetInt("CurrentLevel"));
                PlayerPrefs.SetString("Loading screen scene", PlayerPrefs.GetInt("CurrentLevel").ToString());
                selectedScene = "LoadingScreenLevel";
            }
        }

        if (PlayerPrefs.GetInt("Boosts") == 1) //boosts just appeared
        {
            int catchUpBoosts = PlayerPrefs.GetInt("CatchUpBoosts");
            if (catchUpBoosts > 0)
            {
                --catchUpBoosts;
                PlayerPrefs.SetInt("CatchUpBoosts", catchUpBoosts);
            }

            if (catchUpBoosts > 0)
            {
                selectedScene = "Boosts";
                PlayerPrefs.SetInt("No Downpayment", 1);
            }
            else
            {
                //don't use SetCurrentLevel because it screws boost apparitions
                //SetCurrentLevel(PlayerPrefs.GetInt("CurrentLevel"));
                PlayerPrefs.SetString("Loading screen scene", PlayerPrefs.GetInt("CurrentLevel").ToString());
                selectedScene = "LoadingScreenLevel";
            }
        }

        if( PlayerPrefs.GetInt("New endless") == 1 )
        {
            ResetEndlessState();
            LoadEndlessStateIntoCurrent();
            PlayerPrefs.SetString("Loading screen scene", PlayerPrefs.GetString("New endless scene"));
            selectedScene = "LoadingScreenEndless";
        }

        if (PlayerPrefs.GetInt("Continue endless") == 1)
        {
            LoadEndlessStateIntoCurrent();
            PlayerPrefs.SetString("Loading screen scene", PlayerPrefs.GetString("New endless scene"));
            selectedScene = "LoadingScreenEndless";
        }

        ResetFlags();
        SceneManager.LoadScene(selectedScene);
    }
    void ResetFlags()
    {
        PlayerPrefs.SetInt("Start", 0);
        PlayerPrefs.SetString("Start menu", "");
        PlayerPrefs.SetString("Level", "");
        PlayerPrefs.SetInt("Shop", 0);
        PlayerPrefs.SetInt("Boosts", 0);
        PlayerPrefs.SetInt("Replay tutorial", 0);
        PlayerPrefs.SetInt("Replay finale", 0);
        PlayerPrefs.SetInt("New endless", 0);
        PlayerPrefs.SetInt("Continue endless", 0);
        PlayerPrefs.SetInt("DifficultySelection", 0);
    }
}
