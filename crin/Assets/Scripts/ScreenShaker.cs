﻿using System.Collections;
using UnityEngine;

public class ScreenShaker : MonoBehaviour
{
    public float ShakeDurationStart = 0;
    public float ShakeDurationEnd = 0;
    public float ShakeSizeStart = 0;
    public float ShakeSizeEnd = 0;
    public float IncreaseDuration = 0;
    float ShakeDuration = 0;
    float ShakeSize = 0;

    void Start ()
    {
        ShakeDuration = ShakeDurationStart;
        ShakeSize = ShakeSizeStart;
        StartCoroutine(Increase());
        StartCoroutine(Shake());
        
    }
    IEnumerator Increase()
    {
        float startTime = Time.time;
        float endTime = startTime + IncreaseDuration;
        while (endTime >= Time.time)
        {
            ShakeDuration = Mathf.Lerp(ShakeDurationStart, ShakeDurationEnd, (Time.time - startTime) / IncreaseDuration);
            ShakeSize = Mathf.Lerp(ShakeSizeStart, ShakeSizeEnd, (Time.time - startTime) / IncreaseDuration);
            yield return null;
        }
        yield return null;
    }

    IEnumerator Shake()
    {
        var rectTrans = gameObject.GetComponent<RectTransform>();
        Vector2 origPos = rectTrans.anchoredPosition;
        while ( true )
        {
            //move left
            var position = rectTrans.anchoredPosition;
            {
                float startTime = Time.time;
                float endTime = startTime + ShakeDuration / 2;
                while (endTime >= Time.time)
                {
                    float delta = Mathf.Lerp(0, ShakeSize, (Time.time - startTime) / ShakeDuration / 2);
                    rectTrans.anchoredPosition = new Vector3(position.x - delta, position.y);
                    yield return null;
                }
            }

            //move right
            position = rectTrans.anchoredPosition;
            {
                float startTime = Time.time;
                float endTime = startTime + ShakeDuration / 2;
                while (endTime >= Time.time)
                {
                    float delta = Mathf.Lerp(0, ShakeSize, (Time.time - startTime) / ShakeDuration / 2);
                    rectTrans.anchoredPosition = new Vector3(position.x + delta, position.y);
                    yield return null;
                }
            }
        }
    }
}
