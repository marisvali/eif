﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchingPlayer : MonoBehaviour
{
    private bool touching = false;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.name == "ClickArea")
            return;

        if (collider.tag == "Player")
        {
            touching = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.name == "ClickArea")
            return;

        if (collider.tag == "Player")
        {
            touching = false;
        }
    }

    public bool IsTouching()
    {
        return touching;
    }
}