using UnityEngine;

public class SetupCanvasPortrait : MonoBehaviour
{
    private void OnEnable()
    {
        Canvas canvas = GetComponent<Canvas>();
        float factorWidth = (float)Screen.width / 1080.0f;
        float factorHeight = (float)Screen.height / 1920.0f;
        canvas.scaleFactor = Mathf.Min(factorWidth, factorHeight);
    }
}
