﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CampaignState : MonoBehaviour
{
    class MobData
    {
        public string tag;
        public Vector3 pos;
        public int health;
    }
    class CampaignStateData
    {
        public Dictionary<string, int> Ints = new Dictionary<string, int>();
        public Dictionary<string, string> Strings = new Dictionary<string, string>();
        public Dictionary<string, float> Floats = new Dictionary<string, float>();
    }
    MobData GetMobData(MobExpiration mob)
    {
        MobData mobData = new MobData();
        if (mob.tag == "RejectionAux")
            mobData.tag = mob.gameObject.name;
        else
            mobData.tag = mob.tag;
        mobData.pos = mob.transform.position;
        mobData.health = mob.GetComponent<MobHealth>().GetHealth();
        return mobData;
    }
    void SpawnMob(MobData mob)
    {
        if( mob.tag.Contains("Rejection") )
        {
            GameObject spawnedMob = (GameObject)Instantiate(Resources.Load("Prefabs/Mobs/" + mob.tag), mob.pos, Quaternion.identity);
            spawnedMob.name = mob.tag;
            spawnedMob.GetComponent<MobHealth>().startHealth = mob.health;
        }
        else
        {
            
        }
    }
    public void LoadCampaignState()
    {
        //destroy mobs and spawners
        foreach (var mob in g.i.mobs)
            if( mob.gameObject != null && mob.GetComponent<RejectionAI>() == null)
                Object.DestroyImmediate(mob.gameObject);

        //spawn mobs
        int nrMobs = PlayerPrefs.GetInt("CampaignNrMobs");
        List<MobData> mobs = new List<MobData>();
        for (int idx = 0; idx < nrMobs; ++idx)
        {
            MobData mob = new MobData();
            mob.tag = PlayerPrefs.GetString("CampaignMobTag" + idx);
            mob.pos.x = PlayerPrefs.GetFloat("CampaignMobPosX" + idx);
            mob.pos.y = PlayerPrefs.GetFloat("CampaignMobPosY" + idx);
            mob.pos.z = PlayerPrefs.GetFloat("CampaignMobPosZ" + idx);
            mob.health = PlayerPrefs.GetInt("CampaignMobHealth" + idx);
            mobs.Add(mob);
        }

        foreach (var mob in mobs)
            SpawnMob(mob);

        //spawn spawners
        int nrSpawners = PlayerPrefs.GetInt("CampaignNrSpawners");
        for (int idx = 0; idx < nrSpawners; ++idx)
        {
            string prefix = "CampaignSpawner" + idx.ToString();
            Vector3 pos;
            pos.x = PlayerPrefs.GetFloat(prefix + "PoxX");
            pos.y = PlayerPrefs.GetFloat(prefix + "PoxY");
            pos.z = PlayerPrefs.GetFloat(prefix + "PoxZ");
            MobSpawner spawner = ((GameObject)Instantiate(Resources.Load("Prefabs/Mobs/MobSpawner"), pos, Quaternion.identity)).GetComponent<MobSpawner>();
            spawner.NrDepressions = PlayerPrefs.GetInt(prefix + "NrDepressions");
            spawner.NrSocialAnxieties = PlayerPrefs.GetInt(prefix + "NrSocialAnxieties");
            spawner.NrAngers = PlayerPrefs.GetInt(prefix + "NrAngers");
            spawner.NrParanoias = PlayerPrefs.GetInt(prefix + "NrParanoias");
            spawner.NrTerrors = PlayerPrefs.GetInt(prefix + "NrTerrors");
            spawner.timeUntilAppearance = PlayerPrefs.GetFloat(prefix + "TimeUntilAppearance");
            spawner.timeBetweenSpawns = PlayerPrefs.GetFloat(prefix + "TimeBetweenSpawns");
            spawner.timeAfterLastSpawn = PlayerPrefs.GetFloat(prefix + "TimeAfterLastSpawn");
            spawner.stopped = PlayerPrefs.GetInt(prefix + "Stopped") != 0;
            spawner.owner = (MobSpawner.OwnerEnum)PlayerPrefs.GetInt(prefix + "Owner");
        }

        //resources
        int nrResources = PlayerPrefs.GetInt("CampaignResourcesNr");
        for (int idx = 0; idx < nrResources; ++idx)
        {
            string prefix = "CampaignResource" + idx.ToString();
            Vector3 pos;
            pos.x = PlayerPrefs.GetFloat(prefix + "PosX");
            pos.y = PlayerPrefs.GetFloat(prefix + "PosY");
            pos.z = PlayerPrefs.GetFloat(prefix + "PosZ");
            GameObject resourceDrop = g.i.spawnDrops.InstantiateResourceDrop(pos);
            resourceDrop.GetComponent<Resource>().amount = PlayerPrefs.GetInt(prefix + "Amount");
        }

        //hearts
        int nrHearts = PlayerPrefs.GetInt("CampaignHeartsNr");
        for (int idx = 0; idx < nrHearts; ++idx)
        {
            string prefix = "CampaignHeart" + idx.ToString();
            Vector3 pos;
            pos.x = PlayerPrefs.GetFloat(prefix + "PosX");
            pos.y = PlayerPrefs.GetFloat(prefix + "PosY");
            pos.z = PlayerPrefs.GetFloat(prefix + "PosZ");
            g.i.spawnDrops.InstantiateHealthDrop(pos);
        }

        //update rejection to refer to the auxiliaries that we loaded
        var rejection = g.i.mobs.FirstOrDefault(x => x.GetComponent<RejectionAI>());
        if (rejection != null)
            rejection.GetComponent<RejectionAI>().UpdateRejectionAuxConnections();
    }

    private void Start()
    {
        frameIdx = 10000;
    }

    private int frameIdx = 10000;
    void Update()
    {
        if (PlayerPrefs.GetInt("Replaying finale") != 0)
            return;

        ++frameIdx;
        if (frameIdx < 60)
            return;
        frameIdx = 0;

        //save state
        CampaignStateData stateData = new CampaignStateData();
        AddMobsData(ref stateData);
        AddMobSpawnersData(ref stateData);
        AddDropsData(ref stateData);

        foreach (var val in stateData.Ints)
            PlayerPrefs.SetInt(val.Key, val.Value);
        foreach (var val in stateData.Strings)
            PlayerPrefs.SetString(val.Key, val.Value);
        foreach (var val in stateData.Floats)
            PlayerPrefs.SetFloat(val.Key, val.Value);
    }

    void AddMobsData(ref CampaignStateData stateData)
    {
        List<MobData> mobs = new List<MobData>();

        foreach (var mob in g.i.mobs)
        {
            if ((mob != null) && !mob.GetComponent<MobExpiration>().Expired() && mob.GetComponent<RejectionAI>() == null)
                {
                    bool fictional = false;
                    if (mob.tag == "Depression" && mob.GetComponent<DepressionAI>().IsFictional())
                        fictional = true;
                    if (mob.tag == "SocialAnxiety" && mob.GetComponent<SocialAnxietyAI>().IsFictional())
                        fictional = true;
                    if (mob.tag == "Anger" && mob.GetComponent<AngerAI>().IsFictional())
                        fictional = true;
                    if (!fictional)
                        mobs.Add(GetMobData(mob.GetComponent<MobExpiration>()));
                }
        }

        //add rejection auxiliaries which are expired while they are retreated but still alive
        foreach (var mob in g.i.mobs)
            if(mob.GetComponent<RejectionAuxAI>() != null
                && mob.GetComponent<MobExpiration>().Expired()
                && !mob.GetComponent<RejectionAuxAI>().IsDead() )
                mobs.Add(GetMobData(mob.GetComponent<MobExpiration>()));

        stateData.Ints["CampaignNrMobs"] = mobs.Count;
        for (int idx = 0; idx < mobs.Count; ++idx)
        {
            stateData.Strings["CampaignMobTag" + idx] = mobs[idx].tag;
            stateData.Floats["CampaignMobPosX" + idx] = mobs[idx].pos.x;
            stateData.Floats["CampaignMobPosY" + idx] = mobs[idx].pos.y;
            stateData.Floats["CampaignMobPosZ" + idx] = mobs[idx].pos.z;
            stateData.Ints["CampaignMobHealth" + idx] = mobs[idx].health;
        }
    }

    void AddMobSpawnersData(ref CampaignStateData stateData)
    {
        // var spawners = GetAllMobs.GetOnlySpawners();
        // int nrAliveSpawners = 0;
        // foreach( var spawnerObj in spawners )
        //     if (spawnerObj != null)
        //     {
        //         MobSpawner spawner = spawnerObj.GetComponent<MobSpawner>();
        //         int nrDepressions = 0;
        //         int nrSocialAnxieties = 0;
        //         int nrAngers = 0;
        //         int nrParanoias = 0;
        //         int nrTerrors = 0;
        //         List<string> mobsLeft = spawner.GetMobsLeftToSpawn();
        //         foreach (string mob in mobsLeft)
        //         {
        //             if (mob == "Depression")
        //                 ++nrDepressions;
        //             if (mob == "SocialAnxiety")
        //                 ++nrSocialAnxieties;
        //             if (mob == "Anger")
        //                 ++nrAngers;
        //             if (mob == "Paranoia")
        //                 ++nrParanoias;
        //             if (mob == "Terror")
        //                 ++nrTerrors;
        //         }
                
        //         string prefix = "CampaignSpawner" + nrAliveSpawners.ToString();
        //         stateData.Ints[prefix + "NrDepressions"] = nrDepressions;
        //         stateData.Ints[prefix + "NrSocialAnxieties"] = nrSocialAnxieties;
        //         stateData.Ints[prefix + "NrAngers"] = nrAngers;
        //         stateData.Ints[prefix + "NrParanoias"] = nrParanoias;
        //         stateData.Ints[prefix + "NrTerrors"] = nrTerrors;
        //         stateData.Floats[prefix + "TimeUntilAppearance"] = spawner.GetTimeLeftBeforeSpawning();
        //         stateData.Floats[prefix + "TimeBetweenSpawns"] = spawner.timeBetweenSpawns;
        //         stateData.Floats[prefix + "TimeAfterLastSpawn"] = spawner.timeAfterLastSpawn;
        //         stateData.Floats[prefix + "PoxX"] = spawner.transform.position.x;
        //         stateData.Floats[prefix + "PoxY"] = spawner.transform.position.y;
        //         stateData.Floats[prefix + "PoxZ"] = spawner.transform.position.z;
        //         stateData.Ints[prefix + "Stopped"] = spawner.stopped ? 1 : 0;
        //         stateData.Ints[prefix + "Owner"] = (int)spawner.owner;
        //         ++nrAliveSpawners;
        //     }
        // stateData.Ints["CampaignNrSpawners"] = nrAliveSpawners;
    }

    void AddDropsData(ref CampaignStateData stateData)
    {
        var allDrops = g.i.spawnDrops.GetAllDrops();
        List<GameObject> resources = new List<GameObject>();
        foreach(var drop in allDrops)
            if (drop.tag == "Resource")
                resources.Add(drop);

        stateData.Ints["CampaignResourcesNr"] = resources.Count;
        for (int idx = 0; idx < resources.Count; ++idx)
        {
            string prefix = "CampaignResource" + idx.ToString();
            stateData.Floats[prefix + "PosX"] = resources[idx].gameObject.transform.position.x;
            stateData.Floats[prefix + "PosY"] = resources[idx].gameObject.transform.position.y;
            stateData.Floats[prefix + "PosZ"] = resources[idx].gameObject.transform.position.z;
            stateData.Ints[prefix + "Amount"] = resources[idx].gameObject.GetComponent<Resource>().amount;
        }

        List<GameObject> hearts = new List<GameObject>();
        foreach (var drop in allDrops)
            if (drop.tag == "Health")
                hearts.Add(drop);
        
        PlayerPrefs.SetInt("CampaignHeartsNr", hearts.Count);
        for (int idx = 0; idx < hearts.Count; ++idx)
        {
            string prefix = "CampaignHeart" + idx.ToString();
            stateData.Floats[prefix + "PosX"] = hearts[idx].transform.position.x;
            stateData.Floats[prefix + "PosY"] = hearts[idx].transform.position.y;
            stateData.Floats[prefix + "PosZ"] = hearts[idx].transform.position.z;
        }
    }
}
