﻿using UnityEngine;

public class FinishedGame : MonoBehaviour
{
	void Start()
    {
        PlayerPrefs.SetInt("FinishedCampaign", 1);
        PlayerPrefs.SetFloat("StartMenuMusicTime", 0);
    }
}