﻿using UnityEngine;

public class MoveStraightTo : MonoBehaviour
{
    private bool targetSet = false;
    private Vector2 target;
    private float speed = 0;

    void FixedUpdate()
    {
        if (!targetSet)
            return;
        if (AtTarget())
            return;

        //compute a position closer to the target
        Rigidbody2D body = GetComponent<Rigidbody2D>();
        body.velocity = Vector3.zero;
        body.angularVelocity = 0;
        Vector2 dir = target - body.position;
        Vector2 movement = dir.normalized * speed * Time.deltaTime;
        Vector2 newPosition = body.position + movement;

        //check if the new position overlaps unwalkable nodes in the AStar graph
        bool validPosition = true;
        Bounds bounds = new Bounds();
        bounds.extents = new Vector3(0.1f, 0.1f, 1000);
        bounds.center = new Vector3(newPosition.x, newPosition.y);
        var nodes = g.i.gridGraph.GetNodesInRegion(bounds);
        foreach (var node in nodes)
        {
            if (!node.Walkable)
            {
                validPosition = false;
                break;
            }   
        }
        
        if ( validPosition )
        {
            //move to the new position if it is valid
            body.MovePosition(newPosition);
        }
        else
        {
            //end movement if the new position is invalid
            target = body.position;
        }
    }
    public void SetTarget(Vector2 target, float speed)
    {
        targetSet = true;
        this.target = target;
        this.speed = speed;
    }
    public bool AtTarget()
    {
        Rigidbody2D body = GetComponent<Rigidbody2D>();
        Vector2 dir = target - body.position;
        return dir.magnitude < 50;
    }
    public void Disable()
    {
        targetSet = false;
    }
}
