﻿using UnityEngine;
// Note this line, if it is left out, the script won't know that the class 'Path' exists and it will throw compiler errors
// This line should always be present at the top of scripts which use pathfinding
using Pathfinding;
public class Pathing : WorldObject
{
    // The point to move to
    GameObject target = null;
    public bool zombie = false;
    public bool disabled = false;
    private Seeker seeker;
    // The calculated path
    public Path path;
    // The AI's speed in meters per second
    public float speed = 10.0f;
    // The max distance from the AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance = 40;
    // The waypoint we are currently moving towards
    private int currentWaypoint = 0;
    // How often to recalculate the path (in seconds)
    public float repathRate = 0.5f;
    private float lastRepath = -9999;
    bool doRepath = false;
    System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
    private float lastUpdateTime = 0;
    public void Start()
    {
        seeker = GetComponent<Seeker>();
        timer.Start();
        lastUpdateTime = timer.ElapsedMilliseconds;
    }
    public void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            //get the point in the path that is closest to our current position
            bool first = true;
            double minDist = 0;
            int minDistIdx = 0;
            for( int idx = 0; idx < path.vectorPath.Count; ++idx )
            {
                Vector3 pt = new Vector3(path.vectorPath[idx].x, path.vectorPath[idx].y, transform.position.z);
                double dist = (pt - transform.position).sqrMagnitude;
                if( first || dist < minDist )
                {
                    minDistIdx = idx;
                    minDist = dist;
                    first = false;
                }
            }
            currentWaypoint = minDistIdx;
        }
    }

    public void SetTarget(GameObject obj)
    {
        target = obj;
        doRepath = true;
    }
    protected override void UpdateImpl()
    {
        if (disabled)
            return;

        if (zombie)
        {
            return;
        }

        if (target == null)
        {
            return;
        }

        Rigidbody2D targetBody = target.GetComponent<Rigidbody2D>();
        if (Time.time - lastRepath > repathRate)
            doRepath = true;

        if (doRepath && seeker.IsDone())
        {
            lastRepath = Time.time + Random.value * repathRate * 0.5f;
            // Start a new path to the targetPosition, call the the OnPathComplete function
            // when the path has been calculated (which may take a few frames depending on the complexity)
            seeker.StartPath(transform.position, targetBody.position, OnPathComplete);
            doRepath = false;
            path = null;
        }
        if (path == null)
        {
            // We have no path to follow yet, so don't do anything
            return;
        }
        
        if (currentWaypoint == path.vectorPath.Count)
        {
            return;
        }
        
        //it's important that we iterate here, otherwise for big speeds we might get
        //a stutter in the motion
        while (true)
        {
            Vector3 wp = new Vector3(path.vectorPath[currentWaypoint].x, path.vectorPath[currentWaypoint].y, transform.position.z);
            double dist = (transform.position - wp).sqrMagnitude;
            if (dist < nextWaypointDistance * nextWaypointDistance)
            {
                currentWaypoint++;
                if (currentWaypoint == path.vectorPath.Count)
                    return;
            }
            else
                break;
        }

        // Direction to the next waypoint
        Vector3 dir = (new Vector3(path.vectorPath[currentWaypoint].x, path.vectorPath[currentWaypoint].y, transform.position.z) - transform.position).normalized;

        if (dir.x < 0)
            transform.localEulerAngles = new Vector3(0, 180, 0);
        else
            transform.localEulerAngles = new Vector3(0, 0, 0);

        dir *= speed * Time.deltaTime * 10;
        transform.position += dir;
    }

    public void Repath()
    {
        doRepath = true;
    }

    public bool AtTarget()
    {
        if (disabled || zombie || target == null)
            return true;
        if (path != null && currentWaypoint == path.vectorPath.Count)
            return true;
        return false;
    }
}