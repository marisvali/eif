﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayAmmo : MonoBehaviour
{
    int ammo = -1;

	void Update ()
    {
        int currentAmmo = PlayerPrefs.GetInt("PlayerAmmo");
        if (ammo != currentAmmo)
        {
            Text text = GetComponent<Text>();
            text.text = currentAmmo.ToString();
            ammo = currentAmmo;
        }
    }
}