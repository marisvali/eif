﻿using UnityEngine;

public class ResourceCollector : MonoBehaviour
{    
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Resource")
        {
            collider.gameObject.SetActive(false);
        }
    }
}
