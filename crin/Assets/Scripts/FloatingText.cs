﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour
{
    public Color negativeColor;
    public Color positiveColor;
    public AudioClip positiveAudio;
    public AudioClip negativeAudio;

    public void Initialize(int amount)
    {
        Text text = transform.GetComponentInChildren<Text>();
        if (amount < 0)
        {
            text.color = negativeColor;
            text.text = amount.ToString();
            GetComponent<AudioSource>().clip = negativeAudio;
            GetComponent<AudioSource>().Play();
        }
        else
        {
            text.color = positiveColor;
            text.text = "+" + amount.ToString();
            GetComponent<AudioSource>().clip = positiveAudio;
            GetComponent<AudioSource>().Play();
        }
    }

    public void Initialize(string message)
    {
        Text text = transform.GetComponentInChildren<Text>();
        text.color = positiveColor;
        text.text = message;
        GetComponent<AudioSource>().clip = positiveAudio;
        GetComponent<AudioSource>().Play();
    }
}