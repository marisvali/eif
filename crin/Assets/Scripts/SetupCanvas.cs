﻿using UnityEngine;

public class SetupCanvas : MonoBehaviour
{
    private void OnEnable()
    {
        Canvas canvas = GetComponent<Canvas>();
        float factorWidth;
        float factorHeight;
        if (g.Portrait())
        {
            factorWidth = (float)Screen.height / 1920.0f;
            factorHeight = (float)Screen.width / 1080.0f;
        }
        else
        {
            factorWidth = (float)Screen.width / 1920.0f;
            factorHeight = (float)Screen.height / 1080.0f;    
        }
        
        canvas.scaleFactor = Mathf.Min(factorWidth, factorHeight);
    }
}
