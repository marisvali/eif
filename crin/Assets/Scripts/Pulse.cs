﻿using UnityEngine;
using System.Collections;
public class Pulse : MonoBehaviour
{
    public float transitionTime = 0;
    public float peakDelay = 0;
    public float percentGrowth = 0;
    public bool alternateUpDown = true;
    public bool autoStart = false;
    public float totalPulseTime = 0;
    
    private Vector3 initialScale;

    void Awake()
    {
        initialScale = transform.localScale;
    }

    void OnEnable()
    {
        if(autoStart)
        {
            StartPulsing();
        }
    }
    
    void OnDisable()
    {
        if(autoStart)
        {
            StopPulsing();
        }
    }

    public void StartPulsing()
    {
        StartCoroutine(Transition(true));
        if(totalPulseTime > 0)
            StartCoroutine(StopPulsingAfter(totalPulseTime));
    }
    
    public void StopPulsing()
    {
        StopAllCoroutines();
        transform.localScale = initialScale;
    }

    IEnumerator StopPulsingAfter(float duration)
    {
        yield return new WaitForSeconds(duration);
        StopPulsing();
    }

    IEnumerator Transition(bool goingUp)
    {
        float startTime = Time.time;
        float endTime = startTime + transitionTime;
        float growth = 0;
        
        while (endTime >= Time.time)
        {
            float start = 0;
            float end = 0;
            if( goingUp )
            {
                start = 1.0f;
                end = 1.0f + percentGrowth / 100.0f;
            }
            else
            {
                start = 1.0f + percentGrowth / 100.0f;
                end = 1.0f;
            }

            growth = Mathf.Lerp(start, end, (Time.time - startTime) / transitionTime);
            transform.localScale = initialScale * growth;
            yield return null;
        }

        yield return new WaitForSeconds(peakDelay);
        if( alternateUpDown )
        {
            yield return Transition(!goingUp);
        }
        else
        {
            transform.localScale = initialScale;
            yield return Transition(true);
        }
    }
}