﻿using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 0;
    public bool alive = true;
    private int health = 0;
    private bool updateUI = false;
    
    public void Hit(int damage)
    {
        updateUI = true;

        //check if we should take away shield instead of decreasing health
        if (health == 6 && PlayerPrefs.GetInt("Shield 1") == 1)
        {
            PlayerPrefs.SetInt("Shield 1", 0);
            return;
        }
        if (health == 4 && PlayerPrefs.GetInt("Shield 2") == 1)
        {
            PlayerPrefs.SetInt("Shield 2", 0);
            return;
        }
        if (health == 2 && PlayerPrefs.GetInt("Shield 3") == 1)
        {
            PlayerPrefs.SetInt("Shield 3", 0);
            return;
        }

        var newHealth = health - damage;
        if (newHealth < 0)
            newHealth = 0;
        health = newHealth;
        if ( health == 0 )
            alive = false;
        PlayerPrefs.SetInt("PlayerHealth", health);

        PlayerPrefs.SetInt("Hits in the last level", PlayerPrefs.GetInt("Hits in the last level") + 1);
    }

    public void AddHealth()
    {
        if (health >= maxHealth && PlayerPrefs.GetInt("Shield 1") == 1)
            return;
        updateUI = true;

        //check if we should give shield instead of increasing health
        if (health == 6 && PlayerPrefs.GetInt("Bought Shield 1") == 1 && PlayerPrefs.GetInt("Shield 1") == 0)
        {
            PlayerPrefs.SetInt("Shield 1", 1);
            return;
        }
        else if (health == 4 && PlayerPrefs.GetInt("Bought Shield 2") == 1 && PlayerPrefs.GetInt("Shield 2") == 0)
        {
            PlayerPrefs.SetInt("Shield 2", 1);
            return;
        }
        else if (health == 2 && PlayerPrefs.GetInt("Bought Shield 3") == 1 && PlayerPrefs.GetInt("Shield 3") == 0)
        {
            PlayerPrefs.SetInt("Shield 3", 1);
            return;
        }
        else
            ++health;
        if (health > 0)
            alive = true;
        PlayerPrefs.SetInt("PlayerHealth", health);
    }

    public int GetHealth()
    {
        return health;
    }

    public void Set(int val)
    {
        updateUI = true;
        health = val;
        alive = health > 0;
        PlayerPrefs.SetInt("PlayerHealth", health);
    }

    public bool UINeedsUpdating()
    {
        return updateUI;
    }

    public void ResetUpdateFlag()
    {
        updateUI = false;
    }
}
