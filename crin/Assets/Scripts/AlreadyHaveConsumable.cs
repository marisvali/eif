﻿using UnityEngine;
using UnityEngine.UI;

public class AlreadyHaveConsumable : MonoBehaviour
{
    public Text text;
    
	public void SetText(string equippedConsumable, string potentialConsumable)
    {
        text.text = "You already have a consumable, <b>" + equippedConsumable + "</b>.\nDo you want to replace it with <b>" + potentialConsumable + "</b>? ";
    }
}