﻿using UnityEngine;

public class SortingOrderUpdater : MonoBehaviour
{
    public SpriteRenderer alwaysInFront;
    public int offsetY = 0;
	void Update()
    {
        int order = -(int)GetComponent<SpriteRenderer>().bounds.min.y;
        if (offsetY != 0)
            order = -((int)transform.position.y + offsetY);
        if (alwaysInFront)
            order = (int)Mathf.Min(order, alwaysInFront.sortingOrder - 1);
        GetComponent<SpriteRenderer>().sortingOrder = order;
    }
}