﻿using System.Collections.Generic;
using UnityEngine;

public class GUIOrienter : MonoBehaviour
{
    public List<GameObject> left;
    public List<GameObject> right;
    
	void Start()
    {
        Orient();
    }
	
    public void Orient()
    {
        var isLeftHanded = PlayerPrefs.GetString("Handedness") == "Left";
        foreach (var obj in right)
            obj.SetActive(!isLeftHanded);
                
        foreach (var obj in left)
            obj.SetActive(isLeftHanded);
    }
}