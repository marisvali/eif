﻿using System.Collections;
using UnityEngine;

public class CameraShaker : MonoBehaviour
{
    public float totalDuration = 0;
    public float nrShakes = 0;
    public float distance = 0;

    Vector3 startPos;
    bool positionNeedsResetting = false;
    Transform tr;

    private void Start()
    {
        tr = GetComponent<Camera>().GetComponent<Transform>();
    }

    public void StartShake()
    {
        StopShake();
        StartCoroutine(Shake());
	}

    public void StopShake()
    {
        StopAllCoroutines();
        if( positionNeedsResetting )
        {
            tr.position = startPos;
            positionNeedsResetting = false;
        }
    }

    IEnumerator Shake()
    {
        float shakeDuration = totalDuration / nrShakes;
        startPos = tr.position;
        positionNeedsResetting = true;
        for ( int idx = 0; idx < nrShakes; ++idx )
        {
            //choose a target pos in a random direction
            float angle = 360 * UnityEngine.Random.value;
            Vector3 dir = Quaternion.Euler(0, 0, angle) * new Vector3(0, 1, 0);
            Vector3 targetPos = startPos + dir * distance;
            float halfShakeDuration = shakeDuration / 2;

            //move to target pos
            float durationSoFar = 0;
            while (durationSoFar < halfShakeDuration)
            {
                float factor = durationSoFar / halfShakeDuration;
                Vector3 newPos = startPos + (targetPos - startPos) * factor;
                tr.position = newPos;
                durationSoFar += Time.deltaTime;
                yield return null;
            }

            //move back from target pos
            durationSoFar = 0;
            while (durationSoFar < halfShakeDuration)
            {
                float factor = durationSoFar / halfShakeDuration;
                Vector3 newPos = targetPos + (startPos - targetPos) * factor;
                tr.position = newPos;
                durationSoFar += Time.deltaTime;
                yield return null;
            }
        }
        tr.position = startPos;
        positionNeedsResetting = false;
        yield return null;
    }
}
