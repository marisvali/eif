﻿using System.Collections.Generic;
using UnityEngine;

public class Boosts : MonoBehaviour
{
    public class Boost
    {
        public Boost(string internalName, string displayedName, string desc, string descEndless)
        { InternalName = internalName; DisplayedName = displayedName; Description = desc; DescriptionEndless = descEndless; }
        public string InternalName;
        public string DisplayedName;
        public string Description;
        public string DescriptionEndless;
    }
    private List<Boost> boosts = new List<Boost>();

    public int MoreAmmoAmount = 0;
    public float FreeShotPercent = 0;
    public float MoreHealthPercent = 0;
    public float DeflectPercent = 0;
    public float LuckyBreaksPercent = 0;
    public int DownpaymentAmount = 0;
    public float MoreMoneyPercent = 0;
    public float LessSpawnsPercent = 0;
    public float SickSpawnsPercent = 0;
    public float SickSpawnsAmount = 0;
    public float SlowerDepressionPercent = 0;
    public float WeakerDepressionPercent = 0;
    public float SlowerProjectilesPercent = 0;
    public float CalmPercent = 0;
    public float BoomPercent = 0;
    public float SlowerAngerPercent = 0;
    public float ShorterTerrorDuration = 0;
    public float ShorterParanoiaDuration = 0;
    public float DiscountPercent = 0;

    void Start ()
    {
        //PlayerPrefs.SetInt("More ammo", MoreAmmoActive ? 1 : 0);
        //PlayerPrefs.SetInt("Free shot", FreeShotActive ? 1 : 0);
        //PlayerPrefs.SetInt("More health", MoreHealthActive ? 1 : 0);
        //PlayerPrefs.SetInt("Deflect", DeflectActive ? 1 : 0);
        //PlayerPrefs.SetInt("Last resort", LastResortActive ? 1 : 0);
        //PlayerPrefs.SetInt("Lucky breaks", LuckyBreaksActive ? 1 : 0);
        //PlayerPrefs.SetInt("Downpayment", DownpaymentActive ? 1 : 0);
        //PlayerPrefs.SetInt("More money", MoreMoneyActive ? 1 : 0);
        //PlayerPrefs.SetInt("Delayed spawns", DelayedSpawnsActive ? 1 : 0);
        //PlayerPrefs.SetInt("Less spawns", LessSpawnsActive ? 1 : 0);
        //PlayerPrefs.SetInt("Sick spawns", SickSpawnsActive ? 1 : 0);
        //PlayerPrefs.SetInt("Slower depression", SlowerDepressionActive ? 1 : 0);
        //PlayerPrefs.SetInt("Weaker depression", WeakerDepressionActive ? 1 : 0);
        //PlayerPrefs.SetInt("Slower projectiles", SlowerProjectilesActive ? 1 : 0);
        //PlayerPrefs.SetInt("Calm", CalmActive ? 1 : 0);
        //PlayerPrefs.SetInt("Terror resistance", TerrorResistanceActive ? 1 : 0);
        //PlayerPrefs.SetInt("Paranoia resistance", ParanoiaResistanceActive ? 1 : 0);
        //PlayerPrefs.SetInt("Boom", BoomActive ? 1 : 0);
        //PlayerPrefs.SetInt("Slower anger", SlowerAngerActive ? 1 : 0);
        //PlayerPrefs.SetInt("Slower terror", SlowerTerrorActive ? 1 : 0);
        //PlayerPrefs.SetInt("Slower paranoia", SlowerParanoiaActive ? 1 : 0);
        HandleLessSpawns();
    }

    int NrReducibleMobs(MobSpawner spawner)
    {
        return spawner.NrAngers + spawner.NrDepressions + spawner.NrSocialAnxieties;
    }

    int CompareSpawners(MobSpawner a, MobSpawner b)
    {
        if (NrReducibleMobs(a) == NrReducibleMobs(b))
            return 0;
        if (NrReducibleMobs(a) < NrReducibleMobs(b))
            return -1;
        else
            return 1;
    }

    private void HandleLessSpawns()
    {
        
    }

    void Update ()
    {
        //MoreAmmoActive = PlayerPrefs.GetInt("More ammo") != 0;
        //FreeShotActive = PlayerPrefs.GetInt("Free shot") != 0;
        //MoreHealthActive = PlayerPrefs.GetInt("More health") != 0;
        //DeflectActive = PlayerPrefs.GetInt("Deflect") != 0;
        //LastResortActive = PlayerPrefs.GetInt("Last resort") != 0;
        //LuckyBreaksActive = PlayerPrefs.GetInt("Lucky breaks") != 0;
        //DownpaymentActive = PlayerPrefs.GetInt("Downpayment") != 0;
        //MoreMoneyActive = PlayerPrefs.GetInt("More money") != 0;
        //DelayedSpawnsActive = PlayerPrefs.GetInt("Delayed spawns") != 0;
        //LessSpawnsActive = PlayerPrefs.GetInt("Less spawns") != 0;
        //SickSpawnsActive = PlayerPrefs.GetInt("Sick spawns") != 0;
        //SlowerDepressionActive = PlayerPrefs.GetInt("Slower depression") != 0;
        //WeakerDepressionActive = PlayerPrefs.GetInt("Weaker depression") != 0;
        //SlowerProjectilesActive = PlayerPrefs.GetInt("Slower projectiles") != 0;
        //CalmActive = PlayerPrefs.GetInt("Calm") != 0;
        //TerrorResistanceActive = PlayerPrefs.GetInt("Terror resistance") != 0;
        //ParanoiaResistanceActive = PlayerPrefs.GetInt("Paranoia resistance") != 0;
        //BoomActive = PlayerPrefs.GetInt("Boom") != 0;
        //SlowerAngerActive = PlayerPrefs.GetInt("Slower anger") != 0;
        //SlowerTerrorActive = PlayerPrefs.GetInt("Slower terror") != 0;
        //SlowerParanoiaActive = PlayerPrefs.GetInt("Slower paranoia") != 0;
    }

    public List<Boost> GetBoosts()
    {
        if (boosts.Count == 0)
        {
            boosts.Add(new Boost("More ammo", "Blaze", "More flames spawn on the map (seven instead of five).", "More flames spawn on the map (twelve instead of five)."));
            boosts.Add(new Boost("Free shot", "Free shot", "You have a 25% chance to shoot without consuming a flame.", "All shots are free."));
            boosts.Add(new Boost("More health", "Heart of gold", "Enemies will drop hearts 75% more often.", "Enemies will drop hearts 500% more often."));
            boosts.Add(new Boost("Deflect", "Evade", "You have a 25% chance to evade enemy attacks.", "Evade all enemy attacks."));
            boosts.Add(new Boost("Lucky breaks", "Lucky breaks", "You have a 30% chance to start a level with full health.", ""));
            boosts.Add(new Boost("Downpayment", "Small fortune", "You get 100 crystals.", ""));
            boosts.Add(new Boost("More money", "Hoarder's delight", "Enemies drop shards 50% more often.", ""));
            boosts.Add(new Boost("Less spawns", "Broken void", "Dark portals will spawn fewer enemies.", ""));
            boosts.Add(new Boost("Sick spawns", "Cursed void", "Dark portals will spawn enemies with 1 less hit point (except Anger).", ""));
            boosts.Add(new Boost("Slower depression", "Slower depression", "Depression's movement speed is reduced by 40%.", "Depression's movement speed is reduced by 80%."));
            boosts.Add(new Boost("Weaker depression", "Weaker depression", "50% chance that a Depression will die when it latches on to you.", "Depression will die when it latches on to you."));
            boosts.Add(new Boost("Slower projectiles", "Impaired shot", "Social Anxiety's shots are 40% slower.", "Social Anxiety's shots are 80% slower."));
            boosts.Add(new Boost("Calm", "Iron will", "You no longer lose crystals when Anger hits you.", "You no longer lose points when Anger hits you."));
            boosts.Add(new Boost("Terror resistance", "Nightlight", "You have a larger radius of visibility around you during Panic attacks.", "You have a larger radius of visibility around you during Panic attacks."));
            boosts.Add(new Boost("Boom", "Chain reaction", "30% chance that when an enemy dies all enemies in the current area take 1 damage.", "When an enemy dies all enemies in the current area take 1 damage."));
            boosts.Add(new Boost("Slower anger", "Tranquility", "Anger's movement speed is reduced by 50%.", "Anger's movement speed is reduced by 80%."));
            boosts.Add(new Boost("Shorter terror", "Early dawn", "Panic attacks last 50% less.", "Panic attacks last 90% less."));
            boosts.Add(new Boost("Shorter paranoia", "Broken visions", "The effect of Paranoia lasts 50% less.", "The effect of Paranoia lasts 90% less."));
            boosts.Add(new Boost("Discount", "Happy hour", "Shop items cost 20% less.", ""));
        }
        return boosts;
    }
}
