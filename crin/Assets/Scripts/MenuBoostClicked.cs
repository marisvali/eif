﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MenuBoostClicked : MonoBehaviour, IPointerDownHandler
{
    public bool isSelected = false;

    public void OnPointerDown(PointerEventData eventData)
    {
        isSelected = !isSelected;
    }
}
