﻿using UnityEngine;

public class DepressionHiveMind : MonoBehaviour
{
    DepressionAI attached = null;

    public void SetAttached(DepressionAI dep)
    {
        attached = dep;
    }
    public void RemoveAttached(DepressionAI dep)
    {
        if( attached == dep )
            attached = null;
    }
    public bool CanAttach(DepressionAI dep)
    {
        return attached == null || attached == dep;
    }
    public bool HasAttached()
    {
        return attached != null;
    }
}