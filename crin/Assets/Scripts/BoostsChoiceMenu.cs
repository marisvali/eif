﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BoostsChoiceMenu : MonoBehaviour
{
    public List<AudioClip> sounds = new List<AudioClip>();

    public List<Toggle> toggles = new List<Toggle>();
    public List<Text> names = new List<Text>();
    public List<Image> backgrounds = new List<Image>();
    public Text description;
    
    List<Boosts.Boost> mBoosts;
    int[] mSoundSequence = { 6, 3, 6, 2, 4, 1, 2, 6, 3, 6, 2, 4, 5, 3 };
    int mSoundIdx = 0;
    int mLastToggleIdx = -1;

    List<Boosts.Boost> Choose4Boosts()
    {
        List<Boosts.Boost> chosenBoosts = new List<Boosts.Boost>();

        var allBoosts = g.i.boosts.GetBoosts();
        if ( PlayerPrefs.GetInt("ChosenBoosts") == 1 )
        {
            for (int idx = 0; idx < 4; ++idx)
            {
                string internalName = PlayerPrefs.GetString("ChosenBoost" + idx.ToString());
                foreach (var boost in allBoosts)
                    if (boost.InternalName == internalName)
                        chosenBoosts.Add(boost);
            }
        }
        else
        {
            List<Boosts.Boost> availableBoosts = new List<Boosts.Boost>();
            foreach (var boost in allBoosts)
            {
                if (PlayerPrefs.GetInt(boost.InternalName) != 0)
                    continue;

                if (boost.InternalName == "More health" && PlayerPrefs.GetString("Difficulty") == "VeryHard")
                    continue;

                if (boost.InternalName == "Lucky breaks" && PlayerPrefs.GetString("Difficulty") == "VeryHard")
                    continue;

                if (boost.InternalName == "Downpayment" && PlayerPrefs.GetInt("No Downpayment") == 1)
                    continue;

                availableBoosts.Add(boost);
            }
                    

            if (availableBoosts.Count < 4)
                throw new System.Exception("Not enough available boosts remaining");

            while (chosenBoosts.Count < 4)
            {
                int chosenBoost = UnityEngine.Random.Range(0, availableBoosts.Count);
                chosenBoosts.Add(availableBoosts[chosenBoost]);
                availableBoosts.RemoveAt(chosenBoost);
            }

            PlayerPrefs.SetInt("ChosenBoosts", 1);
            for( int idx = 0; idx < chosenBoosts.Count; ++idx )
                PlayerPrefs.SetString("ChosenBoost" + idx.ToString(), chosenBoosts[idx].InternalName);
        }

        return chosenBoosts;
    }
    void Start()
    {
        mBoosts = Choose4Boosts();
        for (int idx = 0; idx < 4; ++idx)
        {
            names[idx].text = mBoosts[idx].DisplayedName;
            backgrounds[idx].sprite = Resources.Load<Sprite>("Images/Boosts/Boosts " + mBoosts[idx].InternalName);
        }

        UpdateButtons();

        PlayerPrefs.SetInt("No Downpayment", 0); //took this instruction into consideration, now reset it
    }

    void Update()
    {
    }

    void PlayNextSound()
    {
        int toggleIdx = -1;
        for (int idx = 0; idx < toggles.Count; ++idx)
            if (toggles[idx].isOn)
                toggleIdx = idx;

        Record.Event("boosts", "selection changed", mBoosts[toggleIdx].DisplayedName);

        if (mLastToggleIdx == toggleIdx)
            return;

        g.i.audioManager.PlayPositionIndependentClip(sounds[mSoundSequence[mSoundIdx] - 1]);
        ++mSoundIdx;
        if (mSoundIdx >= mSoundSequence.Length)
            mSoundIdx = 0;
        mLastToggleIdx = toggleIdx;
    }
    public void SelectionChanged(Toggle toggle)
    {
        PlayNextSound();
        UpdateButtons();
    }

    public void UpdateButtons()
    {
        for (int idx = 0; idx < toggles.Count; ++idx)
        {
            ColorBlock cb = toggles[idx].colors;
            if (toggles[idx].isOn)
            {
                description.text = mBoosts[idx].Description;
                cb.normalColor = new Color(1, 1, 1, 1);
                cb.highlightedColor = new Color(1, 1, 1, 1);
            }
            else
            {
                cb.normalColor = new Color(1, 1, 1, 0);
                cb.highlightedColor = new Color(1, 1, 1, 0);
            }
            toggles[idx].colors = cb;
        }
    }

    public void PressedConfirm()
    {
        bool found = false;
        int selectedIdx = 0;
        for (int idx = 0; idx < toggles.Count; ++idx)
            if (toggles[idx].isOn)
            {
                selectedIdx = idx;
                found = true;
                break;
            }
        if( found )
        {
            ConfirmedSelection();
        }
    }

    public void ConfirmedSelection()
    {
        bool found = false;
        for (int idx = 0; idx < toggles.Count; ++idx)
            if (toggles[idx].isOn)
            {
                Record.Event("boosts", "selection changed", mBoosts[idx].DisplayedName);
                PlayerPrefs.SetInt(mBoosts[idx].InternalName, 1);
                int boostIdx = PlayerPrefs.GetInt("BoostIdx") + 1;
                PlayerPrefs.SetInt("BoostIdx", boostIdx);
                PlayerPrefs.SetString("Boost" + boostIdx.ToString(), mBoosts[idx].InternalName);
                found = true;
            }
        if (found)
        {
            if (PlayerPrefs.GetInt("Downpayment") != 0)
            {
                PlayerPrefs.SetInt("Resources", PlayerPrefs.GetInt("Resources") + g.i.boosts.DownpaymentAmount);
            }
            PlayerPrefs.SetInt("ChosenBoosts", 0);
            PlayerPrefs.SetInt("Boosts", 1);
            SceneManager.LoadScene("SceneSelection");
        }
    }
}
