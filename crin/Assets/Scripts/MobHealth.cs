﻿using UnityEngine;

public class MobHealth : MonoBehaviour
{
    public int maxHealth = 0;
    public bool alive = true;
    private int health = 0;
    private GameObject[] healthPieces = null;
    public GameObject healthPiece = null;
    public bool startAtMax = true;
    public float offsetX = 0;
    public float offsetY = 0;
    public bool circularPlacement = false;
    public float radius = 0;
    public int startHealth = 0;
    public SpriteRenderer mainSprite;
    
    void Start()
    {
        if (startHealth == 0 || startAtMax)
            startHealth = maxHealth;

        health = startHealth;

        if (healthPiece != null)
        {
            healthPieces = new GameObject[maxHealth];
            Bounds bounds = mainSprite.bounds;
            float sizeX = healthPiece.GetComponent<SpriteRenderer>().bounds.size.x;
            float sizeY = healthPiece.GetComponent<SpriteRenderer>().bounds.size.y;

            if( circularPlacement )
            {
                //circle
                Vector2 circleCenter = bounds.center;
                circleCenter += new Vector2(offsetX, offsetY);
                float diameter = radius * 2 * Mathf.PI;
                float totalSlots = diameter / (sizeX * 1.1f);
                float anglePerSlot = 360.0f / totalSlots;
                float startAngle = -maxHealth * anglePerSlot / 2;
                for( int idx = 0; idx < maxHealth; ++idx )
                {
                    float angle = startAngle + anglePerSlot * idx + anglePerSlot / 2;
                    Vector2 dir = Quaternion.Euler(0, 0, angle) * new Vector2(0, 1);
                    Vector2 pos = circleCenter + radius * dir;
                    healthPieces[idx] = Instantiate(healthPiece, new Vector3(pos.x, pos.y, 0), Quaternion.identity);
                    healthPieces[idx].transform.parent = transform;
                }
            }
            else
            {
                //line
                Vector2 pos = new Vector2();
                pos.x = bounds.min.x + (bounds.size.x - sizeX * maxHealth) / 2 + offsetX;
                pos.y = bounds.max.y + offsetY;
                for (int idx = 0; idx < maxHealth; ++idx)
                {
                    Vector2 pos2d = pos;
                    pos2d.x += (sizeX) * (idx + 0.5f);
                    healthPieces[idx] = Instantiate(healthPiece, new Vector3(pos2d.x, pos2d.y, 0), Quaternion.identity);
                    healthPieces[idx].transform.parent = transform;
                }
            }
            UpdateHealthPieces();
        }
    }

    public void Hit(int damage)
    {
        var newHealth = health - damage;
        if (newHealth < 0)
            newHealth = 0;
        health = newHealth;
        if (health == 0)
        {
            alive = false;
        }
        UpdateHealthPieces();
    }

    public int GetHealth()
    {
        return health;
    }

    public void SetHealth(int val)
    {
        health = val;
        alive = health > 0;
        UpdateHealthPieces();
    }

    void UpdateHealthPieces()
    {
        if (healthPiece && (healthPieces != null))
            for (int idx = maxHealth - 1; idx >= 0; --idx)
                if (idx >= health)
                    healthPieces[idx].SetActive(false);
                else
                    healthPieces[idx].SetActive(true);
    }
    public void HideHealth()
    {
        foreach (var healthPiece in healthPieces)
            healthPiece.SetActive(false);
    }
    public void ShowHealth()
    {
        UpdateHealthPieces();
    }
}
