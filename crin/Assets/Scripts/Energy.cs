﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : MonoBehaviour
{
    public float maxEnergy = 10;
    public float energyPerMove = 3;
    public float regenPerSecond = 1.5f;
    public float timeBeforeRegen = 1f;
    private float energy = 1;
    private float timeSinceLastSpending = 0;

    void Start ()
    {
        energy = maxEnergy;
	}
	void Update ()
    {
        timeSinceLastSpending += Time.deltaTime;

        if( timeSinceLastSpending > timeBeforeRegen)
        {
            if (energy < maxEnergy)
                energy += regenPerSecond * Time.deltaTime;
            if (energy > maxEnergy)
                energy = maxEnergy;
        }
	}
    public float GetEnergy()
    {
        return energy;
    }
    public void SpendEnergy(float value)
    {
        energy -= value;
        if (energy < 0)
            energy = 0;
        timeSinceLastSpending = 0;
    }
}
