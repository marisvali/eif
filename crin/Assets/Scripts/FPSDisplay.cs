﻿using UnityEngine;
using System.Collections.Generic;

public class FPSDisplay : MonoBehaviour
{
    // float deltaTime = 0.0f;
    bool first = true;
    int smallestAverageFPS = 0;
    int currentFrameIdx = 0;
    int averageFPS = 0;
    Queue<int> lastFPS = new Queue<int>();
    int averageRange = 30;

    private void Start()
    {
        first = true;
        currentFrameIdx = 0;
    }

    void Update()
    {
        int fps = (int)(1.0 / Time.deltaTime);

        lastFPS.Enqueue(fps);
        if( lastFPS.Count > averageRange )
            lastFPS.Dequeue();

        ++currentFrameIdx;
        if (currentFrameIdx < 100)
            return;


        averageFPS = 0;
        foreach (var prevFPS in lastFPS)
            averageFPS += prevFPS;
        averageFPS /= lastFPS.Count;

        if (first || averageFPS < smallestAverageFPS)
        {
            smallestAverageFPS = averageFPS;
            first = false;
        }
    }

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h * 20 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 20 / 100;
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        string text = smallestAverageFPS.ToString() + " " + averageFPS.ToString();
        GUI.Label(rect, text, style);
    }
}