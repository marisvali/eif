﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RejectionAI : WorldObject
{
    public float hitTime = 0;
    public float attackTime = 0;
    public float timeBetweenAttacks = 0;
    public bool waveAttackEnabled = true;
    public AudioClip attack;
    public AudioClip retreat;
    public AudioClip comeback;
    public List<RejectionAuxAI> auxiliaries = new List<RejectionAuxAI>();
    public GameObject rejectionWave;
    public GameObject endSequencePrefab;

    float lastStateChangeMoment = 0;
    bool stateJustChanged = false;
    bool wasJustHit = false;
    bool freeze = false;
    float timeSinceFreeze = 0;
    Animator Anim;
    bool endTriggered = false;
    float timeSinceLastAttack = 0;
    float attackStartMoment = 0;
    CameraShaker cameraShaker;

    enum State
    {
        Idle,
        Hit,
        Attacking,
        Frozen,
        End
    }
    State currentState;
    void Start()
    {
        currentState = State.Idle;
        stateJustChanged = true;
        Anim = transform.GetComponent<Animator>();
        timeSinceLastAttack = Time.time;
        cameraShaker = g.i.cameraMain.GetComponent<CameraShaker>();
        if (PlayerPrefs.GetString("Gender") == "Female")
        {
            Anim.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("Animations/Rejection/RejectionFem");
            GetComponent<Mob>().icon.sprite = Resources.Load<Sprite>("Images/Mobs/Rejection/Fem_RejectionIdle01");
        }
        else
        {
            Anim.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("Animations/Rejection/Rejection");
        }
    }
    void Idle()
    {
        if (stateJustChanged)
        {
            Anim.SetInteger("AnimationState", 0);
        }
        if (endTriggered)
        {
            currentState = State.End;
            return;
        }
        if (wasJustHit)
        {
            currentState = State.Hit;
            return;
        }
        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }

        if (Time.time - timeSinceLastAttack > timeBetweenAttacks)
        {
            currentState = State.Attacking;
            return;
        }
    }
    void Frozen()
    {
        if (stateJustChanged)
        {
            Anim.speed = 0;
        }

        if (endTriggered)
        {
            freeze = false;
            Anim.speed = 1;
            currentState = State.End;
            return;
        }

        if (wasJustHit)
        {
            Anim.speed = 1;
            currentState = State.Hit;
            return;
        }

        float time = Time.time - timeSinceFreeze;
        if (time > 5)
        {
            timeSinceLastAttack = Time.time;
            freeze = false;
            Anim.speed = 1;
            currentState = State.Idle;
            foreach (var aux in auxiliaries)
                aux.OnEmerge();
            return;
        }
    }
    void Hit()
    {
        if (stateJustChanged)
        {
            wasJustHit = false;
            Anim.SetInteger("AnimationState", 1);
        }
        if (endTriggered)
        {
            currentState = State.End;
            return;
        }
        if (wasJustHit || stateJustChanged && freeze)
        {
            lastStateChangeMoment = Time.time;
            Anim.Play("RejectionHit", -1, 0f);
            wasJustHit = false;
            return;
        }
        float timeSpentHit = Time.time - lastStateChangeMoment;
        if (timeSpentHit > hitTime)
        {
            if (freeze)
                currentState = State.Frozen;
            else
                currentState = State.Idle;
            return;
        }
    }
    void Attacking()
    {
        if (stateJustChanged)
        {
            Anim.SetInteger("AnimationState", 2);
            foreach (var aux in auxiliaries)
                aux.OnRetreat();
            if (auxiliaries.Count > 0)
                g.i.audioManager.PlayClip(retreat, transform.position);
            attackStartMoment = Time.time;
            g.i.audioManager.PlayPositionIndependentClip(attack);
        }
        if (endTriggered)
        {
            currentState = State.End;
            return;
        }
        if (freeze)
        {
            currentState = State.Frozen;
            return;
        }

        float timeSpentAttacking = Time.time - attackStartMoment;
        if (timeSpentAttacking > attackTime)
        {
            timeSinceLastAttack = Time.time;
            currentState = State.Idle;
            if (auxiliaries.Count > 0)
                g.i.audioManager.PlayClip(comeback, transform.position);
            foreach (var aux in auxiliaries)
                aux.OnEmerge();
            return;
        }
    }
    void GenerateRejectionWave()
    {
        if( waveAttackEnabled )
        {
            g.Clone(rejectionWave, new Vector2(4138, -989));
            cameraShaker.StartShake();
        }
    }
    void End()
    {
        if (stateJustChanged)
        {
            g.Clone(endSequencePrefab, new Vector3(2891, 510, 3));
            gameObject.SetActive(false);
        }
    }
    protected override void UpdateImpl()
    {
        State oldState = currentState;
        switch (currentState)
        {
            case State.Idle:
                Idle();
                break;
            case State.Hit:
                Hit();
                break;
            case State.Attacking:
                Attacking();
                break;
            case State.Frozen:
                Frozen();
                break;
            case State.End:
                End();
                break;
        }
        State newState = currentState;
        stateJustChanged = oldState != newState;
        if (stateJustChanged)
            lastStateChangeMoment = Time.time;
    }
    public void Freeze()
    {
        freeze = true;
        timeSinceFreeze = Time.time;
        timeSinceLastAttack += 5;
        attackStartMoment += 5;
    }
    public void OnHit()
    {
        if( currentState != State.Attacking )
            wasJustHit = true;
    }
    public bool IsProtected()
    {
        foreach (var aux in auxiliaries)
            if (aux.GetComponent<MobHealth>().alive)
                return true;
        return false;
    }
    public void TriggerEndSequence()
    {
        endTriggered = true;
    }
    public void UpdateRejectionAuxConnections()
    {
        StartCoroutine(UpdateRejectionAuxConnectionsImpl());
    }

    private IEnumerator UpdateRejectionAuxConnectionsImpl()
    {
        //skip two frames so that an earlier destroy has a chance to activate on the auxiliaries
        yield return null;
        yield return null;
        
        auxiliaries = g.i.mobs
                        .Where(x => x.GetComponent<RejectionAuxAI>())
                        .Select(x => x.GetComponent<RejectionAuxAI>())
                        .ToList();
    }
}