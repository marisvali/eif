﻿using System.Collections.Generic;
using UnityEngine;

public class SocialAnxietyProjectile : WorldObject
{
    public int damage = 0;
    public List<string> attackTags = new List<string>();
    public int ammoCost = 0;
    public SpriteRenderer icon;
    public GameObject projectileEnd;
    
    Vector2 startRegion;
    float oldSpeed = 0;
    float timeSinceFreeze = 0;
    bool freeze = false;

    private void Start()
    {
        startRegion = g.i.Region(gameObject.transform.position);
    }
    protected override void UpdateImpl()
    {
        if (g.i.Region(gameObject.transform.position) != startRegion)
            Destroy(gameObject);
        if(freeze)
        {
            float time = Time.time - timeSinceFreeze;
            if (time > 5)
            {
                freeze = false;
                GetComponent<MoveStraight>().speed = oldSpeed;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        foreach (var tag in attackTags)
            if (collider.tag == tag)
            {
                Record.Event("mob attack", "social anxiety", "projectile hit player");
                collider.GetComponent<Player>().SocialAnxietyAttack(damage);
                g.Clone(projectileEnd, transform.position);
                GameObject.Destroy(gameObject);
            }
        if( collider.tag == "HardObstacle" )
        {
            g.Clone(projectileEnd, transform.position);
            GameObject.Destroy(gameObject);
        }
    }
    public void Freeze()
    {
        oldSpeed = GetComponent<MoveStraight>().speed;
        GetComponent<MoveStraight>().speed = 0;
        freeze = true;
        timeSinceFreeze = Time.time;
    }
    public void MakeFictional()
    {
        icon.enabled = false;
    }
}