﻿using UnityEngine;

public class MenuButton : MonoBehaviour
{
    public GameObject mMenu;
    public void Awake()
    {
        mMenu.SetActive(false);
    }
    public void ShowMenu()
    {
        g.i.Pause();
        mMenu.SetActive(true);
    }

    public void HideMenu()
    {
        mMenu.SetActive(false);
        g.i.Unpause();
    }
}