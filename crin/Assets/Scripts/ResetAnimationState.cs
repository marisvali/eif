﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetAnimationState : StateMachineBehaviour
{
    int lastState = 0;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        bool comingFromExit = animator.GetInteger("AnimationState") == lastState; //check if we are coming from a transition with HasExit = true
        bool forceReset = animator.GetInteger("AnimationStateDoReset") == 1;
        if (forceReset)
            animator.SetInteger("AnimationStateDoReset", 0);

        if (comingFromExit || forceReset)
        {
            //reset the animation state to 0 so it doesn't go back into the same animation where it was
            animator.SetInteger("AnimationState", 0);
        }   
        else
        {
            //leave the animation state as it is, as it might indicate the transition to another state
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        lastState = animator.GetInteger("AnimationState");
    }
}
