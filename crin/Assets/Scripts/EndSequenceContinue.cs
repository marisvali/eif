﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndSequenceContinue : MonoBehaviour
{
    public void Continue()
    {
        PlayerPrefs.SetInt("FinishedCampaign", 1);
        PlayerPrefs.SetFloat("StartMenuMusicTime", 0);
        PlayerPrefs.SetString("Level", "End");
        SceneManager.LoadScene("SceneSelection");
    }
}
