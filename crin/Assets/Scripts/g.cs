using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class g : MonoBehaviour
{
    public const int nRegions = 3; // 3x3 minimap is decided here
    static public readonly Vector2 regionSize = new Vector2(1080, 1080); // Hardcode the size to avoid
    // floating point errors. I used to get the world size from the minimap , converting
    // viewport(0, 0) and viewport(1, 1) to world coordinates. This introduced noticeable
    // deviations. Might as well hardcode values that are very stable.
    static public readonly Rect worldRect = new Rect(Vector2.zero, regionSize * nRegions);
    public static g i { get; private set; }

    g()
    {
        i = this;
    }

    public Camera cameraMain;
    public Canvas cameraMainCanvas;
    public Camera cameraMinimap;
    public Camera cameraMenu;
    public MinimapBehavior minimap;
    public AudioManager audioManager;
    public SpawnDrops spawnDrops;
    public Boosts boosts;
    [HideInInspector]
    public Player player;
    [HideInInspector]
    public GameObject cameraMainCylinder
    {
        get
        {
            if (PlayerPrefs.GetString("Handedness") == "Left")
            {
                return cameraMainCylinderLeft;
            }
            else
            {
                return cameraMainCylinderRight;
            }
        }
    }
    public GameObject cameraMainCylinderLeft;
    public GameObject cameraMainCylinderRight;
    private PlayerHealth playerHealthLoaded;
    public PlayerHealth playerHealth
    {
        get
        {
            if (!playerHealthLoaded)
                playerHealthLoaded = player.GetComponent<PlayerHealth>();
            return playerHealthLoaded;
        }
    }
    public DepressionHiveMind hiveMindDepression;
    public TerrorHiveMind hiveMindPanic;
    public Pathfinding.GridGraph gridGraph
    {
        get
        {
            return AstarPath.active.graph;
        }
    }
    public List<Mob> mobs = new List<Mob>();
    [SerializeField]
    private AudioClip stopwatchClip;
    [SerializeField]
    private AudioClip firestormClip;
    public MainScript mainScript;
    public List<Ammo> ammos = new List<Ammo>();
    public List<HealthDrop> healthDrops = new List<HealthDrop>();
    public List<Resource> resources = new List<Resource>();
    public Warning warningFog;

    bool mStopwatchActive = false;
    bool mPaused = true;

    void Awake()
    {
        // The problem is that I have to load the player before I load the main script because the main script uses the player. But if I do that, the player can't register itself in the g object because the g object loads at the same time as the main script.
        player = Player.i;
        Player.i = null; // Throw an error if anyone else wants to use it.
    }

    public bool PlayerCanTeleportInMainView(Vector2 destination)
    {
        //check that the player will be completely within the main view
        var worldRect = WorldRectOfMainCamera();
        Bounds cameraBounds = new Bounds(worldRect.center, worldRect.size);
        cameraBounds.extents = new Vector3(cameraBounds.extents.x, cameraBounds.extents.y, 10000);
        var extendedBounds = i.player.GetComponent<Collider2D>().bounds;
        Vector3 offset1 = extendedBounds.center - i.player.transform.position;
        var normalizedExtendedBounds = new Bounds(
            new Vector3(destination.x + offset1.x, destination.y + offset1.y, 0),
            new Vector3(extendedBounds.size.x, extendedBounds.size.y, 1000));
        if (!cameraBounds.Contains(normalizedExtendedBounds.min) || !cameraBounds.Contains(normalizedExtendedBounds.max))
            return false;

        var playerBounds = i.player.placementBounds.bounds;
        Vector3 offset2 = playerBounds.center - i.player.transform.position;
        var normalizedBounds = new Bounds(
            new Vector3(destination.x + offset2.x, destination.y + offset2.y, 0),
            new Vector3(playerBounds.size.x, playerBounds.size.y, 1000));

        //check that the player will not overlap obstacles
        var nodes = gridGraph.GetNodesInRegion(normalizedBounds);
        foreach (var node in nodes)
            if (!node.Walkable)
                return false;
        return true;
    }

    public Vector2 GetRandomPosition(Vector2 refPos, float radius)
    {
        // Get any position on the map.
        Vector2 rndPos = GetRandomPosition();

        // If necessary, bring it closer to refPos so that it's within the radius.
        Vector2 dirVec = rndPos - refPos;
        if (Mathf.Sqrt(dirVec.sqrMagnitude) > radius)
            rndPos = refPos + dirVec.normalized * radius * Random.value;
        return rndPos;
    }

    public Vector2 GetRandomPositionInRegion(Vector2 region)
    {
        return GetRandomPositionInRect(RegionRect(region).Shrinked(200));
    }

    public Vector2 GetRandomPosition()
    {
        return GetRandomPositionInRect(worldRect);
    }

    static public Vector2 GetRandomPositionInRect(Rect rect)
    {
        return rect.min + new Vector2(Random.value * rect.width, Random.value * rect.height);
    }

    public bool PlayerCanTeleportGenerally(Vector2 destination)
    {
        var playerBounds = i.player.placementBounds.bounds;
        Vector3 offset2 = playerBounds.center - i.player.transform.position;
        var normalizedBounds = new Bounds(
            new Vector3(destination.x + offset2.x, destination.y + offset2.y, 0),
            new Vector3(playerBounds.size.x, playerBounds.size.y, 1000));

        //check that the player will not overlap obstacles
        var nodes = gridGraph.GetNodesInRegion(normalizedBounds);
        foreach (var node in nodes)
            if (!node.Walkable)
                return false;
        return true;
    }

    public Vector3 GetFreeRandomPosition(Vector3 position, float radius)
    {
        Vector3 newPos = position;
        for (int idx = 0; idx < 1000; ++idx)
        {
            newPos = GetRandomPosition(position, radius);
            Bounds bounds = new Bounds(newPos,
                new Vector3(gridGraph.nodeSize * 2, gridGraph.nodeSize * 2, 1000));
            var nodes = gridGraph.GetNodesInRegion(bounds);
            if (nodes.Count > 0)
            {
                bool goodPosition = true;
                foreach (var node in nodes)
                    if (!node.Walkable)
                    {
                        goodPosition = false;
                        break;
                    }
                if (goodPosition)
                    return newPos;
            }
        }
        return newPos;
    }

    public Vector3 GetFreeRandomPositionInRegion(Vector2 region, Vector3 position)
    {
        Vector3 newPos = position;
        for (int idx = 0; idx < 1000; ++idx)
        {
            newPos = GetRandomPositionInRegion(region);
            Bounds bounds = new Bounds(newPos,
                new Vector3(gridGraph.nodeSize * 2, gridGraph.nodeSize * 2, 1000));
            var nodes = gridGraph.GetNodesInRegion(bounds);
            bool goodPosition = true;
            foreach (var node in nodes)
                if (!node.Walkable)
                {
                    goodPosition = false;
                    break;
                }
            if (goodPosition)
                return newPos;
        }
        return newPos;
    }

    public Vector3 GetFreeRandomPosition()
    {
        Vector3 newPos = new Vector3(0, 0, 0);
        for (int idx = 0; idx < 1000; ++idx)
        {
            newPos = GetRandomPosition();

            if (PositionIsFree(newPos))
                return newPos;
        }
        return newPos;
    }
    public bool PositionIsFree(Vector3 pos)
    {
        Bounds bounds = new Bounds(pos,
                new Vector3(gridGraph.nodeSize * 2, gridGraph.nodeSize * 2, 1000));
        if (bounds.min.x <= 0 || bounds.min.y <= 0 ||
           bounds.max.x >= 3240 || bounds.max.y >= 3240)
            return false;
        var nodes = gridGraph.GetNodesInRegion(bounds);
        foreach (var node in nodes)
            if (!node.Walkable)
                return false;
        return true;
    }

    static public float RndValBetween(float min, float max)
    {
        return min + (int)((max - min) * UnityEngine.Random.value);
    }

    static public void AddScore(int score)
    {
        int currentScore = PlayerPrefs.GetInt("CurrentScore") + score;
        PlayerPrefs.SetInt("CurrentScore", currentScore);
    }

    public void ExecuteStopwatch()
    {
        foreach (var mob in g.i.mobs)
            if (mob.GetComponent<DepressionAI>())
                mob.GetComponent<DepressionAI>().Freeze();
            else if (mob.GetComponent<SocialAnxietyAI>())
                mob.GetComponent<SocialAnxietyAI>().Freeze();
            else if (mob.GetComponent<AngerAI>())
                mob.GetComponent<AngerAI>().Freeze();
            else if (mob.GetComponent<TerrorAI>())
                mob.GetComponent<TerrorAI>().Freeze();
            else if (mob.GetComponent<TerrorHiveMind>())
                mob.GetComponent<TerrorHiveMind>().Freeze();
            else if (mob.GetComponent<ParanoiaAI>())
                mob.GetComponent<ParanoiaAI>().Freeze();
            else if (mob.GetComponent<RejectionAuxAI>())
                mob.GetComponent<RejectionAuxAI>().Freeze();
            else if (mob.GetComponent<RejectionAI>())
                mob.GetComponent<RejectionAI>().Freeze();
            // the objects below are not mobs, I should get them using different mechanisms
            else if (mob.GetComponent<MobSpawner>())
                mob.GetComponent<MobSpawner>().Freeze();
            else if (mob.GetComponent<Ammo>())
                mob.GetComponent<Ammo>().Freeze();
            else if (mob.GetComponent<SocialAnxietyProjectile>())
                mob.GetComponent<SocialAnxietyProjectile>().Freeze();
            else if (mob.GetComponent<RejectionWave>())
                mob.GetComponent<RejectionWave>().Freeze();

        audioManager.PlayPositionIndependentClip(stopwatchClip);
        StartCoroutine(HandleStopwatchActivationDeactivation());
    }

    public bool IsStopwatchActive()
    {
        return mStopwatchActive;
    }

    private IEnumerator HandleStopwatchActivationDeactivation()
    {
        mStopwatchActive = true;
        yield return new WaitForSeconds(5.1f);
        mStopwatchActive = false;
    }

    public void ExecuteFirestorm()
    {
        var mobs = new List<Mob>();
        foreach (var mob in g.i.mobs)
            if (mob.GetComponent<DepressionAI>())
                mobs.Add(mob);
            else if (mob.GetComponent<SocialAnxietyAI>())
                mobs.Add(mob);
            else if (mob.GetComponent<AngerAI>())
                mobs.Add(mob);
            else if (mob.GetComponent<TerrorAI>())
                mobs.Add(mob);
            else if (mob.GetComponent<ParanoiaAI>())
                mobs.Add(mob);
            else if (mob.GetComponent<RejectionAuxAI>())
                mobs.Add(mob);

        foreach (var mob in mobs)
            if (!mob.GetComponent<MobExpiration>().Expired())
                mob.GetComponent<Explodable>().Explode();
        audioManager.PlayPositionIndependentClip(firestormClip);
    }

    static public int MaxAmmo()
    {
        int maxAmmo = 5;
        if (PlayerPrefs.GetInt("Bought Reinforced glass 1") == 1)
            maxAmmo = 6;
        if (PlayerPrefs.GetInt("Bought Reinforced glass 2") == 1)
            maxAmmo = 7;
        if (PlayerPrefs.GetInt("Bought Reinforced glass 3") == 1)
            maxAmmo = 8;
        if (PlayerPrefs.GetInt("Bought Reinforced glass 4") == 1)
            maxAmmo = 9;
        if (PlayerPrefs.GetInt("Bought Reinforced glass 5") == 1)
            maxAmmo = 10;
        return maxAmmo;
    }

    static public IEnumerator AddScene(string sceneName)
    {
        prof.Start();
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        while (!async.isDone)
            yield return null;
        yield return null;
        prof.Capture($"loaded scene {sceneName} in");
    }

    static int Next(int maxExclusive)
    {
        return UnityEngine.Random.Range(0, maxExclusive);
    }

    static int Next(int minInclusive, int maxExclusive)
    {
        return UnityEngine.Random.Range(0, maxExclusive);
    }


    [SerializeField]
    private GameObject pathTarget;
    static public GameObject CreatePathTarget()
    {
        return (GameObject)Instantiate(i.pathTarget, Vector3.zero, Quaternion.identity, i.transform);
    }

    [SerializeField]
    private GameObject splash;
    static public void Splash(Vector3 pos)
    {
        Instantiate(i.splash, pos, Quaternion.identity, i.transform);
    }

    static public GameObject Clone(GameObject obj)
    {
        return (GameObject)Instantiate(obj, Vector3.zero, Quaternion.identity, i.transform);
    }

    static public GameObject Clone(GameObject obj, Vector2 pos)
    {
        return (GameObject)Instantiate(obj, pos, Quaternion.identity, i.transform);
    }

    static public GameObject Clone(GameObject obj, Transform transform)
    {
        return (GameObject)Instantiate(obj, Vector3.zero, Quaternion.identity, transform);
    }

    static public Vector2 RotatePointAroundPivot(Vector2 point, Vector2 pivot, Quaternion quaternion)
    {
        return (Vector2)(quaternion * (Vector3)(point - pivot) + (Vector3)pivot);
    }

    // If the camera is rotated 90 degrees on the Z axis the viewport will think (0, 0) is 
    // bottom right in world points. This is because the viewport ignores the rotation of the
    // camera.
    // In my mind I have the following mapping between viewport and world coordinates:
    // viewport(0, 0) = world(bottom, left)
    // viewport(0, 1) = world(top, left)
    // viewport(1, 1) = world(top, right)
    // viewport(1, 0) = world(bottom, right)
    // That works when the camera is not rotated.
    // When I rotate the camera by 90 degrees on Z, the actual mapping is:
    // viewport(0, 0) = world(bottom, right)
    // viewport(0, 1) = world(bottom, left)
    // viewport(1, 1) = world(top, left)
    // viewport(1, 0) = world(top, right)
    // In order to get world(bottom, left) and world(top, right) consistently, I need to get
    // the following mapping when the camera is rotated 90 degrees on the Z axis:
    // (0, 0) -> (0, 1)
    // (0, 1) -> (1, 1)
    // (1, 1) -> (1, 0)
    // (1, 0) -> (0, 0)
    // I have to rotate the [(0, 0), (1, 1)] rectangle around the (0.5, 0.5) point to get the
    // [(0, 1), (1, 0)] rectangle. So I need to rotate points around a pivot, using the
    // opposite rotation than the one applied to the camera.        
    // var quaternion = Quaternion.Inverse(g.i.cameraMain.transform.rotation);
    // var p1 = g.RotatePointAroundPivot(new Vector2(0, 0), new Vector2(0.5f, 0.5f), quaternion);
    // var p2 = g.RotatePointAroundPivot(new Vector2(0, 1), new Vector2(0.5f, 0.5f), quaternion);
    // var p3 = g.RotatePointAroundPivot(new Vector2(1, 1), new Vector2(0.5f, 0.5f), quaternion);
    // var p4 = g.RotatePointAroundPivot(new Vector2(1, 0), new Vector2(0.5f, 0.5f), quaternion);
    // Later update: it turns out that I can get away with not using viewports so much because
    // it's slower and the floating point operations cause errors. I'll leave these functions here
    // as comments because they were very useful and I may want to use them in the future.
    // static public Vector2 RotatedBottomLeftViewport(Camera camera)
    // {
    //     Quaternion quaternion = Quaternion.Inverse(camera.transform.rotation);
    //     return RotatePointAroundPivot(new Vector2(0, 0), new Vector2(0.5f, 0.5f), quaternion);
    // }
    // static public Vector2 RotatedTopRightViewport(Camera camera)
    // {
    //     Quaternion quaternion = Quaternion.Inverse(camera.transform.rotation);
    //     return RotatePointAroundPivot(new Vector2(1, 1), new Vector2(0.5f, 0.5f), quaternion);
    // }
    // static public Vector2 RotatedViewportToWorldPoint(Camera camera, Vector3 point)
    // {
    //     Quaternion quaternion = Quaternion.Inverse(camera.transform.rotation);
    //     return RotatePointAroundPivot(point, new Vector2(0.5f, 0.5f), quaternion);
    // }

    public Rect WorldRectOfMainCamera()
    {
        return RegionRect(cameraMain.transform.position);
    }

    static public Vector2 RandomRegion()
    {
        return new Vector2(Random.Range(0, nRegions), Random.Range(0, nRegions));
    }

    public Vector2 Region(Vector3 worldPos)
    {
        Vector2 regionFloat = worldPos / regionSize;
        Vector2 region = new Vector2(Mathf.Floor(regionFloat.x), Mathf.Floor(regionFloat.y));
        return region;
    }

    public Rect RegionRect(Vector2 region)
    {
        Rect regionRect = new Rect(worldRect.min + regionSize * region, regionSize);
        return regionRect;
    }

    public Rect RegionRect(Vector3 worldPos)
    {
        Vector2 regionSize = worldRect.size / nRegions;
        Vector2 regionFloat = worldPos / regionSize;
        Vector2 region = new Vector2(Mathf.Floor(regionFloat.x), Mathf.Floor(regionFloat.y));
        Rect regionRect = new Rect(worldRect.min + regionSize * region, regionSize);
        return regionRect;
    }

    // Mobs need to be only partially visibile before they can be attacked.
    public bool MobPartiallyInRegion(Mob mob, Vector2 region)
    {
        return RegionRect(region).Overlaps(mob.hitArea.bounds.ToRect());
    }

    // Mobs need to be fully inside the region before they start attacking.
    public bool MobCompletelyInRegion(Mob mob, Vector2 region)
    {
        return RegionRect(region).Contains(mob.hitArea.bounds.ToRect());
    }

    public void Pause()
    {
        mPaused = true;
        // Time.timeScale = 0.0001f;
        Time.timeScale = 0;
    }

    public void Unpause()
    {
        mPaused = false;
        Time.timeScale = 1.0f;
        //musicLevel.Unpause();
    }

    public bool Paused()
    {
        return mPaused;
    }

    public static bool Portrait()
    {
        return Screen.width < Screen.height;
    }

    public static void RotateCamera(Camera camera)
    {
        Rect r = camera.rect;
        camera.rect = new Rect(r.y, 1 - r.x - r.width, r.height, r.width);
        camera.transform.localEulerAngles = new Vector3(0, 0, 90);
    }
}

public static class Extensions
{
    public static Rect ToRect(this Bounds bounds)
    {
        return new Rect(bounds.min, bounds.size);
    }

    public static bool Contains(this Rect first, Rect second)
    {
        return first.Contains(second.min) && first.Contains(second.max);
    }

    public static Rect Shrinked(this Rect first, float margin)
    {
        return new Rect(first.min + new Vector2(margin, margin),
                first.size - new Vector2(2 * margin, 2 * margin));
    }
}