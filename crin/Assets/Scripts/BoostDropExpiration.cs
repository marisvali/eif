﻿using System.Collections;
using UnityEngine;

public class BoostDropExpiration : MonoBehaviour
{
    public float expirationTime = 30.0f;
    string boostName;

    public void SetBoostName(string name)
    {
        boostName = name;
        StartCoroutine(Expire());
    }

    IEnumerator Expire()
    {
        yield return new WaitForSecondsRealtime(expirationTime);
        PlayerPrefs.SetInt(boostName, 0);

        int boostIdx = PlayerPrefs.GetInt("BoostIdx");
        for (int idx = 1; idx <= boostIdx; ++idx)
        {
            if (PlayerPrefs.GetString("Boost" + idx.ToString()) == boostName)
            {
                RemoveBoostIdx(idx);
                break;
            }
        }
    }

    void RemoveBoostIdx(int boostIdx)
    {
        for (int idx = boostIdx; idx < 6; ++idx)
            PlayerPrefs.SetString("Boost" + idx, PlayerPrefs.GetString("Boost" + idx + 1));
        PlayerPrefs.SetString("Boost6", "");
        PlayerPrefs.SetInt("BoostIdx", PlayerPrefs.GetInt("BoostIdx") - 1);
        GameObject.Destroy(this);
    }
}
