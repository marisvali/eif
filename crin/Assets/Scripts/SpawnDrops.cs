﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDrops : MonoBehaviour
{
    public float DepressionHealthDropRate = 0;
    public float DepressionResourceDropRate = 0;
    public int DepressionResourceDropAmount = 0;
    public float DepressionBoostDropRate = 0;
    public float DepressionConsumableDropRate = 0;
    public float SocialAnxietyHealthDropRate = 0;
    public float SocialAnxietyResourceDropRate = 0;
    public int SocialAnxietyResourceDropAmount = 0;
    public float SocialAnxietyBoostDropRate = 0;
    public float SocialAnxietyConsumableDropRate = 0;
    public float AngerHealthDropRate = 0;
    public float AngerResourceDropRate = 0;
    public int AngerResourceDropAmount = 0;
    public float AngerBoostDropRate = 0;
    public float AngerConsumableDropRate = 0;
    public float TerrorHealthDropRate = 0;
    public float TerrorResourceDropRate = 0;
    public int TerrorResourceDropAmount = 0;
    public float TerrorBoostDropRate = 0;
    public float TerrorConsumableDropRate = 0;
    public float ParanoiaHealthDropRate = 0;
    public float ParanoiaResourceDropRate = 0;
    public int ParanoiaResourceDropAmount = 0;
    public float ParanoiaBoostDropRate = 0;
    public float ParanoiaConsumableDropRate = 0;
    public GameObject healthDrop;
    public GameObject resourceDrop;
    public GameObject boostDrop;
    public GameObject firestormDrop;
    public GameObject stopwatchDrop;
    public GameObject boomRef;
    
    float DropRadius = 100;

    List<GameObject> allDrops = new List<GameObject>();

    private bool PointsAreCloseIn2D(List<Vector3> pts, Vector3 refPt, double dist)
    {
        Vector2 refPt2D = refPt;
        foreach (Vector3 pt in pts)
        {
            Vector2 pt1 = pt;
            if ((pt1 - refPt2D).magnitude < dist)
                return true;
        }
        return false;
    }

    private List<Vector3> GetPositionsToAvoid()
    {
        List<Vector3> posToAvoid = new List<Vector3>();

        //avoid drops
        var drops = GetAllDrops();
        foreach (var obj in drops)
            posToAvoid.Add(obj.transform.position);

        //avoid ammos
        foreach (var ammo in g.i.ammos)
            posToAvoid.Add(ammo.transform.position);

        //avoid the player's current position
        posToAvoid.Add(g.i.player.transform.position);

        return posToAvoid;
    }

    private List<Vector3> FindFreePositionsAround(Vector3 center, int nrPositions)
    {
        List<Vector3> positions = new List<Vector3>();
        if (nrPositions == positions.Count)
            return positions;

        List<Vector3> posToAvoid = GetPositionsToAvoid();

        //move around the center in concentric circles until we find a free position
        int minDistanceToOtherObjects = 300;
        int radiusStep = minDistanceToOtherObjects;
        int maxRadius = 3000;
        int circleAngleStep = 10;
        for( int radius = 0; radius < maxRadius; radius += radiusStep )
            for( int circleAngle = 0; circleAngle < 360; circleAngle += circleAngleStep )
            {
                Vector3 dir = new Vector3(0, radius, 0);
                dir = Quaternion.Euler(0, 0, circleAngle) * dir;
                Vector3 pos = center + dir;

                if( !g.i.PositionIsFree(pos) )
                    continue;

                if (PointsAreCloseIn2D(posToAvoid, pos, minDistanceToOtherObjects))
                    continue;

                if (PointsAreCloseIn2D(positions, pos, minDistanceToOtherObjects))
                    continue;

                positions.Add(pos);
                if (positions.Count >= nrPositions)
                    return positions;
            }

        int nrPositionsLeft = nrPositions - positions.Count;
        for (int idx = 0; idx < nrPositionsLeft; ++idx)
            positions.Add(center);
        return positions;
    }

    public void RegisterDeath(Mob mob, float healthDropRate, float resourceDropRate, int resourceDropAmount, int scoreDrop, float boostDropRate, float consumableDropRate)
    {
        Vector3 pos = mob.transform.position;

        //decide what drops
        bool dropHealth = HealthIsDropped(healthDropRate);
        bool dropResource = ResourceIsDropped(resourceDropRate);
        bool dropBoost = BoostIsDropped(boostDropRate);
        bool dropConsumable = ConsumableIsDropped(consumableDropRate);

        List<Vector3> additionalPositionsToAvoid = new List<Vector3>();

        int nrDrops = 0;
        if (dropHealth)
            ++nrDrops;
        if (dropResource)
            ++nrDrops;
        if (dropBoost)
            ++nrDrops;
        if (dropConsumable)
            ++nrDrops;

        List<Vector3> dropPos = new List<Vector3>();

        //if( mainScript.endless )
        if( false )
        {
            // dropPos = FindFreePositionsAround(pos, nrDrops);
        }
        else
        {
            Vector3 healthPos = pos;
            Vector3 resourcePos = pos;
            if (nrDrops > 1)
            {
                //compute two random positions away from each other
                Vector3 dir1 = new Vector3(UnityEngine.Random.value - 0.5f, UnityEngine.Random.value - 0.5f, 0);
                dir1.Normalize();

                healthPos = pos + dir1 * DropRadius;
                resourcePos = pos - dir1 * DropRadius;

                if (!g.i.PositionIsFree(healthPos))
                    healthPos = g.i.GetFreeRandomPosition(pos, DropRadius);
                if (!g.i.PositionIsFree(resourcePos))
                    resourcePos = g.i.GetFreeRandomPosition(pos, DropRadius);
            }
            dropPos.Add(healthPos);
            dropPos.Add(resourcePos);
        }
        
        int dropPosIdx = 0;

        //drop health
        if ( dropHealth )
        {
            InstantiateHealthDrop(dropPos[dropPosIdx++]);
        }

        //drop resource
        if( dropResource)
        {
            GameObject resource = InstantiateResourceDrop(dropPos[dropPosIdx++]);
            resource.GetComponent<Resource>().amount = resourceDropAmount;
        }

        //drop boost
        if (dropBoost)
        {
            GameObject boost = InstantiateBoostDrop(dropPos[dropPosIdx++]);
            boost.GetComponent<BoostDrop>().Initialize();
        }

        //drop consumable
        if (dropConsumable)
        {
            if( UnityEngine.Random.value < 0.5 )
                InstantiateFirestormDrop(dropPos[dropPosIdx++]);
            else
                InstantiateStopwatchDrop(dropPos[dropPosIdx++]);
        }

        //instantiate chain reaction
        if (PlayerPrefs.GetInt("Boom") != 0 && mob.GetComponent<Explodable>().CanGenerateChainReaction())
        {
            if((UnityEngine.Random.value * 100f) < g.i.boosts.BoomPercent)
            {
                var boom = g.Clone(boomRef, pos);
                boom.GetComponent<Boom>().SetStartMob(mob);
            }
        }
    }
    
    bool HealthIsDropped(float healthDropRate)
    {
        if (PlayerPrefs.GetString("Difficulty") == "VeryHard")
            return false;

        int healthOnMap = g.i.healthDrops.Count;

        if (PlayerPrefs.GetInt("Bought Shield 1") == 1)
        {
            int totalHealth = g.i.playerHealth.GetHealth() + healthOnMap;
            if (PlayerPrefs.GetInt("Shield 1") == 1)
                ++totalHealth;

            if (totalHealth >= g.i.playerHealth.maxHealth + 1)
                return false;
        }
        else
        {
            int totalHealth = g.i.playerHealth.GetHealth() + healthOnMap;
            if (totalHealth >= g.i.playerHealth.maxHealth)
                return false;
        }

        if (PlayerPrefs.GetInt("More health") != 0)
            healthDropRate += g.i.boosts.MoreHealthPercent / 100f * healthDropRate;

        double dropRoll = UnityEngine.Random.value * 100;
        return dropRoll < healthDropRate;
    }
    bool ResourceIsDropped(float resourceDropRate)
    {
        if (PlayerPrefs.GetInt("More money") != 0)
            resourceDropRate += g.i.boosts.MoreMoneyPercent / 100f * resourceDropRate;

        double dropRoll = UnityEngine.Random.value * 100;
        return dropRoll < resourceDropRate;
    }
    bool BoostIsDropped(float boostDropRate)
    {
        return false; //don't drop boosts in campaign mode
        
        // double dropRoll = UnityEngine.Random.value * 100;
        // return dropRoll < boostDropRate;
    }
    bool ConsumableIsDropped(float consumableDropRate)
    {
        return false; //don't drop consumables in campaign mode

        // double dropRoll = UnityEngine.Random.value * 100;
        // return dropRoll < consumableDropRate;
    }
    public GameObject InstantiateBoostDrop(Vector3 pos)
    {
        GameObject obj = g.Clone(boostDrop, pos);
        allDrops.Add(obj);
        return obj;
    }
    public GameObject InstantiateHealthDrop(Vector3 pos)
    {
        GameObject obj = g.Clone(healthDrop, pos);
        allDrops.Add(obj);
        return obj;
    }
    public GameObject InstantiateResourceDrop(Vector3 pos)
    {
        GameObject obj = g.Clone(resourceDrop, pos);
        allDrops.Add(obj);
        return obj;
    }
    public GameObject InstantiateFirestormDrop(Vector3 pos)
    {
        GameObject obj = g.Clone(firestormDrop, pos);
        allDrops.Add(obj);
        return obj;
    }
    public GameObject InstantiateStopwatchDrop(Vector3 pos)
    {
        GameObject obj = g.Clone(stopwatchDrop, pos);
        allDrops.Add(obj);
        return obj;
    }
    void RemoveInactiveDropsFromAllDrops()
    {
        List<GameObject> activeDrops = new List<GameObject>();
        foreach (var drop in allDrops)
            if (!(drop == null) && drop.activeSelf)
                activeDrops.Add(drop);
        allDrops = activeDrops;
    }
    public List<GameObject> GetAllDrops()
    {
        RemoveInactiveDropsFromAllDrops();
        return allDrops;
    }
}