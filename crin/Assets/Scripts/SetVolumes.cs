﻿using UnityEngine;
using UnityEngine.Audio;

class SetVolumes : MonoBehaviour
{
    public AudioMixer masterMixer;
    void Start()
    {
        if (masterMixer)
        {
            masterMixer.SetFloat("MusicVolume", PlayerPrefs.GetFloat("MusicVolume"));
            masterMixer.SetFloat("SoundEffectsVolume", PlayerPrefs.GetFloat("SoundEffectsVolume"));   
        }
    }
}