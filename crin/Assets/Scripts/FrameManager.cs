using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameManager : MonoBehaviour
{
    public float durationUntilQuit = 5f;
    List<float> mFrames = new List<float>();
    int frameIdx = 0;

    void Awake()
    {
        //DontDestroyOnLoad(gameObject);
        var frameRate = Screen.currentResolution.refreshRate;
        Debug.Log($"FrameManager------------------------------");
        Debug.Log($"frame rate: {frameRate}");
        Application.targetFrameRate = frameRate;
        if (durationUntilQuit > 0)
            StartCoroutine(AutoQuit(durationUntilQuit));
    }
    IEnumerator AutoQuit(float duration)
    {
        yield return new WaitForSecondsRealtime(duration);

        prof.OutputAll();

        bool none = true;
        for (int iFrame = 0; iFrame < mFrames.Count; ++iFrame)
            if (iFrame <= 2 && mFrames[iFrame] >= 60f || // the first 3 frames are weird on my phone for some reason
                iFrame > 2 && mFrames[iFrame] >= 18f)
            {
                Debug.Log("FrameManager: frame " + (iFrame + 1) + ": " + mFrames[iFrame]);
                none = false;
            }
        if (none)
            Debug.Log("all frames are under 18 ms");

#if UNITY_EDITOR
        // Application.Quit() does not work in the editor so UnityEditor.EditorApplication.isPlaying needs to be set to false to end the game.
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
    void Update()
    {
        ++frameIdx;
        var frameTime = Time.unscaledDeltaTime * 1000;
        // mFrames.Add(frameTime);

        if (frameTime >= 18)
            Debug.Log($"FrameManager: frame {frameIdx}: {frameTime}");
    }

}