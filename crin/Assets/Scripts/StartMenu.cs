﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour
{
    public bool currentlyInGame = false;
    public AudioClip menuChangeClip;
    public AudioClip terrorHit;

    GameObject newGameButton;
    GameObject continueButton;
    GameObject debugOptionsPanel;
    GameObject endlessOptionsPanel;
    AudioSource musicSource;
    List<GameObject> upgrades = new List<GameObject>();
    List<Image> boostIcons = new List<Image>();
    GameObject leftHandedButton;
    GameObject rightHandedButton;
    GameObject mainButtons;
    GameObject optionsButtons;
    GameObject audioOptionsButtons;
    SetupCameras setupCameras;
    GUIOrienter guiOrienter;
    GameObject backgroundRegular;
    GameObject backgroundHealed;
    AudioMixer masterMixer;
    Slider musicVolumeSlider;
    Slider soundEffectsSlider;
    AudioManager audioManager;
    GameObject resetConfirmPanel;
    GameObject alreadyReplayingTutorialPanel;
    GameObject femaleButton;
    GameObject maleButton;
    GameObject cannotReplayFinalePanel;
    GameObject creditsPanel;
    GameObject background2Male;
    GameObject background2Female;
    GameObject endlessButtons;
    GameObject backButton;
    GameObject endlessRulesPanel;
    GameObject interruptEndlessPanel;
    GameObject interruptCampaignPanel;
    GameObject upgradesText;
    GameObject upgradesParent;
    GameObject boostsText;
    GameObject boostsParent;
    GameObject boostsOutlines;
    GameObject restartGamePanel;
    GameObject resourcesIcon;
    GameObject resourcesText;
    GameObject scoreText;
    GameObject levelText;
    GameObject infinitySign;
    GameObject boostDescription;
    int lastBoostDescription = -1;
    GameObject newEndlessButton;
    GameObject continueEndlessButton;
    Text difficultyDescription;
    GameObject difficultyCasual;
    GameObject difficultyMedium;
    GameObject difficultyHard;
    GameObject difficultyVeryHard;
    string difficultySelected;
    GameObject difficultySelect;
    GameObject changeDifficultyPanel;

    void Awake()
    {
        audioManager = GameObject.FindObjectOfType<AudioManager>();

        debugOptionsPanel = GameObject.Find("DebugOptionsPanel");
        debugOptionsPanel.SetActive(false);
        endlessOptionsPanel = GameObject.Find("EndlessOptionsPanel");
        endlessOptionsPanel.SetActive(false);
        resetConfirmPanel = GameObject.Find("ResetPanel");
        resetConfirmPanel.SetActive(false);
        alreadyReplayingTutorialPanel = GameObject.Find("AlreadyReplayingTutorialPanel");
        alreadyReplayingTutorialPanel.SetActive(false);
        cannotReplayFinalePanel = GameObject.Find("CannotReplayFinalePanel");
        cannotReplayFinalePanel.SetActive(false);
        creditsPanel = GameObject.Find("CreditsPanel");
        creditsPanel.SetActive(false);
        endlessRulesPanel = GameObject.Find("EndlessRulesPanel");
        endlessRulesPanel.SetActive(false);
        interruptEndlessPanel = GameObject.Find("InterruptEndlessPanel");
        interruptEndlessPanel.SetActive(false);
        interruptCampaignPanel = GameObject.Find("InterruptCampaignPanel");
        interruptCampaignPanel.SetActive(false);
        restartGamePanel = GameObject.Find("RestartGamePanel");
        restartGamePanel.SetActive(false);
        boostDescription = GameObject.Find("BoostDescription");
        boostDescription.SetActive(false);
        changeDifficultyPanel = GameObject.Find("ChangeDifficultyPanel");
        changeDifficultyPanel.SetActive(false);

        if (!PlayerPrefs.HasKey("StartMenuMusicTime"))
            PlayerPrefs.SetFloat("StartMenuMusicTime", 0);
        GameObject musicObject = GameObject.Find("Music");
        if (musicObject)
        {
            if (PlayerPrefs.GetInt("FinishedCampaign") == 0)
            {
                GameObject other = GameObject.Find("MusicHealed");
                Destroy(other);
            }
            else
            {
                Destroy(musicObject);
                musicObject = GameObject.Find("MusicHealed");
            }
            musicSource = musicObject.GetComponent<AudioSource>();
            musicSource.time = PlayerPrefs.GetFloat("StartMenuMusicTime");
        }

        upgradesText = GameObject.Find("UpgradesText");
        upgradesParent = GameObject.Find("Upgrades");
        boostsText = GameObject.Find("BoostsText");
        boostsParent = GameObject.Find("Boosts");
        boostsOutlines = GameObject.Find("BoostsOutlines");

        resourcesIcon = GameObject.Find("Crystal");
        resourcesText = GameObject.Find("ResourcesText");
        scoreText = GameObject.Find("ScoreText");
        levelText = GameObject.Find("LevelText");
        infinitySign = GameObject.Find("InfinitySign");

        foreach (Transform child in upgradesParent.transform)
            upgrades.Add(child.gameObject);
        boostIcons.AddRange(boostsParent.GetComponentsInChildren<Image>());

        debugOptionsPanel.transform.Find("Invulnerable").GetComponent<Toggle>().isOn = PlayerPrefs.GetInt("PlayerInvulnerable") != 0;
        debugOptionsPanel.transform.Find("InfiniteAmmo").GetComponent<Toggle>().isOn = PlayerPrefs.GetInt("PlayerInfiniteAmmo") != 0;

        //left handed/right handed
        leftHandedButton = gameObject.transform.Find("Panel/MenuButtons/Options/LeftHanded").gameObject;
        rightHandedButton = gameObject.transform.Find("Panel/MenuButtons/Options/RightHanded").gameObject;

        //male/female
        femaleButton = gameObject.transform.Find("Panel/MenuButtons/Options/Female").gameObject;
        maleButton = gameObject.transform.Find("Panel/MenuButtons/Options/Male").gameObject;

        mainButtons = gameObject.transform.Find("Panel/MenuButtons/Main").gameObject;
        optionsButtons = gameObject.transform.Find("Panel/MenuButtons/Options").gameObject;
        audioOptionsButtons = gameObject.transform.Find("Panel/MenuButtons/Audio").gameObject;
        endlessButtons = gameObject.transform.Find("Panel/MenuButtons/Endless").gameObject;

        mainButtons.SetActive(true);
        HideOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);

        setupCameras = GameObject.FindObjectOfType<SetupCameras>();
        guiOrienter = GameObject.FindObjectOfType<GUIOrienter>();

        //backgrounds
        backgroundRegular = gameObject.transform.Find("Panel/BackgroundRegular").gameObject;
        backgroundHealed = gameObject.transform.Find("Panel/BackgroundHealed").gameObject;
        background2Male = backgroundRegular.transform.Find("Background2").gameObject;
        background2Female = backgroundRegular.transform.Find("Background2Fem").gameObject;

        musicVolumeSlider = gameObject.transform.Find("Panel/MenuButtons/Audio/MusicSlider").GetComponent<Slider>();
        soundEffectsSlider = gameObject.transform.Find("Panel/MenuButtons/Audio/SoundEffectsSlider").GetComponent<Slider>();
    }
    void Start()
    {
        masterMixer.SetFloat("MusicVolume", PlayerPrefs.GetFloat("MusicVolume"));
        masterMixer.SetFloat("SoundEffectsVolume", PlayerPrefs.GetFloat("SoundEffectsVolume"));
        //}
        //private void OnEnable()
        //{
        newGameButton = gameObject.transform.Find("Panel/MenuButtons/Main/New game").gameObject;
        continueButton = gameObject.transform.Find("Panel/MenuButtons/Main/Continue").gameObject;
        bool canContinue =
            PlayerPrefs.GetInt("PlayerHealthCampaign") > 0 ||
            (PlayerPrefs.GetInt("Playing tutorial") == 1) ||
            (PlayerPrefs.GetInt("Replaying finale") == 1) ||
            ((PlayerPrefs.GetString("Difficulty") == "Casual" ||
            PlayerPrefs.GetString("Difficulty") == "Medium") &&
            PlayerPrefs.GetInt("CurrentLevel") > 0);

        if (canContinue)
        {
            newGameButton.SetActive(false);
            continueButton.SetActive(true);
        }
        else
        {
            newGameButton.SetActive(true);
            continueButton.SetActive(false);
        }

        newEndlessButton = gameObject.transform.Find("Panel/MenuButtons/Main/New endless").gameObject;
        continueEndlessButton = gameObject.transform.Find("Panel/MenuButtons/Main/Continue endless").gameObject;
        bool canContinueEndless = PlayerPrefs.GetInt("PlayerHealthEndless") > 0;
        if (canContinueEndless)
        {
            newEndlessButton.SetActive(false);
            continueEndlessButton.SetActive(true);
        }
        else
        {
            newEndlessButton.SetActive(true);
            continueEndlessButton.SetActive(false);
        }

        backButton = gameObject.transform.Find("Panel/MenuButtons/Main/Back").gameObject;
        Debug.Log("start menu Start currently in game: " + currentlyInGame);
        if (currentlyInGame)
        {
            if (PlayerPrefs.GetString("LastGameTypePlayed") == "Campaign" || PlayerPrefs.GetInt("Replaying finale") == 1)
            {
                continueButton.GetComponent<Button>().interactable = false;
                continueEndlessButton.GetComponent<Button>().interactable = true;
            }
            else
            {
                continueButton.GetComponent<Button>().interactable = true;
                continueEndlessButton.GetComponent<Button>().interactable = false;
            }
            backButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            continueButton.GetComponent<Button>().interactable = true;
            continueEndlessButton.GetComponent<Button>().interactable = true;
            backButton.GetComponent<Button>().interactable = false;
        }
        
        

        if (PlayerPrefs.GetInt("FinishedCampaign") == 0)
        {
            backgroundRegular.SetActive(true);
            backgroundHealed.SetActive(false);
            if (PlayerPrefs.GetString("Gender") == "Male")
            {
                background2Male.SetActive(true);
                background2Female.SetActive(false);
            }
            else
            {
                background2Male.SetActive(false);
                background2Female.SetActive(true);
            }
        }
        else
        {
            backgroundRegular.SetActive(false);
            backgroundHealed.SetActive(true);
        }

        //show upgrades
        List<string> upgradeNames = new List<string>();
        List<int> upgradeCounts = new List<int>();
        upgradeNames.Add("Shield");
        upgradeCounts.Add(3);
        upgradeNames.Add("Reinforced glass");
        upgradeCounts.Add(5);
        upgradeNames.Add("Phase ring");
        upgradeCounts.Add(2);
        upgradeNames.Add("Piercing light");
        upgradeCounts.Add(0);
        upgradeNames.Add("Portal charm");
        upgradeCounts.Add(0);
        for (int idx = 0; idx < upgradeNames.Count; ++idx)
        {
            string numberPostfix = "";
            if (upgradeCounts[idx] > 0)
                numberPostfix = " 1";
            if (PlayerPrefs.GetInt("Bought " + upgradeNames[idx] + numberPostfix) == 0)
            {
                foreach (Transform child in upgrades[idx].transform)
                    child.gameObject.SetActive(false);
            }
            else
            {
                foreach (Transform child in upgrades[idx].transform)
                    child.gameObject.SetActive(true);

                if (upgradeCounts[idx] > 0)
                {
                    int boughtIdx = 2;
                    while (PlayerPrefs.GetInt("Bought " + upgradeNames[idx] + " " + boughtIdx.ToString()) != 0)
                        ++boughtIdx;
                    --boughtIdx;
                    var countText = upgrades[idx].transform.Find("Count").gameObject.GetComponent<Text>();
                    countText.text = boughtIdx.ToString() + "/" + upgradeCounts[idx];
                }
            }
        }

        //show boosts
        int boostIdx = PlayerPrefs.GetInt("BoostIdx");
        for (int idx = 1; idx <= 6; ++idx)
        {
            string boostName = PlayerPrefs.GetString("Boost" + idx.ToString());
            if (idx <= boostIdx)
            {
                boostIcons[idx - 1].sprite = Resources.Load<Sprite>("Images/Boosts/Boosts " + boostName);
                boostIcons[idx - 1].color = new Color(255, 255, 255, 255);
            }
            else
            {
                boostIcons[idx - 1].color = new Color(255, 255, 255, 0);
            }
        }

        //show resources/score and level text/infinity sign
        if (PlayerPrefs.GetString("LastGameTypePlayed") == "Campaign")
        {
            resourcesIcon.SetActive(true);
            resourcesText.SetActive(true);
            scoreText.SetActive(false);
            levelText.SetActive(true);
            infinitySign.SetActive(false);
        }
        else
        {
            resourcesIcon.SetActive(false);
            resourcesText.SetActive(false);
            scoreText.SetActive(true);
            levelText.SetActive(false);
            infinitySign.SetActive(true);
        }

        UpdateLeftRightButtons();
        UpdateFemaleMaleButtons();
    }
    void UpdateLeftRightButtons()
    {
        if (!optionsButtons.activeSelf)
            return;

        Color leftColor, rightColor;
        if (PlayerPrefs.GetString("Handedness") == "Left")
        {
            leftColor = new Color(1, 1, 1, 1);
            rightColor = new Color(1, 1, 1, 0.5f);
        }
        else
        {
            leftColor = new Color(1, 1, 1, 0.5f);
            rightColor = new Color(1, 1, 1, 1);
        }

        leftHandedButton.transform.Find("Image").GetComponent<Image>().color = leftColor;

        rightHandedButton.transform.Find("Image").GetComponent<Image>().color = rightColor;
    }
    public void NewSession()
    {
        StartCoroutine("NewSessionCoroutine");
    }
    public void ContinueSession()
    {
        StartCoroutine("ContinueSessionCoroutine");
    }
    public void ContinueSessionYes()
    {
        if (currentlyInGame)
            g.i.Unpause();
        else
            PlayerPrefs.SetFloat("StartMenuMusicTime", musicSource.time);

        bool canContinue =
            PlayerPrefs.GetInt("PlayerHealthCampaign") > 0 ||
            (PlayerPrefs.GetInt("Playing tutorial") == 1) ||
            ((PlayerPrefs.GetString("Difficulty") == "Casual" ||
            PlayerPrefs.GetString("Difficulty") == "Medium") &&
            PlayerPrefs.GetInt("CurrentLevel") > 0);
        if (canContinue)
            PlayerPrefs.SetString("Start menu", "Continue");
        else
            PlayerPrefs.SetString("Start menu", "New");

        SceneManager.LoadScene("SceneSelection");
    }
    public void ContinueSessionNo()
    {
        mainButtons.SetActive(true);
        HideOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);

        interruptEndlessPanel.SetActive(false);

        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
    void LoadCustomScene(string scene)
    {
        if (currentlyInGame)
            g.i.Unpause();
        else
            PlayerPrefs.SetFloat("StartMenuMusicTime", musicSource.time);

        SceneManager.LoadScene(scene);
    }
    public void OptionsClicked()
    {
        StartCoroutine("OptionsClickedCoroutine");
    }
    public void BackFromOptionsClicked()
    {
        StartCoroutine("BackFromOptionsClickedCoroutine");
    }
    public void BackFromAudioOptionsClicked()
    {
        StartCoroutine("BackFromAudioOptionsClickedCoroutine");
    }
    public void AudioOptionsClicked()
    {
        StartCoroutine("AudioOptionsClickedCoroutine");
    }
    float VolumeMixerToSlider(float val)
    {
        return Mathf.Pow(2.71828f, val / 20.0f); //(val + 80.0f) / 100.0f;
        //return (val + 80.0f) / 100.0f;
    }
    float VolumeSliderToMixer(float val)
    {
        return Mathf.Log(val) * 20.0f; //val * 100.0f - 80.0f;
        //return val * 100.0f - 80.0f;
    }
    public void UpdateVolumeControls()
    {
        float musicVolume = 0;
        masterMixer.GetFloat("MusicVolume", out musicVolume);
        musicVolumeSlider.value = VolumeMixerToSlider(musicVolume);

        float soundEffectsVolume = 0;
        masterMixer.GetFloat("SoundEffectsVolume", out soundEffectsVolume);
        soundEffectsSlider.value = VolumeMixerToSlider(soundEffectsVolume);
    }
    public void MusicVolumeChanged()
    {
        if (!musicVolumeSlider)
            return;

        float musicVolume = VolumeSliderToMixer(musicVolumeSlider.value);
        masterMixer.SetFloat("MusicVolume", musicVolume);
        PlayerPrefs.SetFloat("MusicVolume", musicVolume);
    }
    public void SoundEffectsVolumeChanged()
    {
        if (!soundEffectsSlider)
            return;

        float soundEffectsVolume = VolumeSliderToMixer(soundEffectsSlider.value);
        masterMixer.SetFloat("SoundEffectsVolume", soundEffectsVolume);
        PlayerPrefs.SetFloat("SoundEffectsVolume", soundEffectsVolume);

        if (!soundEffectsSlider.GetComponent<SoundEffectsVolume>().IsBeingDragged())
        {
            audioManager.PlayPositionIndependentClip(terrorHit);
        }
    }
    public void DebugOptionsClicked()
    {
        debugOptionsPanel.SetActive(true);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
    public void ExitOptionsClicked()
    {
        debugOptionsPanel.SetActive(false);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
    public void EndlessOptionsClicked()
    {
        endlessOptionsPanel.SetActive(true);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
    public void ExitEndlessOptionsClicked()
    {
        endlessOptionsPanel.SetActive(false);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
    public void QuitClicked()
    {
        StartCoroutine("QuitClickedCoroutine");
    }
    public void EditorsChoice()
    {
        Record.Event("editor 1");
        PlayerPrefs.SetString("Start menu", "Editor 1");
        LoadCustomScene("SceneSelection");
    }
    public void ResetAll()
    {
        Record.Event("reset");
        PlayerPrefs.SetString("Start menu", "Reset");
        LoadCustomScene("SceneSelection");
    }
    public void AngerLevel()
    {
        //2
        PlayerPrefs.SetString("Start menu", "2");
        LoadCustomScene("SceneSelection");
    }
    public void DepressionLevel()
    {
        //3
        PlayerPrefs.SetString("Start menu", "3");
        LoadCustomScene("SceneSelection");
    }
    public void AnxietyLevel()
    {
        //5
        PlayerPrefs.SetString("Start menu", "5");
        LoadCustomScene("SceneSelection");
    }
    public void ParanoiaLevel()
    {
        //6
        PlayerPrefs.SetString("Start menu", "6");
        LoadCustomScene("SceneSelection");
    }
    public void TerrorLevel()
    {
        //16
        PlayerPrefs.SetString("Start menu", "16");
        LoadCustomScene("SceneSelection");
    }
    public void RejectionLevel()
    {
        //30
        PlayerPrefs.SetString("Start menu", "30");
        LoadCustomScene("SceneSelection");
    }
    public void CustomLevel()
    {
        string customLevel = GameObject.Find("InputField").gameObject.GetComponent<InputField>().text;
        int level = 0;
        if (int.TryParse(customLevel, out level))
        {
            if (level >= 1 && level <= 30)
            {
                audioManager.PlayPositionIndependentClip(menuChangeClip);
                PlayerPrefs.SetInt("CurrentLevel", level);
                //PlayerPrefs.SetString("Start menu", level.ToString());
                //LoadCustomScene("SceneSelection");
            }
        }
    }
    public void InvulnerableChanged(bool selected)
    {
        PlayerPrefs.SetInt("PlayerInvulnerable", selected ? 1 : 0);
    }
    public void InfiniteAmmoChanged(bool selected)
    {
        PlayerPrefs.SetInt("PlayerInfiniteAmmo", selected ? 1 : 0);
    }
    public void LeftHanded()
    {
        if (PlayerPrefs.GetString("Handedness") != "Left")
        {
            PlayerPrefs.SetString("Handedness", "Left");
            UpdateLeftRightButtons();
            if (setupCameras != null)
                setupCameras.DoSetupCameras();
            if (guiOrienter != null)
                guiOrienter.Orient();
            audioManager.PlayPositionIndependentClip(menuChangeClip);
        }
    }
    public void RightHanded()
    {
        if (PlayerPrefs.GetString("Handedness") != "Right")
        {
            PlayerPrefs.SetString("Handedness", "Right");
            UpdateLeftRightButtons();
            if (setupCameras != null)
                setupCameras.DoSetupCameras();
            if (guiOrienter != null)
                guiOrienter.Orient();
            audioManager.PlayPositionIndependentClip(menuChangeClip);
        }
    }
    public void Female()
    {
        string latestGenderChosen = PlayerPrefs.GetString("Gender");
        if (PlayerPrefs.GetString("UpdateGender") != "")
            latestGenderChosen = PlayerPrefs.GetString("UpdateGender");

        if (latestGenderChosen != "Female")
        {
            PlayerPrefs.SetString("UpdateGender", "Female");
            UpdateFemaleMaleButtons();
            audioManager.PlayPositionIndependentClip(menuChangeClip);
            //if (currentlyInGame)
            {
                HideOptionsButtons();
                restartGamePanel.SetActive(true);
            }
        }
    }
    public void Male()
    {
        string latestGenderChosen = PlayerPrefs.GetString("Gender");
        if (PlayerPrefs.GetString("UpdateGender") != "")
            latestGenderChosen = PlayerPrefs.GetString("UpdateGender");

        if (latestGenderChosen != "Male")
        {
            PlayerPrefs.SetString("UpdateGender", "Male");
            UpdateFemaleMaleButtons();
            audioManager.PlayPositionIndependentClip(menuChangeClip);
            //if (currentlyInGame)
            {
                HideOptionsButtons();
                restartGamePanel.SetActive(true);
            }
        }
    }
    void UpdateFemaleMaleButtons()
    {
        if (!optionsButtons.activeSelf)
            return;

        string latestGenderChosen = PlayerPrefs.GetString("Gender");
        if (PlayerPrefs.GetString("UpdateGender") != "")
            latestGenderChosen = PlayerPrefs.GetString("UpdateGender");

        Color femaleColor, maleColor;
        if (latestGenderChosen == "Female")
        {
            femaleColor = new Color(1, 1, 1, 1);
            maleColor = new Color(1, 1, 1, 0.5f);
        }
        else
        {
            femaleColor = new Color(1, 1, 1, 0.5f);
            maleColor = new Color(1, 1, 1, 1);
        }

        femaleButton.transform.Find("Image").GetComponent<Image>().color = femaleColor;

        maleButton.transform.Find("Image").GetComponent<Image>().color = maleColor;
    }
    public void ResetProgress()
    {
        StartCoroutine("ResetProgressCoroutine");
    }
    public void ResetProgressYes()
    {
        resetConfirmPanel.SetActive(false);

        PlayerPrefs.DeleteAll();

        //debug13
        PlayerPrefs.SetInt("minTotalHealthPhase1", 8);
        PlayerPrefs.SetInt("maxTotalHealthPhase1", 21);
        PlayerPrefs.SetInt("minTotalHealthPhase2", 18);
        PlayerPrefs.SetInt("maxTotalHealthPhase2", 23);
        PlayerPrefs.SetInt("minTotalHealthPhase3", 11);
        PlayerPrefs.SetInt("maxTotalHealthPhase3", 38);
        PlayerPrefs.SetInt("minDelayBetweenWaves", 15);
        PlayerPrefs.SetInt("maxDelayBetweenWaves", 30);
        PlayerPrefs.SetInt("phase1Duration", 90);
        PlayerPrefs.SetInt("phase2Duration", 180);

        PlayerPrefs.SetInt("Start", 1);
        PlayerPrefs.SetString("Difficulty", "Hard");
        SceneManager.LoadScene("SceneSelection");
    }
    public void ResetProgressNo()
    {
        mainButtons.SetActive(false);
        EnterOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);

        resetConfirmPanel.SetActive(false);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
    public void ReplayTutorial()
    {
        StartCoroutine("ReplayTutorialCoroutine");
    }
    public void FailedReplayTutorial()
    {
        mainButtons.SetActive(false);
        EnterOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);

        alreadyReplayingTutorialPanel.SetActive(false);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
    public void ReplayFinale()
    {
        StartCoroutine("ReplayFinaleCoroutine");
    }
    public void FailedReplayFinale()
    {
        mainButtons.SetActive(false);
        EnterOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);

        cannotReplayFinalePanel.SetActive(false);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
    public void ShowCredits()
    {
        StartCoroutine("ShowCreditsCoroutine");
    }
    public void HideCredits()
    {
        mainButtons.SetActive(false);
        EnterOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);

        creditsPanel.SetActive(false);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
    public void NewEndless()
    {
        StartCoroutine("NewEndlessCoroutine");
    }

    public void ExitEndlessRules()
    {
        endlessRulesPanel.SetActive(false);
        PlayerPrefs.SetInt("Displayed endless rules", 1);
        mainButtons.SetActive(false);
        HideOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(true);

        upgradesText.SetActive(true);
        upgradesParent.SetActive(true);
        boostsText.SetActive(true);
        boostsParent.SetActive(true);
        boostsOutlines.SetActive(true);

        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }

    public void BackFromNewEndless()
    {
        StartCoroutine("BackFromNewEndlessCoroutine");
    }

    public void NewEndlessCustom(string endlessScene)
    {
        PlayerPrefs.SetString("New endless scene", endlessScene);
        if (currentlyInGame)
        {
            mainButtons.SetActive(false);
            HideOptionsButtons();
            audioOptionsButtons.SetActive(false);
            endlessButtons.SetActive(false);

            interruptCampaignPanel.SetActive(true);
            audioManager.PlayPositionIndependentClip(menuChangeClip);
        }
        else
            ContinueEndlessYes();
    }
    public void NewEndlessAnger()
    {
        NewEndlessCustom("EndlessAnger");
    }
    public void NewEndlessDepression()
    {
        NewEndlessCustom("EndlessDepression");
    }
    public void NewEndlessParanoia()
    {
        NewEndlessCustom("EndlessParanoia");
    }
    public void NewEndlessSocialAnxiety()
    {
        NewEndlessCustom("EndlessAnxiety");
    }
    public void NewEndlessTerror()
    {
        NewEndlessCustom("EndlessTerror");
    }
    public void ContinueEndless()
    {
        StartCoroutine("ContinueEndlessCoroutine");
    }
    public void ContinueEndlessYes()
    {
        if (currentlyInGame)
            g.i.Unpause();
        else
            PlayerPrefs.SetFloat("StartMenuMusicTime", musicSource.time);

        bool canContinueEndless = PlayerPrefs.GetInt("PlayerHealthEndless") > 0;
        if (canContinueEndless)
        {
            PlayerPrefs.SetInt("Continue endless", 1);
            SceneManager.LoadScene("SceneSelection");
        }
        else
        {
            PlayerPrefs.SetInt("New endless", 1);
            SceneManager.LoadScene("SceneSelection");
        }
    }
    public void ContinueEndlessNo()
    {
        audioManager.PlayPositionIndependentClip(menuChangeClip);

        bool canContinueEndless = PlayerPrefs.GetInt("PlayerHealthEndless") > 0;
        if (canContinueEndless)
        {
            mainButtons.SetActive(true);
            HideOptionsButtons();
            audioOptionsButtons.SetActive(false);
            endlessButtons.SetActive(false);
        }
        else
        {
            mainButtons.SetActive(false);
            HideOptionsButtons();
            audioOptionsButtons.SetActive(false);
            endlessButtons.SetActive(true);
        }

        interruptCampaignPanel.SetActive(false);
    }
    public void BackToGame()
    {
        audioManager.PlayPositionIndependentClip(menuChangeClip);

        g.i.Unpause();
        Destroy(gameObject.transform.parent.gameObject);
    }
    public void RestartGame()
    {
        restartGamePanel.SetActive(false);
        EnterOptionsButtons();

        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
    public void ShowBoostDescription(int boostIdx)
    {
        boostDescription.SetActive(true);
        boostDescription.GetComponent<RectTransform>().anchoredPosition = boostIcons[boostIdx].GetComponent<RectTransform>().anchoredPosition;
        lastBoostDescription = boostIdx;
        Boosts boostsClass = new Boosts();
        var boosts = boostsClass.GetBoosts();
        string boostName = PlayerPrefs.GetString("Boost" + (boostIdx + 1).ToString());
        foreach (var boost in boosts)
            if (boost.InternalName == boostName)
            {
                if (PlayerPrefs.GetString("LastGameTypePlayed") == "Campaign")
                    boostDescription.GetComponentInChildren<Text>().text = boost.Description;
                else
                    boostDescription.GetComponentInChildren<Text>().text = boost.DescriptionEndless;
            }
    }
    public void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            bool somethingSelected = false;
            for (int idx = 0; idx < PlayerPrefs.GetInt("BoostIdx"); ++idx)
            {
                if (boostIcons[idx].GetComponent<MenuBoostClicked>().isSelected)
                {
                    somethingSelected = true;
                    if (idx == lastBoostDescription)
                    {
                        boostDescription.SetActive(false);
                        lastBoostDescription = -1;
                    }
                    else
                        ShowBoostDescription(idx);
                }
            }
            if (!somethingSelected)
            {
                boostDescription.SetActive(false);
                lastBoostDescription = -1;
            }
            for (int idx = 0; idx < 6; ++idx)
                boostIcons[idx].GetComponent<MenuBoostClicked>().isSelected = false;
        }
    }
    
    public void EnterOptionsButtons()
    {
        optionsButtons.SetActive(true);
        upgradesText.SetActive(false);
        upgradesParent.SetActive(false);
        boostsText.SetActive(false);
        boostsParent.SetActive(false);
        boostsOutlines.SetActive(false);
    }
    public void HideOptionsButtons()
    {
        optionsButtons.SetActive(false);
    }
    public void ExitOptionsButtons()
    {
        HideOptionsButtons();
        upgradesText.SetActive(true);
        upgradesParent.SetActive(true);
        boostsText.SetActive(true);
        boostsParent.SetActive(true);
        boostsOutlines.SetActive(true);
    }

    public void DifficultyOptionsClicked()
    {
        StartCoroutine("DifficultyOptionsClickedCoroutine");
    }
    public void BackFromDifficultyOptionsClicked()
    {
        StartCoroutine("BackFromDifficultyOptionsClickedCoroutine");
    }

    IEnumerator NewSessionCoroutine()
    {
        Component[] button;
        button = mainButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "New game"))
            b.interactable = false;

        yield return null;

        button = mainButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;
        backButton.GetComponent<Button>().interactable = currentlyInGame;

        StartCoroutine("NewSessionCoroutine");
        Record.Event("new session");
        if (currentlyInGame)
        {
            mainButtons.SetActive(false);
            HideOptionsButtons();
            audioOptionsButtons.SetActive(false);
            endlessButtons.SetActive(false);

            interruptEndlessPanel.SetActive(true);
            audioManager.PlayPositionIndependentClip(menuChangeClip);
        }
        else
            ContinueSessionYes();
    }

    IEnumerator ContinueSessionCoroutine()
    {
        Component[] button;
        button = mainButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "Continue"))
            b.interactable = false;

        yield return null;

        button = mainButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;
        backButton.GetComponent<Button>().interactable = currentlyInGame;

        if (currentlyInGame)
        {
            mainButtons.SetActive(false);
            HideOptionsButtons();
            audioOptionsButtons.SetActive(false);
            endlessButtons.SetActive(false);

            interruptEndlessPanel.SetActive(true);
            audioManager.PlayPositionIndependentClip(menuChangeClip);
        }
        else
            ContinueSessionYes();
    }

    IEnumerator NewEndlessCoroutine()
    {
        Component[] button;
        GameObject backButton;
        backButton = GameObject.Find("Back");
        button = mainButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "New endless"))
            b.interactable = false;
        backButton.SetActive(false);

        yield return null;

        backButton.SetActive(true);

        button = mainButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;
        backButton.GetComponent<Button>().interactable = currentlyInGame;

        if (PlayerPrefs.GetInt("Displayed endless rules") == 0)
        {
            mainButtons.SetActive(false);
            HideOptionsButtons();
            audioOptionsButtons.SetActive(false);
            endlessButtons.SetActive(false);

            upgradesText.SetActive(false);
            upgradesParent.SetActive(false);
            boostsText.SetActive(false);
            boostsParent.SetActive(false);
            boostsOutlines.SetActive(false);

            endlessRulesPanel.SetActive(true);
            audioManager.PlayPositionIndependentClip(menuChangeClip);
        }
        else
        {
            mainButtons.SetActive(false);
            HideOptionsButtons();
            audioOptionsButtons.SetActive(false);
            endlessButtons.SetActive(true);

            audioManager.PlayPositionIndependentClip(menuChangeClip);
        }
    }

    IEnumerator ContinueEndlessCoroutine()
    {
        Component[] button;
        button = endlessButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "Continue endless"))
            b.interactable = false;

        yield return null;

        foreach (Button b in button)
            b.interactable = true;

        if (currentlyInGame)
        {
            mainButtons.SetActive(false);
            HideOptionsButtons();
            audioOptionsButtons.SetActive(false);
            endlessButtons.SetActive(false);

            interruptCampaignPanel.SetActive(true);
            audioManager.PlayPositionIndependentClip(menuChangeClip);
        }
        else
            ContinueEndlessYes();
    }

    IEnumerator OptionsClickedCoroutine()
    {
        Component[] button;
        GameObject optionsButton, backButton;
        button = mainButtons.GetComponentsInChildren<Button>();
        optionsButton = GameObject.Find("Options");
        backButton = GameObject.Find("Back");
        backButton.SetActive(false);
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "Options"))
            b.interactable = false;
        optionsButton.GetComponent<Button>().interactable = true;

        yield return null;

        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;

        backButton.SetActive(true);

        mainButtons.SetActive(false);
        EnterOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);
        audioManager.PlayPositionIndependentClip(menuChangeClip);

        UpdateLeftRightButtons();
        UpdateFemaleMaleButtons();


    }
    IEnumerator QuitClickedCoroutine()
    {
        Component[] button;
        button = mainButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "Exit game"))
            b.interactable = false;

        yield return null;

        foreach (Button b in button)
            b.interactable = true;
        backButton.GetComponent<Button>().interactable = currentlyInGame;

        PlayerPrefs.SetString("Start menu", "Quit");
        LoadCustomScene("SceneSelection");
    }

    IEnumerator AudioOptionsClickedCoroutine()
    {
        Component[] button;
        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "Audio"))
            b.interactable = false;

        yield return null;

        button = audioOptionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;

        mainButtons.SetActive(false);
        HideOptionsButtons();
        audioOptionsButtons.SetActive(true);
        endlessButtons.SetActive(false);

        audioManager.PlayPositionIndependentClip(menuChangeClip);
        if (currentlyInGame)
            musicLevel.Unpause();
        UpdateVolumeControls();
    }

    IEnumerator ReplayTutorialCoroutine()
    {
        Component[] button;
        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "ReplayTutorial"))
            b.interactable = false;

        yield return null;

        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;

        if (PlayerPrefs.GetInt("Replaying tutorial") == 1)
        {
            mainButtons.SetActive(false);
            HideOptionsButtons();
            audioOptionsButtons.SetActive(false);
            endlessButtons.SetActive(false);

            alreadyReplayingTutorialPanel.SetActive(true);
            audioManager.PlayPositionIndependentClip(menuChangeClip);
        }
        else
        {
            PlayerPrefs.SetInt("Replay tutorial", 1);
            SceneManager.LoadScene("SceneSelection");
        }
    }

    IEnumerator ReplayFinaleCoroutine()
    {
        Component[] button;
        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "ReplayFinale"))
            b.interactable = false;

        yield return null;

        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;

        if (PlayerPrefs.GetInt("FinishedCampaign") == 0)
        {
            mainButtons.SetActive(false);
            HideOptionsButtons();
            audioOptionsButtons.SetActive(false);
            endlessButtons.SetActive(false);

            cannotReplayFinalePanel.SetActive(true);
            audioManager.PlayPositionIndependentClip(menuChangeClip);
        }
        else
        {
            PlayerPrefs.SetInt("Replay finale", 1);
            SceneManager.LoadScene("SceneSelection");
        }
    }

    IEnumerator ResetProgressCoroutine()
    {
        Component[] button;
        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "ResetProgress"))
            b.interactable = false;

        yield return null;

        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;

        mainButtons.SetActive(false);
        HideOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);

        resetConfirmPanel.SetActive(true);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }

    IEnumerator ShowCreditsCoroutine()
    {
        Component[] button;
        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "Credits"))
            b.interactable = false;

        yield return null;

        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;

        mainButtons.SetActive(false);
        HideOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);

        creditsPanel.SetActive(true);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }

    IEnumerator BackFromOptionsClickedCoroutine()
    {
        Component[] button;
        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "Back"))
            b.interactable = false;

        yield return null;

        button = mainButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;
        backButton.GetComponent<Button>().interactable = currentlyInGame;

        mainButtons.SetActive(true);
        ExitOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);

        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }

    IEnumerator BackFromAudioOptionsClickedCoroutine()
    {
        Component[] button, slider;
        button = audioOptionsButtons.GetComponentsInChildren<Button>();
        slider = audioOptionsButtons.GetComponentsInChildren<Slider>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "Back"))
            b.interactable = false;
        foreach (Slider s in slider)
            s.interactable = false;

        yield return null;

        button = optionsButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;
        foreach (Slider s in slider)
            s.interactable = true;

        mainButtons.SetActive(false);
        EnterOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);

        audioManager.PlayPositionIndependentClip(menuChangeClip);
        if (currentlyInGame)
            musicLevel.Pause();
    }

    IEnumerator BackFromNewEndlessCoroutine()
    {
        Component[] button;
        button = endlessButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button.Where(bb => bb.GetComponent<Button>().name != "Back"))
            b.interactable = false;

        yield return null;

        button = endlessButtons.GetComponentsInChildren<Button>();
        foreach (Button b in button)
            b.interactable = true;

        mainButtons.SetActive(true);
        HideOptionsButtons();
        audioOptionsButtons.SetActive(false);
        endlessButtons.SetActive(false);
        audioManager.PlayPositionIndependentClip(menuChangeClip);
    }
}
