﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boom : MonoBehaviour
{
    public float duration = 0.0f;
    public GameObject energyBeam;
    Mob startMob;

    void Start()
    {
        StartCoroutine(ChainReaction());
    }

    public void SetStartMob(Mob mob)
    {
        startMob = mob;
    }

    IEnumerator ChainReaction()
    {
        Vector2 sourceRegion = g.i.Region(transform.position);

        List<string> tags = new List<string>();
        tags.Add("Depression");
        tags.Add("SocialAnxiety");
        tags.Add("Anger");
        tags.Add("Paranoia");
        tags.Add("Terror");
        tags.Add("RejectionAux");
        var mobs = new List<Mob>();
        foreach (var mob in g.i.mobs)
            if (tags.Contains(mob.tag))
                mobs.Add(mob);
                
        var affectedMobs = new List<Mob>();
        affectedMobs.Add(startMob);
        foreach (var mob in mobs)
            if (!mob.GetComponent<MobExpiration>().Expired())
            {
                Vector2 mobRegion = g.i.Region(mob.transform.position);
                if (mobRegion == sourceRegion && mob != startMob)
                    affectedMobs.Add(mob);
            }
        
        for( int idx = 1; idx < affectedMobs.Count; ++idx )
        {
            yield return new WaitForSeconds(0.15f);
            GameObject projectile = g.Clone(energyBeam);
            affectedMobs[idx].GetComponent<Explodable>().DisableChainReaction();
            projectile.GetComponent<EnergyBeam>().Initialize(affectedMobs[idx - 1].transform.position, affectedMobs[idx], false, false, false);
        }
        yield return null;
    }
}