﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public void PlayClip(AudioClip clip, Vector3 pos)
    {
        var worldRect = g.i.WorldRectOfMainCamera();
        Bounds bounds = new Bounds(worldRect.center, worldRect.size);
        bounds.extents = new Vector3(bounds.extents.x, bounds.extents.y, 10000);
        if (bounds.Contains(pos))
            GetComponent<AudioSource>().PlayOneShot(clip);
    }

    public void PlayPositionIndependentClip(AudioClip clip)
    {
        GetComponent<AudioSource>().PlayOneShot(clip);
    }

    public void PlayPositionIndependentClip(AudioClip clip, float volumeScale)
    {
        GetComponent<AudioSource>().PlayOneShot(clip, volumeScale);
    }
}