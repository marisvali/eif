﻿using UnityEngine;

public class TerrorAI : WorldObject
{
    public float timeBeforeFirstTerrorAttack = 0;
    public float timeBetweenTerrorAttacks = 0;
    public float activeTimeForTerrorAttack = 0;
    public float hitTime = 0;
    public AudioClip hit;
    public AudioClip attack;
    public AudioClip death;
    public GameObject icon;
    public Animator Anim;
    public SpriteRenderer visibility;
    public Sprite icon1;
    public Sprite icon2;
    public Sprite icon3;
    
    float lastStateChangeMoment = 0;
    bool stateJustChanged = false;
    bool freeze = false;
    float timeSinceFreeze = 0;
    bool wasTerrorizingWhenFrozen = false;
    bool wasJustHit = false;
    bool wasTerrorizingWhenHit = false;
    
    enum State
    {
        Dead,
        Waiting,
        Terrorizing,
        Frozen,
        Hit
    }
    State currentState;
    void Start()
    {   
        g.i.hiveMindPanic.AddActiveTerror();
        visibility.enabled = true;
        wasJustHit = false;
        freeze = false;
        stateJustChanged = true;
        GetComponent<MobHealth>().SetHealth(GetComponent<MobHealth>().startHealth);
        GetComponent<MobExpiration>().ResetExpiration();
        currentState = State.Waiting;
    }
    
    void Dead()
    {
        if (stateJustChanged)
        {
            if (g.i.spawnDrops)
                g.i.spawnDrops.RegisterDeath(GetComponent<Mob>(), g.i.spawnDrops.TerrorHealthDropRate, g.i.spawnDrops.TerrorResourceDropRate, g.i.spawnDrops.TerrorResourceDropAmount, GetComponent<MobHealth>().maxHealth, g.i.spawnDrops.TerrorBoostDropRate, g.i.spawnDrops.TerrorConsumableDropRate);
            g.i.hiveMindPanic.RemoveActiveTerror();
            Record.Event("mob dead", "terror");
            Anim.SetInteger("AnimationState", 1);
            GetComponent<MobExpiration>().StartExpiration();
            visibility.enabled = false;
            g.i.audioManager.PlayClip(death, transform.position);
            icon.transform.localPosition = new Vector3(17.517f, 99.003f, 0);
            icon.GetComponent<SpriteRenderer>().sprite = icon3;
        }
    }
    void Wait()
    {
        if (stateJustChanged)
        {
            Anim.SetInteger("AnimationState", 0);
            icon.transform.localPosition = new Vector3(0.017f, 0.003f, 0);
            icon.GetComponent<SpriteRenderer>().sprite = icon1;
        }
        if (!g.i.player.GetComponent<PlayerHealth>().alive)
        {
            return; //just idle in the wait state
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }
        if (wasJustHit)
        {
            currentState = State.Hit;
            wasTerrorizingWhenHit = false;
            return;
        }

        if (freeze)
        {
            currentState = State.Frozen;
            wasTerrorizingWhenFrozen = false;
            return;
        }

        if (g.i.hiveMindPanic.CurrentlyTerrorizing())
        {
            currentState = State.Terrorizing;
            return;
        }
    }
    void Terrorize()
    {
        if (stateJustChanged)
        {
            Anim.SetInteger("AnimationState", 2);
            icon.transform.localPosition = new Vector3(17.517f, 99.003f, 0);
            icon.GetComponent<SpriteRenderer>().sprite = icon2;
            g.i.audioManager.PlayPositionIndependentClip(attack);
        }

        if (!g.i.player.GetComponent<PlayerHealth>().alive)
        {
            g.i.hiveMindPanic.RemoveActiveTerror();
            currentState = State.Waiting;
            return; //just idle in the wait state
        }

        if (!GetComponent<MobHealth>().alive)
        {
            Record.Event("mob attack", "terror", "stop darkness");
            currentState = State.Dead;
            return;
        }

        if (wasJustHit)
        {
            currentState = State.Hit;
            wasTerrorizingWhenHit = true;
            return;
        }

        if (freeze)
        {
            StopAllCoroutines();
            currentState = State.Frozen;
            return;
        }

        if (!g.i.hiveMindPanic.CurrentlyTerrorizing())
        {
            currentState = State.Waiting;
            return;
        }
    }

    void Frozen()
    {
        if (stateJustChanged)
        {
            Anim.speed = 0;
        }

        if (!GetComponent<MobHealth>().alive)
        {
            Record.Event("mob attack", "terror", "stop darkness");
            freeze = false;
            Anim.speed = 1;
            currentState = State.Dead;
            return;
        }

        if (wasJustHit)
        {
            Anim.speed = 1;
            currentState = State.Hit;
            return;
        }

        float time = Time.time - timeSinceFreeze;
        if ( time > 5 )
        {
            freeze = false;
            Anim.speed = 1;
            if (wasTerrorizingWhenFrozen)
                currentState = State.Terrorizing;
            else
                currentState = State.Waiting;
            return;
        }
    }
    protected override void UpdateImpl()
    {
        State oldState = currentState;
        switch (currentState)
        {
            case State.Dead:
                Dead();
                break;
            case State.Waiting:
                Wait();
                break;
            case State.Terrorizing:
                Terrorize();
                break;
            case State.Frozen:
                Frozen();
                break;
            case State.Hit:
                Hit();
                break;
        }
        State newState = currentState;
        stateJustChanged = oldState != newState;
        if (stateJustChanged)
            lastStateChangeMoment = Time.time;
    }
    public void Freeze()
    {
        freeze = true;
        timeSinceFreeze = Time.time;
    }

    public void OnHit()
    {
        g.Splash(transform.position);
        wasJustHit = true;
    }

    void Hit()
    {
        if (stateJustChanged)
        {
            wasJustHit = false;
            Anim.SetInteger("AnimationState", 3);
            g.i.audioManager.PlayClip(hit, transform.position);
        }
        if (!GetComponent<MobHealth>().alive)
        {
            currentState = State.Dead;
            return;
        }
        if (wasJustHit || stateJustChanged && freeze)
        {
            lastStateChangeMoment = Time.time;
            Anim.Play("TerrorHit", -1, 0f);
            wasJustHit = false;
            g.i.audioManager.PlayClip(hit, transform.position);
            return;
        }
        float timeSpentHit = Time.time - lastStateChangeMoment;
        if (timeSpentHit > hitTime)
        {
            if (freeze)
                currentState = State.Frozen;
            else if (wasTerrorizingWhenHit)
                currentState = State.Terrorizing;
            else
                currentState = State.Waiting;
            return;
        }
    }
}