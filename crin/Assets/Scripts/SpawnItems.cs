﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnItems : WorldObject
{
    public float spawnRate = 1;
    public int maxItems = 2;
    public GameObject item;
    public bool avoidSA = false;

    int itemsOnTheWay = 0;
    public bool active = true;
    public List<Vector2> allowedRegions = new List<Vector2>();
    bool firstUpdate = true;

    bool ValidSpawnPosition(Vector2 pos, List<Collider2D> collidersToAvoid, List<GameObject> allDrops)
    {
        if (!g.i.PlayerCanTeleportGenerally(pos))
            return false;

        //check if it overlaps a forbidden collider
        foreach (var collider in collidersToAvoid)
            if (collider.OverlapPoint(pos))
                return false;

        //check if it's too close to another drop
        foreach (var drop in allDrops)
        {
            Vector2 dropPos = drop.transform.position;
            if ((dropPos - pos).magnitude < 200)
                return false;
        }
            
        return true;
    }

    void Spawn()
    {
        if (!active)
            return;

        Vector2 newPos = new Vector2(0, 0);

        List<Collider2D> collidersToAvoid = new List<Collider2D>();
        
        var tags = new List<string>();
        tags.Add("Paranoia");
        tags.Add("Terror");
        if (avoidSA)
            tags.Add("SocialAnxiety");
        tags.Add("Rejection");
        tags.Add("RejectionAux");
        
        foreach (var mob in g.i.mobs)
            if (tags.Contains(mob.tag))
                collidersToAvoid.Add(mob.clickArea);
                
        collidersToAvoid.Add(g.i.player.GetComponent<Collider2D>());

        List<GameObject> allDrops = g.i.spawnDrops.GetAllDrops();

        for (int idx = 0; idx < 1000; ++idx)
        {
            Vector2 region;
            if(allowedRegions.Count > 0)
            {
                int regionIdx = Random.Range(0, allowedRegions.Count);
                region = allowedRegions[regionIdx];
            }
            else
            {
                region = g.RandomRegion();
            }
            
            newPos = g.GetRandomPositionInRect(g.i.RegionRect(region).Shrinked(100f));
            if (ValidSpawnPosition(newPos, collidersToAvoid, allDrops))
                break;
        }
        Instantiate(item, new Vector2(newPos.x, newPos.y), Quaternion.identity);
    }

    IEnumerator DelayedSpawn()
    {
        ++itemsOnTheWay;
        yield return new WaitForSeconds(spawnRate);
        --itemsOnTheWay;
        Spawn();
        yield return null;
    }

	protected override void UpdateImpl()
    {   
        int totalAmmo = g.i.ammos.Count;
        int realMaxItems = maxItems;
        //debug13: handle this boost somehow
        if (PlayerPrefs.GetInt("More ammo") != 0)
            realMaxItems += g.i.boosts.MoreAmmoAmount;
        totalAmmo += itemsOnTheWay;

        if (firstUpdate)
        {
            for (int idx = totalAmmo; idx < maxItems; ++idx)
                Spawn();
            firstUpdate = false;
        }
        else
        {
            for (int idx = totalAmmo; idx < realMaxItems; ++idx)
                StartCoroutine(DelayedSpawn());
        }
	}
}
