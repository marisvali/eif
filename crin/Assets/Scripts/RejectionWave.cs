﻿using System.Collections;
using UnityEngine;

public class RejectionWave : WorldObject
{
    public AudioClip waveSound;
    public Collider2D externalCollider;
    public Collider2D internalCollider;
    
    Collider2D[] results = new Collider2D[100];
    Player player;
	
    void Start()
    {
        g.i.audioManager.PlayPositionIndependentClip(waveSound);
    }

	protected override void UpdateImpl()
    {
        bool externalTouched = false;
        bool internalTouched = false;
        ContactFilter2D contactFilter = new ContactFilter2D();
        contactFilter.useTriggers = true;
        int nrResults = g.i.player.placementBounds.OverlapCollider(contactFilter, results);
        for (int idx = 0; idx < nrResults; ++idx)
        {
            if( results[idx].name == externalCollider.name )
                externalTouched = true;
            if (results[idx].name == internalCollider.name)
                internalTouched = true;
        }
        if( externalTouched && !internalTouched )
            player.RejectionWaveAttack(1);
	}

    public void Freeze()
    {
        StartCoroutine(FreezeImpl());
    }

    IEnumerator FreezeImpl()
    {
        GetComponent<AutoDestroy>().DelayBy(5);
        float prevSpeed = GetComponent<Animator>().speed;
        GetComponent<Animator>().speed = 0;
        yield return new WaitForSeconds(5);
        GetComponent<Animator>().speed = prevSpeed;
    }
}