﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PrimaryScene : MonoBehaviour
{
    public SpriteRenderer image;
    System.Diagnostics.Stopwatch mWatch = new System.Diagnostics.Stopwatch();

    void Awake()
    {
        PlayerPrefs.SetString("Sound", "");
    }

    void Start()
    {
        //debug13
        PlayerPrefs.SetInt("PlayerHealth", 6);
        PlayerPrefs.SetInt("PlayerAmmo", g.MaxAmmo());

        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        mWatch.Restart();

        int level = PlayerPrefs.GetInt("Level");
        if (level < 1 || level > 29)
        {
            level = 1;
            PlayerPrefs.SetInt("Level", level);
        }

        Application.backgroundLoadingPriority = ThreadPriority.High;
        prof.Start();

        yield return g.AddScene("PlayerFem");
        yield return g.AddScene("Common");
        yield return g.AddScene(level.ToString());
        var asyncObj = AstarPath.active.ScanAsync();

        Image fadeInImage = GetComponentInChildren<Image>();
        Color col = fadeInImage.color;
        float totalAlphaDif = 1.0f;
        float alphaDifPerSecond = totalAlphaDif / 0.3f;
        while (col.a > 0.0f)
        {
            col.a -= alphaDifPerSecond * Time.unscaledDeltaTime;
            fadeInImage.color = col;
            yield return null;
        }

        yield return asyncObj;

        prof.Capture("loading scenes");
        Debug.Log($"game active");
        g.i.Unpause();
        SceneManager.UnloadSceneAsync("PrimaryScene");
    }

    bool mFirst = true;
    void Update()
    {
        if (mFirst)
        {
            mFirst = false;
            //Debug.Log($"game active");
        }
    }

    void OnDestroy()
    {
        //Debug.Log($"loading time: {mWatch.Elapsed.TotalMilliseconds}");
    }

    IEnumerator FadeOutSplashScreen(float durationTarget)
    {
        float momentStart = Time.realtimeSinceStartup;
        float durationCurrent = 0;
        do
        {
            yield return null;
            durationCurrent = Time.realtimeSinceStartup - momentStart;
            float alpha = 1.0f - durationCurrent / durationTarget;
            image.color = new Color(1, 1, 1, alpha);
        }
        while (durationCurrent < durationTarget);
    }
}