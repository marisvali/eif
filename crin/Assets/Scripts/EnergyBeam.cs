﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBeam : MonoBehaviour
{
    public float duration = 0.0f;
    public int ammoCost = 0;
    public Transform sprite;

    public void Initialize(Vector2 source, Mob target, bool hitMultiple, bool stopAtFirstMob, bool stopAtObstacle)
    {
        //compute the actual dest point and the target
        Vector2 dest = new Vector2(target.transform.position.x, target.transform.position.y);
        if (target.clickArea != null)
        {
            Vector3 dest3 = target.clickArea.bounds.center;
            dest = new Vector2(dest3.x, dest3.y);
        }

        if (hitMultiple || stopAtFirstMob)
            Extend(source, ref dest);
        if (stopAtFirstMob && !hitMultiple)
        {
            var firstMob = GetFirstMobInTheWay(source, ref dest);
            if (firstMob)
                target = firstMob;
        }
        if (stopAtObstacle)
        {
            if (ApplyObstaclesLimit(source, ref dest))
            {
                target = null; //the target is no longer valid
                g.Splash(dest); //create splash at the obstacle hitting point
            }
        }

        //change the sprite's transform to make it a segment running from
        //source to target

        //make the beam as long as it needs to be
        float length = (dest - source).magnitude;
        float spriteLength = sprite.GetComponent<SpriteRenderer>().bounds.extents.x * 2;
        sprite.localScale = new Vector3(length / spriteLength, sprite.localScale.y, sprite.localScale.z);

        //rotate the beam
        Vector2 dir = dest - source;
        float dirAngle = Quaternion.FromToRotation(Vector3.right, dir).eulerAngles.z;
        transform.Rotate(Vector3.forward, dirAngle);

        //translate middle of the beam to the middle between source and target
        transform.position = (source + dest) / 2;

        Destroy(gameObject, duration);
        if (hitMultiple)
            HitAll(source, dest);
        else
        {
            Hit(target);
        }
    }
    public void Initialize(Vector2 source, Vector2 dest, bool hitMultiple, bool stopAtFirstMob, bool stopAtObstacle)
    {
        if (hitMultiple)
            Extend(source, ref dest);
        if (stopAtFirstMob && !hitMultiple)
        {
            var mob = GetFirstMobInTheWay(source, ref dest);
            if (mob)
            {
                Initialize(source, mob, hitMultiple, stopAtFirstMob, stopAtObstacle);
                return;
            }
        }

        if (stopAtObstacle)
        {
            ApplyObstaclesLimit(source, ref dest);
        }

        //change the sprite's transform to make it a segment running from
        //source to target

        //make the beam as long as it needs to be
        float length = (dest - source).magnitude;
        float spriteLength = sprite.GetComponent<SpriteRenderer>().bounds.extents.x * 2;
        sprite.localScale = new Vector3(length / spriteLength, sprite.localScale.y, sprite.localScale.z);

        //rotate the beam
        Vector2 dir = dest - source;
        float dirAngle = Quaternion.FromToRotation(Vector3.right, dir).eulerAngles.z;
        transform.Rotate(Vector3.forward, dirAngle);

        //translate middle of the beam to the middle between source and target
        transform.position = (source + dest) / 2;

        g.Splash(dest); //create splash at the hitting point
        Destroy(gameObject, duration);
    }
    void Extend(Vector2 source, ref Vector2 dest)
    {
        Vector2 dir = dest - source;
        dir.Normalize();
        float length = 3000; //a large length, larger than the viewing rectangle
        dest = source + dir * length;
    }
    Mob GetFirstMobInTheWay(Vector2 source, ref Vector2 dest)
    {
        Vector2 dir = dest - source;
        float length = (dest - source).magnitude;
        dir.Normalize();

        //check if we hit any mobs
        int layerMask = 1 << 25;
        var hits = Physics2D.RaycastAll(source, dir, length, layerMask);

        if (hits.Length <= 0)
            return null; //no mobs hit

        //we hit some mobs, get the closest one
        float minDist = 0;
        bool first = true;
        foreach (var hit in hits)
        {
            if (hit.collider.transform.parent.gameObject.GetComponent<MobExpiration>().Expired())
                continue;
            Vector2 hitPoint = hit.collider.bounds.center;
            float dist = (source - hitPoint).sqrMagnitude;
            if (first || dist < minDist)
            {
                dist = minDist;
                first = false;
                dest = hitPoint;
                return hit.collider.gameObject.transform.parent.GetComponent<Mob>();
            }
        }
        return null;
    }
    bool ApplyObstaclesLimit(Vector2 source, ref Vector2 dest)
    {
        Vector2 dir = dest - source;
        float length = (dest - source).magnitude;
        dir.Normalize();

        //check if we hit any obstacles
        int layerMask = 1 << 26;
        var hits = Physics2D.RaycastAll(source, dir, length, layerMask);

        if (hits.Length <= 0)
            return false; //no obstacles hit

        //we hit an obstacle, get the closest one
        float minDist = 0;
        bool first = true;
        foreach (var hit in hits)
        {
            float dist = (source - hit.point).sqrMagnitude;
            if (first || dist < minDist)
            {
                dist = minDist;
                first = false;
                dest = hit.point;
            }
        }
        return true;
    }
    void HitAll(Vector2 source, Vector2 dest)
    {
        Vector2 dir = dest - source;
        int layerMask = 1 << 25;
        var hits = Physics2D.RaycastAll(source, dir, dir.magnitude, layerMask);

        List<string> tags = new List<string>();
        tags.Add("Depression");
        tags.Add("SocialAnxiety");
        tags.Add("Anger");
        tags.Add("Paranoia");
        tags.Add("Terror");
        tags.Add("RejectionAux");

        Vector2 sourceRegion = g.i.Region(source);
        foreach (var hit in hits)
        {
            if (tags.Contains(hit.transform.gameObject.tag) &&
                !hit.transform.gameObject.GetComponent<MobExpiration>().Expired())
            {
                bool sameRegion = g.i.MobPartiallyInRegion(hit.transform.gameObject.GetComponent<Mob>(), sourceRegion);
                if (sameRegion)
                    Hit(hit.transform.gameObject.GetComponent<Mob>());
            }
        }
    }
    void Hit(Mob target)
    {
        if (target == null)
            return;
        if (target.GetComponent<MobHealth>() != null)
            target.GetComponent<MobHealth>().Hit(1);
        DepressionAI dai = target.GetComponent<DepressionAI>();
        if (dai != null)
            dai.OnHit();
        SocialAnxietyAI sai = target.GetComponent<SocialAnxietyAI>();
        if (sai != null)
            sai.OnHit();
        AngerAI aai = target.GetComponent<AngerAI>();
        if (aai != null)
            aai.OnHit();
        ParanoiaAI pai = target.GetComponent<ParanoiaAI>();
        if (pai != null)
            pai.OnHit();
        TerrorAI tai = target.GetComponent<TerrorAI>();
        if (tai != null)
            tai.OnHit();
        RejectionAuxAI rai = target.GetComponent<RejectionAuxAI>();
        if (rai != null)
            rai.OnHit();
    }
}
