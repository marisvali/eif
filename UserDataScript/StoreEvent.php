<!DOCTYPE html>
<html>
<body>

<h1>Crin data collection script</h1>

<?php

function LogMessage($message) {
	file_put_contents("./LogStoreEvent.txt", $message . "\n", FILE_APPEND);
}

//LogMessage("---------------------------");

$servername = "localhost";
$username = "id1708038_crin";
$password = "parola";
$dbname = "id1708038_crin";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
	LogMessage("Connection failed: " . $conn->connect_error);
    die();
}

LogMessage("Connected successfully.");

$stmt = $conn->prepare("INSERT INTO Events (DeviceID, Time, Event, EventIdx, Details1, Details2, Details3, Details4, Details5) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
$stmt->bind_param("sssssssss", $_POST["deviceID"], $_POST["time"], $_POST["event"], $_POST["eventIdx"], $_POST["details1"], $_POST["details2"], $_POST["details3"], $_POST["details4"], $_POST["details5"]);

if ($stmt->execute() === TRUE) {
    //LogMessage("New record created successfully.");
} else {
	LogMessage("Error: " . $conn->error);
	die();
}

$conn->close();
?>

</body>
</html>