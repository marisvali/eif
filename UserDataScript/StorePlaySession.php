<!DOCTYPE html>
<html>
<body>

<h1>Crin data collection script</h1>

<?php

function LogMessage($message) {
	file_put_contents("./LogStorePlaySession.txt", $message . "\n", FILE_APPEND);
}

LogMessage("---------------------------");

$servername = "localhost";
$username = "id1708038_crin";
$password = "parola";
$dbname = "id1708038_crin";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
	LogMessage("Connection failed: " . $conn->connect_error);
    die();
}

LogMessage("Connected successfully.");

//$states = file_get_contents($_FILES['states']['tmp_name']);
$playSession = file_get_contents($_FILES['playSession']['tmp_name']);
//LogMessage("States: " . $states);

// $sql = "INSERT INTO PlaySessions (DeviceID, SceneIdx, StartTime, EndTime, States) VALUES ('" . $_POST["deviceID"] . "', '" . $_POST["sceneIdx"] . "', '" . $_POST["startTime"] . "', '" . $_POST["endTime"] . "', '" . mysqli_escape_string($states) . "')";



$stmt = $conn->prepare("INSERT INTO PlaySessions (DeviceID, SceneIdx, StartTime, EndTime, Session) VALUES (?, ?, ?, ?, ?)");
$stmt->bind_param("sssss", $_POST["deviceID"], $_POST["sceneIdx"], $_POST["startTime"], $_POST["endTime"], $playSession);
//$stmt = $conn->prepare("INSERT INTO PlaySessions (DeviceID) VALUES (?)");
//$stmt->bind_param("s", $_POST["deviceID"]);

if ($stmt->execute() === TRUE) {
    LogMessage("New record created successfully.");
} else {
	LogMessage("Error: " . $conn->error);
	die();
}

/*
$sql = "SELECT MAX(ID) AS MaxID FROM PlaySessions";
$result = $conn->query($sql);
if ($result->num_rows != 1) {
	LogMessage("Unexpected result.");
	die();
}

$row = $result->fetch_assoc();
$sessionID = $row["MaxID"];
$inputsCount = $_POST["inputsCount"];

for( $i = 0; $i < $inputsCount; ++$i ) {
	$time = $_POST["time" . $i];
	$x = $_POST["x" . $i];
	$y = $_POST["y" . $i];
	$sql = "INSERT INTO PlayerInputs (SessionID, Time, X, Y) VALUES (" . $sessionID . ", " . $time . ", " . $x . ", " . $y . ")";
	if ($conn->query($sql) === TRUE) {
		LogMessage("New record created successfully with: " . $sql);
	} else {
		LogMessage("Error: " . $sql . "   " . $conn->error);
		die();
	}
}
*/

$conn->close();
?>

</body>
</html>