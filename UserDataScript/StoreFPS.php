<!DOCTYPE html>
<html>
<body>

<h1>Crin data collection script</h1>

<?php

function LogMessage($message) {
	file_put_contents("./LogStoreFPS.txt", $message . "\n", FILE_APPEND);
}

//LogMessage("---------------------------");

$servername = "localhost";
$username = "id1708038_crin";
$password = "parola";
$dbname = "id1708038_crin";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
	LogMessage("Connection failed: " . $conn->connect_error);
    die();
}

$stmt = $conn->prepare("INSERT INTO FPS (SessionID, FrameIdx, FPS) VALUES (?, ?, ?)");
$stmt->bind_param("sss", $_POST["SessionID"], $_POST["FrameIdx"], $_POST["FPS"]);

if ($stmt->execute() === TRUE) {
    //LogMessage("New record created successfully.");
} else {
	LogMessage("Error: " . $conn->error);
	die();
}

$conn->close();
?>

</body>
</html>